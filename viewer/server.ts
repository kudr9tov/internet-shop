import 'zone.js/node';

import { APP_BASE_HREF } from '@angular/common';
import { applyDomino } from '@ntegral/ngx-universal-window';
import { join } from 'path';
import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import * as compression from 'compression';
import { existsSync } from 'fs';

import { AppServerModule } from './src/main.server';
import { environment } from '@environments/environment';

const BROWSER_DIR = join(process.cwd(), 'dist/viewer/browser');
applyDomino(global, join(BROWSER_DIR, 'index.html'));
// The Express app is exported so that it can be used by serverless Functions.
export function app(): express.Express {
    const server = express();
    const distFolder = join(process.cwd(), 'dist/viewer/browser');
    const indexHtml = existsSync(join(distFolder, 'index.original.html'))
        ? 'index.original.html'
        : 'index';

    // to gzip static assets
    //  const compressionModule = compression();
    server.use(compression());

    // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
    server.engine(
        'html',
        ngExpressEngine({
            bootstrap: AppServerModule,
        }),
    );

    server.set('view engine', 'html');
    server.set('views', distFolder);

    // Example Express Rest API endpoints
    // server.get('/api/**', (req, res) => { });
    // Serve static files from /browser
    server.get(
        '*.*',
        express.static(distFolder, {
            maxAge: '1y',
        }),
    );

    // All regular routes use the Universal engine
    server.get('*', (req, res) => {
        res.setHeader('X-Frame-Options', 'DENY');
        const url = environment.apiPath + req.originalUrl;
        res.render(indexHtml, {
            req,
            res,
            url,
            providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }],
        });
    });

    return server;
}

function run(): void {
    const port = process.env['PORT'] || 4000;
    // const proxy = require('http-proxy-middleware');
    // const apiProxy = proxy('/api', {target: 'http://gktorg.ru:8081/', changeOrigin: true});

    // Start up the Node server
    const server = app();
    // server.use(apiProxy);
    server.listen(port, () => {
        console.log(`Node Express server listening on http://localhost:${port}`);
    });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
    run();
}

export * from './src/main.server';
