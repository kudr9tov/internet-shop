import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';

import { PipesModule } from '@common/pipes/pipes.module';
import { RequestPriceDialogModule } from '@common/request-price-dialog/request-price-dialog.module';
import { ImageWrapperModule } from '@common/image-wrapper/image-wrapper.module';
import { PriceRequestApiService } from '@common/services/price-request-api.service';

import { InterestDiscountModule } from '../interest-discount/interest-discount.module';
import { FlexCardComponent } from './flex-card/flex-card.component';
import { RatingComponent } from './rating/rating.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS } from 'ng-lazyload-image';
import { LazyLoadImageHooks } from '@common/hooks/lazyload-image.hook';
import { AppIconModule } from '@common/app-icon/app-icon.module';

@NgModule({
    declarations: [FlexCardComponent, RatingComponent],
    imports: [
        CommonModule,
        InterestDiscountModule,
        RequestPriceDialogModule,
        MatButtonModule,
        MatCardModule,
        HttpClientModule,
        RouterModule,
        ImageWrapperModule,
        MatTooltipModule,
        PipesModule,
        LazyLoadImageModule,
        AppIconModule,
    ],
    providers: [
        PriceRequestApiService,
        { provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks },
    ],
    exports: [FlexCardComponent, RatingComponent],
})
export class ProductCardModule {}
