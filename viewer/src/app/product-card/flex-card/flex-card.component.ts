import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { MatDialog } from '@angular/material/dialog';

import { ProductModel } from '@common/models/product.model';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { RequestPriceDialogComponent } from '@common/request-price-dialog/request-price-dialog.component';
import { RequestPriceModel } from '@common/models/price-request.model';
import { SetError, SetSuccess } from '@common/toast';
import { AddToWishlist } from '@common/auth/store/wishlist/wishlist.actions';

import { AddToBasket } from '../../shared/basket-popup/store/basket-popup.actions';

@Component({
    selector: 'flex-card',
    templateUrl: './flex-card.component.html',
    styleUrls: ['./flex-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FlexCardComponent {
    @Input() product!: ProductModel;
    @Input() labelHeight = 40;
    @Input() layout: 'grid-sm' | 'grid-nl' | 'grid-lg' | 'list' | 'horizontal' | null = null;

    addingToCart = false;
    @Input() addingToWishlist = false;

    newDate = +new Date() - 2678400000;

    constructor(
        private cd: ChangeDetectorRef,
        private dialog: MatDialog,
        public service: PriceRequestApiService,
        public store: Store,
    ) {}

    addToCart(): void {
        if (this.addingToCart) {
            return;
        }
        if (!this.product.price) {
            this.onAddRequest();
            return;
        }

        this.addingToCart = true;
        this.store.dispatch(new AddToBasket(this.product)).subscribe({
            complete: () =>
                setTimeout(() => {
                    this.addingToCart = false;
                    this.cd.markForCheck();
                }, 300),
        });
    }

    addToWishlist(): void {
        if (!this.addingToWishlist && this.product) {
            this.addingToWishlist = true;

            this.store.dispatch(new AddToWishlist(this.product)).subscribe({
                complete: () =>
                    setTimeout(() => {
                        this.addingToWishlist = false;
                        this.cd.markForCheck();
                    }, 300),
            });
        }
    }

    onAddRequest(): void {
        this.dialog
            .open(RequestPriceDialogComponent, {
                panelClass: 'request-price',
                data: { productId: this.product.id },
            })
            .afterClosed()
            .subscribe((result: any) => {
                if (!result) {
                    return;
                }
                this.service.create(new RequestPriceModel({ ...result })).subscribe({
                    complete: () => {
                        this.store.dispatch(new SetSuccess('Запрос отправлен'));
                    },
                    error: () => this.store.dispatch(new SetError('Произошла ошибка')),
                });
            });
    }

    // addToWishlist(): void {
    //     if (this.addingToWishlist) {
    //         return;
    //     }

    //     this.addingToWishlist = true;
    //     this.wishlist.add(this.product).subscribe({
    //         complete: () => {
    //             this.addingToWishlist = false;
    //             this.cd.markForCheck();
    //         }
    //     });
    // }

    // addToCompare(): void {
    //     if (this.addingToCompare) {
    //         return;
    //     }

    //     this.addingToCompare = true;
    //     this.compare.add(this.product).subscribe({
    //         complete: () => {
    //             this.addingToCompare = false;
    //             this.cd.markForCheck();
    //         }
    //     });
    // }

    // showQuickview(): void {
    //     if (this.showingQuickview) {
    //         return;
    //     }

    //     this.showingQuickview = true;
    //     this.quickview.show(this.product).subscribe({
    //         complete: () => {
    //             this.showingQuickview = false;
    //             this.cd.markForCheck();
    //         }
    //     });
    // }
}
