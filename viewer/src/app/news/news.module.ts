import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { ShopSidebarModule } from '../components/shop-sidebar/shop-sidebar.module';
import { PaginationModule } from '../shared/pagination/pagination.module';
import { PostCardModule } from '../shared/post-card/post-card.module';
import { NewsComponent } from './components/news/news.component';
import { NewsRoutingModule } from './news-routing.module';
import { PostComponent } from './components/post/post.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { NewsState } from './store/news.state';
import { TextEditorApiService } from '@common/services/text-editor-api.service';
import { PipesModule } from '@common/pipes/pipes.module';
import { PageHeaderModule } from '../shared/page-header/page-header.module';
import { CommentStateModule } from '../comment-state/comment-state.module';
import { ComponentsModule } from '../components/components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
    declarations: [NewsComponent, PostComponent, PostDetailsComponent],
    imports: [
        CommonModule,
        ShopSidebarModule,
        NgxsModule.forFeature([NewsState]),
        PostCardModule,
        PaginationModule,
        PipesModule,
        NewsRoutingModule,
        ComponentsModule,
        PageHeaderModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        CommentStateModule,
    ],
    providers: [TextEditorApiService],
})
export class NewsModule {}
