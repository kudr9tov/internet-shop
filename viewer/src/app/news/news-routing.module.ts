import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsComponent } from './components/news/news.component';
import { PostComponent } from './components/post/post.component';
import { PostResolver } from './resolvers/post.resolver';

const routes: Routes = [
    {
        path: '',
        component: NewsComponent,
    },
    {
        path: 'post' + '/:id',
        component: PostComponent,
        resolve: {
            catalog: PostResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [PostResolver],
})
export class NewsRoutingModule {}
