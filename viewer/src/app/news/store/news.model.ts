import { TextEditorModel } from '@common/models/text-editor.model';

export interface NewsStateModel {
    posts: TextEditorModel[];
    post: TextEditorModel | null;
}
