import { Injectable } from '@angular/core';
import { TextEditorModel } from '@common/models/text-editor.model';
import { TextEditorApiService } from '@common/services/text-editor-api.service';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoadPost, LoadPosts } from './news.actions';

import { NewsStateModel } from './news.model';

@State<NewsStateModel>({
    name: 'newsState',
    defaults: {
        post: null,
        posts: [],
    },
})
@Injectable()
export class NewsState {
    @Selector()
    static posts({ posts }: NewsStateModel): TextEditorModel[] {
        return posts;
    }

    @Selector()
    static post({ post }: NewsStateModel): TextEditorModel | null {
        return post;
    }

    constructor(private apiService: TextEditorApiService) {}

    @Action(LoadPosts)
    onLoadposts({ patchState }: StateContext<NewsStateModel>): Observable<any> {
        return this.apiService
            .getList()
            .pipe(tap((posts) => patchState({ posts: posts.filter((x) => !x.system) })));
    }

    @Action(LoadPost)
    onLoadPost({ patchState }: StateContext<NewsStateModel>, { id }: LoadPost): Observable<any> {
        return this.apiService.get(id).pipe(tap((post) => patchState({ post })));
    }
}
