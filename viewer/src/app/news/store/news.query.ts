import { LinkModel } from '@common/models/link.model';
import { TextEditorModel } from '@common/models/text-editor.model';
import { Selector } from '@ngxs/store';
import { NewsState } from './news.state';

export class NewsQuery {
    @Selector([NewsState.post])
    static newsBreadcrumb(item: TextEditorModel): LinkModel[] {
        return [
            new LinkModel({ label: 'Главная', url: '/' }),
            new LinkModel({ label: 'Новости', url: '/news' }),
            new LinkModel({ label: item.name, url: `/news/post/${item.id}` }),
        ];
    }
}
