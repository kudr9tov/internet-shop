export class LoadPosts {
    static readonly type = '[News] Load post';
}

export class LoadPost {
    static readonly type = '[News] Load posts';
    constructor(public id: number) {}
}
