import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { LoadPost } from '../store/news.actions';

@Injectable()
export class PostResolver implements Resolve<any> {
    constructor(public store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const id = +route.params['id'];
        return this.store.dispatch(new LoadPost(id));
    }
}
