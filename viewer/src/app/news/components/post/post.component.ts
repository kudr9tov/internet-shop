import { Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { LinkModel } from '@common/models/link.model';

import { NewsQuery } from '../../store/news.query';

@Component({
    selector: 'post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss'],
})
export class PostComponent {
    @Select(NewsQuery.newsBreadcrumb) newsBreadcrumb$!: Observable<LinkModel[]>;

    sidebarPosition: 'start' | 'end' = 'start';

    layout: 'classic' | 'full' = 'classic';
}
