import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { AppConstants } from '@common/constants/app.const';
import { LinkModel } from '@common/models/link.model';
import { TextEditorModel } from '@common/models/text-editor.model';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { LoadPosts } from '../../store/news.actions';
import { NewsState } from '../../store/news.state';

@Component({
    selector: 'news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsComponent implements OnInit {
    @Select(NewsState.posts) posts$!: Observable<TextEditorModel[]>;

    sidebarPosition: 'start' | 'end' = 'end';
    layout: 'classic' | 'grid' | 'list' = 'list';
    header = 'Последние новости';
    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: 'Новости', url: '/news' }),
    ];

    constructor(private titleService: Title, private store: Store, private metaTagService: Meta) {
        this.titleService.setTitle(AppConstants.newsTitle);
        this.metaTagService.updateTag({
            name: 'description',
            content: AppConstants.newsDescription,
        });
    }

    getPostCardLayout(): 'grid-nl' | 'grid-lg' | 'list-nl' | 'list-sm' {
        return {
            classic: 'grid-lg',
            grid: 'grid-nl',
            list: 'list-nl',
        }[this.layout] as 'grid-nl' | 'grid-lg' | 'list-nl' | 'list-sm';
    }

    ngOnInit(): void {
        this.store.dispatch(new LoadPosts());
    }
}
