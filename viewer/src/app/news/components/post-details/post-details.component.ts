import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ObserverComponent } from '@common/abstract/observer.component';
import { CommentType } from '@common/enums/comment-type.enum';
import { CommentModel } from '@common/models/comment.model';
import { CreateCommentModel } from '@common/models/create-comment.model';
import { TextEditorModel } from '@common/models/text-editor.model';
import { SeoDescriptionPipes } from '@common/pipes/seo-description.pipe';
import { CustomValidators } from '@common/regex/custom.validators';
import { environment } from '@environments/environment';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { CreatePostComment, LoadPostComments } from '../../../comment-state/store/comment.actions';
import { CommentState } from '../../../comment-state/store/comment.state';
import { SeoSocialShareService } from '../../../services/seo.service';
import { NewsState } from '../../store/news.state';

@Component({
    selector: 'post-details',
    templateUrl: './post-details.component.html',
    styleUrls: ['./post-details.component.scss'],
})
export class PostDetailsComponent extends ObserverComponent implements OnInit {
    @Input() layout: 'classic' | 'full' = 'classic';

    currentUrl = window.location.href;

    post!: TextEditorModel;

    companyName = environment.name;

    mobilePhone = environment.phone;

    seoDescr!: string;

    formGroup!: FormGroup;

    author = environment.name + ' ' + environment.apiPath;

    logo = environment.logo;

    @Select(NewsState.post) post$!: Observable<TextEditorModel>;

    @Select(CommentState.postComments) postComments$!: Observable<CommentModel[]>;

    constructor(
        private seoService: SeoSocialShareService,
        private seoDescription: SeoDescriptionPipes,
        private store: Store,
        private formBuilder: FormBuilder,
    ) {
        super();
    }

    ngOnInit(): void {
        this.formGroup = this.formBuilder.group({
            firstName: [''],
            lastName: [''],
            comment: [''],
            email: ['', [Validators.required, CustomValidators.emailValidator()]],
        });
        this.subscriptions.push(this.postSubscription());
    }

    onSave(): void {
        if (this.formGroup.invalid) {
            CustomValidators.validateAllFormFields(this.formGroup);
            return;
        }
        const model = this.formGroup.value;
        const comment = new CreateCommentModel({
            commentType: CommentType.Post,
            entityLinkedId: this.post.id,
            author: model.firstName + ' ' + model.lastName + ' ' + model.email,
            comment: model.comment,
        });
        this.store.dispatch(new CreatePostComment(comment));
    }

    private postSubscription(): Subscription {
        return this.post$.subscribe((value) => {
            this.post = value;
            this.store.dispatch(new LoadPostComments(value.id));
            this.seoDescr = this.seoDescription.transform(this.post.content, 146);
            this.seoService.setData({
                title: this.post.name + new Date(+this.post.created),
                description: this.seoDescr,
                image: this.post.imageUrl,
                type: 'website',
                keywords: this.post.name.split(' ').join(', '),
                imageAuxData: {
                    alt: this.post.name,
                    height: 968,
                    width: 504,
                    mimeType: 'image/jpeg',
                    secureUrl: this.post.imageUrl,
                },
            });
        });
    }
}
