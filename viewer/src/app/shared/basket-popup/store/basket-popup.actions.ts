import { ProductModel } from '@common/models/product.model';
import { CustomerInfoModel } from '@common/models/customer-info.model';

export class BasketDismissed {
    static readonly type = '[Basket] Dismissed';

    constructor(public result: any) {}
}

export class RemoveFromBasket {
    static readonly type = '[Basket] remove from Basket';

    constructor(public payload: number) {}
}

export class AddToBasket {
    static readonly type = '[Basket] add to Basket';

    constructor(public payload: ProductModel, public count = 1) {}
}

export class CreateOrder {
    static readonly type = '[Basket] create order';

    constructor(public model: CustomerInfoModel) {}
}

export class UpdateCount {
    static readonly type = '[Basket] update count';

    constructor(public payload: ProductModel, public number: number) {}
}

export class ResetBasket {
    static readonly type = '[Basket] reset basket';
}
