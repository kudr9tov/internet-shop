import { ProductModel } from '@common/models/product.model';

export interface BasketStateModel {
    orders: ProductModel[];
    isSuccess: boolean;
}
