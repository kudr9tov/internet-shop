import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { catchError, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { OrderModel } from '@common/models/order.model';
import { ProductListModel } from '@common/models/product-list.model';
import { ProductModel } from '@common/models/product.model';

import {
    AddToBasket,
    CreateOrder,
    RemoveFromBasket,
    ResetBasket,
    UpdateCount,
} from './basket-popup.actions';
import { BasketApiService } from '../services/basket-api.services';
import { BasketStateModel } from './basket-popup.model';
import { SetSuccess } from '@common/toast';

@State<BasketStateModel>({
    name: 'basket',
    defaults: {
        orders: [],
        isSuccess: false,
    },
})
@Injectable()
export class BasketState {
    constructor(private apiService: BasketApiService, private store: Store) {}

    @Selector()
    static orders({ orders }: BasketStateModel): ProductModel[] {
        return orders;
    }

    @Selector()
    static isSuccess({ isSuccess }: BasketStateModel): boolean {
        return isSuccess;
    }

    @Selector()
    static cost({ orders }: BasketStateModel): number {
        const reducer = (accumulator: any, currentValue: any) => accumulator + currentValue;
        return (
            orders
                ?.map(
                    (x) =>
                        (x.discount ? x.price - x.price * (x.discount / 100) : x.price) * x.count,
                )
                .reduce(reducer) || 0
        );
    }

    @Selector()
    static totalCount({ orders }: BasketStateModel): number {
        const reducer = (accumulator: any, currentValue: any) => accumulator + currentValue;
        return orders?.map((x) => 1 * x.count).reduce(reducer) || 0;
    }

    @Action(CreateOrder)
    onCreateBrand(
        { patchState, getState }: StateContext<BasketStateModel>,
        { model }: CreateOrder,
    ): Observable<any> {
        const { orders } = getState();
        const totalCost = this.store.selectSnapshot(BasketState.cost);
        const products = orders?.map(
            (x) => new ProductListModel({ productId: x.id, quantity: x.count }),
        );
        const createModel = new OrderModel({
            customerInfo: model,
            products,
            totalCost,
        });
        return this.apiService.create(createModel).pipe(
            tap(() => patchState({ isSuccess: true, orders: [] })),
            catchError((error: HttpErrorResponse) => {
                patchState({ isSuccess: false });
                return of(error);
            }),
        );
    }

    @Action(AddToBasket)
    onAddToBasket(
        { setState, getState, dispatch }: StateContext<BasketStateModel>,
        { payload, count }: AddToBasket,
    ): void {
        const { orders } = getState();
        const index = orders.findIndex((x) => x.id === payload.id);

        if (index !== -1) {
            setState(
                patch({
                    orders: updateItem<ProductModel>(
                        (x) => x.id === payload.id,
                        patch({ count: orders[index].count + count }),
                    ),
                }),
            );
        } else {
            setState(
                patch({
                    orders: append([new ProductModel({ ...payload, count })]),
                }),
            );
        }
        dispatch(new SetSuccess(`Товар "${payload.name}" добавлен в корзину!`));
    }

    @Action(UpdateCount)
    onUpdateCount(
        { setState }: StateContext<BasketStateModel>,
        { payload, number }: UpdateCount,
    ): void {
        const newCount = payload.count + number;
        if (newCount <= 0) {
            return;
        }

        setState(
            patch({
                orders: updateItem<ProductModel>(
                    (x) => x.id === payload.id,
                    patch({ count: newCount }),
                ),
            }),
        );
    }

    @Action(RemoveFromBasket)
    onRemoveFromBasket(
        { setState }: StateContext<BasketStateModel>,
        { payload }: RemoveFromBasket,
    ): void {
        setState(
            patch({
                orders: removeItem<ProductModel>((x) => x.id === payload),
            }),
        );
    }

    @Action(ResetBasket)
    onResetBasket({ patchState }: StateContext<BasketStateModel>): void {
        patchState({ orders: [] });
    }
}
