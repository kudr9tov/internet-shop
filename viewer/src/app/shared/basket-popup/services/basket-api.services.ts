import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { BaseApiService } from '@common/abstract/base-api.service';
import { OrderModel } from '@common/models/order.model';

@Injectable()
export class BasketApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }
    public create(model: OrderModel): Observable<any> {
        return this.httpPost('order', (x) => new OrderModel(x), model);
    }
}
