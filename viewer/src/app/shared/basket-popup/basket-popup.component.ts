import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Inject,
    ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { MatStepper } from '@angular/material/stepper';
import { FormBuilder, Validators } from '@angular/forms';

import { ProductModel } from '@common/models/product.model';
import { CustomerInfoModel } from '@common/models/customer-info.model';

import { BasketState } from './store/basket-popup.state';
import {
    AddToBasket,
    CreateOrder,
    RemoveFromBasket,
    UpdateCount,
} from './store/basket-popup.actions';
import { BasketStepper } from './enums/basket-stepper.enum';
import { CustomValidators } from '@common/regex/custom.validators';

@Component({
    selector: 'basket-popup',
    templateUrl: 'basket-popup.component.html',
    styleUrls: ['basket-popup.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasketPopupComponent implements AfterViewInit {
    @Select(BasketState.orders) orders$!: Observable<ProductModel[]>;

    @Select(BasketState.cost) cost$!: Observable<number>;

    @Select(BasketState.totalCount) totalCount$!: Observable<number>;

    @Select(BasketState.isSuccess) isSuccess$!: Observable<boolean>;

    @ViewChild('stepper') stepper!: MatStepper;

    header = 'Корзина';

    validateText = 'Поле обязательно для заполнения';

    secondFormGroup = this.fb.group({
        name: [''],
        organization: [''],
        requisites: [''],
        address: [''],
        phone: ['', Validators.required],
        email: ['', [Validators.required, CustomValidators.emailValidator()]],
        city: [''],
        isConfirm: [false],
    });

    constructor(
        private dialogRef: MatDialogRef<BasketPopupComponent>,
        public ref: ChangeDetectorRef,
        public store: Store,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        if (this.data && this.data.product) {
            this.store.dispatch(new AddToBasket(this.data.product));
        }

        dialogRef.backdropClick().subscribe(() => {
            this.cleanBasket();
        });
    }

    ngAfterViewInit(): void {
        if (this.stepper) {
            this.stepper.selectedIndex = this.data
                ? this.data.currentStep
                : BasketStepper.OrderOverview;
        }
        this.ref.detectChanges();
    }

    onSave(): void {
        if (this.secondFormGroup.invalid) {
            CustomValidators.validateAllFormFields(this.secondFormGroup);
            return;
        }

        const customer = new CustomerInfoModel({
            ...(this.secondFormGroup.value as any),
        });
        this.store.dispatch(new CreateOrder(customer));
    }

    onClose(): void {
        this.cleanBasket();
        this.dialogRef.close();
    }

    onRemove(id: number): void {
        this.store.dispatch(new RemoveFromBasket(id));
    }

    onUpdateCount(model: ProductModel, number: number): void {
        this.store.dispatch(new UpdateCount(model, number));
    }

    cleanBasket(): void {
        if (this.data && this.data.product) {
            this.store.dispatch(new RemoveFromBasket(this.data.product.id));
        }
    }
}
