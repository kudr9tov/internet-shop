import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { MatStepperModule } from '@angular/material/stepper';

import { PipesModule } from '@common/pipes/pipes.module';
import { AppIconModule } from '@common/app-icon/app-icon.module';

import { BasketState } from './store/basket-popup.state';
import { BasketPopupComponent } from './basket-popup.component';
import { BasketApiService } from './services/basket-api.services';
import { PageHeaderModule } from '../page-header/page-header.module';
import { InputNumberModule } from '../../catalog/components/input-number/input-number.module';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [BasketPopupComponent],
    imports: [
        CommonModule,
        FormsModule,
        MatAutocompleteModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatStepperModule,
        MatToolbarModule,
        PipesModule,
        AppIconModule,
        RouterModule,
        PageHeaderModule,
        ReactiveFormsModule,
        NgxsModule.forFeature([BasketState]),
        InputNumberModule,
    ],
    providers: [BasketApiService],
})
export class BasketModule {}
