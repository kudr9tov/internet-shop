import { Component, Input } from '@angular/core';
import { TextEditorModel } from '@common/models/text-editor.model';

@Component({
    selector: 'post-card',
    templateUrl: './post-card.component.html',
    styleUrls: ['./post-card.component.scss'],
})
export class PostCardComponent {
    @Input() post!: TextEditorModel;
    @Input() descriptionLength = 100;
    @Input() layout: 'grid-nl' | 'grid-lg' | 'list-nl' | 'list-sm' | null = null;
}
