import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS } from 'ng-lazyload-image';

import { LazyLoadImageHooks } from '@common/hooks/lazyload-image.hook';
import { PipesModule } from '@common/pipes/pipes.module';

import { PostCardComponent } from './post-card.component';

@NgModule({
    exports: [PostCardComponent],
    imports: [CommonModule, LazyLoadImageModule, RouterModule, PipesModule],
    declarations: [PostCardComponent],
    providers: [{ provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks }],
})
export class PostCardModule {}
