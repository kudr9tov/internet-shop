import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { WINDOW } from './window.providers';

@Injectable({
    providedIn: 'root',
})
export class CanonicalService {
    constructor(@Inject(DOCUMENT) private dom: Document, @Inject(WINDOW) private window: Window) {}

    setCanonicalURL(url?: string): void {
        const canURL = url === undefined ? this.dom.URL : url;
        const link: HTMLLinkElement = this.dom.createElement('link');
        link.setAttribute('rel', 'canonical');
        this.dom.head.appendChild(link);
        link.setAttribute('href', canURL);
    }

    updateCanonicalUrl(url?: string): void {
        const canURL = url === undefined ? this.dom.URL : this.window.location.origin + url;
        const head = this.dom.getElementsByTagName('head')[0];
        let element: HTMLLinkElement | null =
            this.dom.querySelector(`link[rel='canonical']`) || null;
        if (element == null) {
            element = this.dom.createElement('link') as HTMLLinkElement;
            head.appendChild(element);
        }
        element.setAttribute('rel', 'canonical');
        element.setAttribute('href', canURL);
    }
}
