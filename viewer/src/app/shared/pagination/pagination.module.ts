import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppIconModule } from '@common/app-icon/app-icon.module';

import { PaginationComponent } from './pagination.component';

@NgModule({
    exports: [PaginationComponent],
    imports: [CommonModule, AppIconModule],
    declarations: [PaginationComponent],
})
export class PaginationModule {}
