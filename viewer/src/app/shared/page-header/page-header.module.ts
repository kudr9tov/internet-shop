import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { PageHeaderComponent } from './components/page-header/page-header.component';

@NgModule({
    declarations: [PageHeaderComponent],
    imports: [CommonModule, RouterModule, AppIconModule],
    exports: [PageHeaderComponent],
})
export class PageHeaderModule {}
