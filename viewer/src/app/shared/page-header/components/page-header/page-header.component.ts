import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { LinkModel } from '@common/models/link.model';

@Component({
    selector: 'page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageHeaderComponent {
    @Input() header = '';
    @Input() breadcrumbs: LinkModel[] | null = [];
}
