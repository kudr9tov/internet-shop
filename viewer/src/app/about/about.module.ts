import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NguCarouselModule } from '@ngu/carousel';
import { NgxsModule } from '@ngxs/store';

import { AboutComponent } from './components/about/about.component';
import { AboutRoutingModule } from './about-routing.module';
import { AboutState } from './store/about.state';

import { CertificateListModule } from '@common/certificate-list/certificate-list.module';
import { CommonApiService } from '@common/services/common-api.service';
import { PipesModule } from '@common/pipes/pipes.module';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS } from 'ng-lazyload-image';
import { LazyLoadImageHooks } from '@common/hooks/lazyload-image.hook';
import { PageHeaderModule } from '../shared/page-header/page-header.module';

@NgModule({
    declarations: [AboutComponent],
    imports: [
        CommonModule,
        NguCarouselModule,
        AboutRoutingModule,
        CertificateListModule,
        PipesModule,
        LazyLoadImageModule,
        PageHeaderModule,
        NgxsModule.forFeature([AboutState]),
    ],
    providers: [CommonApiService, { provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks }],
})
export class AboutModule {}
