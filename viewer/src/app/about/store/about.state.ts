import { Injectable } from '@angular/core';
import { TextEditorModel } from '@common/models/text-editor.model';
import { CommonApiService } from '@common/services/common-api.service';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoadAboutText } from './about.actions';
import { AboutStateModel } from './about.model';
import { environment } from '@environments/environment';

@State<AboutStateModel>({
    name: 'aboutState',
    defaults: {
        aboutText: '',
    },
})
@Injectable()
export class AboutState {
    @Selector()
    static aboutText({ aboutText }: AboutStateModel): string {
        return aboutText;
    }

    constructor(private site: CommonApiService) {}

    @Action(LoadAboutText)
    onLoadAboutText({ patchState }: StateContext<AboutStateModel>): Observable<any> {
        return this.site
            .get(environment.aboutTextId)
            .pipe(tap((text: TextEditorModel) => patchState({ aboutText: text.content })));
    }
}
