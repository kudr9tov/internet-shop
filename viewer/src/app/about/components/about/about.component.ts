import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { filter, Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { Meta, Title } from '@angular/platform-browser';
import { NguCarouselConfig } from '@ngu/carousel';

import { AboutState } from '../../store/about.state';
import { LoadAboutText } from '../../store/about.actions';

import { CertificateListState } from '@common/certificate-list/store/certificate-list.state';
import { ListModel } from '@common/models/list.model';
import { LoadCertificateList } from '@common/certificate-list/store/certificate-list.actions';
import { ObserverComponent } from '@common/abstract/observer.component';
import { AppConstants } from '@common/constants/app.const';
import { LinkModel } from '@common/models/link.model';

@Component({
    selector: 'about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutComponent extends ObserverComponent implements OnInit {
    @Select(AboutState.aboutText) aboutText$!: Observable<string>;

    @Select(CertificateListState.certificates) certificates$!: Observable<ListModel[]>;

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: 'О компании', url: '/about' }),
    ];

    images: ListModel[] = [];

    height = 300;

    slideIndex = 0;

    carouselTile!: NguCarouselConfig;

    constructor(private store: Store, private titleService: Title, private metaTagService: Meta) {
        super();
        this.titleService.setTitle(AppConstants.aboutTitle);
        this.metaTagService.updateTag({
            name: 'description',
            content: AppConstants.aboutDescription,
        });
    }

    ngOnInit(): void {
        this.store.dispatch([new LoadAboutText(), new LoadCertificateList()]);
        this.initSubscriptions();
        this.carouselTile = {
            grid: { xs: 2, sm: 3, md: 3, lg: 5, all: 0 },
            slide: 2,
            speed: 400,
            interval: { timing: 4000, initialDelay: 1000 },
            animation: 'lazy',
            load: 2,
            velocity: 0.2,
            point: {
                visible: true,
            },
            touch: true,
            loop: true,
            easing: 'ease',
        };
    }

    openModal(): void {
        let modal = document.getElementById('imgModal');
        if (!modal) {
            return;
        }
        modal.style.display = 'flex';
    }

    closeModal(): void {
        let modal = document.getElementById('imgModal');
        if (!modal) {
            return;
        }
        modal.style.display = 'none';
    }

    plusSlides(n: number): void {
        this.showSlides((this.slideIndex += n));
    }

    currentSlide(n: number): void {
        this.showSlides((this.slideIndex = n));
    }

    showSlides(n: number): void {
        let i;
        const slides = document.getElementsByClassName(
            'img-slides',
        ) as HTMLCollectionOf<HTMLElement>;
        const dots = document.getElementsByClassName('images') as HTMLCollectionOf<HTMLElement>;
        if (n > slides.length) {
            this.slideIndex = 1;
        }
        if (n < 1) {
            this.slideIndex = slides.length;
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = 'none';
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(' active', '');
        }
        slides[this.slideIndex - 1].style.display = 'block';
        if (dots && dots.length > 0) {
            dots[this.slideIndex - 1].className += ' active';
        }
    }

    private initSubscriptions(): void {
        this.subscriptions.push(this.certificateSubscription());
    }

    private certificateSubscription(): Subscription {
        return this.certificates$.pipe(filter((x) => !!x)).subscribe((certificates) => {
            this.images = certificates;
        });
    }
}
