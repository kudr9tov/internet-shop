import { NgModule } from '@angular/core';

// modules (angular)
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// modules

// components
import { MobileHeaderComponent } from './components/mobile-header/mobile-header.component';
import { MobileLinksComponent } from './components/mobile-links/mobile-links.component';
import { MobileMenuComponent } from './components/mobile-menu/mobile-menu.component';
import { ComponentsModule } from '../components/components.module';
import { DirectiveModule } from '@common/directives/directive.modul';
import { AppIconModule } from '@common/app-icon/app-icon.module';
@NgModule({
    declarations: [
        // components
        MobileHeaderComponent,
        MobileLinksComponent,
        MobileMenuComponent,
    ],
    imports: [
        // modules (angular)
        CommonModule,
        RouterModule,
        // modules
        ComponentsModule,
        AppIconModule,
        DirectiveModule,
    ],
    exports: [
        // components
        MobileHeaderComponent,
        MobileMenuComponent,
    ],
})
export class MobileModule {}
