import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MobileMenuService } from '@common/services/mobile-menu.service';

export interface MobileMenuItem {
    label: string;
    type: 'link' | 'divider' | 'button';
    url: string;
    children?: MobileMenuItem[];
}

@Component({
    selector: 'app-mobile-menu',
    templateUrl: './mobile-menu.component.html',
    styleUrls: ['./mobile-menu.component.scss'],
})
export class MobileMenuComponent implements OnDestroy, OnInit {
    private destroy$: Subject<any> = new Subject();

    isOpen = false;
    links = [
        { label: 'Оборудование', url: '/catalog', type: 'link' },
        { label: 'О компании', url: '/about', type: 'link' },
        { label: 'Каталоги', url: '/magazines', type: 'link' },
        { label: 'Бренды', url: '/brands', type: 'link' },
        { label: 'Сервис', url: '/service', type: 'link' },
        { label: 'Контакты', url: '/contacts', type: 'link' },
        { label: 'Новости', url: '/news', type: 'link' },
        { label: 'Войти', url: '/login', type: 'link' },
    ] as MobileMenuItem[];

    constructor(public mobilemenu: MobileMenuService) {}

    ngOnInit(): void {
        this.mobilemenu.isOpen$
            .pipe(takeUntil(this.destroy$))
            .subscribe((isOpen) => (this.isOpen = isOpen));
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    onItemClick(event: MobileMenuItem): void {
        if (event.type === 'link') {
            this.mobilemenu.close();
        }

        // if (event.data && event.data.language) {
        //     console.log(event.data.language); // change language
        // }
    }
}
