import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

import { ImageWrapperModule } from '@common/image-wrapper/image-wrapper.module';

import { SubcategoriesComponent } from './subcategories.component';
import { SubcategoriesRoutingModule } from './subcategories-routing.module';
import { PageHeaderModule } from '../shared/page-header/page-header.module';

@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatCardModule,
        ImageWrapperModule,
        PageHeaderModule,
        SubcategoriesRoutingModule,
    ],
    declarations: [SubcategoriesComponent],
})
export class SubcategoriesModule {}
