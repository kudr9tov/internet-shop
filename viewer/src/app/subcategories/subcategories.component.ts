import { Component, OnInit } from '@angular/core';
import { combineLatestWith, Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { AppHelper } from '@common/constants/app.const';
import { CategoriesQuery } from '@common/auth/store/categories/categories.query';
import { ObserverComponent } from '@common/abstract/observer.component';
import { CategoryLinkModel } from '@common/models/category-link.model';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { LinkModel } from '@common/models/link.model';

import { SeoSocialShareService } from '../services/seo.service';

@Component({
    selector: 'subcategories',
    templateUrl: './subcategories.component.html',
    styleUrls: ['./subcategories.component.scss'],
})
export class SubcategoriesComponent extends ObserverComponent implements OnInit {
    @Select(CategoriesQuery.subCategories)
    subCategories$!: Observable<CategoryLinkModel[]>;

    @Select(CategoriesQuery.subCategoryBreadcrumb)
    subCategoriesBreadcrumb$!: Observable<LinkModel[]>;

    subCategory!: TodoItemFlatNode[];

    categories!: CategoryLinkModel[];

    constructor(public store: Store, private seoService: SeoSocialShareService) {
        super();
    }

    ngOnInit(): void {
        this.subscriptions.push(this.subCategoriesSubscription());
    }

    trackByFn(_: number, item: { id: number }): number {
        return item.id;
    }

    private subCategoriesSubscription(): Subscription {
        return this.subCategories$
            .pipe(combineLatestWith(this.subCategoriesBreadcrumb$))
            .subscribe(([categories, breadcrumb]) => {
                if (!categories || !categories.length || !breadcrumb) {
                    return;
                }
                this.categories = categories;
                const words = categories?.map((x) => x.name).join(', ');
                const breadcrumbWords = breadcrumb?.map((x) => x.label).join(', ');
                const currentItem = breadcrumb[breadcrumb?.length - 1];
                const title = `${currentItem?.label} - ${
                    breadcrumb[breadcrumb?.length - 2]?.label || ''
                }`;
                const image = currentItem && currentItem.image ? currentItem.image : '';
                this.seoService.setData({
                    title: AppHelper.getCategoryTitle(title),
                    description: AppHelper.categoryDescription(title),
                    image: image,
                    type: 'website',
                    keywords: breadcrumbWords + ', ' + words,
                    imageAuxData: {
                        alt: currentItem.label,
                        height: 968,
                        width: 504,
                        mimeType: 'image/jpeg',
                        secureUrl: image,
                    },
                });
            });
    }
}
