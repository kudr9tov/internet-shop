import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

import { CategoryModel } from '@common/models/category.model';
import { SelectCategoryId } from '@common/auth/store/categories/categories.actions';

@Injectable()
export class CategoryResolver implements Resolve<CategoryModel> {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<CategoryModel> {
        const id = +route.params['id'];

        return this.store.dispatch(new SelectCategoryId(id));
    }
}
