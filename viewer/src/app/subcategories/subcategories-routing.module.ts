import { SubcategoriesComponent } from './subcategories.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CategoryResolver } from './category-resolver';

const routes: Routes = [
    {
        path: ':id',
        component: SubcategoriesComponent,
        resolve: {
            catalog: CategoryResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [CategoryResolver],
})
export class SubcategoriesRoutingModule {}
