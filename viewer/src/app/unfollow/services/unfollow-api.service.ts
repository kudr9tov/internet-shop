import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from '@common/abstract/base-api.service';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Injectable()
export class UnfollowApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(email: string): Observable<any> {
        return this.httpGet(`mail/unfollow?email=${email}`, (x) => x);
    }
}
