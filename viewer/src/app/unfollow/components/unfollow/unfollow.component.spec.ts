import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { UnfollowComponent } from './unfollow.component';

describe('UnfollowComponent', () => {
    let component: UnfollowComponent;
    let fixture: ComponentFixture<UnfollowComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [UnfollowComponent],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(UnfollowComponent);
        component = fixture.componentInstance;
    }));

    afterEach(() => {
        fixture.destroy();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
