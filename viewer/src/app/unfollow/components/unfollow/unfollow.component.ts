import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { SetError, SetSuccess } from '@common/toast';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { UnfollowApiService } from '../../services/unfollow-api.service';

@Component({
    selector: 'unfollow',
    templateUrl: './unfollow.component.html',
    styleUrls: ['./unfollow.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnfollowComponent {
    email: string;

    step = 1;

    constructor(
        private router: Router,
        public store: Store,
        public ref: ChangeDetectorRef,
        public apiService: UnfollowApiService,
    ) {
        this.email = (this.router as any).browserUrlTree?.queryParams?.email;
    }

    onMainPage(): void {
        this.store.dispatch(new Navigate(['/']));
    }

    onUnsubscribe(): void {
        this.apiService.get(this.email).subscribe(
            () => {
                this.step = 2;
                this.ref.detectChanges();
                this.store.dispatch(new SetSuccess('Вы успешно отписались от рассылки!'));
            },
            (error) => {
                this.store.dispatch(new SetError(error.message));
            },
        );
    }
}
