import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UnfollowComponent } from './components/unfollow/unfollow.component';

const routes: Routes = [
    {
        path: '',
        component: UnfollowComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UnfollowRoutingModule {}
