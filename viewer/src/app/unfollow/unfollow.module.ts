import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { UnfollowComponent } from './components/unfollow/unfollow.component';
import { UnfollowRoutingModule } from './unfollow-routing.module';
import { UnfollowApiService } from './services/unfollow-api.service';

@NgModule({
    declarations: [UnfollowComponent],
    imports: [CommonModule, UnfollowRoutingModule, MatCardModule, MatButtonModule],
    providers: [UnfollowApiService],
})
export class UnfollowModule {}
