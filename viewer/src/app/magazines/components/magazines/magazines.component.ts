import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { Navigate } from '@ngxs/router-plugin';
import { Meta, Title } from '@angular/platform-browser';

import { BrandModel } from '@common/models/brand.model';
import { CatalogModel } from '@common/models/catalog.model';
import { AppConstants } from '@common/constants/app.const';
import { ObserverComponent } from '@common/abstract/observer.component';
import { ShopSidebarService } from '@common/services/shop-sidebar.service';

import { MagazinesState } from '../../store/magazines.state';
import { LoadBrandsWithMagazines } from '../../store/magazines.actions';
import { LinkModel } from '@common/models/link.model';
import { environment } from '@environments/environment';

@Component({
    selector: 'magazines',
    templateUrl: './magazines.component.html',
    styleUrls: ['./magazines.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MagazinesComponent extends ObserverComponent implements OnInit {
    viewMode: 'grid' | 'grid-with-features' | 'list' = 'grid';

    @Select(MagazinesState.brands)
    brands$!: Observable<BrandModel[]>;

    @Select(MagazinesState.magazine)
    magazine$!: Observable<CatalogModel>;

    @Select(MagazinesState.isOpenDialog)
    isOpenDialog$!: Observable<boolean>;

    catalogs!: CatalogModel[];

    magazine!: CatalogModel;

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: 'Каталоги', url: '/magazines' }),
    ];

    header = 'Каталоги';

    constructor(
        public store: Store,
        private titleService: Title,
        private metaTagService: Meta,
        public sidebar: ShopSidebarService,
    ) {
        super();
        this.subscriptions.push(this.magazineSubscription());
    }

    trackByFn(_: number, item: { id: number }): any {
        return item.id;
    }

    ngOnInit(): void {
        this.store.dispatch(new LoadBrandsWithMagazines());
    }

    onSelect(id: number): void {
        this.store.dispatch(new Navigate(['magazines', id]));
    }

    private magazineSubscription(): Subscription {
        return this.magazine$.subscribe((magazine: CatalogModel) => {
            if (magazine) {
                this.magazine = magazine;
            }

            this.titleService.setTitle(
                magazine ? magazine.name : `${environment.name}: ` + AppConstants.magazinesTitle,
            );
            this.metaTagService.updateTag({
                name: 'description',
                content: magazine
                    ? AppConstants.magazinesDescription + ' ' + magazine.catalogFileUrl
                    : AppConstants.magazinesDescription,
            });
        });
    }
}
