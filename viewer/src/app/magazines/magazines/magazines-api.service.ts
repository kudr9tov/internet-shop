import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { BaseAuthorizedApiService } from '@common/auth/services/base-authorized-api.service';
import { BrandModel } from '@common/models/brand.model';
import { CatalogModel } from '@common/models/catalog.model';

@Injectable()
export class MagazinesApiService extends BaseAuthorizedApiService {
    constructor(http: HttpClient, protected override store: Store) {
        super(http, store);
    }

    public getList(): Observable<BrandModel[]> {
        return this.httpGet('catalog', (x) => new BrandModel(x));
    }

    public get(id: number): Observable<CatalogModel> {
        return this.httpGet(`catalog/${id}`, (x) => new CatalogModel(x));
    }
}
