import { State, Selector, Action, StateContext } from '@ngxs/store';

import { MagazinesStateModel } from './magazines.model';
import { LoadBrandsWithMagazines, LoadMagazine, ToggleDialog } from './magazines.actions';
import { BrandModel } from '@common/models/brand.model';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MagazinesApiService } from '../magazines/magazines-api.service';
import { Injectable } from '@angular/core';
import { CatalogModel } from '@common/models/catalog.model';

@State<MagazinesStateModel>({
    name: 'magazines',
    defaults: {
        brands: [],
        magazine: null,
        isOpenDialog: true,
    },
})
@Injectable()
export class MagazinesState {
    @Selector()
    static brands({ brands }: MagazinesStateModel): BrandModel[] {
        return brands || [];
    }

    @Selector()
    static magazine({ magazine }: MagazinesStateModel): CatalogModel | null {
        return magazine;
    }

    @Selector()
    static isOpenDialog({ isOpenDialog }: MagazinesStateModel): boolean {
        return isOpenDialog;
    }

    constructor(private apiService: MagazinesApiService) {}

    @Action(ToggleDialog)
    onToggleDialog(
        { patchState }: StateContext<MagazinesStateModel>,
        { isOpenDialog }: ToggleDialog,
    ): void {
        patchState({ isOpenDialog });
    }

    @Action(LoadBrandsWithMagazines)
    onLoadBrandsWithMagazines({ patchState }: StateContext<MagazinesStateModel>): Observable<any> {
        return this.apiService.getList().pipe(
            tap((brands: BrandModel[]) => {
                patchState({ brands });
            }),
        );
    }

    @Action(LoadMagazine)
    onLoadMagazine(
        { patchState }: StateContext<MagazinesStateModel>,
        { id }: LoadMagazine,
    ): Observable<any> {
        return this.apiService.get(id).pipe(
            tap((magazine: CatalogModel) => {
                patchState({ magazine });
            }),
        );
    }
}
