import { BrandModel } from '@common/models/brand.model';
import { CatalogModel } from '@common/models/catalog.model';

export interface MagazinesStateModel {
    brands: BrandModel[];
    magazine: CatalogModel | null;
    isOpenDialog: boolean;
}
