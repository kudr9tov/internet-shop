export class LoadBrandsWithMagazines {
    static readonly type = '[Magazines] Load brands with magazines';
}

export class LoadMagazine {
    static readonly type = '[Magazines] Load magazine';
    constructor(public id: number) {}
}

export class ToggleDialog {
    static readonly type = '[Magazines] toggle dialog';
    constructor(public isOpenDialog: boolean) {}
}
