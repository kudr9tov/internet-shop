import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { LoadMagazine } from '../store/magazines.actions';
import { CatalogModel } from '@common/models/catalog.model';

@Injectable()
export class MagazineResolver implements Resolve<CatalogModel> {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<CatalogModel> {
        const id = +route.params['id'];
        return this.store.dispatch(new LoadMagazine(id));
    }
}
