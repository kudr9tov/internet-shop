import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MagazinesComponent } from './components/magazines/magazines.component';
import { MagazineResolver } from './resolvers/magazine.resolver';

const routes: Routes = [
    {
        path: '',
        component: MagazinesComponent,
    },
    {
        path: ':id',
        component: MagazinesComponent,
        resolve: {
            magazines: MagazineResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [MagazineResolver],
})
export class MagazinesRoutingModule {}
