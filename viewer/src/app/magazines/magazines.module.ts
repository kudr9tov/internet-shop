import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

import { PipesModule } from '@common/pipes/pipes.module';
import { DirectiveModule } from '@common/directives/directive.modul';
import { AppIconModule } from '@common/app-icon/app-icon.module';

import { MagazinesComponent } from './components/magazines/magazines.component';
import { MagazinesRoutingModule } from './magazines-routing.module';
import { MagazinesState } from './store/magazines.state';
import { MagazinesApiService } from './magazines/magazines-api.service';
import { ShopSidebarModule } from '../components/shop-sidebar/shop-sidebar.module';
import { AppCollapseModule } from '../app-collapse/app-collapse.module';
import { PageHeaderModule } from '../shared/page-header/page-header.module';

@NgModule({
    imports: [
        CommonModule,
        NgxsModule.forFeature([MagazinesState]),
        PipesModule,
        NgxExtendedPdfViewerModule,
        ShopSidebarModule,
        AppCollapseModule,
        DirectiveModule,
        AppIconModule,
        PageHeaderModule,
        MagazinesRoutingModule,
    ],
    declarations: [MagazinesComponent],
    providers: [MagazinesApiService],
})
export class MagazinesModule {}
