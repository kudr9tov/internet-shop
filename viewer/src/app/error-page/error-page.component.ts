import { isPlatformServer } from '@angular/common';
import { Component, Inject, OnInit, Optional, PLATFORM_ID } from '@angular/core';
import { environment } from '@environments/environment';
import { RESPONSE } from '@nguniversal/express-engine/tokens';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { Response } from 'express';

@Component({
    selector: 'app-error-page',
    templateUrl: './error-page.component.html',
    styleUrls: ['./error-page.component.scss'],
})
export class ErrorPageComponent implements OnInit {
    constructor(
        public store: Store,
        @Optional() @Inject(RESPONSE) public response: Response,
        @Inject(PLATFORM_ID) public platformId: any,
    ) {}

    erroreImagePath = `${environment.apiPath}/images/SKcnr_error-page.png`;

    ngOnInit() {
        if (isPlatformServer(this.platformId)) {
            this.response.status(404);
        }
    }

    onMain(): void {
        this.store.dispatch(new Navigate(['/']));
    }
}
