import { Injectable } from '@angular/core';
import { TextEditorModel } from '@common/models/text-editor.model';
import { CommonApiService } from '@common/services/common-api.service';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoadServiceText } from './service.actions';

import { ServiceStateModel } from './service.model';
import { environment } from '@environments/environment';

@State<ServiceStateModel>({
    name: 'serviceState',
    defaults: {
        serviceText: '',
    },
})
@Injectable()
export class ServiceState {
    @Selector()
    static serviceText({ serviceText }: ServiceStateModel): string {
        return serviceText;
    }

    constructor(private site: CommonApiService) { }

    @Action(LoadServiceText)
    onLoadServiceText({ patchState }: StateContext<ServiceStateModel>): Observable<any> {
        return this.site
            .get(environment.serviceTextId)
            .pipe(tap((text: TextEditorModel) => patchState({ serviceText: text.content })));
    }
}
