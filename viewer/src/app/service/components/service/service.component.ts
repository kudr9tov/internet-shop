import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { AppConstants } from '@common/constants/app.const';

import { ServiceState } from '../../store/service.state';
import { LoadServiceText } from '../../store/service.actions';
import { LinkModel } from '@common/models/link.model';

@Component({
    selector: 'service',
    templateUrl: './service.component.html',
    styleUrls: ['./service.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceComponent {
    @Select(ServiceState.serviceText) serviceText$!: Observable<string>;

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: 'Сервис', url: '/service' }),
    ];

    constructor(private titleService: Title, private store: Store, private metaTagService: Meta) {
        this.store.dispatch(new LoadServiceText());
        this.titleService.setTitle(AppConstants.serviceTitle);
        this.metaTagService.updateTag({
            name: 'description',
            content: AppConstants.serviceDescription,
        });
    }
}
