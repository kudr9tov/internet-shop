import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PipesModule } from '@common/pipes/pipes.module';
import { CommonApiService } from '@common/services/common-api.service';
import { ServiceComponent } from './components/service/service.component';
import { ServiceRoutingModule } from './service-routing.module';
import { NgxsModule } from '@ngxs/store';
import { ServiceState } from './store/service.state';
import { PageHeaderModule } from '../shared/page-header/page-header.module';

@NgModule({
    declarations: [ServiceComponent],
    imports: [
        CommonModule,
        ServiceRoutingModule,
        PipesModule,
        PageHeaderModule,
        NgxsModule.forFeature([ServiceState]),
    ],
    providers: [CommonApiService],
})
export class ServiceModule {}
