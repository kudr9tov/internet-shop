import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { DirectiveModule } from '@common/directives/directive.modul';
import { AppCollapseComponent } from './components/app-collapse/app-collapse.component';

@NgModule({
    declarations: [AppCollapseComponent],
    imports: [CommonModule, AppIconModule, DirectiveModule],
    exports: [AppCollapseComponent],
})
export class AppCollapseModule {}
