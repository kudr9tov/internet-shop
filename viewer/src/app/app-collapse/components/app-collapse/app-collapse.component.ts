import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-collapse',
    templateUrl: './app-collapse.component.html',
    styleUrls: ['./app-collapse.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppCollapseComponent {
    @Input() name!: string;

    @Input() isCollapsed = true;
}
