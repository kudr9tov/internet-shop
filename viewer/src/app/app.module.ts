import { LOCALE_ID, NgModule } from '@angular/core';
import { APP_BASE_HREF, CommonModule, registerLocaleData } from '@angular/common';
import { BrowserModule, HammerModule, provideClientHydration } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { TransferHttpCacheModule } from '@nguniversal/common';
import localeRu from '@angular/common/locales/ru';

import { AuthModule } from '@common/auth/auth.module';
import { ToastModule } from '@common/toast/toast.module';

import { AppComponent } from './app.component';
import { appRouterProviders } from './app-routing.module';
import { CanonicalService } from './shared/canonical.service';
import { WINDOW_PROVIDERS } from './shared/window.providers';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HeaderModule } from './header/header.module';
import { RootComponent } from './components/components/root/root.component';
import { MobileModule } from './mobile/mobile.module';
import { LandingFooterModule } from './landing-footer/landing-footer.module';
import { FilterDataModule } from '@common/filter/filter-data.module';
import { IsPreloadingStrategy } from './shared/is-preloading.strategy';
import { BasketModule } from './shared/basket-popup/basket-popup.module';
import { NgxsStoreModule } from './store.nodule';
import { HttpClientModule } from '@angular/common/http';

registerLocaleData(localeRu, 'ru');

@NgModule({
    declarations: [AppComponent, RootComponent, ErrorPageComponent],
    imports: [
        AuthModule,
        BrowserModule,
        NgxsStoreModule,
        CommonModule,
        FilterDataModule,
        MatButtonModule,
        TransferHttpCacheModule,
        HttpClientModule,
        MatDialogModule,
        HammerModule,
        LandingFooterModule,
        ToastModule,
        HeaderModule,
        MobileModule,
        BasketModule,

        appRouterProviders,
    ],
    bootstrap: [AppComponent],
    providers: [
        provideClientHydration(),
        CanonicalService,
        IsPreloadingStrategy,
        WINDOW_PROVIDERS,
        { provide: LOCALE_ID, useValue: 'ru' },
        { provide: APP_BASE_HREF, useValue: '/' },
        // {
        //     provide: APP_INITIALIZER,
        //     useFactory: (platformId: object) => () => {
        //       if (isPlatformBrowser(platformId)) {
        //         const dom = ɵgetDOM();
        //         const styles = Array.prototype.slice.apply(
        //           dom.getDefaultDocument().querySelectorAll('style[ng-transition]')
        //         );
        //         styles.forEach(el => {
        //           // Remove ng-transition attribute to prevent Angular appInitializerFactory
        //           // to remove server styles before preboot complete
        //           el.removeAttribute('ng-transition');
        //         });
        //         dom.getDefaultDocument().addEventListener('PrebootComplete', () => {
        //           // After preboot complete, remove the server scripts
        //           styles.forEach(el => dom.remove(el));
        //         });
        //       }
        //     },
        //     deps: [PLATFORM_ID],
        //     multi: true,
        //   }
    ],
})
export class AppModule {}
