import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PrivacyRoutingModule } from './privacy-routing.module';
import { PrivacyComponent } from './components/privacy/privacy.component';
import { PipesModule } from '@common/pipes/pipes.module';
import { NgxsModule } from '@ngxs/store';
import { PrivacyState } from './store/privacy.state';
import { CommonApiService } from '@common/services/common-api.service';
import { PageHeaderModule } from '../shared/page-header/page-header.module';

@NgModule({
    declarations: [PrivacyComponent],
    imports: [
        CommonModule,
        PipesModule,
        PrivacyRoutingModule,
        PageHeaderModule,
        NgxsModule.forRoot([PrivacyState]),
    ],
    providers: [CommonApiService],
})
export class PrivacyModule {}
