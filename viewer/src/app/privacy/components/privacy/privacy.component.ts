import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { AppConstants } from '@common/constants/app.const';

import { LoadPrivacyText } from '../../store/privacy.actions';
import { PrivacyState } from '../../store/privacy.state';
import { LinkModel } from '@common/models/link.model';

@Component({
    selector: 'privacy',
    templateUrl: './privacy.component.html',
    styleUrls: ['./privacy.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrivacyComponent {
    @Select(PrivacyState.privacyText) privacyText$!: Observable<string>;

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({
            label: 'Политика обработки персональных данных',
            url: '/privacy',
        }),
    ];

    constructor(private titleService: Title, private store: Store, private metaTagService: Meta) {
        this.store.dispatch(new LoadPrivacyText());
        this.titleService.setTitle(AppConstants.privacyTitle);
        this.metaTagService.updateTag({
            name: 'description',
            content: AppConstants.privacyDescription,
        });
    }
}
