import { Injectable } from '@angular/core';
import { TextEditorModel } from '@common/models/text-editor.model';
import { CommonApiService } from '@common/services/common-api.service';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoadPrivacyText } from './privacy.actions';

import { PrivacyStateModel } from './privacy.model';

@State<PrivacyStateModel>({
    name: 'privacyState',
    defaults: {
        privacyText: '',
    },
})
@Injectable()
export class PrivacyState {
    readonly privacyTextId = 5;

    @Selector()
    static privacyText({ privacyText }: PrivacyStateModel): string {
        return privacyText;
    }

    constructor(private site: CommonApiService) {}

    @Action(LoadPrivacyText)
    onLoadPrivacyText({ patchState }: StateContext<PrivacyStateModel>): Observable<any> {
        return this.site
            .get(this.privacyTextId)
            .pipe(tap((text: TextEditorModel) => patchState({ privacyText: text.content })));
    }
}
