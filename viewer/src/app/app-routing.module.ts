import { RouterModule, Routes } from '@angular/router';
import { RootComponent } from './components/components/root/root.component';
import { ErrorPageComponent } from './error-page/error-page.component';

const routes: Routes = [
    {
        path: '',
        component: RootComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('./landing/landing.module').then((m) => m.LandingModule),
            },
            {
                path: 'login',
                loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
            },
            {
                path: 'catalog',
                loadChildren: () => import('./catalog/catalog.module').then((m) => m.CatalogModule),
            },
            {
                path: 'subcategories',
                loadChildren: () =>
                    import('./subcategories/subcategories.module').then(
                        (m) => m.SubcategoriesModule,
                    ),
            },
            {
                path: 'magazines',
                loadChildren: () =>
                    import('./magazines/magazines.module').then((m) => m.MagazinesModule),
            },
            {
                path: 'about',
                loadChildren: () => import('./about/about.module').then((m) => m.AboutModule),
            },
            {
                path: 'service',
                loadChildren: () => import('./service/service.module').then((m) => m.ServiceModule),
            },
            {
                path: 'brands',
                loadChildren: () => import('./brands/brands.module').then((m) => m.BrandsModule),
            },
            {
                path: 'contacts',
                loadChildren: () =>
                    import('./contacts/contacts.module').then((m) => m.ContactsModule),
            },
            {
                path: 'privacy',
                loadChildren: () => import('./privacy/privacy.module').then((m) => m.PrivacyModule),
            },
            {
                path: 'news',
                loadChildren: () => import('./news/news.module').then((m) => m.NewsModule),
            },
            {
                path: 'unfollow',
                loadChildren: () =>
                    import('./unfollow/unfollow.module').then((m) => m.UnfollowModule),
            },
            {
                path: 'wishlist',
                loadChildren: () =>
                    import('./wishlist/wishlist.module').then((m) => m.WishlistModule),
            },
            { path: '404', component: ErrorPageComponent },
            { path: '**', redirectTo: '/404' },
        ],
    },
];

export const appRouterProviders = RouterModule.forRoot(routes);
