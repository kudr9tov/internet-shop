import { CreateCommentModel } from '@common/models/create-comment.model';

export class LoadPostComments {
    static readonly type = '[Comment] Load Post Comments';
    constructor(public id: number) {}
}

export class LoadProductComments {
    static readonly type = '[Comment] Load Products Comments';
    constructor(public id: number) {}
}

export class CreatePostComment {
    static readonly type = '[Comment] Create Post Comment';

    constructor(public model: CreateCommentModel) {}
}

export class CreateProductComment {
    static readonly type = '[Comment] Create Product Comment';

    constructor(public model: CreateCommentModel) {}
}

export class UpdateComment {
    static readonly type = '[Comment] Update Comment';

    constructor(public model: CreateCommentModel) {}
}

export class DeleteComment {
    static readonly type = '[Comment] delete Comment';

    constructor(public id: number) {}
}
