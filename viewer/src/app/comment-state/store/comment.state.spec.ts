import { NgxsModule, Store } from '@ngxs/store';
import { TestBed } from '@angular/core/testing';

import { CommentState } from './comment.state';

describe('CommentState', () => {
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([CommentState])],
        });

        store = TestBed.get(Store);
        resetStore();
        resetMockProviders();
    });

    it('should create store', () => {
        expect(store).toBeTruthy();
    });

    function resetMockProviders(): void {}

    function resetStore(): void {
        store.reset({
            commentState: {},
        });
    }
});
