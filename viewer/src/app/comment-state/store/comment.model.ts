import { CommentModel } from '@common/models/comment.model';

export interface CommentStateModel {
    postComments: CommentModel[];
    productComments: CommentModel[];
}
