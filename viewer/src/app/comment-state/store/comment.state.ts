import { Injectable } from '@angular/core';
import { CommentType } from '@common/enums/comment-type.enum';
import { CommentModel } from '@common/models/comment.model';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { append, patch } from '@ngxs/store/operators';
import { Observable, tap } from 'rxjs';
import { CommentApiService } from '../services/comment-api.service';
import {
    CreatePostComment,
    CreateProductComment,
    DeleteComment,
    LoadPostComments,
    LoadProductComments,
} from './comment.actions';

import { CommentStateModel } from './comment.model';

@State<CommentStateModel>({
    name: 'commentState',
    defaults: {
        postComments: [],
        productComments: [],
    },
})
@Injectable()
export class CommentState {
    @Selector()
    static postComments({ postComments }: CommentStateModel): CommentModel[] {
        return postComments;
    }

    @Selector()
    static productComments({ productComments }: CommentStateModel): CommentModel[] {
        return productComments;
    }

    constructor(private apiService: CommentApiService) {}

    @Action(LoadPostComments)
    onLoadPostComments(
        { patchState }: StateContext<CommentStateModel>,
        { id }: LoadPostComments,
    ): Observable<any> {
        return this.apiService.get(id, CommentType.Post).pipe(
            tap((postComments: CommentModel[]) => {
                patchState({ postComments });
            }),
        );
    }

    @Action(LoadProductComments)
    onLoadProductComments(
        { patchState }: StateContext<CommentStateModel>,
        { id }: LoadProductComments,
    ): Observable<any> {
        return this.apiService.get(id, CommentType.Product).pipe(
            tap((productComments: CommentModel[]) => {
                patchState({ productComments });
            }),
        );
    }

    @Action(CreatePostComment)
    onCreatePostComment(
        { setState }: StateContext<CommentStateModel>,
        { model }: CreatePostComment,
    ): Observable<any> {
        return this.apiService.create(model).pipe(
            tap((post: CommentModel) => {
                setState(patch({ postComments: append([post]) }));
            }),
        );
    }

    @Action(CreateProductComment)
    onCreateProductComment(
        { setState }: StateContext<CommentStateModel>,
        { model }: CreateProductComment,
    ): Observable<any> {
        return this.apiService.create(model).pipe(
            tap((post: CommentModel) => {
                setState(patch({ productComments: append([post]) }));
            }),
        );
    }

    @Action(DeleteComment)
    onDeleteComment(
        { setState }: StateContext<CommentStateModel>,
        { id }: DeleteComment,
    ): Observable<any> {
        return this.apiService.delete(id).pipe(
            tap(() =>
                setState(
                    patch({
                        //    brands: removeItem<CommentModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }
}
