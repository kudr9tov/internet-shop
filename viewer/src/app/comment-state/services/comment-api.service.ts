import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from '@common/abstract/base-api.service';
import { CommentType } from '@common/enums/comment-type.enum';
import { CommentModel } from '@common/models/comment.model';
import { CreateCommentModel } from '@common/models/create-comment.model';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Injectable()
export class CommentApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(id: number, type: CommentType): Observable<any> {
        return this.httpGet(`comments/${type}/${id}`, (x) => new CommentModel(x));
    }

    public create(model: CreateCommentModel): Observable<any> {
        return this.httpPost('comments', (x) => x, model);
    }

    // public update(model: CreateProductModel): Observable<any> {
    //     return this.httpPost('products', x => x, model);
    // }

    public delete(id: number): Observable<any> {
        return this.httpDelete('products', id, (x) => x);
    }
}
