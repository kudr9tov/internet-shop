import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { CommentState } from './store/comment.state';
import { CommentApiService } from './services/comment-api.service';

@NgModule({
    declarations: [],
    imports: [CommonModule, NgxsModule.forFeature([CommentState])],
    providers: [CommentApiService],
})
export class CommentStateModule {}
