export class LoadSlides {
    static readonly type = '[Landing] Load slides';
}

export class LoadPosts {
    static readonly type = '[Landing] Load posts';
}

export class AddSubscription {
    static readonly type = '[Landing] Add Subscription';
    constructor(public email: string) {}
}
