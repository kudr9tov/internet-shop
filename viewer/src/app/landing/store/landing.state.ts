import { State, Selector, Action, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

import { Slide } from '@common/models/slide.model';

import { AddSubscription, LoadPosts, LoadSlides } from './landing.actions';
import { LandingApiService } from '../services/landing-api.service';
import { LandingStateModel } from './landing.model';
import { TextEditorModel } from '@common/models/text-editor.model';

@State<LandingStateModel>({
    name: 'landingState',
    defaults: {
        slides: [],
        posts: [],
    },
})
@Injectable()
export class LandingState {
    @Selector()
    static slides({ slides }: LandingStateModel): Slide[] {
        return slides || [];
    }

    @Selector()
    static posts({ posts }: LandingStateModel): TextEditorModel[] {
        return posts || [];
    }

    constructor(private apiService: LandingApiService) {}

    @Action(LoadSlides)
    onLoadSlides({ patchState }: StateContext<LandingStateModel>): Observable<any> {
        return this.apiService.get().pipe(
            tap((slides: Slide[]) => {
                patchState({ slides });
            }),
        );
    }

    @Action(LoadPosts)
    onLoadPosts({ patchState }: StateContext<LandingStateModel>): Observable<any> {
        return this.apiService.getPosts().pipe(
            tap((posts: TextEditorModel[]) => {
                patchState({
                    posts: posts.filter((x) => !x.system).sort((a, b) => b.created - a.created),
                });
            }),
        );
    }

    @Action(AddSubscription)
    onAddSubscription(
        _: StateContext<LandingStateModel>,
        { email }: AddSubscription,
    ): Observable<any> {
        return this.apiService.createEmail(email);
    }
}
