import { Slide } from '@common/models/slide.model';
import { TextEditorModel } from '@common/models/text-editor.model';

export interface LandingStateModel {
    slides: Slide[];
    posts: TextEditorModel[];
}
