import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';

import { BrandsDataModule } from '@common/brands/brands-data.module';
import { PipesModule } from '@common/pipes/pipes.module';
import { ImageWrapperModule } from '@common/image-wrapper/image-wrapper.module';

import { LandingComponent } from './landing.component';
import { LandingFeaturesComponent } from './shared/components/landing-features/landing-features.component';
import { LandingHeroComponent } from './shared/components/landing-hero/landing-hero.component';
import { LandingRoutingModule } from './landing-routing.module';
import { ProductCardModule } from '../product-card/product-card.module';
import { LandingCategoriesComponent } from './shared/components/landing-categories/landing-categories.component';
import { NewSaleState } from './shared/store/new-sale.state';
import { NewSaleService } from './shared/services/new-sale.service';
import { NguCarouselModule } from '@ngu/carousel';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS } from 'ng-lazyload-image';
import { LazyLoadImageHooks } from '@common/hooks/lazyload-image.hook';
import { LandingBrandsComponent } from './shared/components/landing-brands/landing-brands.component';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { ComponentsModule } from '../components/components.module';
import { LandingPostsComponent } from './shared/components/landing-posts/landing-posts.component';
import { LandingApiService } from './services/landing-api.service';
import { LandingState } from './store/landing.state';
import { PostCardModule } from '../shared/post-card/post-card.module';
import { MainDirectionsComponent } from './shared/components/main-directions/main-directions.component';

@NgModule({
    declarations: [
        LandingComponent,
        LandingFeaturesComponent,
        LandingHeroComponent,
        LandingCategoriesComponent,
        LandingBrandsComponent,
        MainDirectionsComponent,
        LandingPostsComponent,
    ],
    imports: [
        CommonModule,
        BrandsDataModule,
        NgxsModule.forFeature([NewSaleState, LandingState]),
        FormsModule,
        LandingRoutingModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatMenuModule,
        NguCarouselModule,
        PipesModule,
        FormsModule,
        OverlayModule,
        ImageWrapperModule,
        ProductCardModule,
        ReactiveFormsModule,
        LazyLoadImageModule,
        ComponentsModule,
        PostCardModule,
        RouterModule,
        AppIconModule,
    ],
    providers: [
        NewSaleService,
        LandingApiService,
        { provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks },
    ],
})
export class LandingModule {}
