import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseApiService } from '@common/abstract/base-api.service';
import { Observable } from 'rxjs';
import { Slide } from '@common/models/slide.model';
import { TextEditorModel } from '@common/models/text-editor.model';

@Injectable()
export class LandingApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(): Observable<any> {
        return this.httpGet('carousel', (x) => new Slide(x));
    }

    public createEmail(email: string): Observable<any> {
        return this.httpPost('mail', (x) => x, [email]);
    }

    public getPosts(): Observable<TextEditorModel[]> {
        return this.httpGet('links', (x) => x);
    }
}
