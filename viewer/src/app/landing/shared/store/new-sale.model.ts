import { ProductModel } from '@common/models/product.model';

export interface NewSaleStateModel {
    products: ProductModel[];
    newProducts: ProductModel[];
    isLoading: boolean;
    selectedCategoryId: number;
}
