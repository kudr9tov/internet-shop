import { Action, State, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';

import { NewSaleStateModel } from './new-sale.model';
import { SetBlockHeaderGroup } from './new-sale.actions';
import { NewSaleService } from '../services/new-sale.service';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProductsSearchModel } from '@common/models/products-search.model';
import { ProductModel } from '@common/models/product.model';

@State<NewSaleStateModel>({
    name: 'newsale',
    defaults: {
        products: [],
        newProducts: [],
        isLoading: false,
        selectedCategoryId: 0,
    },
})
@Injectable()
export class NewSaleState {
    @Selector()
    static newProducts({ newProducts }: NewSaleStateModel): ProductModel[] {
        return newProducts || [];
    }

    @Selector()
    static isLoading({ isLoading }: NewSaleStateModel): boolean {
        return isLoading;
    }

    @Selector()
    static selectedCategoryId({ selectedCategoryId }: NewSaleStateModel): number {
        return selectedCategoryId;
    }

    constructor(private service: NewSaleService) {}

    @Action(SetBlockHeaderGroup)
    oSetBlockHeaderGroup(
        { patchState }: StateContext<NewSaleStateModel>,
        { id }: SetBlockHeaderGroup,
    ): Observable<any> {
        patchState({ isLoading: true });
        const search = { newItems: true } as any;
        if (id) {
            search['category'] = id;
        }
        return this.service.getCotalog(10, 0, search).pipe(
            tap((filterProducts: ProductsSearchModel) => {
                patchState({
                    newProducts: filterProducts.products || [],
                    selectedCategoryId: id,
                });
            }),
            finalize(() => patchState({ isLoading: false })),
        );
    }
}
