import { Selector } from '@ngxs/store';

import { CategoryModel } from '@common/models/category.model';
import { SortHelper } from '@common/helpers/sort.helper';
import { CategoryState } from '@common/auth/store/categories/categories.state';
import { NewSaleState } from './new-sale.state';
import { BlockHeaderGroupModel } from '@common/models/block-header-group.model';

export class NewSaleQuery {
    @Selector([CategoryState.categories, NewSaleState.selectedCategoryId])
    static blockHeaderGroup(
        categories: CategoryModel[],
        selectedCategoryId: number,
    ): BlockHeaderGroupModel[] {
        const blockHeaderGroup = [
            new BlockHeaderGroupModel({
                id: 0,
                name: 'Все',
                current: selectedCategoryId === 0,
            }),
        ];
        if (categories && categories.length) {
            blockHeaderGroup.push(
                ...categories
                    ?.map(
                        (x) =>
                            new BlockHeaderGroupModel({
                                id: x.id,
                                name: x.name,
                                current: x.id === selectedCategoryId,
                            }),
                    )
                    .sort(SortHelper.compareByNamePriorFn),
            );
        }
        return blockHeaderGroup;
    }
}
