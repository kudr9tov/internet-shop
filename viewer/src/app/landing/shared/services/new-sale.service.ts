import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseAuthorizedApiService } from '@common/auth/services/base-authorized-api.service';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ProductsSearchModel } from '@common/models/products-search.model';

@Injectable()
export class NewSaleService extends BaseAuthorizedApiService {
    constructor(http: HttpClient, protected override store: Store) {
        super(http, store);
    }

    public getCotalog(limit: number, skip: number, filter: any = null): Observable<any> {
        const url =
            Object.keys(filter).length !== 0
                ? `products?limit=${limit}&skip=${skip}&filter=${encodeURIComponent(
                      JSON.stringify(filter),
                  )}`
                : `products?limit=${limit}&skip=${skip}`;
        return this.httpGet(url, (x) => new ProductsSearchModel(x));
    }
}
