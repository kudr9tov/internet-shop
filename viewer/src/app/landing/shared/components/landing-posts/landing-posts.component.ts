import {
    AfterViewInit,
    Component,
    ElementRef,
    Inject,
    Input,
    OnInit,
    PLATFORM_ID,
    ViewChild,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { NguCarouselConfig } from '@ngu/carousel';
import { Select, Store } from '@ngxs/store';
import { LandingState } from '../../../store/landing.state';
import { TextEditorModel } from '@common/models/text-editor.model';
import { Observable } from 'rxjs';
import { LoadPosts } from '../../../store/landing.actions';

@Component({
    selector: 'landing-posts',
    templateUrl: './landing-posts.component.html',
    styleUrls: ['./landing-posts.component.scss'],
})
export class LandingPostsComponent implements AfterViewInit, OnInit {
    @Input() header = '';
    @Input() layout: 'list-sm' | 'grid-nl' = 'list-sm';
    @Select(LandingState.posts) posts$!: Observable<TextEditorModel[]>;

    @ViewChild('container', { read: ElementRef }) container!: ElementRef;

    showCarousel = true;

    carouselTile!: NguCarouselConfig;

    carouselOptionsByLayout = {
        'grid-nl': {
            xs: 1,
            sm: 2,
            md: 2,
            lg: 3,
            all: 0,
        },
        'list-sm': {
            xs: 1,
            sm: 2,
            md: 2,
            lg: 2,
            all: 0,
        },
    };

    constructor(@Inject(PLATFORM_ID) private platformId: any, private store: Store) {}

    ngOnInit(): void {
        this.store.dispatch(new LoadPosts());

        this.carouselTile = {
            grid: this.carouselOptionsByLayout[this.layout],
            slide: 2,
            speed: 400,
            interval: { timing: 4000, initialDelay: 1000 },
            animation: 'lazy',
            point: {
                visible: true,
            },
            load: 2,
            velocity: 0.2,
            touch: true,
            loop: true,
            easing: 'ease',
        };
    }

    ngAfterViewInit(): void {
        if (isPlatformBrowser(this.platformId) && this.container) {
            const container = this.container.nativeElement as HTMLElement;
            const containerWidth = container.getBoundingClientRect().width;

            window.addEventListener(
                'load',
                () => {
                    const newContainerWidth = container.getBoundingClientRect().width;

                    if (containerWidth !== newContainerWidth) {
                        this.showCarousel = false;

                        setTimeout(() => (this.showCarousel = true), 0);
                    }
                },
                { passive: true },
            );
        }
    }
}
