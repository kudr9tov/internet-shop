import { Component, ElementRef, Inject, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { filter, Observable, Subscription } from 'rxjs';
import { NguCarouselConfig } from '@ngu/carousel';

import { LoadBrands } from '@common/brands/store/brands.actions';
import { ListModel } from '@common/models/list.model';
import { BrandsQuery } from '@common/brands/store/brands.query';
import { isPlatformBrowser } from '@angular/common';
import { ObserverComponent } from '@common/abstract/observer.component';
@Component({
    selector: 'landing-brands',
    templateUrl: 'landing-brands.component.html',
    styleUrls: ['landing-brands.component.scss'],
})
export class LandingBrandsComponent extends ObserverComponent implements OnInit {
    @Select(BrandsQuery.brands) brands$!: Observable<ListModel[]>;

    @ViewChild('container', { read: ElementRef }) container!: ElementRef;

    brands: ListModel[] = [];

    header = 'Производители';

    lazyBrands!: ListModel[];

    showCarousel = true;

    public carouselTile!: NguCarouselConfig;

    constructor(@Inject(PLATFORM_ID) private platformId: any, private store: Store) {
        super();
    }

    ngOnInit(): void {
        this.store.dispatch(new LoadBrands());
        this.initSubscriptions();
        this.carouselTile = {
            grid: { xs: 3, sm: 4, md: 5, lg: 6, all: 0 },
            slide: 3,
            speed: 400,
            interval: { timing: 4000, initialDelay: 1000 },
            animation: 'lazy',
            point: {
                visible: true,
            },
            load: 2,
            velocity: 0.2,
            touch: true,
            loop: true,
            easing: 'ease',
        };
    }

    ngAfterViewInit(): void {
        if (isPlatformBrowser(this.platformId) && this.container) {
            const container = this.container.nativeElement as HTMLElement;
            const containerWidth = container.getBoundingClientRect().width;

            window.addEventListener(
                'load',
                () => {
                    const newContainerWidth = container.getBoundingClientRect().width;

                    if (containerWidth !== newContainerWidth) {
                        this.showCarousel = false;

                        setTimeout(() => (this.showCarousel = true), 0);
                    }
                },
                { passive: true },
            );
        }
    }

    carouselTileLoad(): void {
        if (this.lazyBrands.length === this.brands.length) {
            return;
        }
        this.lazyBrands.push(...this.brands.slice(7, this.brands.length));
    }

    private initSubscriptions(): void {
        this.subscriptions.push(this.brandsSubscription());
    }

    private brandsSubscription(): Subscription {
        return this.store
            .select(BrandsQuery.brands)
            .pipe(filter((x) => !!x))
            .subscribe((brands) => {
                this.brands = brands;
                this.lazyBrands = brands.slice(0, 7);
                this.showCarousel = !!this.lazyBrands?.length;
            });
    }
}
