import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { filter, Observable, Subscription } from 'rxjs';
import { NguCarouselConfig } from '@ngu/carousel';

import { ObserverComponent } from '@common/abstract/observer.component';

import { Slide } from '../../../../../../../common/src/models/slide.model';
import { LoadSlides } from '../../../store/landing.actions';
import { LandingState } from '../../../store/landing.state';

@Component({
    selector: 'landing-hero',
    templateUrl: 'landing-hero.component.html',
    styleUrls: ['landing-hero.component.scss'],
})
export class LandingHeroComponent extends ObserverComponent implements OnInit {
    @Select(LandingState.slides) slides$!: Observable<Slide[]>;

    public carouselTile!: NguCarouselConfig;

    lazySlides: Slide[] = [];

    slides: Slide[] = [];

    constructor(private store: Store) {
        super();
    }

    ngOnInit(): void {
        this.store.dispatch(new LoadSlides());
        this.initSubscriptions();
        this.carouselTile = {
            grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
            slide: 1,
            speed: 400,
            interval: {
                timing: 4000,
                initialDelay: 1000,
            },
            point: {
                visible: false,
            },
            load: 1,
            loop: true,
            touch: true,
            easing: 'ease',
        };
    }

    carouselTileLoad(): void {
        if (this.lazySlides.length === this.slides.length) {
            return;
        }
        this.lazySlides.push(...this.slides.slice(4, this.slides.length));
    }

    private initSubscriptions(): void {
        this.subscriptions.push(this.certificateSubscription());
    }

    private certificateSubscription(): Subscription {
        return this.store
            .select(LandingState.slides)
            .pipe(filter((x) => !!x))
            .subscribe((slides) => {
                this.slides = slides;
                this.lazySlides = slides.slice(0, 4);
            });
    }
}
