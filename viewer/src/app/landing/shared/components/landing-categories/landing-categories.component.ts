import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { CategoriesQuery } from '@common/auth/store/categories/categories.query';
import { CategoryLinkModel } from '@common/models/category-link.model';

@Component({
    selector: 'landing-categories',
    templateUrl: 'landing-categories.component.html',
    styleUrls: ['landing-categories.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingCategoriesComponent {
    @Select(CategoriesQuery.mainCategories) categories$!: Observable<CategoryLinkModel[]>;

    @Input() layout: 'classic' | 'compact' = 'classic';

    header = 'Основные Категории';
}
