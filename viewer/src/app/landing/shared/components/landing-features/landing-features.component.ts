import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { ProductModel } from '@common/models/product.model';
import { Select, Store } from '@ngxs/store';
import { NewSaleState } from '../../store/new-sale.state';
import { Observable } from 'rxjs';
import { SetBlockHeaderGroup } from '../../store/new-sale.actions';
import { NewSaleQuery } from '../../store/new-sale.query';
import { BlockHeaderGroupModel } from '@common/models/block-header-group.model';

@Component({
    selector: 'landing-features',
    templateUrl: 'landing-features.component.html',
    styleUrls: ['landing-features.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingFeaturesComponent implements OnInit {
    @Select(NewSaleState.newProducts)
    newProducts$!: Observable<ProductModel[]>;

    @Select(NewSaleState.isLoading)
    isLoading$!: Observable<boolean>;

    @Select(NewSaleQuery.blockHeaderGroup) blockHeaderGroup$!: Observable<BlockHeaderGroupModel[]>;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.store.dispatch(new SetBlockHeaderGroup());
    }

    onGroupChange(event: any) {
        this.store.dispatch(new SetBlockHeaderGroup(event.id));
    }
}
