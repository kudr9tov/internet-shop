import { Component, Input } from '@angular/core';
import { environment } from '@environments/environment';

@Component({
    selector: 'main-directions',
    templateUrl: './main-directions.component.html',
    styleUrls: ['./main-directions.component.scss'],
})
export class MainDirectionsComponent {
    @Input() layout: 'classic' | 'boxed' = 'classic';

    primaryColor = environment.primaryColor;

    directions = [
        [
            {
                name: 'Консультации',
                icon: 'support-agent',
                description: 'Подробное консультирование покупателей',
            },
            {
                name: 'Проектирование',
                icon: 'architecture',
                description: 'Дизайн и проектирование помещений',
            },
            {
                name: 'Подбор оборудования',
                icon: 'find-in-page',
                description: 'Ключевой этап проекта',
            },
            {
                name: 'Оптовая и розничная торговля',
                icon: 'trending-up',
                description: 'Высокая скорость проведения сделки.',
            },
        ],
        [
            {
                name: 'Доставка оборудования',
                icon: 'local-shipping',
                description: 'Самые короткие сроки доставки',
            },
            {
                name: 'Подключение и ремонт',
                icon: 'home-repair-service',
                description: 'Подключение и ремонт любой сложности',
            },
            {
                name: 'Сервисное обслуживание',
                icon: 'handyman',
                description: 'Продлить срок службы оборудования',
            },
            {
                name: 'Обучение персонала',
                icon: 'engineering',
                description: 'Обучение и инструктаж персонала',
            },
        ],
    ];
}
