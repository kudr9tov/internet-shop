import { ChangeDetectionStrategy, Component, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { SetSuccess } from '@common/toast';
import { CustomValidators } from '@common/regex/custom.validators';
import { AddSubscription } from './store/landing.actions';
import { SeoSocialShareService } from '../services/seo.service';
import { AppConstants } from '@common/constants/app.const';
import { environment } from '@environments/environment';

@Component({
    selector: 'view-landing',
    templateUrl: 'landing.component.html',
    styleUrls: ['landing.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingComponent {
    email = new FormControl('', [CustomValidators.emailValidator(), Validators.required]);

    constructor(
        private store: Store,
        public content: ElementRef,
        private seoService: SeoSocialShareService,
    ) {
        this.seoService.setData({
            title: AppConstants.tile,
            description: AppConstants.description,
            image: environment.logo,
            type: 'website',
            keywords: AppConstants.keywords,
            imageAuxData: {
                alt: AppConstants.tile,
                height: 968,
                width: 504,
                mimeType: 'image/svg+xml',
                secureUrl: environment.logo,
            },
        });
    }

    onSend(): void {
        this.store.dispatch([
            new AddSubscription(this.email.value as any),
            new SetSuccess('Подписка оформлена!'),
        ]);
        this.email.reset();
    }
}
