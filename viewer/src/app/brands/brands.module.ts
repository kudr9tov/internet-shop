import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrandsDataModule } from '@common/brands/brands-data.module';
import { ImageWrapperModule } from '@common/image-wrapper/image-wrapper.module';
import { PageHeaderModule } from '../shared/page-header/page-header.module';

import { BrandsRoutingModule } from './brands-routing.module';
import { BrandsComponent } from './components/brands/brands.component';

@NgModule({
    declarations: [BrandsComponent],
    imports: [
        CommonModule,
        BrandsRoutingModule,
        BrandsDataModule,
        PageHeaderModule,
        ImageWrapperModule,
    ],
})
export class BrandsModule {}
