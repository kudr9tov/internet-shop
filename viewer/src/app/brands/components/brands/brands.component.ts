import { Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { BrandsQuery } from '@common/brands/store/brands.query';
import { AppConstants } from '@common/constants/app.const';
import { LinkModel } from '@common/models/link.model';
import { ListModel } from '@common/models/list.model';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'brands',
    templateUrl: './brands.component.html',
    styleUrls: ['./brands.component.scss'],
})
export class BrandsComponent {
    @Select(BrandsQuery.brands) brands$!: Observable<ListModel[]>;

    @Select(BrandsQuery.brandsByGroup) brandsByGroup$!: Observable<any[]>;

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: 'Бренды', url: '/brands' }),
    ];

    constructor(private titleService: Title, private metaTagService: Meta) {
        this.titleService.setTitle(AppConstants.brandsTitle);
        this.metaTagService.updateTag({
            name: 'description',
            content: AppConstants.brandsDescription,
        });
    }

    width = '150px';

    height = '150px';
}
