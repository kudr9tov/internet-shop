import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    Input,
    OnChanges,
    OnInit,
    Output,
    PLATFORM_ID,
    SimpleChanges,
    ViewChild,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ProductModel } from '@common/models/product.model';
import { NguCarouselConfig } from '@ngu/carousel';
import { BlockHeaderGroupModel } from '@common/models/block-header-group.model';

@Component({
    selector: 'block-products-carousel',
    templateUrl: './block-products-carousel.component.html',
    styleUrls: ['./block-products-carousel.component.scss'],
})
export class BlockProductsCarouselComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() header = '';
    @Input() layout: 'grid-4' | 'grid-4-sm' | 'grid-5' | 'horizontal' = 'grid-4';
    @Input() rows = 1;
    @Input() products: ProductModel[] | null = [];
    @Input() groups: BlockHeaderGroupModel[] | null = [];
    @Input() withSidebar = false;
    @Input() loading: boolean | null = false;

    @Output() groupChange: EventEmitter<BlockHeaderGroupModel> = new EventEmitter();

    @ViewChild('container', { read: ElementRef }) container!: ElementRef;

    carouselTile!: NguCarouselConfig;

    columns: ProductModel[][] = [];

    showCarousel = true;

    carouselOptionsByLayout: any = {
        'grid-4': {
            xs: 2,
            sm: 3,
            md: 4,
            lg: 4,
            all: 0,
        },
        'grid-4-sm': {
            xs: 1,
            sm: 2,
            md: 3,
            lg: 4,
            all: 0,
        },
        'grid-5': {
            xs: 2,
            sm: 3,
            md: 4,
            lg: 5,
            all: 0,
        },
        horizontal: {
            xs: 1,
            sm: 2,
            md: 3,
            lg: 3,
            all: 0,
        },
    };

    constructor(@Inject(PLATFORM_ID) private platformId: any) {}

    ngOnInit(): void {
        this.carouselTile = {
            grid: this.carouselOptionsByLayout[this.layout],
            slide: 2,
            speed: 400,
            interval: { timing: 4000, initialDelay: 1000 },
            animation: 'lazy',
            point: {
                visible: true,
            },
            load: 2,
            velocity: 0.2,
            touch: true,
            loop: true,
            easing: 'ease',
        };
    }

    ngOnChanges(changes: SimpleChanges): void {
        const properties = Object.keys(changes);

        if (properties.includes('products') || properties.includes('row')) {
            this.columns = [];

            if (this.products && this.rows > 0) {
                const products = this.products.slice();

                while (products.length > 0) {
                    this.columns.push(products.splice(0, this.rows));
                }
            }
        }
    }

    ngAfterViewInit(): void {
        if (isPlatformBrowser(this.platformId) && this.container) {
            const container = this.container.nativeElement as HTMLElement;
            const containerWidth = container.getBoundingClientRect().width;

            window.addEventListener(
                'load',
                () => {
                    const newContainerWidth = container.getBoundingClientRect().width;

                    if (containerWidth !== newContainerWidth) {
                        this.showCarousel = false;

                        setTimeout(() => (this.showCarousel = true), 0);
                    }
                },
                { passive: true },
            );
        }
    }
}
