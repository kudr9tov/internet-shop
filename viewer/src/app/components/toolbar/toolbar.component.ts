import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
} from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Actions, ofActionDispatched, Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { TogglePrimaryMenu } from '@common/auth/store/auth.actions';
import { MatDialog } from '@angular/material/dialog';
import { ResetFilter, SetSearch } from '@common/filter/store/filter.actions';
import { ObserverComponent } from '@common/abstract/observer.component';
import { AuthState } from '@common/auth/store/auth.state';
import { BasketState } from '../../shared/basket-popup/store/basket-popup.state';
import { BasketPopupComponent } from '../../shared/basket-popup/basket-popup.component';
import { environment } from '@environments/environment';

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent extends ObserverComponent implements OnInit {
    @Select(BasketState.cost) cost$: Observable<number>;

    @Select(BasketState.totalCount) totalCount$: Observable<number>;

    @Select(AuthState.isMobile) isMobile$: Observable<boolean>;

    @Input() isMobile = true;

    @Input() searchDebounceTime = 300;

    @Input() showSearch = true;

    @Input() showSearchHint = false;

    @Input() title: string;

    @Input() searchTerm = '';

    @Output() searchChanged = new EventEmitter<string>();

    @Input() mobilePhones: string[];

    @Input() email: string;

    @Output() login = new EventEmitter();

    name: string;

    logo = environment.logo;

    constructor(public store: Store, private actions$: Actions, private dialog: MatDialog) {
        super();
    }

    ngOnInit(): void {
        this.name = environment.name;
        this.initSubscriptions();
    }

    onSearchChange(term: string): void {
        this.showSearchHint = !!term && term.length < 3;
        if (term.length < 3 && this.searchTerm.length < 3) {
            this.searchTerm = term;
            return;
        }
        this.searchTerm = term;
        this.store.dispatch(new SetSearch(this.getSearchTerm()));
    }

    getSearchTerm(): string {
        return this.searchTerm.length < 3 ? '' : this.searchTerm;
    }

    onToggleClick(): void {
        this.store.dispatch(new TogglePrimaryMenu());
    }

    onCall(): void {
        location.href = 'tel:' + this.mobilePhones[0];
    }

    onSend(): void {
        location.href = 'mailto:' + this.email;
    }

    onOpenBasket(): void {
        this.dialog.open(BasketPopupComponent, {
            panelClass: 'huge-dialog',
        });
    }

    onMain(): void {
        this.store.dispatch(new Navigate(['/']));
    }

    private initSubscriptions(): void {
        this.subscriptions.push(
            this.actions$
                .pipe(ofActionDispatched(ResetFilter))
                .subscribe(() => (this.searchTerm = '')),
            //   this.isMobileSubscription(),
        );
    }

    // private isMobileSubscription(): Subscription {
    //     return this.query$.pipe(first()).subscribe((query) => (this.searchTerm = query));
    // }
}
