import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { MatDialogModule } from '@angular/material/dialog';

import { ToolbarComponent } from './toolbar.component';
import { InterestDiscountModule } from '../../interest-discount/interest-discount.module';
import { SearchBarModule } from '../search-bar/search-bar.module';

const components = [ToolbarComponent];
@NgModule({
    declarations: [...components],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatToolbarModule,
        InterestDiscountModule,
        MatTooltipModule,
        SearchBarModule,
        NgxsRouterPluginModule.forRoot(),
    ],
    exports: [...components],
})
export class ToolbarModule {}
