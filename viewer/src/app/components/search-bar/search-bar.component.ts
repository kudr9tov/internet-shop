import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ElementRef,
    ViewChild,
} from '@angular/core';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ProductModel } from '@common/models/product.model';
import { Navigate } from '@ngxs/router-plugin';
import { SearchInCatalog } from '@common/filter/store/filter.actions';
import { FilterState } from '@common/filter/store/filter.state';

@Component({
    selector: 'search-bar',
    templateUrl: 'search-bar.component.html',
    styleUrls: ['search-bar.component.scss'],
    animations: [
        trigger('slideInOut', [
            state('true', style({ width: '*' })),
            state('false', style({ width: '0' })),
            transition('true => false', animate('300ms ease-in')),
            transition('false => true', animate('300ms ease-out')),
        ]),
    ],
})
export class SearchBarComponent implements OnInit {
    @ViewChild('input') inputElement!: ElementRef;

    @Select(FilterState.searchOptions) searchOptions$!: Observable<ProductModel[]>;

    searchDebounceTime = 300;

    @Input() showSearchHint = false;

    @Input() isMobile!: boolean;

    @Input()
    set title(title: string) {
        this.placeholder = `${title || 'Поиск по Каталогу...'}`;
    }

    @Input()
    set searchTerm(searchTerm: string) {
        this.searchControl.setValue(searchTerm);
        this.lastSearch = searchTerm;
    }

    @Output() searchChange = new EventEmitter<string>();

    @Output() enter = new EventEmitter();

    @Output() closed = new EventEmitter<void>();

    @Output() opened = new EventEmitter<void>();

    placeholder!: string;

    searchControl = new FormControl('');

    lastSearch = '';

    searchVisible = false;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.searchControl.valueChanges
            .pipe(
                debounceTime(this.searchDebounceTime),
                distinctUntilChanged(),
                filter((x) => x !== this.lastSearch),
            )
            .subscribe((search: any) => {
                this.lastSearch = search;
                this.searchChange.emit(search);
            });
    }

    onCancel(): void {
        this.searchControl.reset('');
        this.searchVisible = false;
        this.closed.emit();
    }

    onOpen(): void {
        this.opened.emit();
        this.searchVisible = true;
        this.inputElement.nativeElement.focus();
    }

    onOpenCatalog(): void {
        this.store.dispatch(new SearchInCatalog());
    }

    onOpenProduct(id: number): void {
        if (!id) {
            return;
        }
        this.store.dispatch(new Navigate(['catalog', 'product', id]));
    }
}
