import { Component, Input } from '@angular/core';
import { CommentModel } from '@common/models/comment.model';

@Component({
    selector: 'comments-list',
    templateUrl: './comments-list.component.html',
    styleUrls: ['./comments-list.component.scss'],
})
export class CommentsListComponent {
    @Input() comments: CommentModel[] = [];
    @Input() level = 0;
}
