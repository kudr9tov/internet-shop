import {
    Component,
    ElementRef,
    EventEmitter,
    HostBinding,
    Inject,
    Input,
    NgZone,
    OnInit,
    Output,
    ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged, first, takeUntil } from 'rxjs/operators';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { FilterQuery } from '@common/filter/store/filter.query';
import { Actions, ofActionDispatched, Select, Store } from '@ngxs/store';
import { ProductModel } from '@common/models/product.model';
import { ResetFilter, SearchInCatalog, SetSearch } from '@common/filter/store/filter.actions';
import { ObserverComponent } from '@common/abstract/observer.component';
import { RootService } from '../../services/root.service';
import { MatDialog } from '@angular/material/dialog';
import { RequestPriceDialogComponent } from '@common/request-price-dialog/request-price-dialog.component';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { RequestPriceModel } from '@common/models/price-request.model';
import { SetError, SetSuccess } from '@common/toast';
import { BasketState } from '../../shared/basket-popup/store/basket-popup.state';
import { AddToBasket } from '../../shared/basket-popup/store/basket-popup.actions';
import { FilterState } from '@common/filter/store/filter.state';

export type SearchLocation = 'header' | 'indicator' | 'mobile-header';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    exportAs: 'search',
})
export class SearchComponent extends ObserverComponent implements OnInit {
    @Select(BasketState.orders) orders$!: Observable<ProductModel[]>;

    form!: FormGroup;

    hasSuggestions = false;

    searchDebounceTime = 300;

    @Select(FilterState.searchOptions) searchOptions$!: Observable<ProductModel[]>;

    @Select(FilterQuery.query) query$!: Observable<string>;

    @Input() location: SearchLocation = 'header';

    @Output() escape: EventEmitter<void> = new EventEmitter<void>();

    @Output() closeButtonClick: EventEmitter<void> = new EventEmitter<void>();

    @HostBinding('class.search') classSearch = true;

    @HostBinding('class.search--location--header')
    get classSearchLocationHeader(): boolean {
        return this.location === 'header';
    }

    @HostBinding('class.search--location--indicator')
    get classSearchLocationIndicator(): boolean {
        return this.location === 'indicator';
    }

    @HostBinding('class.search--location--mobile-header')
    get classSearchLocationMobileHeader(): boolean {
        return this.location === 'mobile-header';
    }

    @HostBinding('class.search--has-suggestions')
    get classSearchHasSuggestions(): boolean {
        return this.hasSuggestions;
    }

    @HostBinding('class.search--suggestions-open') classSearchSuggestionsOpen = false;

    @ViewChild('input') inputElementRef!: ElementRef;

    get element(): HTMLElement {
        return this.elementRef.nativeElement;
    }

    get inputElement(): HTMLElement {
        return this.inputElementRef.nativeElement;
    }

    constructor(
        @Inject(DOCUMENT) private document: Document,
        private dialog: MatDialog,
        private store: Store,
        private elementRef: ElementRef,
        private zone: NgZone,
        private service: PriceRequestApiService,
        public root: RootService,
        private actions$: Actions,
        public fb: FormBuilder,
    ) {
        super();
    }

    ngOnInit(): void {
        this.initFormGroup();
        this.initSubscriptions();
        this.form
            .get('query')
            ?.valueChanges.pipe(debounceTime(this.searchDebounceTime), distinctUntilChanged())
            .subscribe((search) => {
                this.store.dispatch(new SetSearch(search));
            });
        // this.form
        //     .get('query')
        //     ?.valueChanges.pipe(
        //         throttleTime(250, asyncScheduler, {
        //             leading: true,
        //             trailing: true,
        //         }),
        //         map((query) => query.trim()),
        //         switchMap((query) => {
        //             // const categorySlug =
        //             //     this.form.value.category !== 'all' ? this.form.value.category : null;
        //          //   console.log(categorySlug);
        //             return this.store.dispatch(new SetSearch(query));
        //         }),
        //         takeUntil(this.destroy$),
        //     )
        //     .subscribe((state) => {
        //         this.hasSuggestions = state?.filter?.searchOptions?.length > 0;

        //         if (this.hasSuggestions) {
        //             this.suggestedProducts = state.filter.searchOptions;
        //         }
        //     });

        this.zone.runOutsideAngular(() => {
            fromEvent(this.document, 'click')
                .pipe(takeUntil(this.destroyStream$))
                .subscribe((event) => {
                    const activeElement = this.document.activeElement;

                    // If the inner element still has focus, ignore the click.
                    if (activeElement && activeElement.closest('.search') === this.element) {
                        return;
                    }

                    // Close suggestion if click performed outside of component.
                    if (
                        event.target instanceof HTMLElement &&
                        this.element !== event.target.closest('.search')
                    ) {
                        this.zone.run(() => this.closeSuggestion());
                    }
                });

            fromEvent(this.element, 'focusout')
                .pipe(debounceTime(10), takeUntil(this.destroyStream$))
                .subscribe(() => {
                    if (this.document.activeElement === this.document.body) {
                        return;
                    }

                    // Close suggestions if the focus received an external element.
                    if (
                        this.document.activeElement &&
                        this.document.activeElement.closest('.search') !== this.element
                    ) {
                        this.zone.run(() => this.closeSuggestion());
                    }
                });
        });
    }

    initFormGroup() {
        this.form = this.fb.group({
            category: ['all'],
            query: [''],
        });
    }

    openSuggestion(): void {
        this.classSearchSuggestionsOpen = true;
    }

    closeSuggestion(): void {
        this.classSearchSuggestionsOpen = false;
    }

    getCategoryName(category: any): string {
        return '&nbsp;'.repeat(category.depth * 4) + category.name;
    }

    addToCart(product: ProductModel): void {
        if (!product.price) {
            this.onAddRequest(product.id);
            return;
        }
        this.store.dispatch(new AddToBasket(product));
    }

    onAddRequest(id: number): void {
        this.dialog
            .open(RequestPriceDialogComponent, {
                panelClass: 'request-price',
                data: { productId: id },
            })
            .afterClosed()
            .subscribe((result: any) => {
                if (!result) {
                    return;
                }
                this.service.create(new RequestPriceModel({ ...result })).subscribe({
                    complete: () => {
                        this.store.dispatch(new SetSuccess('Запрос отправлен'));
                    },
                    error: () => this.store.dispatch(new SetError('Произошла ошибка')),
                });
            });
    }

    onCloseSearch(): void {
        this.form.get('query')?.setValue('');
        this.closeButtonClick.next();
    }

    onOpenCatalog(): void {
        this.store.dispatch(new SearchInCatalog());
        this.closeSuggestion();
    }

    // addToCart(product: Product): void {
    //     if (this.addedToCartProducts.includes(product)) {
    //         return;
    //     }

    //     this.addedToCartProducts.push(product);
    //     this.cart.add(product, 1).subscribe({
    //         complete: () => {
    //             this.addedToCartProducts = this.addedToCartProducts.filter(eachProduct => eachProduct !== product);
    //         }
    //     });
    // }

    // private getCategoriesWithDepth(categories: Category[], depth = 0): CategoryWithDepth[] {
    //     return categories.reduce<CategoryWithDepth[]>((acc, category) => [
    //         ...acc,
    //         {...category, depth},
    //         ...this.getCategoriesWithDepth(category.children || [], depth + 1),
    //     ], []);
    // }

    private initSubscriptions(): void {
        this.searchOptionsSubscription();
        this.querySubscription();
        this.actions$
            .pipe(ofActionDispatched(ResetFilter), takeUntil(this.destroyStream$))
            .subscribe(() => this.form.get('query')?.setValue(''));
    }

    private querySubscription(): Subscription {
        return this.store
            .select(FilterQuery.query)
            .pipe(first())
            .subscribe((query) => this.form.get('query')?.setValue(query));
    }

    private searchOptionsSubscription(): Subscription {
        return this.store
            .select(FilterState.searchOptions)
            .pipe(takeUntil(this.destroyStream$))
            .subscribe((searchOptions) => (this.hasSuggestions = searchOptions?.length > 0));
    }
}
