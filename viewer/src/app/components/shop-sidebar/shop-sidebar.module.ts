import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ShopSidebarComponent } from './shop-sidebar.component';
import { ShopSidebarService } from '@common/services/shop-sidebar.service';
import { AppIconModule } from '@common/app-icon/app-icon.module';

@NgModule({
    declarations: [ShopSidebarComponent],
    imports: [CommonModule, AppIconModule],
    exports: [ShopSidebarComponent],
    providers: [ShopSidebarService],
})
export class ShopSidebarModule {}
