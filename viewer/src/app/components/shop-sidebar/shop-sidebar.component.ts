import { Component, Inject, Input, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import { ShopSidebarService } from '@common/services/shop-sidebar.service';
import { fromMatchMedia } from '@common/functions/rxjs/fromMatchMedia';

@Component({
    selector: 'shop-sidebar',
    templateUrl: './shop-sidebar.component.html',
    styleUrls: ['./shop-sidebar.component.scss'],
})
export class ShopSidebarComponent implements OnInit, OnDestroy {
    @Input() offcanvas: 'always' | 'mobile' = 'mobile';

    @Input() title = '';

    private destroy$: Subject<any> = new Subject();
    isOpen = false;

    constructor(
        public sidebar: ShopSidebarService,
        @Inject(PLATFORM_ID)
        private platformId: any,
    ) {}

    ngOnInit(): void {
        this.sidebar.isOpen$.pipe(takeUntil(this.destroy$)).subscribe((isOpen) => {
            if (isOpen) {
                this.open();
            } else {
                this.close();
            }
        });

        if (isPlatformBrowser(this.platformId)) {
            fromMatchMedia('(max-width: 991px)')
                .pipe(takeUntil(this.destroy$))
                .subscribe((media) => {
                    if (this.offcanvas === 'mobile' && this.isOpen && !media.matches) {
                        this.close();
                    }
                });
        }
    }

    ngOnDestroy(): void {
        this.close();
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    private open(): void {
        if (isPlatformBrowser(this.platformId)) {
            const bodyWidth = document.body.offsetWidth;

            document.body.style.overflow = 'hidden';
            document.body.style.paddingRight = document.body.offsetWidth - bodyWidth + 'px';
        }

        this.isOpen = true;
    }

    private close(): void {
        if (isPlatformBrowser(this.platformId)) {
            document.body.style.overflow = '';
            document.body.style.paddingRight = '';
        }

        this.isOpen = false;
    }
}
