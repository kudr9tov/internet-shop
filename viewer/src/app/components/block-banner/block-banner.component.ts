import { Component, Input } from '@angular/core';
import { environment } from '@environments/environment';

@Component({
    selector: 'block-banner',
    templateUrl: './block-banner.component.html',
    styleUrls: ['./block-banner.component.scss'],
})
export class BlockBannerComponent {
    @Input() title = 'Оборудование';
    @Input() text =
        'Торговое оборудование, Холодильное оборудование, Оборудование для предприятий питания, Металическая мебель';

    imageDekstop = `${environment.apiPath}/images/6BC0y_products-gktorg.jpg`;
    imageMobile = `${environment.apiPath}/images/LQyHb_products-gktorg.ru-mobile.jpg`;
}
