import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

interface RouterData {
    headerLayout?: 'classic' | 'compact';
    dropcartType?: DropcartType;
}

export type DropcartType = 'dropdown' | 'offcanvas';

@Component({
    selector: 'app-main',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RootComponent {
    headerLayout: 'classic' | 'compact' = 'classic';
    dropcartType: DropcartType = 'dropdown';

    constructor(public route: ActivatedRoute) {
        this.route.data.subscribe((data: RouterData) => {
            this.headerLayout = data.headerLayout || 'classic';
            this.dropcartType = data.dropcartType || 'dropdown';
        });
    }
}
