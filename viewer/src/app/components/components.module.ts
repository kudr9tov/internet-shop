import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { PipesModule } from '@common/pipes/pipes.module';
import { NguCarouselModule } from '@ngu/carousel';
import { ProductCardModule } from '../product-card/product-card.module';
import { BlockBannerComponent } from './block-banner/block-banner.component';
import { BlockHeaderComponent } from './block-header/block-header.component';
import { BlockProductsCarouselComponent } from './block-products-carousel/block-products-carousel.component';
import { CommentsListComponent } from './comments-list/comments-list.component';

import { SearchComponent } from './search/search.component';
import { FilterDataModule } from '@common/filter/filter-data.module';

const items = [
    SearchComponent,
    BlockProductsCarouselComponent,
    BlockHeaderComponent,
    BlockBannerComponent,
    CommentsListComponent,
];

@NgModule({
    declarations: [...items],
    imports: [
        CommonModule,
        FormsModule,
        AppIconModule,
        RouterModule,
        NguCarouselModule,
        ProductCardModule,
        ReactiveFormsModule,
        PipesModule,
        FilterDataModule,
    ],
    exports: [...items],
})
export class ComponentsModule {}
