import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BlockHeaderGroupModel } from '@common/models/block-header-group.model';
import { NguCarousel } from '@ngu/carousel';

@Component({
    selector: 'block-header',
    templateUrl: './block-header.component.html',
    styleUrls: ['./block-header.component.scss'],
})
export class BlockHeaderComponent {
    @Input() header = '';
    @Input() arrows = false;
    @Input() groups: BlockHeaderGroupModel[] | null = [];
    @Input() carousel!: NguCarousel<any>;

    @Output() groupChange: EventEmitter<BlockHeaderGroupModel> = new EventEmitter();

    constructor() {}

    setGroup(group: BlockHeaderGroupModel): void {
        (this.groups || []).forEach((g) => (g.current = g === group));
        this.groupChange.emit(group);
    }

    onPrev(): void {
        if (!this.carousel) {
            return;
        }
        const point =
            this.carousel.activePoint > 0
                ? this.carousel.activePoint - 1
                : this.carousel.slideItems - 1;
        this.carousel.moveTo(point);
    }

    onNext(): void {
        if (!this.carousel) {
            return;
        }
        const point =
            this.carousel.slideItems === this.carousel.currentSlide
                ? 0
                : this.carousel.activePoint + 1;
        this.carousel.moveTo(point);
    }
}
