import { Component } from '@angular/core';
import { Store } from '@ngxs/store';

import { AppConstants } from '@common/constants/app.const';
import { AuthState } from '@common/auth/store/auth.state';
import { FormControl, Validators } from '@angular/forms';
import { CustomValidators } from '@common/regex/custom.validators';
import { AddSubscription } from '../landing/store/landing.actions';
import { SetSuccess } from '@common/toast';
import { ObserverComponent } from '@common/abstract/observer.component';
import { environment } from '@environments/environment';

@Component({
    selector: 'landing-footer',
    templateUrl: 'landing-footer.component.html',
    styleUrls: ['landing-footer.component.scss'],
})
export class LandingFooterComponent extends ObserverComponent {
    name!: string;

    phone!: string;

    yandexRatingId = environment.yandexRatingId;

    messengerPhone = environment.messengerPhone;

    logo = `${environment.logo}#logo`;

    email = new FormControl('', [CustomValidators.emailValidator(), Validators.required]);

    links = [
        { name: 'Сервис', url: 'service' },
        { name: 'Каталоги', url: 'magazines' },
        { name: 'Контакты', url: 'contacts' },
        { name: 'Согласие на обработку персональных данных', url: 'privacy' },
    ];

    infoes: any = [];

    messengers = [
        { icon: 'fa-viber', link: `viber://chat?number=%2B${environment.phoneNumber}` },
        { icon: 'fa-whatsapp', link: `https://wa.me/${environment.phoneNumber}` },
        { icon: 'fa-telegram', link: `https://telegram.me/${environment.telegram}` },
        //  { icon: 'fa-instagram', link: 'https://www.instagram.com/glav_com/' },
    ];

    constructor(public store: Store) {
        super();
    }

    ngOnInit(): void {
        this.setupInfoes();
        this.name = environment.name;
    }

    onSend(): void {
        this.store.dispatch([
            new AddSubscription(this.email.value as any),
            new SetSuccess('Подписка оформлена!'),
        ]);
        this.email.reset();
    }

    private setupInfoes(): void {
        const siteEmail = this.store.selectSnapshot(AuthState.email);
        const phone = environment.phone;
        this.infoes = [
            {
                icon: 'fa-map-marker',
                text: AppConstants.location,
                href: `${environment.apiPath}/contacts`,
            },
            { icon: 'fa-phone', text: phone, href: 'tel:' + phone },
            { icon: 'fa-envelope', text: siteEmail, href: 'mailto:' + siteEmail },
        ] as any;
    }
}
