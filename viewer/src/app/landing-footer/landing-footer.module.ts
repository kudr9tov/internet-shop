import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguCarouselModule } from '@ngu/carousel';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';

import { ImageWrapperModule } from '@common/image-wrapper/image-wrapper.module';

import { LandingFooterComponent } from './landing-footer.component';
import { TotopComponent } from './totop/totop.component';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { RouterModule } from '@angular/router';
import { PipesModule } from '@common/pipes/pipes.module';

const components = [TotopComponent, LandingFooterComponent] as any;
@NgModule({
    declarations: [...components],
    imports: [
        CommonModule,
        MatButtonModule,
        ReactiveFormsModule,
        NguCarouselModule,
        RouterModule,
        PipesModule,
        ImageWrapperModule,
        AppIconModule,
    ],
    exports: [...components],
})
export class LandingFooterModule {}
