import { Injectable } from '@angular/core';
import { ProductModel } from '@common/models/product.model';

@Injectable({
    providedIn: 'root',
})
export class RootService {
    constructor() {}

    // home(): string {
    //     return '/';
    // }

    // shop(): string {
    //     return `/shop/catalog`;
    // }

    // category(category: Partial<Category>): string {
    //     if (category.type === 'shop') {
    //         const basePath = this.shop();

    //         if ('slug' in category) {
    //             return `${basePath}/${category.slug}`;
    //         }
    //         if ('id' in category) {
    //             return `${basePath}/${category.id}`;
    //         }

    //         throw Error('Provide category with "path", "slug" or "id".');
    //     }
    //     if (category.type === 'blog') {
    //         return this.blog();
    //     }

    //     throw Error('Provided category with unknown type.');
    // }

    product(product: Partial<ProductModel>): string {
        const basePath = '/catalog/product';

        return `${basePath}/${product.id}`;
    }

    // // noinspection JSUnusedLocalSymbols
    // brand(brand: Partial<Brand>): string {
    //     return '/';
    // }

    // cart(): string {
    //     return '/shop/cart';
    // }

    // checkout(): string {
    //     return '/shop/cart/checkout';
    // }

    // wishlist(): string {
    //     return '/shop/wishlist';
    // }

    // blog(): string {
    //     return '/blog';
    // }

    // post(): string {
    //     return `/blog/post-classic`;
    // }

    // login(): string {
    //     return '/account/login';
    // }

    // terms(): string {
    //     return '/site/terms';
    // }

    // notFound(): string {
    //     return `/site/not-found`;
    // }
}
