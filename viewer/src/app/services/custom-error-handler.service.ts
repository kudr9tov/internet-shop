import { Optional, Injectable, ErrorHandler, Inject } from '@angular/core';

import { Store } from '@ngxs/store';

@Injectable()
export class CustomErrorHandlerService extends ErrorHandler {
    constructor(
        @Optional() @Inject('ERROR_WRAPPER') private errorWrapper: any,
        protected store: Store,
    ) {
        super();
    }

    handleError(error: Error) {
        console.error('Custom Error Handler error: ' + error.toString());
        if (this.errorWrapper) {
            //serverSide
            this.errorWrapper.error = error;
        }
    }
}
