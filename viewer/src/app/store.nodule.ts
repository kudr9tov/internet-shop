import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { environment } from '@environments/environment';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsModule } from '@ngxs/store';

@NgModule({
    imports: [
        CommonModule,
        environment.plugins,
        NgxsRouterPluginModule.forRoot(),
        NgxsModule.forRoot([]),
    ],
    exports: [environment.plugins, NgxsModule],
})
export class NgxsStoreModule {}
