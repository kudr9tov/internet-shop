import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login/login.component';
import { PageHeaderModule } from '../shared/page-header/page-header.module';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, PageHeaderModule, LoginRoutingModule],
    declarations: [LoginComponent],
})
export class LoginModule {}
