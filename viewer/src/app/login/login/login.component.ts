import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { FormBuilder, FormGroup } from '@angular/forms';

import {
    Login,
    LoadCurrentUser,
    CheckCurrentUser,
} from '@common/auth/store/current-user/current-user.actions';
import { LoginModel } from '@common/models/login.model';
import { Meta, Title } from '@angular/platform-browser';
import { AppConstants } from '@common/constants/app.const';
import { LinkModel } from '@common/models/link.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    userForm!: FormGroup;

    header = 'Авторизация';

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: this.header, url: '/login' }),
    ];

    constructor(
        public store: Store,
        public fb: FormBuilder,
        private metaTagService: Meta,
        private titleService: Title,
    ) {
        this.titleService.setTitle(AppConstants.loginTitle);
        this.metaTagService.updateTag({
            name: 'description',
            content: AppConstants.loginDescription,
        });
    }

    ngOnInit(): void {
        this.store.dispatch(new CheckCurrentUser());
        this.userForm = this.fb.group({
            email: [null],
            password: [null],
        });
    }
    onLogin(): void {
        localStorage.setItem('isLoggedin', 'true');
        this.store.dispatch(new Login(new LoginModel({ ...this.userForm.value })));
    }

    onSign(): void {
        this.store.dispatch(new LoadCurrentUser());
    }
}
