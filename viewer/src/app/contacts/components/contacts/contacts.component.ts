import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { AuthState } from '@common/auth/store/auth.state';
import { AppConstants } from '@common/constants/app.const';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { LinkModel } from '@common/models/link.model';
import { environment } from '@environments/environment';

@Component({
    selector: 'contacts',
    templateUrl: './contacts.component.html',
    styleUrls: ['./contacts.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactsComponent implements OnInit {
    @Select(AuthState.mobilePhones) mobilePhones$!: Observable<string[]>;

    @Select(AuthState.email) email$!: Observable<string>;

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: 'Контакты', url: '/contacts' }),
    ];

    formGroup!: FormGroup;

    location: number[] = [];

    yandexRatingId = environment.yandexRatingId;

    adress = environment.location?.split(',');

    contacts = [
        {
            title: `РЕЖИМ РАБОТЫ <span itemprop="name">${environment.name}</span>`,
            descriptions: ['Пн-Пт: <b>09:00 - 18:00</b> <br> Сб-Вс: <b>Выходной</b> '],
        },
        {
            icon: 'fa-home',
            title: 'Адрес',
            descriptions: this.adress
                ? [
                      `<span itemprop="postalCode">${this.adress[0]}</span>, `,
                      `<span itemprop="addressLocality">${this.adress[1]}</span>, `,
                      `<span itemprop="streetAddress">${this.adress[2]},${this.adress[3]}</span>`,
                  ]
                : [],
            itemprop: '',
        },
        {
            icon: 'fa-warehouse',
            title: 'Склад',
            descriptions: [environment.storage],
        },
        {
            icon: 'fa-phone',
            title: 'Телефон',
            descriptions: [`<span itemprop="telephone">${environment.phone}</span>`],
        },
        {
            icon: 'fa-fax',
            title: 'Факс',
            descriptions: [`<span itemprop="faxNumber">${environment.phone}</span>`],
        },
        {
            icon: 'fa-envelope',
            title: 'E-mail',
            descriptions: [`<span itemprop="email">info@${environment.domen}</span>`],
        },
    ];

    constructor(public fb: FormBuilder, private metaTagService: Meta, private titleService: Title) {
        this.titleService.setTitle(AppConstants.contactsTitle);
        this.metaTagService.updateTag({
            name: 'description',
            content: AppConstants.contactsDescription,
        });
    }

    ngOnInit(): void {
        this.setupFormGroups();
        this.location = [55.802786, 37.757979];
    }

    onMouse(event: any): void {
        if (event.type === 'mouseenter') {
            event.instance.options.set('preset', 'islands#greenIcon');
        }

        if (event.type === 'mouseleave') {
            event.instance.options.unset('preset');
        }
    }

    setupFormGroups(): void {
        this.formGroup = this.fb.group({
            name: [null, [Validators.required]],
            email: [null, [Validators.required]],
            subject: [null, [Validators.required]],
            content: [null, [Validators.required]],
        });
    }

    onSend(): void {
        console.log('send');
    }
}
