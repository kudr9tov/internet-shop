import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { PipesModule } from '@common/pipes/pipes.module';
import { PageHeaderModule } from '../shared/page-header/page-header.module';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ContactsRoutingModule } from './contacts-routing.module';

@NgModule({
    declarations: [ContactsComponent],
    imports: [
        CommonModule,
        ContactsRoutingModule,
        MatCardModule,
        PipesModule,
        PageHeaderModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
    ],
})
export class ContactsModule {}
