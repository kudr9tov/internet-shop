import { NgModule } from '@angular/core';

// modules (angular)
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// components
import { DepartmentsComponent } from './components/departments/departments.component';
import { HeaderComponent } from './header.component';
import { IndicatorComponent } from './components/indicator/indicator.component';
import { LinksComponent } from './components/links/links.component';
import { MegamenuComponent } from './components/megamenu/megamenu.component';
import { MenuComponent } from './components/menu/menu.component';
import { NavComponent } from './components/nav/nav.component';
import { DirectiveModule } from '@common/directives/directive.modul';
import { ComponentsModule } from '../components/components.module';
import { RequestPriceDialogModule } from '@common/request-price-dialog/request-price-dialog.module';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { MatDialogModule } from '@angular/material/dialog';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { PipesModule } from '@common/pipes/pipes.module';

@NgModule({
    declarations: [
        // components
        DepartmentsComponent,
        HeaderComponent,
        IndicatorComponent,
        LinksComponent,
        MegamenuComponent,
        MenuComponent,
        NavComponent,
    ],
    imports: [
        // modules (angular)
        CommonModule,
        RouterModule,
        ComponentsModule,
        AppIconModule,
        DirectiveModule,
        RequestPriceDialogModule,
        MatDialogModule,
        PipesModule,
    ],
    exports: [
        // components
        HeaderComponent,
    ],
    providers: [PriceRequestApiService],
})
export class HeaderModule {}
