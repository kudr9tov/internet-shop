import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AuthState } from '@common/auth/store/auth.state';
import { environment } from '@environments/environment';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
    @Input() layout: 'classic' | 'compact' = 'classic';

    @Select(AuthState.mobilePhones) mobilePhones$!: Observable<string[]>;

    logo = `${environment.logo}#logo`;

    name = environment.name;
}
