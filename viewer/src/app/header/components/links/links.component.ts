import {
    AfterViewChecked,
    Component,
    ElementRef,
    NgZone,
    OnDestroy,
    OnInit,
    QueryList,
    ViewChildren,
} from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HeaderService } from '@common/services/header.service';
import { LinkModel } from '@common/models/link.model';

export interface NavigationLink extends LinkModel {
    menu?: Menu | Megamenu;
}

export interface Menu {
    type: 'menu';
    items: NestedLink[];
}

export interface Megamenu {
    type: 'megamenu';
    size: 'xl' | 'lg' | 'nl' | 'sm';
    image?: string;
    columns: MegamenuColumn[];
}

export interface MegamenuColumn {
    size: number;
    items: NestedLink[];
}

export interface NestedLink extends LinkModel {
    items?: NestedLink[];
}

@Component({
    selector: 'app-header-links',
    templateUrl: './links.component.html',
    styleUrls: ['./links.component.scss'],
})
export class LinksComponent implements OnInit, OnDestroy, AfterViewChecked {
    @ViewChildren('submenuElement') submenuElements!: QueryList<ElementRef>;
    @ViewChildren('itemElement') itemElements!: QueryList<ElementRef>;

    private destroy$: Subject<any> = new Subject();

    items: NavigationLink[] = [
        { label: 'Оборудование', url: '/catalog' },
        { label: 'О компании', url: '/about' },
        { label: 'Каталоги', url: '/magazines' },
        { label: 'Бренды', url: '/brands' },
        { label: 'Сервис', url: '/service' },
        { label: 'Контакты', url: '/contacts' },
        { label: 'Новости', url: '/news' },
    ];

    hoveredItem: NavigationLink | null = null;

    reCalcSubmenuPosition = false;

    constructor(private header: HeaderService, private zone: NgZone) {}

    onItemMouseEnter(item: NavigationLink): void {
        if (this.hoveredItem !== item) {
            this.hoveredItem = item;

            if (item.menu) {
                this.reCalcSubmenuPosition = true;
            }
        }
    }

    onItemMouseLeave(item: NavigationLink): void {
        if (this.hoveredItem === item) {
            this.hoveredItem = null;
        }
    }

    onTouchClick(event: Event, item: NavigationLink): void {
        if (event.cancelable) {
            if (this.hoveredItem && this.hoveredItem === item) {
                return;
            }

            if (item.menu) {
                event.preventDefault();

                this.hoveredItem = item;
                this.reCalcSubmenuPosition = true;
            }
        }
    }

    onOutsideTouchClick(item: NavigationLink): void {
        if (this.hoveredItem === item) {
            this.zone.run(() => (this.hoveredItem = null));
        }
    }

    onSubItemClick(): void {
        this.hoveredItem = null;
    }

    ngOnInit(): void {
        merge(this.header.navPanelPositionState$, this.header.navPanelVisibility$)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => (this.hoveredItem = null));
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    ngAfterViewChecked(): void {
        if (!this.reCalcSubmenuPosition) {
            return;
        }

        this.reCalcSubmenuPosition = false;

        const itemElement = this.getCurrentItemElement();
        const submenuElement = this.getCurrentSubmenuElement();

        if (!submenuElement || !itemElement) {
            return;
        }

        const submenuTop = submenuElement.getBoundingClientRect().top;
        const viewportHeight = window.innerHeight;
        const paddingBottom = 20;

        submenuElement.style.maxHeight = `${viewportHeight - submenuTop - paddingBottom}px`;

        // calc megamenu position
        if (this.hoveredItem?.menu?.type !== 'megamenu') {
            return;
        }

        const container = submenuElement.offsetParent;

        if (!container) {
            return;
        }

        const containerWidth = container.getBoundingClientRect().width;
        const megamenuWidth = submenuElement.getBoundingClientRect().width;

        const itemPosition = itemElement.offsetLeft;
        const megamenuPosition = Math.round(Math.min(itemPosition, containerWidth - megamenuWidth));

        submenuElement.style.left = megamenuPosition + 'px';
    }

    getCurrentItemElement(): HTMLDivElement | null {
        if (!this.hoveredItem) {
            return null;
        }

        const index = this.items?.indexOf(this.hoveredItem);
        const elements = this.itemElements.toArray();

        if (index === -1 || !elements[index]) {
            return null;
        }

        return elements[index].nativeElement as HTMLDivElement;
    }

    getCurrentSubmenuElement(): HTMLDivElement | null {
        if (!this.hoveredItem) {
            return null;
        }

        const index = this.items?.filter((x) => x.menu)?.indexOf(this.hoveredItem);
        const elements = this.submenuElements.toArray();

        if (index === -1 || !elements[index]) {
            return null;
        }

        return elements[index].nativeElement as HTMLDivElement;
    }
}
