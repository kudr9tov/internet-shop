import {
    AfterViewChecked,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    Output,
    QueryList,
    ViewChild,
    ViewChildren,
} from '@angular/core';
import { CategoryModel } from '@common/models/category.model';

@Component({
    selector: 'app-header-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent implements AfterViewChecked {
    @Input() layout: 'classic' | 'topbar' = 'classic';
    @Input() items: CategoryModel[] = [];
    @Input() firstParentId!: number;
    @Input() categories: CategoryModel[] = [];

    @Output() itemClick: EventEmitter<CategoryModel> = new EventEmitter<CategoryModel>();

    @ViewChild('menuElement') elementRef!: ElementRef;
    @ViewChildren('submenuElement') submenuElements!: QueryList<ElementRef>;
    @ViewChildren('itemElement') itemElements!: QueryList<ElementRef>;

    hoveredItem: CategoryModel | null = null;
    reCalcSubmenuPosition = false;

    get element(): HTMLDivElement {
        return this.elementRef.nativeElement;
    }

    onItemMouseEnter(item: CategoryModel): void {
        if (this.hoveredItem !== item) {
            this.hoveredItem = item;

            if (item.subCategory) {
                this.reCalcSubmenuPosition = true;
            }
        }
    }

    onMouseLeave(): void {
        this.hoveredItem = null;
    }

    onTouchClick(event: Event, item: CategoryModel): void {
        if (event.cancelable) {
            if (this.hoveredItem && this.hoveredItem === item) {
                return;
            }

            if (item.subCategory) {
                event.preventDefault();

                this.hoveredItem = item;
                this.reCalcSubmenuPosition = true;
            }
        }
    }

    onSubItemClick(item: CategoryModel): void {
        this.hoveredItem = null;
        this.itemClick.emit(item);
    }

    ngAfterViewChecked(): void {
        if (!this.reCalcSubmenuPosition) {
            return;
        }

        this.reCalcSubmenuPosition = false;

        const itemElement = this.getCurrentItemElement();
        const submenuElement = this.getCurrentSubmenuElement();

        if (!submenuElement || !itemElement) {
            return;
        }

        const menuRect = this.element.getBoundingClientRect();
        const itemRect = itemElement.getBoundingClientRect();
        const submenuRect = submenuElement.getBoundingClientRect();

        const viewportHeight = window.innerHeight;
        const paddingY = 20;
        const paddingBottom = Math.min(viewportHeight - itemRect.bottom, paddingY);
        const maxHeight = viewportHeight - paddingY - paddingBottom;

        submenuElement.style.maxHeight = `${maxHeight}px`;

        const submenuHeight = submenuElement.getBoundingClientRect().height;
        const position = Math.min(
            Math.max(itemRect.top - menuRect.top, 0),
            viewportHeight - paddingBottom - submenuHeight - menuRect.top,
        );

        submenuElement.style.top = `${position}px`;

        const submenuRight = menuRect.left + menuRect.width + submenuRect.width;

        submenuElement.classList.toggle(
            'menu__submenu--reverse',
            submenuRight > document.body.clientWidth,
        );
    }

    getCurrentItemElement(): HTMLDivElement | null {
        if (!this.hoveredItem) {
            return null;
        }

        const index = this.items?.indexOf(this.hoveredItem);
        const elements = this.itemElements.toArray();

        if (index === -1 || !elements[index]) {
            return null;
        }

        return elements[index].nativeElement as HTMLDivElement;
    }

    getCurrentSubmenuElement(): HTMLDivElement | null {
        if (!this.hoveredItem) {
            return null;
        }

        const index = this.items?.filter((x) => x.subCategory)?.indexOf(this.hoveredItem);
        const elements = this.submenuElements.toArray();

        if (index === -1 || !elements[index]) {
            return null;
        }

        return elements[index].nativeElement as HTMLDivElement;
    }

    trackByFn(_: number, item: { id: number }): number {
        return item.id;
    }
}
