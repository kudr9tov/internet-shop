import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CategoryModel } from '@common/models/category.model';

@Component({
    selector: 'app-header-megamenu',
    templateUrl: './megamenu.component.html',
    styleUrls: ['./megamenu.component.scss'],
})
export class MegamenuComponent {
    @Input() menu!: CategoryModel[];

    @Output() itemClick: EventEmitter<CategoryModel> = new EventEmitter<CategoryModel>();

    constructor() {}
}
