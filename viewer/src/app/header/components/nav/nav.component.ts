import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Inject,
    Input,
    NgZone,
    OnDestroy,
    PLATFORM_ID,
    ViewChild,
} from '@angular/core';
import { fromEvent, merge, Observable, Subject } from 'rxjs';
import { filter, first, shareReplay, takeUntil } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import { Select, Store } from '@ngxs/store';
import { MatDialog } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';

import { fromMatchMedia } from '@common/functions/rxjs/fromMatchMedia';
import { HeaderService } from '@common/services/header.service';
import { WishlistState } from '@common/auth/store/wishlist/wishlist.state';
import { AuthState } from '@common/auth/store/auth.state';

import { BasketState } from '../../../shared/basket-popup/store/basket-popup.state';
import { BasketPopupComponent } from '../../../shared/basket-popup/basket-popup.component';

export type NavStickyMode = 'alwaysOnTop' | 'pullToShow';

@Component({
    selector: 'app-header-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavComponent implements OnDestroy, AfterViewInit {
    @Input() departments = true;
    @Input() logo = false;
    @Input() search = false;
    @Input() stickyMode: NavStickyMode | false = false;

    @ViewChild('element') elementRef!: ElementRef;

    private destroy$: Subject<any> = new Subject();

    @Select(WishlistState.wishlistCount) wishlist$!: Observable<number>;

    @Select(BasketState.totalCount) totalCount$!: Observable<number>;

    @Select(AuthState.email) email$!: Observable<string>;

    stuckFrom: number | null = null;
    staticFrom: number | null = null;
    scrollPosition = 0;
    scrollDistance = 0;

    media!: Observable<MediaQueryList>;

    get element(): HTMLDivElement {
        return this.elementRef?.nativeElement;
    }

    constructor(
        @Inject(PLATFORM_ID) private platformId: any,
        private dialog: MatDialog,
        public store: Store,
        public zone: NgZone,
        public header: HeaderService,
    ) {}

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    ngAfterViewInit(): void {
        if (this.stickyMode && isPlatformBrowser(this.platformId)) {
            this.media = fromMatchMedia('(min-width: 992px)', false).pipe(
                shareReplay({ bufferSize: 1, refCount: true }),
            );
            this.media
                .pipe(takeUntil(this.destroy$))
                .subscribe((media) => this.onMediaChange(media));
        }
    }

    onCartClick(): void {
        this.dialog.open(BasketPopupComponent, {
            panelClass: 'huge-dialog',
        });
    }

    onEmail(email: string): void {
        location.href = 'mailto:' + email;
    }

    onScroll(): void {
        const scrollCurrentPosition = window.pageYOffset;
        const scrollDelta = scrollCurrentPosition - this.scrollPosition;

        // Resets the distance if the scroll changes direction.
        if (scrollDelta < 0 !== this.scrollDistance < 0) {
            this.scrollDistance = 0;
        }

        const distanceToShow = 10; // in pixels
        const distanceToHide = 25; // in pixels

        this.scrollPosition = scrollCurrentPosition;
        this.scrollDistance += scrollDelta;

        if (
            this.stuckFrom &&
            this.header.navPanelPosition === 'static' &&
            scrollCurrentPosition > this.stuckFrom
        ) {
            this.makeSticky();
        }
        if (
            this.staticFrom &&
            this.header.navPanelPosition === 'sticky' &&
            scrollCurrentPosition <= this.staticFrom
        ) {
            this.makeStatic();
        }

        if (this.header.navPanelPosition === 'sticky') {
            if (this.stickyMode === 'pullToShow') {
                if (
                    this.scrollDistance <= -distanceToShow &&
                    this.header.navPanelVisibility === 'hidden'
                ) {
                    this.show();
                }
                if (
                    this.scrollDistance >= distanceToHide &&
                    this.header.navPanelVisibility === 'shown'
                ) {
                    this.hide();
                }
            } else if (
                this.stickyMode === 'alwaysOnTop' &&
                this.header.navPanelVisibility === 'hidden'
            ) {
                this.show();
            }
        }
    }

    onMediaChange(media: MediaQueryList): void {
        if (media.matches) {
            const takeUntil$ = merge(
                this.media.pipe(
                    filter((x) => !x.matches),
                    first(),
                ),
                this.destroy$,
            );

            this.header.departmentsArea$
                .pipe(takeUntil(takeUntil$))
                .subscribe(() => setTimeout(() => this.calcBreakpoints(), 0));

            this.zone.runOutsideAngular(() => {
                fromEvent(window, 'scroll', { passive: true })
                    .pipe(takeUntil(takeUntil$))
                    .subscribe(() => this.onScroll());
            });

            this.calcBreakpoints();
        } else {
            this.makeStatic();
        }
    }

    calcBreakpoints(): void {
        if (this.header.departmentsArea) {
            const rect = this.header.departmentsArea.getBoundingClientRect();

            this.stuckFrom = rect.top + rect.height + window.screenTop + 50 + window.pageYOffset;
            this.staticFrom = this.stuckFrom;
        } else {
            const elementRect = this.element.getBoundingClientRect();

            this.staticFrom = elementRect.top + window.pageYOffset;
            this.stuckFrom = elementRect.top + elementRect.height + window.pageYOffset;
        }
    }

    private makeStatic(): void {
        this.element.classList.remove('nav-panel--stuck');
        this.element.classList.remove('nav-panel--shown');

        this.element.style.transition = 'none';
        this.element.getBoundingClientRect(); // force reflow
        this.element.style.transition = '';

        this.zone.run(() => (this.header.navPanelPosition = 'static'));
        this.zone.run(() => (this.header.navPanelVisibility = 'hidden'));
    }

    private makeSticky(): void {
        this.element.classList.add('nav-panel--stuck');

        this.element.style.transition = 'none';
        this.element.getBoundingClientRect(); // force reflow
        this.element.style.transition = '';

        this.zone.run(() => (this.header.navPanelPosition = 'sticky'));
    }

    private show(): void {
        this.element.classList.add('nav-panel--shown');

        this.zone.run(() => (this.header.navPanelVisibility = 'shown'));
    }

    private hide(): void {
        this.element.classList.remove('nav-panel--shown');

        this.zone.run(() => (this.header.navPanelVisibility = 'hidden'));
    }

    onLogin(): void {
        this.store.dispatch(new Navigate(['login']));
    }
}
