import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CatalogResolver } from '@common/catalogData/resolvers/catalog.resolver';

import { CatalogComponent } from './components/catalog/catalog.component';
import { ProductComponent } from './components/product/product.component';

const routes: Routes = [
    {
        path: '',
        component: CatalogComponent,
    },
    {
        path: 'product' + '/:id',
        component: ProductComponent,
        resolve: {
            catalog: CatalogResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CatalogRoutingModule {}
