import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { NgModule } from '@angular/core';
import { NguCarouselModule } from '@ngu/carousel';

import { BrandsDataModule } from '@common/brands/brands-data.module';
import { BreadcrumbsModule } from '@common/breadcrumbs/breadcrumbs.module';
import { CatalogDataModule } from '@common/catalogData/catalog-data.module';
import { CountriesDataModule } from '@common/countries/countries-data.module';
import { MatCardModule } from '@angular/material/card';
import { PipesModule } from '@common/pipes/pipes.module';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { DirectiveModule } from '@common/directives/directive.modul';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './components/catalog/catalog.component';
import { InterestDiscountModule } from '../interest-discount/interest-discount.module';
import { ProductCardModule } from '../product-card/product-card.module';
import { SearchBarModule } from '../components/search-bar/search-bar.module';
import { SubCategoriesComponent } from './components/sub-categories/sub-categories.component';
import { ProductsViewComponent } from './components/products-view/products-view.component';
import { WidgetFiltersComponent } from './components/widget-filters/widget-filters.component';
import { WidgetProductsComponent } from './components/widget-products/widget-products.component';
import { NgxsModule } from '@ngxs/store';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { ShopSidebarModule } from '../components/shop-sidebar/shop-sidebar.module';
import { AppCollapseModule } from '../app-collapse/app-collapse.module';
import { PaginationModule } from '../shared/pagination/pagination.module';
import { PageHeaderModule } from '../shared/page-header/page-header.module';
import { ProductTabsComponent } from './components/product-tabs/product-tabs.component';
import { ComponentsModule } from '../components/components.module';
import { ProductGalleryComponent } from './components/product-gallery/product-gallery.component';
import { ProductComponent } from './components/product/product.component';
import { ProductInfoComponent } from './components/product-info/product-info.component';
import { ShareButtonsComponent } from './components/share-buttons/share-buttons.component';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS } from 'ng-lazyload-image';
import { LazyLoadImageHooks } from '@common/hooks/lazyload-image.hook';
import { MatButtonModule } from '@angular/material/button';
import { InputNumberModule } from './components/input-number/input-number.module';

@NgModule({
    declarations: [
        CatalogComponent,
        SubCategoriesComponent,
        ProductsViewComponent,
        WidgetFiltersComponent,
        WidgetProductsComponent,
        ProductGalleryComponent,
        ProductTabsComponent,
        ProductComponent,
        ShareButtonsComponent,
        ProductInfoComponent,
    ],
    imports: [
        NgxsModule.forFeature(),
        CatalogDataModule,
        BrandsDataModule,
        BreadcrumbsModule,
        CountriesDataModule,

        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CatalogRoutingModule,
        MatChipsModule,
        NguCarouselModule,
        InterestDiscountModule,
        PipesModule,
        ProductCardModule,
        PageHeaderModule,
        SearchBarModule,
        MatTooltipModule,
        MatCardModule,
        MatButtonModule,
        MatTreeModule,
        DirectiveModule,
        ShopSidebarModule,
        ComponentsModule,
        AppIconModule,
        PaginationModule,
        AppCollapseModule,
        LazyLoadImageModule,
        InputNumberModule,
    ],
    providers: [
        PriceRequestApiService,
        { provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks },
    ],
})
export class CatalogModule {}
