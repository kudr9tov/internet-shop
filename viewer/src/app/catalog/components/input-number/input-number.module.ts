import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { InputNumberComponent } from './input-number.component';

@NgModule({
    exports: [InputNumberComponent],
    imports: [CommonModule],
    declarations: [InputNumberComponent],
})
export class InputNumberModule {}
