import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CatalogModel } from '@common/models/catalog.model';

@Component({
    selector: 'product-tabs',
    templateUrl: './product-tabs.component.html',
    styleUrls: ['./product-tabs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductTabsComponent {
    @Input() withSidebar = false;
    @Input() tab: 'description' | 'specification' | 'reviews' | 'catalogs' = 'description';

    @Input() techProperties!: any[];

    @Input() catalogs: CatalogModel[] | undefined = [];

    private _description!: string | undefined;
    @Input()
    get description(): string | undefined {
        return this._description;
    }
    set description(val: string | undefined) {
        this.tab = val ? 'description' : 'specification';
        this._description = val;
    }
    // reviews: Review[] = reviews;

    disclaimer =
        'Информация о технических характеристиках, комплекте поставки, стране-производителе и внешнем виде товара носит справочный характер и основана на актуальной информации, доступной на момент публикации.';
}
