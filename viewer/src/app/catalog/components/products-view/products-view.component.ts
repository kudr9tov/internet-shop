import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs';
import { ShopSidebarService } from '@common/services/shop-sidebar.service';
import { Select, Store } from '@ngxs/store';
import { CatalogState } from '@common/catalogData/store/catalog.state';
import { CatalogQuery } from '@common/catalogData/store/catalog.query';
import { ProductModel } from '@common/models/product.model';
import { ObserverComponent } from '@common/abstract/observer.component';
import { FilterQuery } from '@common/filter/store/filter.query';
import {
    RemoveSubCategory,
    ResetFilter,
    SetBrand,
    SetCategory,
    SetCountry,
    SetPageIndex,
    SetItemsPerPage,
} from '@common/filter/store/filter.actions';
import { SelectableListModel } from '@common/models/selectable-list.model';
import { DirectoryType } from '@common/enums/directory-type.enum';

export type Layout = 'grid' | 'grid-with-features' | 'list';

@Component({
    selector: 'app-products-view',
    templateUrl: './products-view.component.html',
    styleUrls: ['./products-view.component.scss'],
})
export class ProductsViewComponent extends ObserverComponent implements OnInit, OnDestroy {
    @Input() layout: Layout = 'grid';
    @Input() grid: 'grid-3-sidebar' | 'grid-4-full' | 'grid-5-full' = 'grid-3-sidebar';
    @Input() offcanvas: 'always' | 'mobile' = 'mobile';

    @Select(CatalogState.total) total$!: Observable<number>;

    @Select(CatalogQuery.catalog) catalog$!: Observable<ProductModel[]>;

    @Select(CatalogQuery.pageCount) pageCount$!: Observable<number>;

    @Select(FilterQuery.pageIndex) pageIndex$!: Observable<number>;

    @Select(CatalogQuery.legendOptions) legendOptions$!: Observable<string>;

    @Select(FilterQuery.selectedItems) selectedItems$!: Observable<SelectableListModel[]>;

    @Select(FilterQuery.pageSizeOptions) pageSizeOptions$!: Observable<number[]>;

    @Select(CatalogState.isLoadingCatalog)
    isLoadingCatalog$!: Observable<boolean>;

    private destroy$: Subject<any> = new Subject();

    listOptionsForm!: FormGroup;
    filtersCount = 0;

    // pageSize = 12;

    //   pageSizeOptions: number[] = [this.pageSize, 24, 36];

    constructor(private fb: FormBuilder, public sidebar: ShopSidebarService, public store: Store) {
        super();
        this.listOptionsForm = this.fb.group({
            page: this.fb.control(1),
            limit: this.fb.control(0),
            sort: this.fb.control(3),
        });
    }

    ngOnInit(): void {
        this.initSubscriptions();
    }

    override ngOnDestroy(): void {
        super.ngOnDestroy();
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    setLayout(value: Layout): void {
        this.layout = value;
    }

    onPageChange(page: number): void {
        this.store.dispatch(new SetPageIndex(page));
    }

    onLimitChange(event: any): void {
        this.store.dispatch(new SetItemsPerPage(+event.target.value));
    }

    resetFilters(): void {
        this.store.dispatch(new ResetFilter());
    }

    onRemove(id: number, type: DirectoryType, isParent: boolean): void {
        if (!id) {
            return;
        }
        switch (type) {
            case DirectoryType.Brands:
                this.store.dispatch(new SetBrand(id));
                break;
            case DirectoryType.Category:
                if (isParent) {
                    this.store.dispatch(new SetCategory(id));
                } else {
                    this.store.dispatch(new RemoveSubCategory(id));
                }
                break;
            case DirectoryType.Countries:
                this.store.dispatch(new SetCountry(id));
                break;
        }
    }

    trackByFn(_: number, item: { id: number }): number {
        return item.id;
    }

    private itemPerPageSubscription(): Subscription {
        return this.store
            .select(FilterQuery.itemsPerPage)
            .subscribe((itemsPerPage: number) =>
                this.listOptionsForm.controls['limit'].setValue(itemsPerPage),
            );
    }

    private initSubscriptions(): void {
        this.subscriptions.push(this.itemPerPageSubscription());
    }
}
