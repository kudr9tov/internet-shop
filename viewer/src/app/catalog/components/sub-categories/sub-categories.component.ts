import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { Component } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { take } from 'rxjs/operators';

import { CategoryModel } from '@common/models/category.model';
import { FilterQuery } from '@common/filter/store/filter.query';
import { FilterState } from '@common/filter/store/filter.state';
import { ObserverComponent } from '@common/abstract/observer.component';
import {
    OpenCatalog,
    RemoveSubCategory,
    SetSubCategory,
} from '@common/filter/store/filter.actions';
import { SortHelper } from '@common/helpers/sort.helper';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';

@Component({
    selector: 'sub-categories',
    templateUrl: './sub-categories.component.html',
    styleUrls: ['./sub-categories.component.scss'],
})
export class SubCategoriesComponent extends ObserverComponent {
    subCategories: CategoryModel[] = [];

    /** Map from flat node to nested node. This helps us finding the nested node to be modified */
    flatNodeMap = new Map<TodoItemFlatNode, CategoryModel>();

    /** Map from nested node to flattened node. This helps us to keep the same object for selection */
    nestedNodeMap = new Map<CategoryModel, TodoItemFlatNode>();

    /** A selected parent node to be inserted */
    selectedParent: TodoItemFlatNode | null = null;

    treeControl: FlatTreeControl<TodoItemFlatNode>;

    treeFlattener: MatTreeFlattener<CategoryModel, TodoItemFlatNode>;

    dataSource: MatTreeFlatDataSource<CategoryModel, TodoItemFlatNode>;

    /** The selection for checklist */
    checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

    getLevel = (node: TodoItemFlatNode) => node.level;

    isExpandable = (node: TodoItemFlatNode) => node.expandable;

    getChildren = (node: CategoryModel): CategoryModel[] => node.subCategory;

    hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

    hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

    /**
     * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
     */
    transformer = (node: CategoryModel, level: number) => {
        const existingNode = this.nestedNodeMap.get(node);
        const flatNode =
            existingNode && existingNode.item === node.name ? existingNode : new TodoItemFlatNode();
        flatNode.item = node.name;
        flatNode.id = node.id;
        flatNode.level = level;
        flatNode.parentCategoryId = node.parentCategoryId;
        flatNode.expandable = !!node.subCategory;
        this.flatNodeMap.set(flatNode, node);
        this.nestedNodeMap.set(node, flatNode);
        return flatNode;
    };

    constructor(private actions$: Actions, public store: Store) {
        super();
        this.treeFlattener = new MatTreeFlattener(
            this.transformer,
            this.getLevel,
            this.isExpandable,
            this.getChildren,
        );
        this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    }

    ngOnInit(): void {
        this.subscriptions.push(
            this.subCategoriesSubscription(),
            //   this.selectionSubscription(),
            this.selectedCategorySubscription(),
            this.removeSubCategorySubscription(),
            this.openCatalogSubscription(),
        );
    }

    toggleSubCategory(ids: TodoItemFlatNode[]): void {
        if (!ids) {
            return;
        }
        this.store.dispatch(new SetSubCategory(ids));
    }

    trackByFn(_: number, item: { id: number }): number {
        return item.id;
    }

    /** Whether all the descendants of the node are selected */
    descendantsAllSelected(node: TodoItemFlatNode): boolean {
        const descendants = this.treeControl.getDescendants(node);
        return descendants.every((child) => this.checklistSelection.isSelected(child));
    }

    /** Whether part of the descendants are selected */
    descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
        const descendants = this.treeControl.getDescendants(node);
        const result = descendants.some((child) => this.checklistSelection.isSelected(child));
        return result && !this.descendantsAllSelected(node);
    }

    /** Toggle the to-do item selection. Select/deselect all the descendants node */
    todoItemSelectionToggle(node: TodoItemFlatNode): void {
        this.checklistSelection.toggle(node);
        const descendants = this.treeControl.getDescendants(node);
        this.checklistSelection.isSelected(node)
            ? this.checklistSelection.select(...descendants)
            : this.checklistSelection.deselect(...descendants);
        this.toggleSubCategory(this.checklistSelection.selected);
    }

    todoLeafItemSelectionToggle(node: TodoItemFlatNode): void {
        this.checklistSelection.toggle(node);
        this.checkAllParentsSelection(node);
        this.toggleSubCategory(this.checklistSelection.selected);
    }

    /* Checks all the parents when a leaf node is selected/unselected */
    checkAllParentsSelection(node: TodoItemFlatNode): void {
        let parent: TodoItemFlatNode | null = this.getParentNode(node);
        while (parent) {
            this.checkRootNodeSelection(parent);
            parent = this.getParentNode(parent);
        }
    }

    /* Get the parent node of a node */
    getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
        const currentLevel = this.getLevel(node);

        if (currentLevel < 1) {
            return null;
        }

        const startIndex = this.treeControl?.dataNodes?.indexOf(node) - 1;

        for (let i = startIndex; i >= 0; i--) {
            const currentNode = this.treeControl.dataNodes[i];

            if (this.getLevel(currentNode) < currentLevel) {
                return currentNode;
            }
        }
        return null;
    }

    /** Check root node checked state and change it accordingly */
    checkRootNodeSelection(node: TodoItemFlatNode): void {
        const nodeSelected = this.checklistSelection.isSelected(node);
        const descendants = this.treeControl.getDescendants(node);
        const descAllSelected = descendants.every((child) =>
            this.checklistSelection.isSelected(child),
        );
        if (nodeSelected && !descAllSelected) {
            this.checklistSelection.deselect(node);
        } else if (!nodeSelected && descAllSelected) {
            this.checklistSelection.select(node);
        }
    }

    // private selectionSubscription(): Subscription {
    //     return this.checklistSelection.changed.subscribe(() => this.toggleSubCategory(this.checklistSelection && this.checklistSelection.selected));
    // }

    private selectedCategorySubscription(): Subscription {
        return this.store
            .select(FilterState.subCategoryIds)
            .pipe(take(1))
            .subscribe((values) => {
                this.updateCheckList(values);
            });
    }

    private subCategoriesSubscription(): Subscription {
        return this.store.select(FilterQuery.subCategories2).subscribe((values: any) => {
            this.clearSelections();
            this.dataSource.data = [...values].sort(SortHelper.compareByNameFn);
        });
    }
    private removeSubCategorySubscription(): Subscription {
        return this.actions$
            .pipe(ofActionDispatched(RemoveSubCategory))
            .subscribe(({ id }: RemoveSubCategory) => {
                this.updateSelections(id);
            });
    }

    private openCatalogSubscription(): Subscription {
        return this.actions$
            .pipe(ofActionDispatched(OpenCatalog))
            .subscribe(({ item }: OpenCatalog) => {
                this.clearSelections();
                if (item.parentCategoryId) {
                    this.updateSelections(item.id);
                }
            });
    }

    private updateCheckList(ids: number[]): void {
        let selected: TodoItemFlatNode[] = [];
        (this.treeControl.dataNodes || []).forEach((x) => {
            if (ids.includes(x.id)) {
                selected.push(x);
            }
        });
        this.checklistSelection.select(...selected);
    }

    private clearSelections(): void {
        this.nestedNodeMap.clear();
        this.flatNodeMap.clear();
        this.checklistSelection.clear();
    }

    private updateSelections(id: number): void {
        const value = this.treeControl.dataNodes.find((x) => x.id === id);
        if (value?.expandable) {
            this.todoItemSelectionToggle(value);
        } else {
            this.todoLeafItemSelectionToggle(value as any);
        }
    }
}
