import { Component, Input } from '@angular/core';
import { CatalogState } from '@common/catalogData/store/catalog.state';
import { ProductModel } from '@common/models/product.model';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-widget-products',
    templateUrl: './widget-products.component.html',
    styleUrls: ['./widget-products.component.scss'],
})
export class WidgetProductsComponent {
    @Input() header = '';

    @Select(CatalogState.lastViewProducts)
    lastViewProducts$!: Observable<ProductModel[]>;
}
