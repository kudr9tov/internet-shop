import { Select, Store } from '@ngxs/store';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Meta, Title } from '@angular/platform-browser';

import { FilterQuery } from '@common/filter/store/filter.query';
import { LoadBrands } from '@common/brands/store/brands.actions';
import { LoadCountries } from '@common/countries/store/countries.actions';
import { ObserverComponent } from '@common/abstract/observer.component';
import { ActivatedRoute } from '@angular/router';
import { SetAllFilter } from '@common/filter/store/filter.actions';
@Component({
    selector: 'catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CatalogComponent extends ObserverComponent implements OnInit {
    @Select(FilterQuery.titleDescription) titleDescription$!: Observable<{
        title: string;
        description: string;
    }>;

    columns: 3 | 4 | 5 = 3;

    viewMode: 'grid' | 'grid-with-features' | 'list' = 'grid';

    sidebarPosition: 'start' | 'end' = 'start';

    title = 'Фильтр';

    constructor(
        public store: Store,
        private titleService: Title,
        private metaTagService: Meta,
        public route: ActivatedRoute,
    ) {
        super();
        this.subscriptions.push(this.titleSubscription());
    }
    ngOnInit(): void {
        this.store.dispatch([new LoadBrands(), new LoadCountries()]);
        this.route.queryParams.subscribe((value) => this.store.dispatch(new SetAllFilter(value)));
    }

    private titleSubscription(): Subscription {
        return this.store
            .select(FilterQuery.titleDescription)
            .subscribe((object: { title: string; description: string }) => {
                this.titleService.setTitle(object.title);
                const { description } = object;
                this.metaTagService.updateTag({
                    name: 'description',
                    description,
                });
            });
    }
}
