import {
    ChangeDetectionStrategy,
    Component,
    Inject,
    Input,
    OnDestroy,
    OnInit,
    PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs';

import { CategoryModel } from '@common/models/category.model';
import { FilterQuery } from '@common/filter/store/filter.query';
import { Actions, ofActionCompleted, Select, Store } from '@ngxs/store';
import { SelectableListModel } from '@common/models/selectable-list.model';
import { ObserverComponent } from '@common/abstract/observer.component';
import { PriceModel } from '@common/models/price.model';
import { debounceTime, distinctUntilChanged, first } from 'rxjs/operators';
import {
    RemoveSubCategory,
    ResetFilter,
    SetBrand,
    SetCategory,
    SetCountry,
    SetPrice,
    ToggleNew,
    ToggleSale,
} from '@common/filter/store/filter.actions';
import { FilterState } from '@common/filter/store/filter.state';
import { CategoryExtModel } from '@common/models/category-ext.model';

@Component({
    selector: 'app-widget-filters',
    templateUrl: './widget-filters.component.html',
    styleUrls: ['./widget-filters.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetFiltersComponent extends ObserverComponent implements OnInit, OnDestroy {
    @Input() offcanvas: 'always' | 'mobile' = 'mobile';

    @Select(FilterQuery.countries) countries$!: Observable<SelectableListModel[]>;

    @Select(FilterState.disabledSubCategory)
    disabledSubCategory$!: Observable<boolean>;

    @Select(FilterQuery.categories) categories$!: Observable<CategoryExtModel[]>;

    @Select(FilterQuery.brands) brands$!: Observable<SelectableListModel[]>;

    @Select(FilterQuery.isNew) isNew$!: Observable<boolean>;

    @Select(FilterQuery.isSale) isSale$!: Observable<boolean>;

    private destroy$: Subject<any> = new Subject();

    filter = new FormControl([0, 0]);

    price!: PriceModel;

    filters: Filter[] = [
        new CheckFilter({ type: 'countries', name: 'Страна' }),
        new CheckFilter({ type: 'brands', name: 'Производитель' }),
        new RadioFilter({ type: 'category', name: 'Тип оборудования' }),
        new CategoryFilter({ type: 'subCategory', name: 'Вид Оборудования' }),
        new RangeFilter({ type: 'price', name: 'Цена' }),
    ];
    filtersForm!: FormGroup;

    isPlatformBrowser = isPlatformBrowser(this.platformId);

    constructor(
        @Inject(PLATFORM_ID) private platformId: any,
        public store: Store,
        public fb: FormBuilder,
        private actions$: Actions,
    ) {
        super();
    }

    ngOnInit(): void {
        this.filtersForm = this.fb.group({
            filter: this.filter,
        });
        this.initSubscriptions();
        this.filter.valueChanges
            .pipe(debounceTime(300), distinctUntilChanged())
            .subscribe((search: any) =>
                this.store.dispatch(
                    new SetPrice(new PriceModel({ min: search[0] || 0, max: search[1] || 0 })),
                ),
            );
    }

    override ngOnDestroy(): void {
        super.ngOnDestroy();
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    trackBySlug(_: number, item: { slug: string }): any {
        return item.slug;
    }

    toggleCountry(id: number): void {
        if (!id) {
            return;
        }
        this.store.dispatch(new SetCountry(id));
    }

    toggleBrand(id: number): void {
        if (!id) {
            return;
        }
        this.store.dispatch(new SetBrand(id));
    }

    onSetCategory(id: number): void {
        if (!id) {
            return;
        }
        this.store.dispatch(new SetCategory(id));
    }

    removeSubCategory(id: number): void {
        if (!id) {
            return;
        }
        this.store.dispatch(new RemoveSubCategory(id));
    }

    onReset(): void {
        this.store.dispatch(new ResetFilter());
    }

    trackByFn(_: number, item: { id: number }): number {
        return item.id;
    }

    onToggleSale(): void {
        this.store.dispatch(new ToggleSale());
    }

    onToggleNew(): void {
        this.store.dispatch(new ToggleNew());
    }

    reset(): void {
        const formValues: { [key: string]: any } = {};

        this.filters.forEach((filter) => {
            switch (filter.type) {
                case 'price':
                    formValues[filter.slug] = [filter.min, filter.max];
                    break;
                case 'category':
                    formValues[filter.slug] = filter.items[0].slug;
                    break;
            }
        });

        this.filtersForm.setValue(formValues);
    }

    getRangeControl(filter: Filter): FormControl {
        return this.filtersForm.get(filter.slug) as FormControl;
    }

    private initSubscriptions(): void {
        this.subscriptions.push(
            this.priceSubscription(),
            this.origPriceSubscription(),
            this.actions$
                .pipe(ofActionCompleted(ResetFilter))
                .subscribe(() => this.filter.setValue([this.price.min || 0, this.price.max])),
        );
    }

    private priceSubscription(): Subscription {
        return this.store
            .select(FilterQuery.price)
            .pipe(first())
            .subscribe((model: PriceModel) => this.filter.setValue([model.min || 0, model.max]));
    }

    private origPriceSubscription(): Subscription {
        return this.store
            .select(FilterQuery.origPrice)
            .subscribe((model: PriceModel) => (this.price = model));
    }
}

abstract class FilterBase {
    slug!: string;
    name!: string;
}
abstract class ValuableFilterBase {
    value: any;
}
export class CategoryFilter extends FilterBase {
    type!: 'subCategory';
    root!: boolean;
    items!: CategoryModel[];

    public constructor(fields?: Partial<CategoryFilter>) {
        super();
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
export type RangeFilterValue = [number, number];
export class RangeFilter extends FilterBase implements ValuableFilterBase {
    type!: 'price';
    value!: RangeFilterValue;
    min!: number;
    max!: number;

    public constructor(fields?: Partial<RangeFilter>) {
        super();
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
export interface FilterItem {
    slug: string;
    name: string;
    count: number;
}
export type ListFilterValue = string[];
export class CheckFilter extends FilterBase implements ValuableFilterBase {
    type!: 'countries' | 'brands';
    value!: ListFilterValue;
    items!: FilterItem[];

    public constructor(fields?: Partial<CheckFilter>) {
        super();
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
export class RadioFilter extends FilterBase implements ValuableFilterBase {
    type!: 'category';
    value!: string;
    items!: FilterItem[];

    public constructor(fields?: Partial<RadioFilter>) {
        super();
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
export class SerializedFilterValues {
    [slug: string]: string;
}

export type Filter = CategoryFilter | RangeFilter | CheckFilter | RadioFilter;
