import { Component, OnDestroy } from '@angular/core';
import { ObserverComponent } from '@common/abstract/observer.component';
import { AddLastViewProducts } from '@common/catalogData/store/catalog.actions';
import { CatalogQuery } from '@common/catalogData/store/catalog.query';
import { CatalogState } from '@common/catalogData/store/catalog.state';
import { AppHelper } from '@common/constants/app.const';
import { LinkModel } from '@common/models/link.model';
import { ProductModel } from '@common/models/product.model';
import { environment } from '@environments/environment';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { combineLatestWith } from 'rxjs/operators';
import { SeoSocialShareService } from '../../../services/seo.service';

@Component({
    selector: 'product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
})
export class ProductComponent extends ObserverComponent implements OnDestroy {
    @Select(CatalogState.recomendedProducts) relatedProducts$!: Observable<ProductModel[]>;
    @Select(CatalogState.isLoadingRecommendedProducts)
    isLoadingRecommendedProducts$!: Observable<boolean>;
    @Select(CatalogQuery.breadcrumb) breadcrumb$!: Observable<LinkModel[]>;
    @Select(CatalogState.product) product$!: Observable<ProductModel>;
    product!: ProductModel;

    layout: 'standard' | 'columnar' | 'sidebar' = 'sidebar';
    sidebarPosition: 'start' | 'end' = 'start'; // For LTR scripts "start" is "left" and "end" is "right"
    promotions = [
        {
            title: 'БОНУС ПЛЮС',
            description: 'Получите бонус плюс за покупку более трех продуктов',
            icon: 'giftcard',
        },
        {
            title: 'БЕСПЛАТНАЯ ДОСТАВКА',
            description: 'Бесплатная доставка для всех заказов на сумму свыше 100.000 рублей.',
            icon: 'local-shipping',
        },
        {
            title: 'ГАРАНТИЯ ВОЗВРАТА ДЕНЕГ',
            description: '100% гарантия возврата денег',
            icon: 'monetization',
        },
        {
            title: 'ОНЛАЙН ПОДДЕРЖКА 24/7',
            description: `Позвоните нам: ${environment.phone}`,
            icon: 'history',
        },
    ];
    constructor(public store: Store, private seoService: SeoSocialShareService) {
        super();
        this.subscriptions.push(this.selectedCategoryNameSubscription());
    }

    override ngOnDestroy(): void {
        this.store.dispatch(new AddLastViewProducts(this.product));
        super.ngOnDestroy();
    }

    private selectedCategoryNameSubscription(): Subscription {
        return this.product$
            .pipe(combineLatestWith(this.breadcrumb$))
            .subscribe(([product, breadcrumb]) => {
                if (product) {
                    this.product = product;
                }
                const words = breadcrumb?.map((x) => x.label).join();

                this.seoService.setData({
                    title: AppHelper.getProductTitle(product),
                    description: AppHelper.description(product),
                    image: product && product.image ? product.image : '',
                    type: 'website',
                    keywords:
                        `${product.properties?.brand?.value},${product.properties?.country?.value},` +
                        words,
                    imageAuxData: {
                        alt: product.name,
                        height: 968,
                        width: 504,
                        mimeType: 'image/jpeg',
                        secureUrl: product && product.image ? product.image : '',
                    },
                });
            });
    }
}
