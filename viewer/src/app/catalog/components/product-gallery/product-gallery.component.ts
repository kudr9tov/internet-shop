import {
    Component,
    ElementRef,
    HostBinding,
    Inject,
    Input,
    OnDestroy,
    OnInit,
    PLATFORM_ID,
    QueryList,
    ViewChild,
    ViewChildren,
} from '@angular/core';
import {
    PhotoSwipeItem,
    PhotoSwipeModelRef,
    PhotoSwipeService,
    PhotoSwipeThumbBounds,
} from '../../services/photo-swipe.service';
import { isPlatformBrowser } from '@angular/common';
import { NavigationStart, Router } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NguCarouselConfig } from '@ngu/carousel';
import { ProductLayout } from '../product-info/product-info.component';

export interface ProductGalleryItem {
    id: string;
    image: string;
}

@Component({
    selector: 'product-gallery',
    templateUrl: './product-gallery.component.html',
    styleUrls: ['./product-gallery.component.scss'],
})
export class ProductGalleryComponent implements OnInit, OnDestroy {
    private destroy$: Subject<any> = new Subject();

    items: ProductGalleryItem[] = [];

    currentItem: ProductGalleryItem | null = null;

    carouselOptions!: NguCarouselConfig;

    thumbnailsCarouselOptions!: NguCarouselConfig;

    galleryRef: PhotoSwipeModelRef | null = null;

    galleryIsClosed = false;

    @Input() productLayout: ProductLayout = 'standard';

    @Input() set images(images: string[]) {
        this.items = images?.map((image, index) => ({
            id: `image-${index}`,
            image,
        }));
        this.currentItem = this.items[0] || null;
    }

    @HostBinding('class.product-gallery') classProductGallery = true;

    @ViewChild('featuredCarousel') featuredCarousel!: any;

    @ViewChild('thumbnailsCarousel') thumbnailsCarousel!: any;

    @ViewChildren('imageElement', { read: ElementRef })
    imageElements!: QueryList<ElementRef>;

    constructor(
        @Inject(PLATFORM_ID) private platformId: any,
        private photoSwipe: PhotoSwipeService,
        private router: Router,
    ) {}

    ngOnInit(): void {
        if (this.productLayout !== 'quickview' && isPlatformBrowser(this.platformId)) {
            this.photoSwipe.load().subscribe();
        }

        this.thumbnailsCarouselOptions = {
            grid: { xs: 2, sm: 3, md: 4, lg: 5, all: 0 },
            slide: 1,
            touch: true,
            point: {
                visible: false,
            },
        };

        this.carouselOptions = {
            grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
            slide: 1,
            touch: true,
            point: {
                visible: false,
            },
        };

        this.router.events
            .pipe(
                filter((event) => event instanceof NavigationStart),
                takeUntil(this.destroy$),
            )
            .subscribe(() => {
                this.galleryIsClosed = true;

                if (this.galleryRef) {
                    this.galleryRef.close();
                }
            });
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    featuredCarouselTranslated(event: any): void {
        if (event.slides && event.slides.length) {
            const activeImageId = event.slides[0].id;

            this.currentItem =
                this.items.find((x) => x.id === activeImageId) || this.items[0] || null;

            if (
                !this.thumbnailsCarousel.slidesData.find(
                    (slide: { id: any; isActive: any }) =>
                        slide.id === activeImageId && slide.isActive,
                )
            ) {
                this.thumbnailsCarousel.to(activeImageId);
            }
        }
    }

    getDirDependentIndex(index: number): number {
        // we need to invert index id direction === 'rtl' because photoswipe do not support rtl
        // if (this.direction.isRTL()) {
        //     return this.items.length - 1 - index;
        // }

        return index;
    }

    onFeaturedImageClick(event: MouseEvent, image: any): void {
        if (this.productLayout !== 'quickview') {
            event.preventDefault();

            this.openPhotoSwipe(image);
        }
    }

    onThumbnailImageClick(item: ProductGalleryItem, index: number): void {
        this.featuredCarousel.moveTo(index);
        this.currentItem = item;
    }

    openPhotoSwipe(item: ProductGalleryItem | null): void {
        if (!item) {
            return;
        }

        const imageElements = this.imageElements?.map((x) => x.nativeElement);
        const images: PhotoSwipeItem[] = this.items?.map((eachItem, i) => {
            const tag: HTMLImageElement = imageElements[i];
            const width =
                (tag.dataset['width'] && parseFloat(tag.dataset['width'])) || tag.naturalWidth;
            const height =
                (tag.dataset['height'] && parseFloat(tag.dataset['height'])) || tag.naturalHeight;

            return {
                src: eachItem.image,
                msrc: eachItem.image,
                w: width,
                h: height,
            };
        });

        // if (this.direction.isRTL()) {
        //     images.reverse();
        // }

        // noinspection JSUnusedGlobalSymbols
        const options = {
            getThumbBoundsFn: ((index: number) => {
                if (this.galleryIsClosed) {
                    return null;
                }

                return this.getThumbBounds(index);
            }) as (index: number) => PhotoSwipeThumbBounds,
            index: this.getDirDependentIndex(this.items?.indexOf(item)),
            bgOpacity: 0.9,
            history: false,
        };

        this.photoSwipe.open(images, options).subscribe((galleryRef) => {
            galleryRef.listen('beforeChange', () => {
                this.featuredCarousel.to(
                    this.items[this.getDirDependentIndex(galleryRef.getCurrentIndex())].id,
                );
            });

            this.galleryRef = galleryRef;
        });
    }

    getThumbBounds(index: number): PhotoSwipeThumbBounds | null {
        const imageElements = this.imageElements.toArray();
        const dirDependentIndex = this.getDirDependentIndex(index);

        if (!imageElements[dirDependentIndex]) {
            return null;
        }

        const tag = imageElements[dirDependentIndex].nativeElement;
        const width = tag.naturalWidth;
        const height = tag.naturalHeight;
        const rect = tag.getBoundingClientRect();
        const ration = Math.min(rect.width / width, rect.height / height);
        const fitWidth = width * ration;
        const fitHeight = height * ration;

        return {
            x: rect.left + (rect.width - fitWidth) / 2 + window.pageXOffset,
            y: rect.top + (rect.height - fitHeight) / 2 + window.pageYOffset,
            w: fitWidth,
        };
    }
}
