import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AppHelper } from '@common/constants/app.const';
import { RequestPriceModel } from '@common/models/price-request.model';
import { ProductModel } from '@common/models/product.model';
import { SeoDescriptionPipes } from '@common/pipes/seo-description.pipe';
import { RequestPriceDialogComponent } from '@common/request-price-dialog/request-price-dialog.component';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { SetError, SetSuccess } from '@common/toast';
import { environment } from '@environments/environment';
import { Store } from '@ngxs/store';
import { AddToBasket } from '../../../shared/basket-popup/store/basket-popup.actions';

export type ProductLayout = 'standard' | 'sidebar' | 'columnar' | 'quickview';

@Component({
    selector: 'product-info',
    templateUrl: './product-info.component.html',
    styleUrls: ['./product-info.component.scss'],
})
export class ProductInfoComponent {
    @Input() layout: ProductLayout = 'standard';

    _product!: ProductModel;
    get product(): ProductModel {
        return this._product;
    }
    @Input()
    set product(value: ProductModel) {
        this._product = value;
        this.description = value?.description
            ? SeoDescriptionPipes.prototype.transform(value?.description, 120)
            : AppHelper.description(value);
    }

    description!: string;
    siteUrl = environment.apiPath;
    quantity: FormControl = new FormControl(1);
    nextWeek = new Date();
    addingToCart = false;
    addingToWishlist = false;
    addingToCompare = false;
    pravoInfo =
        'Внимание! Цены, указанные на сайте, могут отличаться от фактических. Информация о товарах не является публичной офертой (ст. 437 п. 1 ГК РФ). Уточнить информацию о товаре вы можете у менеджеров по продажам.';
    constructor(
        public store: Store,
        public service: PriceRequestApiService,
        private dialog: MatDialog,
    ) {
        this.nextWeek.setDate(new Date().getDate() + 7);
    }

    addToCart(): void {
        if (!this.addingToCart && this.product && this.quantity.value > 0) {
            this.addingToCart = true;
            this.product.price
                ? this.store
                      .dispatch(new AddToBasket(this.product, this.quantity.value))
                      .subscribe({
                          complete: () => (this.addingToCart = false),
                      })
                : this.onAddRequest();
            // this.cart.add(this.product, this.quantity.value)
        }
    }

    onAddRequest(): void {
        this.dialog
            .open(RequestPriceDialogComponent, {
                panelClass: 'request-price',
                data: { productId: this.product.id },
            })
            .afterClosed()
            .subscribe((result: any) => {
                if (!result) {
                    this.addingToCart = false;
                    return;
                }
                this.service.create(new RequestPriceModel({ ...result })).subscribe({
                    complete: () => {
                        this.store.dispatch(new SetSuccess('Запрос отправлен'));
                        this.addingToCart = false;
                    },
                    error: () => {
                        this.store.dispatch(new SetError('Произошла ошибка'));
                        this.addingToCart = false;
                    },
                });
            });
    }

    // addToWishlist(): void {
    //     if (!this.addingToWishlist && this.product) {
    //         this.addingToWishlist = true;

    //         this.wishlist.add(this.product).subscribe({complete: () => this.addingToWishlist = false});
    //     }
    // }

    // addToCompare(): void {
    //     if (!this.addingToCompare && this.product) {
    //         this.addingToCompare = true;

    //         this.compare.add(this.product).subscribe({complete: () => this.addingToCompare = false});
    //     }
    // }
}
