import { ChangeDetectorRef, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LinkModel } from '@common/models/link.model';
import { RequestPriceModel } from '@common/models/price-request.model';
import { ProductModel } from '@common/models/product.model';
import { RequestPriceDialogComponent } from '@common/request-price-dialog/request-price-dialog.component';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { SetError, SetSuccess } from '@common/toast';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddToBasket } from '../../shared/basket-popup/store/basket-popup.actions';
import { RemoveFromWishlist } from '../../../../../common/src/auth/store/wishlist/wishlist.actions';
import { WishlistState } from '@common/auth/store/wishlist/wishlist.state';

@Component({
    selector: 'wishlist',
    templateUrl: './wishlist.component.html',
    styleUrls: ['./wishlist.component.scss'],
})
export class WishlistComponent {
    @Select(WishlistState.wishlist) wishlist$!: Observable<ProductModel[]>;

    breadcrumbs = [
        new LinkModel({ label: 'Главная', url: '/' }),
        new LinkModel({ label: 'Лист ожидания', url: '/wishlist' }),
    ];

    addingToCart = false;

    constructor(
        private cd: ChangeDetectorRef,
        private dialog: MatDialog,
        public service: PriceRequestApiService,
        public store: Store,
    ) {}

    addToCart(product: ProductModel): void {
        if (this.addingToCart) {
            return;
        }
        if (!product.price) {
            this.onAddRequest(product);
            return;
        }

        this.addingToCart = true;
        this.store.dispatch(new AddToBasket(product)).subscribe({
            complete: () => {
                this.addingToCart = false;
                this.cd.markForCheck();
            },
        });
    }

    onRemove(id: number): void {
        this.store.dispatch(new RemoveFromWishlist(id));
    }

    onAddRequest(product: ProductModel): void {
        this.dialog
            .open(RequestPriceDialogComponent, {
                panelClass: 'request-price',
                data: { productId: product.id },
            })
            .afterClosed()
            .subscribe((result: any) => {
                if (!result) {
                    return;
                }
                this.service.create(new RequestPriceModel({ ...result })).subscribe({
                    complete: () => {
                        this.store.dispatch(new SetSuccess('Запрос отправлен'));
                    },
                    error: () => this.store.dispatch(new SetError('Произошла ошибка')),
                });
            });
    }
}
