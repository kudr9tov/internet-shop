import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { WishlistComponent } from './components/wishlist.component';
import { WishlistRoutingModule } from './wishlist-routing.module';
import { NgxsModule } from '@ngxs/store';
import { PageHeaderModule } from '../shared/page-header/page-header.module';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { PipesModule } from '@common/pipes/pipes.module';
import { AppIconModule } from '@common/app-icon/app-icon.module';

@NgModule({
    declarations: [WishlistComponent],
    imports: [
        CommonModule,
        WishlistRoutingModule,
        PageHeaderModule,
        AppIconModule,
        PipesModule,
        NgxsModule.forFeature(),
    ],
    providers: [PriceRequestApiService],
})
export class WishlistModule {}
