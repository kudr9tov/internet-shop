import { Actions, ofActionDispatched, Select, Store } from '@ngxs/store';
import {
    ChangeDetectionStrategy,
    Component,
    Inject,
    NgZone,
    OnInit,
    PLATFORM_ID,
    Renderer2,
} from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { RouterNavigation } from '@ngxs/router-plugin';
import { Observable, Subscription } from 'rxjs';

import { AuthState } from '@common/auth/store/auth.state';
import { DeviceSizeComponent } from '@common/abstract/device-size.component';
import { LoadCategories } from '@common/auth/store/categories/categories.actions';

import { CanonicalService } from './shared/canonical.service';
import { AppConstants } from '@common/constants/app.const';
import { environment } from '@environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { NavigationEnd, Router } from '@angular/router';
import { filter, first } from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent extends DeviceSizeComponent implements OnInit {
    @Select(AuthState.mobilePhones) mobilePhones$!: Observable<string[]>;

    @Select(AuthState.email) email$!: Observable<string>;

    headerLayout: 'classic' | 'compact' = 'classic';

    constructor(
        @Inject(PLATFORM_ID) private platformId: any,
        public override store: Store,
        private zone: NgZone,
        private router: Router,
        private canonicalService: CanonicalService,
        private titleService: Title,
        private metaTagService: Meta,
        private actions$: Actions,
        private renderer: Renderer2,
    ) {
        super(store);
        if (isPlatformBrowser(this.platformId)) {
            this.zone.runOutsideAngular(() => {
                this.router.events
                    .pipe(
                        filter((event) => event instanceof NavigationEnd),
                        first(),
                    )
                    .subscribe(() => {
                        const preloader = document.querySelector('.site-preloader');

                        if (preloader === null) {
                            return;
                        }

                        preloader.addEventListener(
                            'transitionend',
                            (event: Event) => {
                                if (
                                    event instanceof TransitionEvent &&
                                    event.propertyName === 'opacity'
                                ) {
                                    preloader.remove();
                                }
                            },
                            { passive: true },
                        );
                        preloader.classList.add('site-preloader__fade');

                        // Sometimes, due to unexpected behavior, the browser (in particular Safari) may not play the transition, which leads
                        // to blocking interaction with page elements due to the fact that the preloader is not deleted.
                        // The following block covers this case.
                        if (getComputedStyle(preloader).opacity === '0' && preloader.parentNode) {
                            preloader.parentNode.removeChild(preloader);
                        }
                    });
            });
        }
        this.subscriptions.push(...this.actionsSubscription());
    }

    ngOnInit(): void {
        this.store.dispatch(new LoadCategories());
        this.canonicalService.updateCanonicalUrl();
        if (environment.production) {
            this.addJsToElement();
        }
        this.addFonts();
        this.titleService.setTitle(AppConstants.tile);
        this.metaTagService.addTags([
            { name: 'keywords', content: AppConstants.keywords },
            { name: 'description', content: AppConstants.description },
            { name: 'robots', content: 'all' },
        ]);
    }

    private actionsSubscription(): Subscription[] {
        //RouterDataResolved
        return [
            this.actions$.pipe(ofActionDispatched(RouterNavigation)).subscribe((action) => {
                this.canonicalService.updateCanonicalUrl(action.event.url);
            }),
        ];
    }

    addFonts(): void {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.crossOrigin = 'anonymous';
        script.async = true;
        script.src = 'https://kit.fontawesome.com/dda1d090dc.js';
        this.renderer.appendChild(document.body, script);
        script.onload = () => console.log('script loaded');
    }

    addJsToElement(): void {
        const body = document.getElementsByTagName('body')[0];
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = 'https://cdn.envybox.io/widget/cbk.css';
        body.appendChild(link);
        link.onload = () => console.log('link loaded');

        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.src =
            'https://cdn.envybox.io/widget/cbk.js?wcb_code=b0a3a58d28570b1478df9e8d29cc39ae';
        this.renderer.appendChild(document.body, script);
        script.onload = () => console.log('script loaded');
    }
}
