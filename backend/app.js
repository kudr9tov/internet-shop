const express = require('express');
const http = require('http');
const path = require('path');

const app = express();

const port = process.env.PORT || 3001;
const proxy = require('http-proxy-middleware')
var apiProxy = proxy('/api', {target: 'http://gktorg.ru/'});


app.use(express.static(__dirname + '../../dist/'));
app.use(apiProxy);
app.get('/*', (req,res) => res.sendFile(path.join(__dirname)))

const server = http.createServer(app)

app.listen(port, () => console.log('Server Conected on port 3001'))