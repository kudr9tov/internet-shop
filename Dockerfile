FROM node:18.13.0-alpine

WORKDIR /usr/src/app

COPY ./dist/viewer /usr/src/app/dist/viewer

EXPOSE 4000

CMD [ "node", "dist/viewer/server/main.js" ]
