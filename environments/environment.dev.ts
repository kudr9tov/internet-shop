import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

export const environment = {
    production: false,
    plugins: [
        NgxsLoggerPluginModule.forRoot({ logger: console, collapsed: false }),
        NgxsReduxDevtoolsPluginModule.forRoot(),
    ],
    apiPath: 'https://gktorg.ru',
    name: 'Главком',
    phone: '+7 (495) 108-18-96',
    phoneNumber: 79165793121,
    logo: 'assets/glavcom-logo.svg',
    domen: 'gktorg.ru',
    telegram: 'gktorg',
    primaryColor: '#1A4173',
    yandexRatingId: 140600016189,
    messengerPhone: '+7-916-930-00-50',
    location: '105122, г.Москва, Щёлковское шоссе, 2А, офис 1040',
    aboutTextId: 4,
    serviceTextId: 3,
    storage: 'г. Видное, Белокаменное шоссе, владение 18',
};
