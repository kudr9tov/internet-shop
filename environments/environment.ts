import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    plugins: [
        NgxsLoggerPluginModule.forRoot({ logger: console, collapsed: false }),
        NgxsReduxDevtoolsPluginModule.forRoot(),
    ],
    apiPath: 'https://gktorg.ru',
    name: 'ГлавКом',
    phone: '+7 (495) 108-18-96',
    phoneNumber: 79169300050,
    logo: 'assets/glavcom-logo.svg',
    domen: 'gktorg.ru',
    telegram: 'gktorg',
    primaryColor: '#1A4173',
    yandexRatingId: 140600016189,
    messengerPhone: '+7-916-930-00-50',
    location: '105122, г.Москва, Щёлковское шоссе, 2А, офис 1040',
    aboutTextId: 4,
    serviceTextId: 3,
    storage: 'г. Видное, Белокаменное шоссе, владение 18',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
