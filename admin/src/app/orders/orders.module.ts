import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgxsModule } from '@ngxs/store';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './components/orders/orders.component';
import { OrderEditComponent } from './components/order-edit/order-edit.component';
import { OrdersState } from './store/orders.state';
import { OrderResolver } from './resolvers/order.resolver';
import { OrdersApiService } from './services/orders-api.service';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
    declarations: [OrderEditComponent, OrdersComponent],
    imports: [
        CommonModule,
        NgxsModule.forFeature([OrdersState]),
        MatButtonModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        FormsModule,
        MatSortModule,
        MatTableModule,
        OrdersRoutingModule,
    ],
    providers: [OrdersApiService, OrderResolver],
})
export class OrdersModule {}
