import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { BaseApiService } from '@common/abstract/base-api.service';
import { OrderModel } from '@common/models/order.model';

@Injectable()
export class OrdersApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(id: number): Observable<any> {
        return this.httpGet(`order/${id}`, (x) => new OrderModel(x));
    }

    public getList(limit: number, skip: number, filter: any = null): Observable<any> {
        const url =
            Object.keys(filter).length !== 0
                ? `order?limit=${limit}&skip=${skip}&filter=${encodeURIComponent(
                      JSON.stringify(filter),
                  )}`
                : `order?limit=${limit}&skip=${skip}`;
        return this.httpGet(url, (x) => x);
    }

    public create(model: OrderModel): Observable<any> {
        return this.httpPost('order', (x) => new OrderModel(x), model);
    }

    public update(model: OrderModel): Observable<any> {
        return this.httpPut('order', (x) => new OrderModel(x), model);
    }

    public updateStatus(id: number, status: string): Observable<any> {
        return this.httpPost(`order/${id}/${status}`, (x) => x, {});
    }

    public delete(id: number): Observable<any> {
        return this.httpDelete('order', id, (x) => x);
    }
}
