import { OrderModel } from '@common/models/order.model';

export interface OrdersStateModel {
    orders: OrderModel[];
    order: OrderModel | null;
}
