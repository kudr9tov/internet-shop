import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { OrderModel } from '@common/models/order.model';

import { OrdersApiService } from '../services/orders-api.service';
import { OrdersStateModel } from './orders.model';
import {
    LoadOrders,
    LoadOrder,
    CreateOrder,
    DeleteOrder,
    UpdateOrder,
    UpdateStatus,
} from './orders.actions';
import { patch, removeItem, append, updateItem } from '@ngxs/store/operators';

@State<OrdersStateModel>({
    name: 'orders',
    defaults: {
        orders: [],
        order: null,
    },
})
@Injectable()
export class OrdersState {
    @Selector()
    static orders({ orders }: OrdersStateModel): OrderModel[] {
        return orders || [];
    }

    @Selector()
    static order({ order }: OrdersStateModel): OrderModel | null {
        return order;
    }

    constructor(private apiService: OrdersApiService) {}

    @Action(LoadOrders)
    onLoadOrders(
        { patchState }: StateContext<OrdersStateModel>,
        { limit, skip }: LoadOrders,
    ): Observable<any> {
        const search = {};
        return this.apiService.getList(limit, skip, search).pipe(
            tap((orders: OrderModel[]) => {
                patchState({
                    orders,
                });
            }),
        );
    }

    @Action(LoadOrder)
    onLoadOrder(
        { patchState }: StateContext<OrdersStateModel>,
        { id }: LoadOrder,
    ): Observable<any> {
        return this.apiService.get(id).pipe(
            tap((order: OrderModel) => {
                patchState({ order });
            }),
        );
    }

    @Action(CreateOrder)
    onCreateOrder(
        { setState }: StateContext<OrdersStateModel>,
        { model }: CreateOrder,
    ): Observable<any> {
        return this.apiService.create(model).pipe(
            tap((order: OrderModel) => {
                tap(() =>
                    setState(
                        patch({
                            orders: append([order]),
                        }),
                    ),
                );
            }),
        );
    }

    @Action(UpdateOrder)
    onUpdateOrder(
        { setState }: StateContext<OrdersStateModel>,
        { model }: UpdateOrder,
    ): Observable<any> {
        return this.apiService.update(model).pipe(
            tap((order: OrderModel) => {
                tap(() =>
                    setState(
                        patch({
                            orders: updateItem<OrderModel>((x) => x.id === order.id, order),
                        }),
                    ),
                );
            }),
        );
    }

    @Action(UpdateStatus)
    onUpdateStatus(
        { setState }: StateContext<OrdersStateModel>,
        { id, status }: UpdateStatus,
    ): Observable<any> {
        return this.apiService.updateStatus(id, status).pipe(
            tap(() => {
                tap(() =>
                    setState(
                        patch({
                            orders: updateItem<OrderModel>(
                                (x) => x.id === id,
                                patch({ status: status }),
                            ),
                        }),
                    ),
                );
            }),
        );
    }

    @Action(DeleteOrder)
    onDeleteOrder(
        { setState }: StateContext<OrdersStateModel>,
        { id }: DeleteOrder,
    ): Observable<any> {
        return this.apiService.delete(id).pipe(
            tap(() =>
                setState(
                    patch({
                        orders: removeItem<OrderModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }
}
