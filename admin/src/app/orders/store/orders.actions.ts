import { OrderModel } from '@common/models/order.model';

export class LoadOrder {
    static readonly type = '[Orders] Load order';

    constructor(public id: number) {}
}

export class LoadOrders {
    static readonly type = '[Orders] Load orders';
    constructor(public limit: number, public skip: number, public filter: string = '') {}
}

export class DeleteOrder {
    static readonly type = '[Orders] delete order';

    constructor(public id: number) {}
}

export class UpdateStatus {
    static readonly type = '[Orders] Update status';

    constructor(public id: number, public status: string) {}
}

export class CreateOrder {
    static readonly type = '[Orders] Create order';

    constructor(public model: OrderModel) {}
}

export class UpdateOrder {
    static readonly type = '[Orders] Update order';

    constructor(public model: OrderModel) {}
}
