import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { Navigate } from '@ngxs/router-plugin';
import { LoadOrders, DeleteOrder, UpdateStatus } from '../../store/orders.actions';
import { OrderModel } from '@common/models/order.model';
import { OrdersState } from '../../store/orders.state';
import { BaseShopTableComponent } from '@common/abstract/base-shop-table.component';

@Component({
    selector: 'orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent extends BaseShopTableComponent<OrderModel> implements OnInit {
    @Select(OrdersState.orders) orders$!: Observable<OrderModel[]>;

    displayedColumns = ['select', 'id', 'name', 'totalCost', 'created', 'status', 'delete'];

    statuses = [
        { id: 'CREATED', name: 'Новый Заказ' },
        { id: 'PROCESSING', name: 'Принят' },
        { id: 'WAITING_PAYMENT', name: 'Ожидание оплаты' },
        { id: 'COMPLETED', name: 'Завершён' },
    ];

    feature = 'заказ';

    getColor(status: any) {
        switch (status) {
            case 'CREATED':
                return 'example-panel-green';
            case 'PROCESSING':
                return 'example-panel-blue';
            case 'WAITING_PAYMENT':
                return 'example-panel-azure';
            case 'COMPLETED':
                return 'example-panel-red';
            default:
                return '';
        }
    }

    constructor(public override store: Store, public override dialog: MatDialog) {
        super(store, dialog);
    }

    override ngOnInit() {
        this.store.dispatch(new LoadOrders(this.pageSize, 0));
        this.subscriptions.push(this.ordersSubscription());
        super.ngOnInit();
    }

    onChangePaginator(event: any): void {
        this.pageSize = event.pageSize || 0;
        this.store.dispatch(new LoadOrders(this.pageSize, this.pageSize * event.pageIndex));
    }

    onOpen(id: number): void {
        if (!id) {
            return;
        }

        this.store.dispatch(new Navigate(['orders', `${id}`]));
    }

    onStatusChange(id: number, status: any): void {
        const request = this.dataSource.data.find((x) => x.id === id);

        if (!request) {
            return;
        }

        request.status = status;
        this.store.dispatch(new UpdateStatus(id, status));
    }

    onLoadItems(search: string): void {
        this.store.dispatch(new LoadOrders(this.pageSize, 0, search));
    }

    onDeleteItem(id: number): void {
        this.store.dispatch(new DeleteOrder(id));
    }

    private ordersSubscription(): Subscription {
        return this.orders$.subscribe((orders: OrderModel[]) => {
            if (!orders) {
                return;
            }
            const newOrders = orders.map((x) => new OrderModel({ ...x }));
            this.dataSource = new MatTableDataSource([...newOrders]);
        });
    }
}
