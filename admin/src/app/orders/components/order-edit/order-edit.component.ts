import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { OrderModel } from '@common/models/order.model';
import { Observable, Subscription } from 'rxjs';
import { OrdersState } from '../../store/orders.state';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { ObserverComponent } from '@common/abstract/observer.component';
import { Navigate } from '@ngxs/router-plugin';
import { ProductListModel } from '@common/models/product-list.model';
import { CustomerInfoModel } from '@common/models/customer-info.model';
import { formatDate } from '@angular/common';

@Component({
    selector: 'order-edit',
    templateUrl: 'order-edit.component.html',
    styleUrls: ['order-edit.component.scss'],
})
export class OrderEditComponent extends ObserverComponent implements OnInit {
    @Select(OrdersState.order) order$!: Observable<OrderModel>;

    orderGroup!: FormGroup;

    customerInfoGroup!: FormGroup;

    productsFormArray!: FormArray;

    order!: OrderModel;

    statuses = [
        { id: 'CREATED', name: 'Новый Заказ' },
        { id: 'PROCESSING', name: 'Принят' },
        { id: 'WAITING_PAYMENT', name: 'Ожидание оплаты' },
        { id: 'COMPLETED', name: 'Завершён' },
    ];

    constructor(public fb: FormBuilder, public store: Store) {
        super();
    }

    ngOnInit(): void {
        this.setupFormGroups();
        this.subscriptions.push(this.orderSubscription());
    }

    onBack(): void {
        this.store.dispatch(new Navigate(['orders']));
    }

    setupFormGroups(): void {
        this.orderGroup = this.fb.group({
            id: [null],
            products: [[]],
            customerInfo: [null],
            comment: [null],
            created: [{ value: null, disabled: true }],
            updated: [{ value: null, disabled: true }],
            totalCost: [null],
            status: [{ value: null }],
        });
    }

    private createOrderFormGroup(order: OrderModel): FormGroup {
        this.productsFormArray = this.createProductsFormArray(order.products);
        this.customerInfoGroup = this.createCustomerInfoFormGroup(order.customerInfo);
        return this.fb.group({
            id: [order.id],
            products: [this.productsFormArray],
            customerInfo: [this.customerInfoGroup],
            comment: [order.comment],
            created: [
                {
                    value: formatDate(new Date(order.created), 'M/d/yyy, H:mm', 'en'),
                    disabled: true,
                },
            ],
            updated: [
                {
                    value: formatDate(new Date(order.updated), 'M/d/yyy, H:mm', 'en'),
                    disabled: true,
                },
            ],
            totalCost: [order.totalCost],
            status: [order.status],
        });
    }

    private createProductsFormArray(products: ProductListModel[]): FormArray {
        const formArray = this.fb.array([]);

        products.forEach((country: ProductListModel) => {
            formArray.push(
                this.fb.group({
                    quantity: [country.quantity],
                    name: [country.name],
                }) as any,
            );
        });
        return formArray;
    }

    private createCustomerInfoFormGroup(customer: CustomerInfoModel): FormGroup {
        return this.fb.group({
            organization: [customer.organization],
            requisites: [customer.requisites],
            city: [customer.city],
            address: [customer.address],
            name: [customer.name],
            phone: [customer.phone],
            email: [customer.email],
        });
    }

    private orderSubscription(): Subscription {
        return this.order$.subscribe((order: OrderModel) => {
            this.order = order;
            this.orderGroup = this.createOrderFormGroup(order);
        });
    }
}
