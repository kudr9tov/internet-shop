import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersComponent } from './components/orders/orders.component';
import { OrderEditComponent } from './components/order-edit/order-edit.component';
import { OrderResolver } from './resolvers/order.resolver';

const routes: Routes = [
    {
        path: '',
        component: OrdersComponent,
    },
    {
        path: ':id',
        component: OrderEditComponent,
        resolve: {
            order: OrderResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class OrdersRoutingModule {}
