import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { NgxsModule } from '@ngxs/store';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { SecondaryMenuModule } from '@common/secondary-menu/secondary-menu.module';
import { UploaderService } from '@common/image-upload/services/uploader.service';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';

import { TextEditorComponent } from './components/text-editor/text-editor.component';
import { TextsComponent } from './components/texts/texts.component';
import { TextEditorApiService } from '../../../../common/src/services/text-editor-api.service';
import { TextEditorRoutingModule } from './text-editor-routing.module';
import { TextEditorState } from './store/text-editor.state';

@NgModule({
    declarations: [TextEditorComponent, TextsComponent],
    imports: [
        CommonModule,
        NgxsModule.forFeature([TextEditorState]),
        FormsModule,
        TextEditorRoutingModule,
        ImageUploadModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSidenavModule,
        MatToolbarModule,
        ReactiveFormsModule,
        SecondaryMenuModule,
        AngularEditorModule,
    ],
    providers: [TextEditorApiService, UploaderService],
})
export class TextEditorModule {}
