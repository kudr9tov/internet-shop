import { ListModel } from '@common/models/list.model';
import { TextEditorModel } from '@common/models/text-editor.model';
import { Selector } from '@ngxs/store';
import { TextEditorState } from './text-editor.state';

export class TextEditorQuery {
    @Selector([TextEditorState.texts])
    static textList(texts: TextEditorModel[]): ListModel[] {
        return texts.map((x) => new ListModel({ ...x }));
    }
}
