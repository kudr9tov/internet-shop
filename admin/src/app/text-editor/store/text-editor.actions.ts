import { TextEditorModel } from '@common/models/text-editor.model';

export class DeleteText {
    static readonly type = '[Text-editor] Delete text';
    constructor(public id: number) {}
}

export class LoadTexts {
    static readonly type = '[Text-editor] Load texts';
}

export class SelectText {
    static readonly type = '[Text-editor] Select text';
    constructor(public text: TextEditorModel | undefined) {}
}

export class SelectDefaultText {
    static readonly type = '[Text-editor] Select default text';
}

export class NavigateToText {
    static readonly type = '[Text-editor] Navigate to text';
    constructor(public id: number) {}
}

export class SelectFirstText {
    static readonly type = '[Text-editor] Select first text';
}

export class NavigateToTexts {
    static readonly type = '[Text-editor] Navigate to texts';
}

export class CreateText {
    static readonly type = '[Text-editor] Create new text';
    constructor(public name: string) {}
}

export class UpdateText {
    static readonly type = '[Text-editor] Update text';
    constructor(public model: TextEditorModel) {}
}
