import { TextEditorModel } from '@common/models/text-editor.model';

export interface TextEditorStateModel {
    texts: TextEditorModel[];
    text: TextEditorModel;
}
