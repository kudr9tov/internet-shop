import { Injectable } from '@angular/core';
import { SortHelper } from '@common/helpers/sort.helper';
import { TextEditorModel } from '@common/models/text-editor.model';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TextEditorApiService } from '../../../../../common/src/services/text-editor-api.service';
import {
    CreateText,
    DeleteText,
    LoadTexts,
    NavigateToText,
    NavigateToTexts,
    SelectDefaultText,
    SelectFirstText,
    SelectText,
    UpdateText,
} from './text-editor.actions';

import { TextEditorStateModel } from './text-editor.model';

@State<TextEditorStateModel>({
    name: 'textEditorState',
    defaults: {
        text: new TextEditorModel(),
        texts: [],
    },
})
@Injectable()
export class TextEditorState {
    @Selector()
    static texts({ texts }: TextEditorStateModel): TextEditorModel[] {
        return texts;
    }

    @Selector()
    static text({ text }: TextEditorStateModel): TextEditorModel {
        return text;
    }

    constructor(private apiService: TextEditorApiService) {}

    @Action(LoadTexts)
    onLoadTexts({ patchState }: StateContext<TextEditorStateModel>): Observable<any> {
        return this.apiService.getList().pipe(tap((texts) => patchState({ texts })));
    }

    @Action(CreateText)
    onCreateText(
        { dispatch, setState }: StateContext<TextEditorStateModel>,
        { name }: CreateText,
    ): Observable<any> {
        if (!name) {
            return of();
        }
        const model = new TextEditorModel({
            name,
            seoUrl: name,
            content: '',
            imageUrl: '',
            system: false,
        });
        return this.apiService.create([model]).pipe(
            tap((text: TextEditorModel) => {
                setState(patch({ texts: append([text]) }));
                dispatch([new NavigateToText(text.id)]);
            }),
        );
    }

    @Action(UpdateText)
    onUpdateText(
        { setState }: StateContext<TextEditorStateModel>,
        { model }: UpdateText,
    ): Observable<any> {
        return this.apiService.update([model]).pipe(
            tap(() => {
                setState(
                    patch({
                        texts: updateItem<TextEditorModel>(
                            (x) => x.id === model.id,
                            patch({
                                name: model.name,
                                content: model.content,
                                seoUrl: model.seoUrl,
                            }),
                        ),
                    }),
                );
            }),
        );
    }

    @Action(DeleteText)
    onDeleteText(
        { setState }: StateContext<TextEditorStateModel>,
        { id }: DeleteText,
    ): Observable<any> {
        return this.apiService.delete(id).pipe(
            tap(() =>
                setState(
                    patch({
                        texts: removeItem<TextEditorModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }

    @Action(SelectDefaultText)
    onSelectDefaultMobileOperator({
        dispatch,
        getState,
    }: StateContext<TextEditorStateModel>): void {
        const { text } = getState();

        if (text && text.id) {
            dispatch(new NavigateToText(text.id));
        } else {
            dispatch(new SelectFirstText());
        }
    }

    @Action(SelectFirstText)
    onSelectFirstMobileOperator({ dispatch, getState }: StateContext<TextEditorStateModel>): void {
        const { texts } = getState();

        if (!texts || !texts.length) {
            dispatch(new NavigateToTexts());
            return;
        }

        const sortedResult = [...texts].sort(SortHelper.compareByNameFn);
        dispatch(new NavigateToText(sortedResult[0].id));
    }

    @Action(SelectText)
    onSelectText({ patchState }: StateContext<TextEditorStateModel>, { text }: SelectText): void {
        patchState({ text });
    }
}
