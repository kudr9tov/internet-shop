import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { TextEditorComponent } from './components/text-editor/text-editor.component';
import { TextsComponent } from './components/texts/texts.component';
import { TextEditorUnsavedChangesGuard } from './guards/text-editor-unsaved-changes.guard';
import { TextEditorResolver } from './resolvers/text-editor-resolver/text-editor-resolver';
import { TextsResolver } from './resolvers/texts-resolver/texts-resolver';

const ROUTES: Routes = [
    {
        path: '',
        component: TextsComponent,
        resolve: {
            mobilePhoneOperators: TextsResolver,
        },
        children: [
            {
                path: ':id',
                component: TextEditorComponent,
                resolve: {
                    mobilePhoneOperator: TextEditorResolver,
                },
                canDeactivate: [TextEditorUnsavedChangesGuard],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
    providers: [
        TextEditorUnsavedChangesGuard,
        UnsavedChangesGuard,
        TextEditorResolver,
        TextsResolver,
    ],
})
export class TextEditorRoutingModule {}
