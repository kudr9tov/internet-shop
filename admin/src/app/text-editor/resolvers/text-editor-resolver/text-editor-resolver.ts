import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';

import { TextEditorModel } from '@common/models/text-editor.model';

import { SelectText } from '../../store/text-editor.actions';
import { TextEditorState } from '../../store/text-editor.state';

@Injectable()
export class TextEditorResolver implements Resolve<TextEditorModel> {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<TextEditorModel> {
        const texts: TextEditorModel[] = this.store.selectSnapshot(TextEditorState.texts);
        const id = +route.params['id'];
        if (!texts || !texts.length) {
            return of();
        }

        const model = texts.find((x) => x.id === id);
        return this.store.dispatch([new SelectText(model)]);
    }
}
