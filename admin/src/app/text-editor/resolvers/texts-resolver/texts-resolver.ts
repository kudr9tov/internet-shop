import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Resolve } from '@angular/router';
import { Store } from '@ngxs/store';

import { TextEditorModel } from '@common/models/text-editor.model';

import { TextEditorState } from '../../store/text-editor.state';
import { LoadTexts } from '../../store/text-editor.actions';

@Injectable()
export class TextsResolver implements Resolve<TextEditorModel[]> {
    constructor(private store: Store) {}

    resolve(): Observable<TextEditorModel[]> {
        const texts: TextEditorModel[] = this.store.selectSnapshot(TextEditorState.texts);
        return texts && texts.length ? of(texts) : this.store.dispatch(new LoadTexts());
    }
}
