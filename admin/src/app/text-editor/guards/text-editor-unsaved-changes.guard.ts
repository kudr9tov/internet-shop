import { Injectable } from '@angular/core';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';
import { TextEditorComponent } from '../components/text-editor/text-editor.component';

@Injectable()
export class TextEditorUnsavedChangesGuard extends UnsavedChangesGuard<TextEditorComponent> {}
