import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ObserverComponent } from '@common/abstract/observer.component';
import { ListModel } from '@common/models/list.model';
import { SelectableListModel } from '@common/models/selectable-list.model';
import { SecondaryMenuState } from '@common/secondary-menu/store/secondary-menu.state';
import { SetSuccess } from '@common/toast';
import { Navigate } from '@ngxs/router-plugin';
import { Actions, ofActionCompleted, ofActionDispatched, Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import {
    CreateText,
    DeleteText,
    NavigateToText,
    NavigateToTexts,
    SelectDefaultText,
    UpdateText,
} from '../../store/text-editor.actions';
import { TextEditorQuery } from '../../store/text-editor.query';

@Component({
    selector: 'texts',
    templateUrl: './texts.component.html',
    styleUrls: ['./texts.component.scss'],
})
export class TextsComponent extends ObserverComponent implements OnInit {
    @Select(TextEditorQuery.textList)
    texts$!: Observable<ListModel[]>;

    @Select(SecondaryMenuState.isSecondaryCollapsed)
    isCollapsed$!: Observable<boolean>;

    @ViewChild('secondarySideNav') secondarySideNav!: MatSidenav;

    filterItems!: SelectableListModel[];

    isCollapsed!: boolean;

    title = 'Нет доступных элементов';

    constructor(private actions$: Actions, private store: Store) {
        super();
    }

    ngOnInit(): void {
        this.initSubscriptions();
        this.store.dispatch(new SelectDefaultText());
    }

    onCreateNew(newItemName: string): void {
        if (newItemName) {
            this.store.dispatch(new CreateText(newItemName));
        } else {
            this.store.dispatch(new NavigateToText(0));
        }
    }

    onCreatingMode(): void {
        this.store.dispatch(new Navigate(['texts']));
    }

    private initSubscriptions(): void {
        this.subscriptions.push(
            this.actions$
                .pipe(ofActionCompleted(CreateText))
                .subscribe(() => this.store.dispatch(new SetSuccess('Публикация создана'))),
            this.actions$
                .pipe(ofActionCompleted(UpdateText))
                .subscribe(() => this.store.dispatch(new SetSuccess('Публикация обнавлена'))),
            this.actions$
                .pipe(ofActionCompleted(DeleteText))
                .subscribe(() => this.store.dispatch(new SetSuccess('Публикация удалена'))),
            this.actions$
                .pipe(ofActionDispatched(NavigateToText))
                .subscribe(({ id }: NavigateToText) =>
                    this.store.dispatch(new Navigate(['texts', id])),
                ),
            this.actions$
                .pipe(ofActionDispatched(NavigateToTexts))
                .subscribe(() => this.store.dispatch(new Navigate(['texts']))),
        );
    }
}
