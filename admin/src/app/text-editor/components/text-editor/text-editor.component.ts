import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ObserverComponent } from '@common/abstract/observer.component';
import { AuthState } from '@common/auth/store/auth.state';
import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';
import { IHaveChanges } from '@common/guards/unsaved-changes.guard';
import { UploaderService } from '@common/image-upload/services/uploader.service';
import { TextEditorModel } from '@common/models/text-editor.model';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Navigate } from '@ngxs/router-plugin';
import { Select, Store } from '@ngxs/store';
import { Observable, of, Subscription } from 'rxjs';
import { DeleteText, UpdateText } from '../../store/text-editor.actions';
import { TextEditorState } from '../../store/text-editor.state';

@Component({
    selector: 'text-editor',
    templateUrl: './text-editor.component.html',
    styleUrls: ['./text-editor.component.scss'],
})
export class TextEditorComponent extends ObserverComponent implements OnInit, IHaveChanges {
    @Select(AuthState.isMobile) isMobile$!: Observable<boolean>;

    @Select(TextEditorState.text)
    text$!: Observable<TextEditorModel>;

    text!: TextEditorModel;

    isAfterDeleted!: boolean;

    textGroup!: FormGroup;

    constructor(
        private dialog: MatDialog,
        private ref: ChangeDetectorRef,
        private store: Store,
        private fb: FormBuilder,
        private uploader: UploaderService,
    ) {
        super();
    }

    ngOnInit(): void {
        this.initSubscriptions();
    }

    haveChanges(): boolean {
        return this.textGroup && this.textGroup.dirty;
    }

    onDelete(): void {
        this.openConfirmDeleteDialog().subscribe((result) => {
            if (result) {
                this.isAfterDeleted = true;
                this.textGroup.markAsPristine();
                this.store.dispatch(new DeleteText(this.text.id));
            }
        });
    }

    onSave(): void {
        if (this.textGroup.invalid) {
            return;
        }
        this.store.dispatch(new UpdateText(new TextEditorModel({ ...this.textGroup.value })));
        this.textGroup.markAsPristine();
    }

    onCancel(): void {
        this.openUnsavedChangesModal().subscribe((result) => {
            if (result) {
                this.textGroup = this.createTextFormGroup(this.text);
            }
        });
    }

    private initSubscriptions(): void {
        this.subscriptions.push(this.textSubscription());
    }

    private textSubscription(): Subscription {
        return this.text$.subscribe((value) => {
            this.text = value;
            if (!value) {
                this.store.dispatch(new Navigate(['texts']));
                return;
            }
            this.textGroup = this.createTextFormGroup(this.text);
            this.ref.detectChanges();
        });
    }

    private createTextFormGroup(text: TextEditorModel): FormGroup {
        return this.fb.group({
            id: [text.id],
            name: [text.name, Validators.required],
            seoUrl: [text.seoUrl],
            imageUrl: [text.imageUrl],
            content: [text.content],
            system: [text.system],
        });
    }

    private openUnsavedChangesModal(): Observable<any> {
        if (this.textGroup.pristine) {
            return of(true);
        }
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Несохраненные изменения',
                message:
                    'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                confirmLabel: 'Отказаться',
                cancelLabel: 'Отменить',
            },
        });
        return dialogRef.afterClosed();
    }

    private openConfirmDeleteDialog(): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Подтверждение удаления',
                message: 'Выбранная категория будет удалена. Вы уверены, что хотите удалить её?',
            },
        });
        return dialogRef.afterClosed();
    }

    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: 'auto',
        minHeight: '0',
        maxHeight: 'auto',
        width: 'auto',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Введите текст...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
            { class: 'arial', name: 'Arial' },
            { class: 'times-new-roman', name: 'Times New Roman' },
            { class: 'calibri', name: 'Calibri' },
            { class: 'comic-sans-ms', name: 'Comic Sans MS' },
        ],
        uploadUrl: 'files/upload',
        upload: (file: File) => this.upload(file),
        uploadWithCredentials: false,
        sanitize: false,
        toolbarPosition: 'top',
    };

    private upload(file: File): Observable<any> {
        const a = this.uploader.uploadForEditor(file, 'files/upload');
        return a;
    }
}
