import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { CategoriesPageComponent } from './pages/categories-page/categories-page.component';
import { CategoryPageComponent } from './pages/category-page/category-page.component';
import { CategoryResolver } from './resolvers/category-resolver/category-resolver';
import { CategoriesResolver } from './resolvers/categories-resolver/categories-resolver';
import { CategoryUnsavedChangesGuard } from './guards/category-unsaved-changes.guard';

const ROUTES: Routes = [
    {
        path: '',
        component: CategoriesPageComponent,
        resolve: {
            mobilePhoneOperators: CategoriesResolver,
        },
        children: [
            {
                path: ':id',
                component: CategoryPageComponent,
                resolve: {
                    mobilePhoneOperator: CategoryResolver,
                },
                canDeactivate: [CategoryUnsavedChangesGuard],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
    providers: [
        CategoriesResolver,
        CategoryUnsavedChangesGuard,
        UnsavedChangesGuard,
        CategoryResolver,
    ],
})
export class CategoriesRoutingModule {}
