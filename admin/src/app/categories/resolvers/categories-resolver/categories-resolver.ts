import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Resolve } from '@angular/router';
import { Store } from '@ngxs/store';

import { CategoryModel } from '@common/models/category.model';
import { LoadCategories } from '@common/auth/store/categories/categories.actions';
import { CategoriesQuery } from '@common/auth/store/categories/categories.query';

@Injectable()
export class CategoriesResolver implements Resolve<CategoryModel[]> {
    constructor(private store: Store) {}

    resolve(): Observable<CategoryModel[]> {
        const categories: CategoryModel[] = this.store.selectSnapshot(CategoriesQuery.categories);
        return categories && categories.length
            ? of(categories)
            : this.store.dispatch(new LoadCategories());
    }
}
