import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';

import { CategoryModel } from '@common/models/category.model';
import { SelectCategory } from '@common/auth/store/categories/categories.actions';
import { CategoriesQuery } from '@common/auth/store/categories/categories.query';
import { TreeHelper } from '@common/helpers/tree.helper';
import { LoadBreadcrumbs } from '@common/breadcrumbs/store/breadcrumbs.actions';

@Injectable()
export class CategoryResolver implements Resolve<CategoryModel> {
    constructor(private store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<CategoryModel> {
        const categories: CategoryModel[] = this.store.selectSnapshot(CategoriesQuery.categories);
        const id = +route.params['id'];
        if (!categories || !categories.length) {
            return of();
        }
        const model = TreeHelper.findeById(id, categories);
        return this.store.dispatch([new SelectCategory(model), new LoadBreadcrumbs(model)]);
    }
}
