import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgModule } from '@angular/core';

import { SecondaryMenuModule } from '@common/secondary-menu/secondary-menu.module';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';
import { BreadcrumbsModule } from '@common/breadcrumbs/breadcrumbs.module';
import { CardItemModule } from '@common/app/card-item/card-item.module';

import { CategoriesPageComponent } from './pages/categories-page/categories-page.component';
import { CategoryPageComponent } from './pages/category-page/category-page.component';
import { CategoriesRoutingModule } from './categories-routing.module';
import { FolderSelectDialogModule } from '@common/folder-select-dialog/folder-select-dialog.module';

@NgModule({
    declarations: [CategoriesPageComponent, CategoryPageComponent],
    imports: [
        BreadcrumbsModule,
        CardItemModule,
        CategoriesRoutingModule,
        CommonModule,
        FormsModule,
        ImageUploadModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSidenavModule,
        MatToolbarModule,
        ReactiveFormsModule,
        SecondaryMenuModule,
        FolderSelectDialogModule,
    ],
})
export class CategoriesModule {}
