import { Injectable } from '@angular/core';

import { CategoryPageComponent } from '../pages/category-page/category-page.component';
import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

@Injectable()
export class CategoryUnsavedChangesGuard extends UnsavedChangesGuard<CategoryPageComponent> {}
