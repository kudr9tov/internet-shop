import { Actions, ofActionCompleted, ofActionDispatched, Select, Store } from '@ngxs/store';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Navigate } from '@ngxs/router-plugin';
import { Observable } from 'rxjs';

import { SetSuccess } from '@common/toast/store/toast.actions';

import { ListModel } from '@common/models/list.model';
import {
    NavigateToCategoriesList,
    NavigateToCategory,
    DeleteCategory,
    CreateCategory,
    UpdateCategory,
    SelectDefaultCategory,
} from '@common/auth/store/categories/categories.actions';
import { ObserverComponent } from '@common/abstract/observer.component';
import { SecondaryMenuState } from '@common/secondary-menu/store/secondary-menu.state';
import { CategoriesQuery } from '@common/auth/store/categories/categories.query';
import { SelectableListModel } from '@common/models/selectable-list.model';

@Component({
    selector: 'categories-page',
    templateUrl: 'categories-page.component.html',
    styleUrls: ['categories-page.component.scss'],
})
export class CategoriesPageComponent extends ObserverComponent implements OnInit {
    @Select(CategoriesQuery.categoriesList)
    categoriesList$!: Observable<ListModel[]>;

    @Select(SecondaryMenuState.isSecondaryCollapsed)
    isCollapsed$!: Observable<boolean>;

    @ViewChild('secondarySideNav') secondarySideNav!: MatSidenav;

    filterItems!: SelectableListModel[];

    isCollapsed!: boolean;

    constructor(private actions$: Actions, private store: Store) {
        super();
    }

    ngOnInit(): void {
        this.initSubscriptions();
        this.store.dispatch(new SelectDefaultCategory());
    }

    onCreateNew(newItemName: string): void {
        if (newItemName) {
            this.store.dispatch(new CreateCategory(newItemName));
        } else {
            this.store.dispatch(new NavigateToCategory(0));
        }
    }

    onCreatingMode(): void {
        this.store.dispatch(new Navigate(['categories']));
    }

    private initSubscriptions(): void {
        this.subscriptions.push(
            this.actions$
                .pipe(ofActionCompleted(CreateCategory))
                .subscribe(() => this.store.dispatch(new SetSuccess('Категория создана'))),
            this.actions$
                .pipe(ofActionCompleted(UpdateCategory))
                .subscribe(() => this.store.dispatch(new SetSuccess('Категория обнавлена'))),
            this.actions$
                .pipe(ofActionCompleted(DeleteCategory))
                .subscribe(() => this.store.dispatch(new SetSuccess('Категория удалена'))),
            this.actions$
                .pipe(ofActionDispatched(NavigateToCategory))
                .subscribe(({ id }: NavigateToCategory) =>
                    this.store.dispatch(new Navigate(['categories', id])),
                ),
            this.actions$
                .pipe(ofActionDispatched(NavigateToCategoriesList))
                .subscribe(() => this.store.dispatch(new Navigate(['categories']))),
        );
    }
}
