import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Observable, of, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { IHaveChanges } from '@common/guards/unsaved-changes.guard';

import { ObserverComponent } from '@common/abstract/observer.component';
import {
    UpdateCategory,
    DeleteCategory,
    CreateSubCategory,
    UpdateSubCategory,
    NavigateToCategory,
    MoveCategory,
} from '@common/auth/store/categories/categories.actions';
import { CategoryModel } from '@common/models/category.model';
import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';
import { ImageHelper } from '@common/image-upload/helpers/image.helper';
import { DirectoryListModel } from '@common/models/directory-list.model';
import { DirectoryType } from '@common/enums/directory-type.enum';
import { ListModel } from '@common/models/list.model';
import { DirectoryModel } from '@common/models/directory.model';
import { CategoriesQuery } from '@common/auth/store/categories/categories.query';
import { AuthState } from '@common/auth/store/auth.state';
import { BreadcrumbsState } from '@common/breadcrumbs/store/breadcrumbs.state';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { FolderSelectDialogComponent } from '@common/folder-select-dialog/folder-select-dialog.component';

@Component({
    selector: 'category-page',
    templateUrl: 'category-page.component.html',
    styleUrls: ['category-page.component.scss'],
})
export class CategoryPageComponent extends ObserverComponent implements IHaveChanges, OnInit {
    @Select(CategoriesQuery.selectedCategory)
    category$!: Observable<CategoryModel>;

    @Select(BreadcrumbsState.breadcrumbs)
    breadcrumbs$!: Observable<TodoItemFlatNode[]>;

    @Select(BreadcrumbsState.parent)
    parent$!: Observable<CategoryModel>;

    @Select(AuthState.isMobile) isMobile$!: Observable<boolean>;

    categoryGroup!: FormGroup;

    subCategoryFormArray!: FormArray;

    category!: CategoryModel;

    isAfterDeleted!: boolean;

    isMobile!: boolean;

    title = 'Вид оборудования';

    constructor(
        private dialog: MatDialog,
        private ref: ChangeDetectorRef,
        private store: Store,
        private fb: FormBuilder,
    ) {
        super();
    }

    ngOnInit(): void {
        this.initSubscriptions();
    }

    haveChanges(): boolean {
        return this.categoryGroup && this.categoryGroup.dirty;
    }

    onCancel(): void {
        this.openUnsavedChangesModal().subscribe((result) => {
            if (result) {
                this.categoryGroup = this.createCategoryFormGroup(this.category);
            }
        });
    }

    onChangeCategory(folderId: number): void {
        this.openChangeCategoryModal(folderId).subscribe(
            (result: { parentId: number; categoryIds: number[] }) => {
                if (result) {
                    this.store.dispatch(new MoveCategory(result.parentId, result.categoryIds));
                }
            },
        );
    }

    onSave(): void {
        if (this.categoryGroup.invalid) {
            return;
        }
        const saveModel = new DirectoryModel({
            ...this.categoryGroup.value,
            value: this.categoryGroup.value.name,
            type: DirectoryType.Category,
            image: ImageHelper.isBase64(this.categoryGroup.controls['image'].value)
                ? null
                : this.categoryGroup.controls['image'].value,
        });

        const models: ListModel[] = [...this.subCategoryFormArray.value];
        const createModels = models.filter((x) => x.id === 0);
        const updateModels = models.filter((x) => x.id !== 0);
        this.store.dispatch([
            new UpdateCategory(saveModel),
            new UpdateSubCategory(
                new DirectoryListModel({
                    type: DirectoryType.Category,
                    entries: updateModels,
                    parentId: this.category.id,
                    level: this.category.level,
                }),
            ),
            new CreateSubCategory(
                new DirectoryListModel({
                    type: DirectoryType.Category,
                    entries: createModels,
                    parentId: this.category.id,
                    level: this.category.level + 1,
                }),
            ),
        ]);
        this.categoryGroup.markAsPristine();
    }

    onDelete(): void {
        this.openConfirmDeleteDialog().subscribe((result) => {
            if (result) {
                this.isAfterDeleted = true;
                this.categoryGroup.markAsPristine();
                this.store.dispatch(new DeleteCategory(this.category.id));
            }
        });
    }

    onCreateSubCategory(): void {
        this.subCategoryFormArray.push(
            this.fb.group({
                id: [0],
                name: [null, Validators.required],
                image: [null],
            }),
        );
    }

    onRemoveSubCategory(index: number): void {
        if (index < 0 || index > this.subCategoryFormArray.length - 1) {
            return;
        }

        const value = this.subCategoryFormArray.at(index).value;
        if (value && value.id) {
            this.openConfirmDeleteDialog().subscribe((result) => {
                if (result) {
                    this.isAfterDeleted = true;
                    this.categoryGroup.markAsPristine();
                    this.store.dispatch(new DeleteCategory(value.id));
                    this.subCategoryFormArray.removeAt(index);
                }
            });
            return;
        }

        this.subCategoryFormArray.removeAt(index);
    }

    onNavigateFromBreadCrumb(model: any): void {
        this.onNavigateToSubCategory(model.id);
    }

    onNavigateToSubCategory(id: number): void {
        this.store.dispatch(new NavigateToCategory(id));
    }

    private initSubscriptions(): void {
        this.subscriptions.push(this.categorySubscription(), this.isMobileSubscription());
    }

    private isMobileSubscription(): Subscription {
        return this.isMobile$.subscribe((isMobile) => (this.isMobile = isMobile));
    }

    private categorySubscription(): Subscription {
        return this.category$.subscribe((value) => {
            this.category = value;
            if (!value) {
                this.store.dispatch(new Navigate(['categories']));
                return;
            }
            this.categoryGroup = this.createCategoryFormGroup(this.category);
            this.ref.detectChanges();
        });
    }

    private openChangeCategoryModal(forbiddenFolderId: number): Observable<any> {
        const allFolders = this.store.selectSnapshot(CategoriesQuery.allCategories);
        const dialogRef = this.dialog.open(FolderSelectDialogComponent, {
            data: {
                folderId: this.category.id,
                allFolders,
                forbiddenFolderId,
                root: 'Основные категории',
                title: 'Переместить',
            },
        });
        return dialogRef.afterClosed();
    }

    private openUnsavedChangesModal(): Observable<any> {
        if (this.categoryGroup.pristine) {
            return of(true);
        }
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Несохраненные изменения',
                message:
                    'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                confirmLabel: 'Отказаться',
                cancelLabel: 'Отменить',
            },
        });
        return dialogRef.afterClosed();
    }

    private openConfirmDeleteDialog(): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Подтверждение удаления',
                message: 'Выбранная категория будет удалена. Вы уверены, что хотите удалить её?',
            },
        });
        return dialogRef.afterClosed();
    }

    private createCategoryFormGroup(category: CategoryModel): FormGroup {
        this.subCategoryFormArray = this.createSubCategoryFormGroup(category.subCategory || []);
        return this.fb.group({
            id: [category.id],
            name: [category.name, Validators.required],
            image: [category.image],
            subCategory: this.subCategoryFormArray,
        });
    }

    private createSubCategoryFormGroup(categories: CategoryModel[]): FormArray {
        const formArray = this.fb.array([]);

        categories.forEach((categiry: CategoryModel) => {
            formArray.push(
                this.fb.group({
                    id: [categiry.id],
                    name: [categiry.name],
                    image: [categiry.image],
                }) as any,
            );
        });
        return formArray;
    }
}
