import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@common/guards/auth.guard';
import { NgModule } from '@angular/core';

export const childRoutes = [
    {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
        data: { icon: 'dashboard', text: 'Главная страница' },
    },
    {
        path: 'orders',
        loadChildren: () => import('./orders/orders.module').then((m) => m.OrdersModule),
        data: { icon: 'reorder', text: 'Заказы' },
    },
    {
        path: 'requests',
        loadChildren: () =>
            import('./price-request/price-request.module').then((m) => m.PriceRequestModule),
        data: { icon: 'request_quote', text: 'Запросы на цену' },
    },
    {
        path: 'send-messages',
        loadChildren: () =>
            import('./send-messages/send-messages.module').then((m) => m.SendMessagesModule),
        data: { icon: 'email', text: 'Отправка писем' },
    },
    {
        path: 'email-to-send',
        loadChildren: () =>
            import('./emails-to-send/emails-to-send.module').then((m) => m.EmailsToSendModule),
        data: { icon: 'folder_shared', text: 'Список почт' },
    },
    {
        path: 'tables',
        loadChildren: () => import('./tables/tables.module').then((m) => m.TablesModule),
        data: { icon: 'table_chart', text: 'Товары' },
    },
    {
        path: 'products',
        loadChildren: () => import('./product/product.module').then((m) => m.ProductModule),
        data: { icon: 'post_add', text: 'Добавить товар' },
    },
    {
        path: 'update-prices',
        loadChildren: () =>
            import('./update-prices/update-prices.module').then((m) => m.UpdatePricesModule),
        data: { icon: 'change_circle', text: 'Парсеры' },
    },
    {
        path: 'categories',
        loadChildren: () =>
            import('./categories/categories.module').then((m) => m.CategoriesModule),
        data: { icon: 'category', text: 'Тип оборудования' },
    },
    {
        path: 'brands',
        loadChildren: () => import('./brands/brands.module').then((m) => m.BrandsModule),
        data: { icon: 'group_work', text: 'Производители' },
    },
    {
        path: 'catalogs',
        loadChildren: () => import('./catalogs/catalogs.module').then((m) => m.CatalogsModule),
        data: { icon: 'picture_as_pdf', text: 'Каталоги' },
    },
    {
        path: 'countries',
        loadChildren: () => import('./countries/countries.module').then((m) => m.CountriesModule),
        data: { icon: 'location_city', text: 'Страны' },
    },
    {
        path: 'certificates',
        loadChildren: () =>
            import('./certificate/certificate.module').then((m) => m.CertificateModule),
        data: { icon: 'receipt_long', text: 'Сертификаты' },
    },
    {
        path: 'texts',
        loadChildren: () =>
            import('./text-editor/text-editor.module').then((m) => m.TextEditorModule),
        data: { icon: 'library_books', text: 'Публикации' },
    },
    {
        path: 'carousel',
        loadChildren: () => import('./carousel/carousel.module').then((m) => m.CarouselModule),
        data: { icon: 'burst_mode', text: 'Карусель' },
    },
];

const defaultPath = { path: '', redirectTo: 'dashboard', pathMatch: 'full' } as any;

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [...childRoutes, defaultPath],
    },
    { path: '**', redirectTo: '' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule],
    providers: [AuthGuard],
})
export class AdminRoutingModule {}
