import { Injectable } from '@angular/core';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { CatalogComponent } from '../components/catalog/catalog.component';

@Injectable()
export class CatalogsUnsavedChangesGuard extends UnsavedChangesGuard<CatalogComponent> {}
