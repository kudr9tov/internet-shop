import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CatalogsComponent } from './components/catalogs/catalogs.component';
import { CatalogComponent } from './components/catalog/catalog.component';
import { SecondaryMenuModule } from '@common/secondary-menu/secondary-menu.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrandsDataModule } from '@common/brands/brands-data.module';
import { CatalogsRoutingModule } from './catalogs-routing.module';
import { CatalogsApiService } from './services/catalogs-api.service';
import { NgxsModule } from '@ngxs/store';
import { CatalogsState } from './store/catalogs.state';
import { CardItemModule } from '@common/app/card-item/card-item.module';

@NgModule({
    declarations: [CatalogsComponent, CatalogComponent],
    imports: [
        CommonModule,
        NgxsModule.forFeature([CatalogsState]),
        BrandsDataModule,
        MatSidenavModule,
        FormsModule,
        MatButtonModule,
        ImageUploadModule,
        MatFormFieldModule,
        MatIconModule,
        CardItemModule,
        MatInputModule,
        MatToolbarModule,
        ReactiveFormsModule,
        SecondaryMenuModule,
        CatalogsRoutingModule,
    ],
    providers: [CatalogsApiService],
})
export class CatalogsModule {}
