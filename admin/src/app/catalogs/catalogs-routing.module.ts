import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogComponent } from './components/catalog/catalog.component';
import { CatalogsComponent } from './components/catalogs/catalogs.component';
import { CatalogResolver } from './resolvers/catalog.resolver';
import { CatalogsUnsavedChangesGuard } from './guards/catalogs-unsaved-changes.guard';
import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';
import { BrandsResolver } from '@common/brands/resolvers/brands.resolver';

const routes: Routes = [
    {
        path: '',
        component: CatalogsComponent,
        resolve: {
            brandsResolvar: BrandsResolver,
        },
        children: [
            {
                path: ':id',
                component: CatalogComponent,
                resolve: {
                    catalogResolvar: CatalogResolver,
                },
                canDeactivate: [CatalogsUnsavedChangesGuard],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [BrandsResolver, CatalogResolver, CatalogsUnsavedChangesGuard, UnsavedChangesGuard],
})
export class CatalogsRoutingModule {}
