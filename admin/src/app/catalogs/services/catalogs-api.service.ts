import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseApiService } from '@common/abstract/base-api.service';
import { Observable } from 'rxjs';
import { CatalogModel } from '@common/models/catalog.model';
import { BrandModel } from '@common/models/brand.model';

@Injectable()
export class CatalogsApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(id: number): Observable<any> {
        return this.httpGet(`catalog/brand/${id}`, (x) => new CatalogModel(x));
    }

    public create(models: BrandModel): Observable<any> {
        return this.httpPost('catalog', (x) => new BrandModel(x), models);
    }

    public update(models: BrandModel): Observable<any> {
        return this.httpPut('catalog', (x) => new BrandModel(x), models);
    }

    public delete(id: number): Observable<any> {
        return this.httpDelete('catalog', id, (x) => x);
    }
}
