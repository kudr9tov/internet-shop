import { TestBed } from '@angular/core/testing';

import { CatalogsApiService } from './catalogs-api.service';

describe('CatalogsApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: CatalogsApiService = TestBed.get(CatalogsApiService);
        expect(service).toBeTruthy();
    });
});
