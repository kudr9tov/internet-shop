import { Component, OnInit, ViewChild } from '@angular/core';
import { ObserverComponent } from '@common/abstract/observer.component';
import { Select, Actions, Store, ofActionCompleted, ofActionDispatched } from '@ngxs/store';
import { BrandsQuery } from '@common/brands/store/brands.query';
import { Observable } from 'rxjs';
import { ListModel } from '@common/models/list.model';
import { BrandsState } from '@common/brands/store/brands.state';
import { SecondaryMenuState } from '@common/secondary-menu/store/secondary-menu.state';
import { MatSidenav } from '@angular/material/sidenav';
import {
    SelectDefaultBrand,
    CreateBrand,
    NavigateToBrand,
    UpdateBrands,
    DeleteBrand,
    NavigateToBrandsList,
} from '@common/brands/store/brands.actions';
import { Navigate } from '@ngxs/router-plugin';
import { SetSuccess } from '@common/toast';
import { SelectableListModel } from '@common/models/selectable-list.model';

@Component({
    selector: 'catalogs',
    templateUrl: './catalogs.component.html',
    styleUrls: ['./catalogs.component.scss'],
})
export class CatalogsComponent extends ObserverComponent implements OnInit {
    @Select(BrandsQuery.brands)
    brands$!: Observable<ListModel[]>;

    @Select(BrandsState.selectedBrand)
    brand$!: Observable<ListModel>;

    @Select(SecondaryMenuState.isSecondaryCollapsed)
    isCollapsed$!: Observable<boolean>;

    @ViewChild('secondarySideNav') secondarySideNav!: MatSidenav;

    filterItems!: SelectableListModel[];

    isCollapsed!: boolean;

    constructor(private actions$: Actions, private store: Store) {
        super();
    }

    ngOnInit(): void {
        this.initSubscriptions();
        this.store.dispatch(new SelectDefaultBrand());
    }

    private initSubscriptions(): void {
        this.subscriptions.push(
            this.actions$
                .pipe(ofActionCompleted(CreateBrand))
                .subscribe(() => this.store.dispatch(new SetSuccess('Бренд создана'))),
            this.actions$
                .pipe(ofActionCompleted(UpdateBrands))
                .subscribe(() => this.store.dispatch(new SetSuccess('Бренд обнавлена'))),
            this.actions$
                .pipe(ofActionCompleted(DeleteBrand))
                .subscribe(() => this.store.dispatch(new SetSuccess('Бренд удалена'))),
            this.actions$
                .pipe(ofActionDispatched(NavigateToBrand))
                .subscribe(({ id }: NavigateToBrand) =>
                    this.store.dispatch(new Navigate(['catalogs', id])),
                ),
            this.actions$
                .pipe(ofActionDispatched(NavigateToBrandsList))
                .subscribe(() => this.store.dispatch(new Navigate(['catalogs']))),
        );
    }
}
