import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormArray, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Observable, Subscription, of } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { CatalogModel } from '@common/models/catalog.model';
import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';
import { FileType } from '@common/enums/file-type.enum';
import { ObserverComponent } from '@common/abstract/observer.component';

import { CatalogsState } from '../../store/catalogs.state';
import { DeleteCatalog, CreateCatalogs, UpdateCatalogs } from '../../store/catalogs.actions';

@Component({
    selector: 'catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.scss'],
})
export class CatalogComponent extends ObserverComponent implements OnInit {
    @Select(CatalogsState.catalogs)
    catalogs$!: Observable<CatalogModel[]>;

    formats = [FileType.Pdf];

    catalogs!: CatalogModel[];

    catalogsArray!: FormArray;

    constructor(
        private dialog: MatDialog,
        private ref: ChangeDetectorRef,
        private store: Store,
        private fb: FormBuilder,
    ) {
        super();
    }

    ngOnInit(): void {
        this.subscriptions.push(this.catalogsSubscription());
    }

    haveChanges(): boolean {
        return this.catalogsArray && this.catalogsArray.dirty;
    }

    onCreateCatalog(): void {
        this.catalogsArray.push(
            this.fb.group({
                id: [0],
                name: [null, Validators.required],
                image: [null],
                brandId: [0],
                brand: [null],
            }),
        );
    }

    onRemoveCatalog(index: number): void {
        this.openConfirmDeleteDialog().subscribe((result) => {
            if (result) {
                if (index < 0 || index > this.catalogsArray.length - 1) {
                    return;
                }
                const catalog = this.catalogsArray.at(index);
                this.catalogsArray.removeAt(index);
                this.catalogsArray.markAsPristine();
                this.store.dispatch(new DeleteCatalog(catalog.value.id));
            }
        });
    }

    onCancel(): void {
        this.openUnsavedChangesModal().subscribe((result) => {
            if (result) {
                this.catalogsArray = this.createCatalogArray(this.catalogs);
            }
        });
    }

    onSave(): void {
        if (this.catalogsArray.invalid) {
            return;
        }
        const models: CatalogModel[] = this.catalogsArray.value.map(
            (x: any) => new CatalogModel({ ...x, catalogFileUrl: x.image }),
        );
        const createModels = models.filter((x) => x.id === 0);
        const updateModels = models.filter((x) => x.id !== 0);
        this.store.dispatch([new UpdateCatalogs(updateModels), new CreateCatalogs(createModels)]);

        this.catalogsArray.markAsPristine();
    }

    private openConfirmDeleteDialog(): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Подтверждение удаления',
                message: 'Выбранный элемент будет удален. Вы уверены, что хотите удалить его?',
            },
        });
        return dialogRef.afterClosed();
    }

    private catalogsSubscription(): Subscription {
        return this.catalogs$.subscribe((value) => {
            this.catalogs = value;
            if (!value) {
                this.store.dispatch(new Navigate(['catalogs']));
                return;
            }
            this.catalogsArray = this.createCatalogArray(value);
            this.ref.detectChanges();
        });
    }

    private createCatalogArray(catalogs: CatalogModel[]): FormArray {
        const formArray = this.fb.array([]);
        if (!catalogs || !catalogs.length) {
            return formArray;
        }
        catalogs.forEach((categiry: CatalogModel) => {
            formArray.push(
                this.fb.group({
                    id: [categiry.id],
                    name: [categiry.name, Validators.required],
                    image: [categiry.catalogFileUrl],
                    brandId: [categiry.brandId],
                    brand: [categiry.brand],
                }) as any,
            );
        });
        return formArray;
    }

    private openUnsavedChangesModal(): Observable<any> {
        if (this.catalogsArray.pristine) {
            return of(true);
        }
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Несохраненные изменения',
                message:
                    'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                confirmLabel: 'Отказаться',
                cancelLabel: 'Отменить',
            },
        });
        return dialogRef.afterClosed();
    }
}
