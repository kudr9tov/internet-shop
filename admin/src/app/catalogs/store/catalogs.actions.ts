import { CatalogModel } from '@common/models/catalog.model';

export class LoadCatalogs {
    static readonly type = '[Catalogs] Load Catalogs';

    constructor(public id: number) {}
}

export class CreateCatalogs {
    static readonly type = '[Carousel] Create Catalogs';

    constructor(public models: CatalogModel[]) {}
}

export class UpdateCatalogs {
    static readonly type = '[Carousel] Update Catalogs';

    constructor(public models: CatalogModel[]) {}
}

export class DeleteCatalog {
    static readonly type = '[Catalogs] Delete Catalog';

    constructor(public id: number) {}
}
