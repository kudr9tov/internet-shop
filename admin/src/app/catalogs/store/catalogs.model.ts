import { CatalogModel } from '@common/models/catalog.model';

export interface CatalogsStateModel {
    catalogs: CatalogModel[];
    selectedId: number | null;
}
