import { State, Action, Selector, StateContext } from '@ngxs/store';

import { CatalogsStateModel } from './catalogs.model';
import { CatalogsApiService } from '../services/catalogs-api.service';
import { CatalogModel } from '@common/models/catalog.model';
import { LoadCatalogs, DeleteCatalog, CreateCatalogs, UpdateCatalogs } from './catalogs.actions';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { patch, removeItem, append } from '@ngxs/store/operators';
import { BrandModel } from '@common/models/brand.model';

@State<CatalogsStateModel>({
    name: 'catalogsState',
    defaults: {
        catalogs: [],
        selectedId: null,
    },
})
@Injectable()
export class CatalogsState {
    @Selector()
    static catalogs({ catalogs }: CatalogsStateModel): CatalogModel[] {
        return catalogs || [];
    }

    constructor(private apiService: CatalogsApiService) {}

    @Action(LoadCatalogs)
    onLoadCatalogs(
        { patchState }: StateContext<CatalogsStateModel>,
        { id }: LoadCatalogs,
    ): Observable<any> {
        return this.apiService.get(id).pipe(
            tap((catalogs: CatalogModel[]) => {
                patchState({
                    catalogs,
                    selectedId: id,
                });
            }),
        );
    }

    @Action(CreateCatalogs)
    onCreateCatalogs(
        { setState, getState }: StateContext<CatalogsStateModel>,
        { models }: CreateCatalogs,
    ): Observable<any> {
        if (!models || !models.length) {
            return of();
        }
        const { selectedId } = getState() as any;
        const model = new BrandModel({ brandId: selectedId, catalogs: models });
        return this.apiService.create(model).pipe(
            tap((catalogs: CatalogModel[]) => {
                tap(() =>
                    setState(
                        patch({
                            catalogs: append(catalogs),
                        }),
                    ),
                );
            }),
        );
    }

    @Action(UpdateCatalogs)
    onUpdateCatalogs(
        { setState, getState }: StateContext<CatalogsStateModel>,
        { models }: UpdateCatalogs,
    ): Observable<any> {
        if (!models || !models.length) {
            return of();
        }
        const { selectedId, catalogs } = getState() as any;
        const model = new BrandModel({ brandId: selectedId, catalogs: models });
        const updatedIds = models.map((x) => x.id);
        const updatedCatalogs = catalogs.filter((x: { id: number }) => updatedIds.includes(x.id));
        return this.apiService.update(model).pipe(
            tap(() => {
                tap(() =>
                    setState(
                        patch({
                            catalogs: [...updatedCatalogs, ...models],
                        }),
                    ),
                );
            }),
        );
    }

    @Action(DeleteCatalog)
    onDeleteOrder(
        { setState }: StateContext<CatalogsStateModel>,
        { id }: DeleteCatalog,
    ): Observable<any> {
        return this.apiService.delete(id).pipe(
            tap(() =>
                setState(
                    patch({
                        catalogs: removeItem<CatalogModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }
}
