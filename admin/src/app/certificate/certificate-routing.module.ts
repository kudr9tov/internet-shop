import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { CertificateComponent } from './components/certificate/certificate.component';
import { CertificateUnsavedChangesGuard } from './guards/certificate-unsaved-changes.guard';

const ROUTES: Routes = [
    {
        path: '',
        component: CertificateComponent,
        canDeactivate: [CertificateUnsavedChangesGuard],
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
    providers: [CertificateUnsavedChangesGuard, UnsavedChangesGuard],
})
export class CertificateRoutingModule {}
