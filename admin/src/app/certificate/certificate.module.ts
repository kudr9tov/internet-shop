import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';
import { NgxsModule } from '@ngxs/store';
import { CertificateRoutingModule } from './certificate-routing.module';
import { CertificateComponent } from './components/certificate/certificate.component';
import { CertificateState } from './store/certificate.state';
import { CertificateApiService } from './services/certificate-api.service';
import { CardItemModule } from '@common/app/card-item/card-item.module';

@NgModule({
    declarations: [CertificateComponent],
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        ImageUploadModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        NgxsModule.forFeature([CertificateState]),
        ReactiveFormsModule,
        CardItemModule,
        CertificateRoutingModule,
    ],
    providers: [CertificateApiService],
})
export class CertificateModule {}
