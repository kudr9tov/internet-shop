import { Injectable } from '@angular/core';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { CertificateComponent } from '../components/certificate/certificate.component';

@Injectable()
export class CertificateUnsavedChangesGuard extends UnsavedChangesGuard<CertificateComponent> {}
