import { ListModel } from '@common/models/list.model';

export interface CertificateStateModel {
    certificates: ListModel[];
}
