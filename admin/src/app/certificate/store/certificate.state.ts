import { Action, Selector, State, StateContext } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { DirectoryType } from '@common/enums/directory-type.enum';
import { DirectoryModel } from '@common/models/directory.model';
import { ListModel } from '@common/models/list.model';

import { CertificateApiService } from '../services/certificate-api.service';
import {
    CreateCertificate,
    DeleteCertificate,
    LoadCertificate,
    UpdateCertificate,
} from './certificate.actions';

import { CertificateStateModel } from './certificate.model';

@State<CertificateStateModel>({
    name: 'certificateState',
    defaults: {
        certificates: [],
    },
})
@Injectable()
export class CertificateState {
    @Selector()
    static certificates({ certificates }: CertificateStateModel): ListModel[] {
        return certificates || [];
    }

    constructor(private apiService: CertificateApiService) {}

    @Action(LoadCertificate)
    onLoadCertificate({ patchState }: StateContext<CertificateStateModel>): Observable<any> {
        return this.apiService.get().pipe(
            tap((certificates: ListModel[]) => {
                patchState({ certificates });
            }),
        );
    }

    @Action(CreateCertificate)
    onCreateCertificate(
        { setState }: StateContext<CertificateStateModel>,
        { model }: CreateCertificate,
    ): Observable<any> {
        if (!model.entries.length) {
            return of();
        }
        return this.apiService.create(model).pipe(
            tap((certificates: ListModel[]) => {
                if (!certificates || !certificates.length) {
                    return;
                }
                setState(
                    patch({
                        certificates: append(certificates),
                    }),
                );
            }),
        );
    }

    @Action(UpdateCertificate)
    onUpdateCertificate(
        { setState }: StateContext<CertificateStateModel>,
        { model }: UpdateCertificate,
    ): Observable<any> {
        if (!model.entries || !model.entries.length) {
            return of();
        }
        return this.apiService.update(model).pipe(
            tap(() => {
                model.entries.forEach((item) => {
                    setState(
                        patch({
                            certificates: updateItem<ListModel>(
                                (s) => s.id === item.id,
                                patch({ name: item.name, image: item.image }),
                            ),
                        }),
                    );
                });
            }),
        );
    }

    @Action(DeleteCertificate)
    onDeleteCertificate(
        { setState }: StateContext<CertificateStateModel>,
        { id }: DeleteCertificate,
    ): Observable<any> {
        const model = new DirectoryModel({
            id,
            type: DirectoryType.Certificate,
        });
        return this.apiService.delete(model).pipe(
            tap(() =>
                setState(
                    patch({
                        certificates: removeItem<ListModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }
}
