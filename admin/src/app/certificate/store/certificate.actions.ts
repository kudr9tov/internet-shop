import { DirectoryListModel } from '@common/models/directory-list.model';

export class LoadCertificate {
    static readonly type = '[Certificate] Load Certificate';
}

export class CreateCertificate {
    static readonly type = '[Certificate] Create Certificate';

    constructor(public model: DirectoryListModel) {}
}

export class UpdateCertificate {
    static readonly type = '[Certificate] Update Certificate';

    constructor(public model: DirectoryListModel) {}
}

export class DeleteCertificate {
    static readonly type = '[Certificate] delete Certificate';

    constructor(public id: number) {}
}
