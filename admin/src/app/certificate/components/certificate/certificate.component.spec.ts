import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CertificateComponent } from './certificate.component';

describe('CertificateComponent', () => {
    let component: CertificateComponent;
    let fixture: ComponentFixture<CertificateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CertificateComponent],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(CertificateComponent);
        component = fixture.componentInstance;
    }));

    afterEach(() => {
        fixture.destroy();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
