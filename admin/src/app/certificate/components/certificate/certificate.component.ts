import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';
import { DirectoryListModel } from '@common/models/directory-list.model';
import { DirectoryType } from '@common/enums/directory-type.enum';
import { ListModel } from '@common/models/list.model';
import { ObserverComponent } from '@common/abstract/observer.component';

import { CertificateState } from '../../store/certificate.state';
import {
    CreateCertificate,
    DeleteCertificate,
    LoadCertificate,
    UpdateCertificate,
} from '../../store/certificate.actions';

@Component({
    selector: 'certificate',
    templateUrl: './certificate.component.html',
    styleUrls: ['./certificate.component.scss'],
})
export class CertificateComponent extends ObserverComponent implements OnInit {
    @Select(CertificateState.certificates)
    certificates$!: Observable<ListModel[]>;

    certificateFormArray!: FormArray;

    certificates!: ListModel[];

    isAfterDeleted!: boolean;

    constructor(private dialog: MatDialog, private store: Store, private fb: FormBuilder) {
        super();
    }

    ngOnInit(): void {
        this.store.dispatch(new LoadCertificate());
        this.subscriptions.push(this.certificateSubscription());
    }

    haveChanges(): boolean {
        return this.certificateFormArray && this.certificateFormArray.dirty;
    }

    onCancel(): void {
        this.openUnsavedChangesModal().subscribe((result) => {
            if (result) {
                this.certificateFormArray = this.createCertificateFormArray(this.certificates);
            }
        });
    }

    onSave(): void {
        if (this.certificateFormArray.invalid) {
            return;
        }
        const models: ListModel[] = [...this.certificateFormArray.value];
        const createModels = models.filter((x) => x.id === 0);
        const updateModels = models.filter((x) => x.id !== 0);
        const actions = [];
        if (createModels.length) {
            actions.push(
                new CreateCertificate(
                    new DirectoryListModel({
                        type: DirectoryType.Certificate,
                        entries: createModels,
                    }),
                ),
            );
        }
        if (updateModels.length) {
            actions.push(
                new UpdateCertificate(
                    new DirectoryListModel({
                        type: DirectoryType.Certificate,
                        entries: updateModels,
                    }),
                ),
            );
        }
        this.store.dispatch(actions);
        this.certificateFormArray.markAsPristine();
    }

    onCreateCertificate(): void {
        this.certificateFormArray.push(
            this.fb.group({
                id: [0],
                name: [null],
                image: [null],
            }),
        );
    }

    onRemoveCertificate(index: number): void {
        if (index < 0 || index > this.certificateFormArray.length - 1) {
            return;
        }
        const listModel = this.certificateFormArray.at(index).value;

        if (listModel.id !== 0) {
            this.openConfirmDeleteDialog().subscribe((result) => {
                if (result) {
                    this.isAfterDeleted = true;
                    this.certificateFormArray.removeAt(index);
                    this.certificateFormArray.markAsPristine();
                    this.store.dispatch(new DeleteCertificate(listModel.id));
                }
            });
        } else {
            this.certificateFormArray.removeAt(index);
        }
    }

    private certificateSubscription(): Subscription {
        return this.certificates$.subscribe((value) => {
            this.certificates = value;
            this.certificateFormArray = this.createCertificateFormArray(this.certificates);
        });
    }

    private openUnsavedChangesModal(): Observable<any> {
        if (this.certificateFormArray.pristine) {
            return of(true);
        }
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Несохраненные изменения',
                message:
                    'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                confirmLabel: 'Отказаться',
                cancelLabel: 'Отменить',
            },
        });
        return dialogRef.afterClosed();
    }

    private openConfirmDeleteDialog(): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Подтверждение удаления',
                message: 'Выбранный сертефикат будет удален. Вы уверены, что хотите удалить его?',
            },
        });
        return dialogRef.afterClosed();
    }

    private createCertificateFormArray(certificates: ListModel[]): FormArray {
        const formArray = this.fb.array([]);

        (certificates && certificates.length ? certificates : []).forEach(
            (certificate: ListModel) => {
                formArray.push(
                    this.fb.group({
                        id: [certificate.id],
                        name: [certificate.name],
                        image: [certificate.image],
                    }) as any,
                );
            },
        );
        return formArray;
    }
}
