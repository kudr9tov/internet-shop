import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BrandsResolver } from '@common/brands/resolvers/brands.resolver';
import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { BrandsUnsavedChangesGuard } from './guards/brands-unsaved-changes.guard';
import { BrandsPageComponent } from './components/brands-page/brands-page.component';

const ROUTES: Routes = [
    {
        path: '',
        component: BrandsPageComponent,
        resolve: {
            brandsResolvar: BrandsResolver,
        },
        canDeactivate: [BrandsUnsavedChangesGuard],
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
    providers: [BrandsResolver, BrandsUnsavedChangesGuard, UnsavedChangesGuard],
})
export class BrandsRoutingModule {}
