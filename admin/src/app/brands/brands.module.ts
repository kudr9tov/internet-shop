import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgModule } from '@angular/core';

import { BrandsDataModule } from '@common/brands/brands-data.module';
import { CardItemModule } from '@common/app/card-item/card-item.module';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';

import { BrandsRoutingModule } from './brands-routing.module';
import { BrandsPageComponent } from './components/brands-page/brands-page.component';

@NgModule({
    declarations: [BrandsPageComponent],
    imports: [
        BrandsRoutingModule,
        BrandsDataModule,
        MatSidenavModule,
        CommonModule,
        CardItemModule,
        FormsModule,
        MatButtonModule,
        ImageUploadModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        ReactiveFormsModule,
    ],
})
export class BrandsModule {}
