import { Injectable } from '@angular/core';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { BrandsPageComponent } from '../components/brands-page/brands-page.component';

@Injectable()
export class BrandsUnsavedChangesGuard extends UnsavedChangesGuard<BrandsPageComponent> {}
