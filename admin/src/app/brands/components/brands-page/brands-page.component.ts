import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, of } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { ObserverComponent } from '@common/abstract/observer.component';
import { BrandsQuery } from '@common/brands/store/brands.query';
import { ListModel } from '@common/models/list.model';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { UpdateBrands, CreateBrands, DeleteBrand } from '@common/brands/store/brands.actions';
import { DirectoryListModel } from '@common/models/directory-list.model';
import { DirectoryType } from '@common/enums/directory-type.enum';
import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'brands-page',
    templateUrl: 'brands-page.component.html',
    styleUrls: ['brands-page.component.scss'],
})
export class BrandsPageComponent extends ObserverComponent implements OnInit {
    @Select(BrandsQuery.brands)
    brands$!: Observable<ListModel[]>;

    brandsFormArray!: FormArray;

    brands!: ListModel[];

    isAfterDeleted!: boolean;

    title = 'Производитель';

    constructor(private dialog: MatDialog, private store: Store, private fb: FormBuilder) {
        super();
    }

    ngOnInit(): void {
        this.subscriptions.push(this.brandsSubscription());
    }

    haveChanges(): boolean {
        return this.brandsFormArray && this.brandsFormArray.dirty;
    }

    onCancel(): void {
        this.openUnsavedChangesModal().subscribe((result) => {
            if (result) {
                this.brandsFormArray = this.createBrandsFormArray(this.brands);
            }
        });
    }

    onSave(): void {
        if (this.brandsFormArray.invalid) {
            return;
        }
        const models: ListModel[] = [...this.brandsFormArray.value];
        const createModels = models.filter((x) => x.id === 0);
        const updateModels = models.filter((x) => x.id !== 0);
        this.store.dispatch([
            new UpdateBrands(
                new DirectoryListModel({
                    type: DirectoryType.Brands,
                    entries: updateModels,
                }),
            ),
            new CreateBrands(
                new DirectoryListModel({
                    type: DirectoryType.Brands,
                    entries: createModels,
                }),
            ),
        ]);
        this.brandsFormArray.markAsPristine();
    }

    onCreateBrand(): void {
        this.brandsFormArray.push(
            this.fb.group({
                id: [0],
                name: [null, Validators.required],
                image: [null],
            }),
        );
    }

    onRemoveBrand(index: number): void {
        if (index < 0 || index > this.brandsFormArray.length - 1) {
            return;
        }
        const listModel = this.brandsFormArray.at(index).value;

        if (listModel.id !== 0) {
            this.openConfirmDeleteDialog().subscribe((result) => {
                if (result) {
                    this.isAfterDeleted = true;
                    this.brandsFormArray.removeAt(index);
                    this.brandsFormArray.markAsPristine();
                    this.store.dispatch(new DeleteBrand(listModel.id));
                }
            });
        } else {
            this.brandsFormArray.removeAt(index);
        }
    }

    private brandsSubscription(): Subscription {
        return this.brands$.subscribe((value) => {
            this.brands = value;
            this.brandsFormArray = this.createBrandsFormArray(this.brands);
        });
    }

    private openUnsavedChangesModal(): Observable<any> {
        if (this.brandsFormArray.pristine) {
            return of(true);
        }
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Несохраненные изменения',
                message:
                    'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                confirmLabel: 'Отказаться',
                cancelLabel: 'Отменить',
            },
        });
        return dialogRef.afterClosed();
    }

    private openConfirmDeleteDialog(): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Подтверждение удаления',
                message: 'Выбранный бренд будет удалена. Вы уверены, что хотите удалить её?',
            },
        });
        return dialogRef.afterClosed();
    }

    private createBrandsFormArray(brands: ListModel[]): FormArray {
        const formArray = this.fb.array([]);

        brands.forEach((brand: ListModel) =>
            formArray.push(
                this.fb.group({
                    id: [brand.id],
                    name: [brand.name, Validators.required],
                    image: [brand.image],
                }) as any,
            ),
        );
        return formArray;
    }
}
