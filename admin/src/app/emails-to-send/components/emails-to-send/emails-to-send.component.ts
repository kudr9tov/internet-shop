import { Component, OnInit } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ObserverComponent } from '@common/abstract/observer.component';
import { EmailSearchModel } from '@common/models/email-search.model';
import { FilterModel } from '@common/models/filter.model';
import { SenderModel } from '@common/models/sender.model';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { LoadEmails, SetPageSize } from '../../store/emails-to-send.actions';
import { EmailsToSendState } from '../../store/emails-to-send.state';

@Component({
    selector: 'emails-to-send',
    templateUrl: './emails-to-send.component.html',
    styleUrls: ['./emails-to-send.component.scss'],
})
export class EmailsToSendComponent extends ObserverComponent implements OnInit {
    @Select(EmailsToSendState.senders) requests$!: Observable<SenderModel[]>;

    @Select(EmailsToSendState.count) count$!: Observable<number>;

    @Select(EmailsToSendState.currentPage) currentPage$!: Observable<number>;

    @Select(EmailsToSendState.pageSize) pageSize$!: Observable<number>;

    displayedColumns = [
        'lastSendMessage',
        'email',
        'phone',
        'director',
        'isSend',
        'isSendTemplate',
    ];

    dataSource!: MatTableDataSource<SenderModel>;

    pageSizeOptions: number[] = [5, 10, 25, 100];

    filter!: FilterModel;

    sort!: FilterModel;

    constructor(public store: Store) {
        super();
    }

    ngOnInit(): void {
        this.store.dispatch(new LoadEmails(new EmailSearchModel()));
        this.subscriptions.push(this.sendersSubscription());
    }

    onChangePaginator(event: any): void {
        this.store.dispatch([
            new SetPageSize(event.pageSize || 0),
            new LoadEmails(
                new EmailSearchModel({
                    limit: event.pageSize || 0,
                    page: event.pageIndex + 1,
                }),
            ),
        ]);
    }

    private sendersSubscription(): Subscription {
        return this.requests$.subscribe((senders: SenderModel[]) => {
            if (!senders) {
                return;
            }
            const newSenders = senders.map((x) => new SenderModel({ ...x }));
            this.dataSource = new MatTableDataSource([...newSenders]);
        });
    }

    sortData(sort: Sort) {
        if (!sort.active || sort.direction === '') {
            return;
        }
        this.sort = new FilterModel({
            type: sort.direction.toUpperCase() === 'ASC' ? 'ASC' : 'DESC',
            filterColumn: sort.active,
        });
        const filters = [this.sort];
        if (this.filter) {
            filters.push(this.filter);
        }
        this.store.dispatch(new LoadEmails(new EmailSearchModel({ filters })));
    }

    applyFilter(event: any): void {
        const filterValue = event.target.value;
        if (!this.dataSource) {
            return;
        }
        this.filter = new FilterModel({
            queryString: filterValue.trim().toLowerCase(),
            type: 'QUERY',
            filterColumn: 'email',
        });
        const filters = [this.filter];
        if (this.sort) {
            filters.push(this.sort);
        }
        this.store.dispatch(new LoadEmails(new EmailSearchModel({ filters })));
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
}
