import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { EmailsToSendComponent } from './emails-to-send.component';

describe('EmailsToSendComponent', () => {
    let component: EmailsToSendComponent;
    let fixture: ComponentFixture<EmailsToSendComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EmailsToSendComponent],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(EmailsToSendComponent);
        component = fixture.componentInstance;
    }));

    afterEach(() => {
        fixture.destroy();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
