import { SenderModel } from '@common/models/sender.model';

export interface EmailsToSendStateModel {
    senders: SenderModel[];
    count: number;
    currentPage: number;
    pageSize: number;
}
