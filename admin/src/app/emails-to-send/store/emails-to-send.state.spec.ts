import { NgxsModule, Store } from '@ngxs/store';
import { TestBed } from '@angular/core/testing';

import { EmailsToSendState } from './emails-to-send.state';

describe('EmailsToSendState', () => {
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([EmailsToSendState])],
        });

        store = TestBed.get(Store);
        resetStore();
        resetMockProviders();
    });

    it('should create store', () => {
        expect(store).toBeTruthy();
    });

    function resetMockProviders(): void {}

    function resetStore(): void {
        store.reset({
            emailsToSendState: {},
        });
    }
});
