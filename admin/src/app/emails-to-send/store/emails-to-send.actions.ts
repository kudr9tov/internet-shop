import { EmailSearchModel } from '@common/models/email-search.model';

export class LoadEmails {
    static readonly type = '[Emails] Load emails to send';

    constructor(public model: EmailSearchModel) {}
}

export class SetPageSize {
    static readonly type = '[Emails] Set page size';

    constructor(public pageSize: number) {}
}
