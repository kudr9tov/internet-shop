import { Injectable } from '@angular/core';
import { SenderListModel } from '@common/models/sender-list.model';
import { SenderModel } from '@common/models/sender.model';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { EmailsToSendApiService } from '../services/emails-to-send-api.service';
import { LoadEmails, SetPageSize } from './emails-to-send.actions';

import { EmailsToSendStateModel } from './emails-to-send.model';

@State<EmailsToSendStateModel>({
    name: 'emailsToSendState',
    defaults: {
        senders: [],
        count: 0,
        currentPage: 0,
        pageSize: 25,
    },
})
@Injectable()
export class EmailsToSendState {
    @Selector()
    static senders({ senders }: EmailsToSendStateModel): SenderModel[] {
        return senders || [];
    }

    @Selector()
    static count({ count }: EmailsToSendStateModel): number {
        return count || 0;
    }

    @Selector()
    static currentPage({ currentPage }: EmailsToSendStateModel): number {
        return currentPage || 0;
    }

    @Selector()
    static pageSize({ pageSize }: EmailsToSendStateModel): number {
        return pageSize || 0;
    }

    constructor(private apiService: EmailsToSendApiService) {}

    @Action(LoadEmails)
    onLoadEmails(
        { patchState, getState }: StateContext<EmailsToSendStateModel>,
        { model }: LoadEmails,
    ): Observable<any> {
        if (!model.page) {
            model.page = getState().currentPage + 1;
        }
        if (!model.limit) {
            model.limit = getState().pageSize;
        }
        return this.apiService.get(model).pipe(
            tap((model: SenderListModel) => {
                patchState({
                    senders: model.mails,
                    count: model.count,
                    currentPage: model.page - 1,
                });
            }),
        );
    }

    @Action(SetPageSize)
    onSetPageSize(
        { patchState }: StateContext<EmailsToSendStateModel>,
        { pageSize }: SetPageSize,
    ): void {
        patchState({ pageSize });
    }
}
