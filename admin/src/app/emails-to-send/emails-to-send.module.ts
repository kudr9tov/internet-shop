import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

import { EmailsToSendApiService } from './services/emails-to-send-api.service';
import { EmailsToSendComponent } from './components/emails-to-send/emails-to-send.component';
import { EmailsToSendState } from './store/emails-to-send.state';
import { EmailsToSendRoutingModule } from './emails-to-send-routing.module';

@NgModule({
    declarations: [EmailsToSendComponent],
    imports: [
        CommonModule,
        EmailsToSendRoutingModule,
        NgxsModule.forFeature([EmailsToSendState]),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        MatSortModule,
        MatTableModule,
    ],
    providers: [EmailsToSendApiService],
})
export class EmailsToSendModule {}
