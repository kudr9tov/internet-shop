import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from '@common/abstract/base-api.service';
import { EmailSearchModel } from '@common/models/email-search.model';
import { SenderListModel } from '@common/models/sender-list.model';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Injectable()
export class EmailsToSendApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(model: EmailSearchModel): Observable<SenderListModel> {
        return this.httpPost('mail/mails', (x) => new SenderListModel(x), model);
    }
}
