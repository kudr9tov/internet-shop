import { TestBed } from '@angular/core/testing';

import { EmailsToSendApiService } from './emails-to-send-api.service';

describe('EmailsToSendApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: EmailsToSendApiService = TestBed.get(EmailsToSendApiService);
        expect(service).toBeTruthy();
    });
});
