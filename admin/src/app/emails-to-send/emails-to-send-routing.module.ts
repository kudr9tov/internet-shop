import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailsToSendComponent } from './components/emails-to-send/emails-to-send.component';

const routes: Routes = [
    {
        path: '',
        component: EmailsToSendComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class EmailsToSendRoutingModule {}
