import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';

import { AuthModule } from '@common/auth/auth.module';
import { ConfirmDialogModule } from '@common/confirm-dialog/confirm-dialog.module';
import { environment } from '@environments/environment';

import { AdminRoutingModule } from './admin-routing.module';
import { OverlayModule } from '@angular/cdk/overlay';
import { AppComponent } from './app.component';
import { TopNavComponent } from './layout/top-nav/top-nav.component';
import { SideNavComponent } from './layout/side-nav/side-nav.component';
import localeFr from '@angular/common/locales/fr';
import { ToastModule } from '@common/toast/toast.module';

registerLocaleData(localeFr, 'fr');

@NgModule({
    declarations: [AppComponent, TopNavComponent, SideNavComponent],
    imports: [
        NgxsModule.forRoot([], { developmentMode: !environment.production }),
        NgxsRouterPluginModule.forRoot(),
        environment.plugins,
        AdminRoutingModule,
        AuthModule,
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        ToastModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatSidenavModule,
        MatToolbarModule,
        OverlayModule,
        ConfirmDialogModule,
    ],
    bootstrap: [AppComponent],
    providers: [{ provide: LOCALE_ID, useValue: 'fr' }],
})
export class AdminModule {}
