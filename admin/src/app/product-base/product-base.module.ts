import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { CatalogDataModule } from '@common/catalogData/catalog-data.module';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';
import { BrandsDataModule } from '@common/brands/brands-data.module';
import { CountriesDataModule } from '@common/countries/countries-data.module';

import { MatListModule } from '@angular/material/list';
import { PipesModule } from '@common/pipes/pipes.module';
import { MatTreeModule } from '@angular/material/tree';
import { ExpandMode, NgxTreeSelectModule } from '@common/tree-select';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
    imports: [
        CommonModule,
        ImageUploadModule,
        BrandsDataModule,
        CountriesDataModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatTreeModule,
        ReactiveFormsModule,
        MatDividerModule,
        FormsModule,
        PipesModule,
        CatalogDataModule,
        MatFormFieldModule,
        MatButtonModule,
        MatSelectModule,
        NgxTreeSelectModule.forRoot({
            idField: 'id',
            textField: 'name',
            expandMode: ExpandMode.Selection,
        }),
    ],
})
export class ProductBaseModule {}
