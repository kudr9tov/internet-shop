import { OnInit, Directive } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';

import { BrandsQuery } from '@common/brands/store/brands.query';
import { CategoriesQuery } from '@common/auth/store/categories/categories.query';
import { CategoryModel } from '@common/models/category.model';
import { CountriesQuery } from '@common/countries/store/countries.query';
import { ListModel } from '@common/models/list.model';
import { LoadBrands } from '@common/brands/store/brands.actions';
import { LoadCategories } from '@common/auth/store/categories/categories.actions';
import { LoadCountries } from '@common/countries/store/countries.actions';
import { ObserverComponent } from '@common/abstract/observer.component';
import { ProductModel } from '@common/models/product.model';

@Directive()
export abstract class ProductBaseComponent extends ObserverComponent implements OnInit {
    @Select(CategoriesQuery.categories) categories$!: Observable<CategoryModel[]>;

    @Select(BrandsQuery.brands) brands$!: Observable<ListModel[]>;

    @Select(CountriesQuery.countries) counries$!: Observable<ListModel[]>;

    productGroup!: FormGroup;

    techProperties!: FormArray;

    AllowParentSelection = true;
    RestructureWhenChildSameName = false;
    ShowFilter = true;
    Disabled = false;

    categories!: CategoryModel[];

    product!: ProductModel;

    isArrowBack = false;

    FilterPlaceholder = 'Напишите здесь для поиска элементов...';

    items = [];

    abstract simpleSelected: CategoryModel;

    constructor(public fb: FormBuilder, public store: Store) {
        super();
    }
    ngOnInit(): void {
        this.setupFormGroups();
        this.store.dispatch([new LoadCategories(), new LoadBrands(), new LoadCountries()]);
        this.subscriptions.push(this.categoriesSubscription());
    }

    setupFormGroups(): void {
        this.productGroup = this.fb.group({
            id: [0],
            name: [null, [Validators.required]],
            price: null,
            discount: null,
            vendorCode: [null],
            description: [null],
            image: [null],
            brandId: null,
            countryId: null,
        });
        this.techProperties = this.fb.array([]);
    }

    onSave(): void {}

    onBack(): void {}

    onRoutingProduct(_: number): void {}

    protected categoriesSubscription(): Subscription {
        return this.categories$.subscribe((categories: CategoryModel[]) => {
            this.categories = categories;
            this.items = this.process(categories);
        });
    }

    process(data: any): any {
        let result = [];
        result = data.map((item: any) => {
            return this.toTreeNode(item);
        });
        return result;
    }

    toTreeNode(node: any) {
        if (node && node.children) {
            node.children.map((item: any) => {
                return this.toTreeNode(item);
            });
        }
        return node;
    }

    trackByFn(index: number, _: any): number {
        return index;
    }

    onAddProperty(): void {
        this.techProperties.push(
            this.fb.group({
                name: [null],
                value: [null],
            }),
        );
    }

    onRemoveProperty(index: number): void {
        if (index < 0 || index > this.techProperties.length - 1) {
            return;
        }

        this.techProperties.removeAt(index);
    }
}
