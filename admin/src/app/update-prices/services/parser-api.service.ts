import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from '@common/abstract/base-api.service';
import { CategoryAssotiationsModel } from '@common/models/category-associations.model';
import { ParserInfoModel } from '@common/models/parser-info.model';
import { ParserStartModel } from '@common/models/parser-start.model';
import { SiteModel } from '@common/models/site.model';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Injectable()
export class ParserApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public getAssociation(id: number): Observable<CategoryAssotiationsModel> {
        return this.httpGet(
            `scrapper/association?siteId=${id}`,
            (x) => new CategoryAssotiationsModel(x),
        );
    }

    public getLastInfo(id: number): Observable<ParserInfoModel[]> {
        return this.httpGet(`scrapper/executions?siteId=${id}`, (x) => new ParserInfoModel(x));
    }

    public getSites(): Observable<SiteModel[]> {
        return this.httpGet('scrapper/sites', (x) => new SiteModel(x));
    }

    public updateAssociation(model: any): Observable<any> {
        return this.httpPost('scrapper/association/update', (x) => x, model);
    }

    public startParsing(model: ParserStartModel): Observable<any> {
        return this.httpPost('scrapper/run', (x) => x, model);
    }

    public stopParsing(model: ParserStartModel): Observable<any> {
        return this.httpPost('scrapper/stop', (x) => x, model);
    }

    // public create(models: Slide[]): Observable<any> {
    //     return this.httpPost('carousel', x => new Slide(x), models);
    // }

    // public delete(id: number): Observable<any> {
    //     return this.httpDelete('carousel', id, x => x);
    // }
}
