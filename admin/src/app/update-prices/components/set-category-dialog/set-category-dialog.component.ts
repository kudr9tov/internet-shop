import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormArray, FormGroup } from '@angular/forms';
import { CategoryModel } from '@common/models/category.model';
import { UpdateAssociationModel } from '@common/models/update-association.model';

@Component({
    selector: 'set-category-dialog',
    templateUrl: 'set-category-dialog.component.html',
    styleUrls: ['set-category-dialog.component.scss'],
})
export class SetCategoryDialogComponent {
    associationsFromArray: FormArray;

    items: any = [];

    allowParentSelection = false;

    simpleSelected!: CategoryModel;
    ShowFilter = true;
    Disabled = false;

    FilterPlaceholder = 'Напишите здесь для поиска элементов...';

    constructor(
        public dialogRef: MatDialogRef<SetCategoryDialogComponent>,
        public ref: ChangeDetectorRef,

        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.associationsFromArray = this.data.associations || [];
        this.items = this.data.items || [];
    }

    onSave(): void {
        const models = this.associationsFromArray.controls.filter((x) => x.dirty);
        const result: UpdateAssociationModel[] = [];
        models.forEach((x) =>
            result.push(
                new UpdateAssociationModel({
                    currentCategoryId: x.value.currentCategoryId,
                    siteCategoryId: x.value.siteCategoryId,
                    exclude: x.value.isSiteCategoryExcluded,
                    action: x.value.isSiteCategoryExcluded
                        ? 'UPDATE'
                        : x.value.isNew
                        ? 'INSERT'
                        : 'UPDATE',
                }),
            ),
        );
        this.dialogRef.close(result);
    }

    onClose(): void {
        this.dialogRef.close();
    }

    valueChange(event: any, formGroup: FormGroup): void {
        formGroup.controls['currentCategoryId'].setValue(event ? event.id : null);
        formGroup.controls['currentCategoryName'].setValue(event ? event.name : null);
        formGroup.markAsDirty();
    }
}
