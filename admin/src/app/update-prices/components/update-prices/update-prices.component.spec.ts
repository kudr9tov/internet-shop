import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { UpdatePricesComponent } from './update-prices.component';

describe('UpdatePricesComponent', () => {
    let component: UpdatePricesComponent;
    let fixture: ComponentFixture<UpdatePricesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [UpdatePricesComponent],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(UpdatePricesComponent);
        component = fixture.componentInstance;
    }));

    afterEach(() => {
        fixture.destroy();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
