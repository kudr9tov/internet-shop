import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';

import { CategoriesQuery } from '@common/auth/store/categories/categories.query';

import {
    CleareHistory,
    LoadHistory,
    LoadСategoryAssociations,
    StartParsing,
    StopParsing,
    UpdateСategoryAssociations,
} from '../../store/update-prices.actions';
import { UpdatePricesState } from '../../store/update-prices.state';
import { SetCategoryDialogComponent } from '../set-category-dialog/set-category-dialog.component';
import { TreeHelper } from '@common/helpers/tree.helper';
import { CategoryModel } from '@common/models/category.model';
import { ObserverComponent } from '@common/abstract/observer.component';
import { ParserInfoModel } from '@common/models/parser-info.model';
import { AssociationModel } from '@common/models/association.model';
import { SiteModel } from '@common/models/site.model';
import { UpdateAssociationModel } from '@common/models/update-association.model';
import { ParserStartModel } from '@common/models/parser-start.model';

@Component({
    selector: 'update-prices',
    templateUrl: './update-prices.component.html',
    styleUrls: ['./update-prices.component.scss'],
})
export class UpdatePricesComponent extends ObserverComponent implements OnInit {
    @Select(UpdatePricesState.sites)
    sites$!: Observable<SiteModel[]>;

    @Select(UpdatePricesState.history)
    histories$!: Observable<ParserInfoModel[]>;

    @Select(UpdatePricesState.categoryAssociations)
    categoryAssociations$!: Observable<AssociationModel[]>;

    @Select(CategoriesQuery.categories) categories$!: Observable<CategoryModel[]>;

    categories!: CategoryModel[];

    items = [];

    sitesFromArray!: FormArray;

    updateFrom!: FormGroup;

    @ViewChild('matMenu', { static: true }) public matMenu: any;

    constructor(private store: Store, private dialog: MatDialog, public fb: FormBuilder) {
        super();
    }

    ngOnInit(): void {
        this.updateFrom = this.fb.group({ options: [''] });
        this.subscriptions.push(this.categoriesSubscription(), this.sitesSubscription());
    }

    allowCloseOnClickOut(): void {
        this.store.dispatch(new CleareHistory());
    }

    onLoadHistory(siteId: number): void {
        this.store.dispatch(new LoadHistory(siteId));
    }

    async onRelationCategory(id: number): Promise<void> {
        await this.store.dispatch(new LoadСategoryAssociations(id)).toPromise();
        this.openCategoryDialog().subscribe((result: UpdateAssociationModel[]) => {
            if (result) {
                this.store.dispatch(new UpdateСategoryAssociations(result));
            }
        });
    }

    onStart(model: any): void {
        const value = new ParserStartModel({ site: model.siteName }) as any;
        value[this.updateFrom.value.options] = true;
        this.store.dispatch(new StartParsing(value, model.id));
    }

    onStop(model: any): void {
        this.store.dispatch(
            new StopParsing(
                new ParserStartModel({
                    site: model.siteName,
                }),
                model.id,
            ),
        );
    }

    private openCategoryDialog(): Observable<any> {
        const associations = this.store.selectSnapshot(UpdatePricesState.categoryAssociations);

        const dialogRef = this.dialog.open(SetCategoryDialogComponent, {
            data: {
                associations: this.setupFormArray(associations),
                items: this.items,
            },
            panelClass: 'set-category-panel',
        });
        return dialogRef.afterClosed();
    }

    process(data: any): any {
        let result = [];
        result = data.map((item: any) => this.toTreeNode(item));
        return result;
    }

    toTreeNode(node: any) {
        if (node && node.children) {
            node.children.map((item: any) => {
                return this.toTreeNode(item);
            });
        }
        return node;
    }

    trackByFn(index: number, _: any): number {
        return index;
    }

    private setupFormArray(associations: AssociationModel[]): FormArray {
        const formArray = this.fb.array([]);

        (Array.isArray(associations) ? associations : []).forEach((item: AssociationModel) => {
            formArray.push(
                this.fb.group({
                    siteCategoryId: [item.siteCategoryId],
                    siteCategoryUrl: [item.siteCategoryUrl],
                    siteCategoryName: [item.siteCategoryName],
                    currentCategoryId: [item.currentCategoryId],
                    currentCategoryName: [item.currentCategoryName],
                    category: [
                        item.currentCategoryId
                            ? TreeHelper.findeById(item.currentCategoryId, this.categories)
                            : null,
                    ],
                    isSiteCategoryExcluded: [item.isSiteCategoryExcluded],
                    isNew: [!item.currentCategoryId],
                }) as any,
            );
        });
        return formArray;
    }

    private setupSitesArray(sites: SiteModel[]): FormArray {
        const formArray = this.fb.array([]);

        (Array.isArray(sites) ? sites : []).forEach((item: any) => {
            formArray.push(
                this.fb.group({
                    id: [item.id],
                    status: [item.status],
                    siteName: [item.siteName],
                    siteUrl: [item.siteUrl],
                    isAvailableUpdateCategory: [item.isAvailableUpdateCategory],
                    isAvailableUpdateProduct: [item.isAvailableUpdateProduct],
                    isAvailableUpdateProductPrice: [item.isAvailableUpdateProductPrice],
                }) as any,
            );
        });
        return formArray;
    }

    protected categoriesSubscription(): Subscription {
        return this.categories$.subscribe((categories: CategoryModel[]) => {
            this.categories = categories;
            this.items = this.process(categories);
        });
    }

    protected sitesSubscription(): Subscription {
        return this.sites$.subscribe((sites: SiteModel[]) => {
            this.sitesFromArray = this.setupSitesArray(sites);
        });
    }
}
