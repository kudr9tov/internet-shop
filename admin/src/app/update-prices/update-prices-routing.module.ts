import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UpdatePricesComponent } from './components/update-prices/update-prices.component';
import { UpdatePricesResolver } from './resolvers/update-prices.resolver';

const routes: Routes = [
    {
        path: '',
        component: UpdatePricesComponent,
        resolve: {
            updatePricesResolver: UpdatePricesResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UpdatePricesRoutingModule {}
