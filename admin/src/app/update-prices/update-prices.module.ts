import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UpdatePricesComponent } from './components/update-prices/update-prices.component';
import { UpdatePricesRoutingModule } from './update-prices-routing.module';
import { NgxsModule } from '@ngxs/store';
import { UpdatePricesState } from './store/update-prices.state';
import { CardItemModule } from '@common/app/card-item/card-item.module';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { SetCategoryDialogComponent } from './components/set-category-dialog/set-category-dialog.component';
import { ExpandMode, NgxTreeSelectModule } from '@common/tree-select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ParserApiService } from './services/parser-api.service';
import { CdkVirtualScrollViewportPatchModule } from '@common/directives/cdk-virtual-scroll-viewport/cdk-virtual-scroll-viewport-patch.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { UpdatePricesResolver } from './resolvers/update-prices.resolver';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
    declarations: [UpdatePricesComponent, SetCategoryDialogComponent],
    imports: [
        CommonModule,
        UpdatePricesRoutingModule,
        MatDialogModule,
        CardItemModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        MatRadioModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatInputModule,
        MatMenuModule,
        CdkVirtualScrollViewportPatchModule,
        ScrollingModule,
        MatDividerModule,
        MatIconModule,
        MatTooltipModule,
        MatButtonModule,
        NgxTreeSelectModule.forRoot({
            idField: 'id',
            textField: 'name',
            expandMode: ExpandMode.Selection,
        }),
        NgxsModule.forFeature([UpdatePricesState]),
    ],
    providers: [ParserApiService, UpdatePricesResolver],
})
export class UpdatePricesModule {}
