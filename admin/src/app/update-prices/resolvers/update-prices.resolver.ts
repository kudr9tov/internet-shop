import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { LoadSites } from '../store/update-prices.actions';
import { LoadCategories } from '@common/auth/store/categories/categories.actions';

@Injectable()
export class UpdatePricesResolver implements Resolve<any> {
    constructor(public store: Store) {}

    resolve(): Observable<any> {
        return this.store.dispatch([new LoadSites(), new LoadCategories()]);
    }
}
