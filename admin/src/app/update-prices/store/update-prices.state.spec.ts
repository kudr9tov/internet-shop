import { NgxsModule, Store } from '@ngxs/store';
import { TestBed } from '@angular/core/testing';

import { UpdatePricesState } from './update-prices.state';

describe('UpdatePricesState', () => {
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([UpdatePricesState])],
        });

        store = TestBed.get(Store);
        resetStore();
        resetMockProviders();
    });

    it('should create store', () => {
        expect(store).toBeTruthy();
    });

    function resetMockProviders(): void {}

    function resetStore(): void {
        store.reset({
            updatePricesState: {},
        });
    }
});
