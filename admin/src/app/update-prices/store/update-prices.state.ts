import { Injectable } from '@angular/core';
import { AssociationModel } from '@common/models/association.model';
import { CategoryAssotiationsModel } from '@common/models/category-associations.model';
import { ParserInfoModel } from '@common/models/parser-info.model';
import { SiteModel } from '@common/models/site.model';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ParserApiService } from '../services/parser-api.service';
import {
    CleareHistory,
    LoadHistory,
    LoadSites,
    LoadСategoryAssociations,
    StartParsing,
    StopParsing,
    UpdateСategoryAssociations,
} from './update-prices.actions';

import { UpdatePricesStateModel } from './update-prices.model';

@State<UpdatePricesStateModel>({
    name: 'updatePricesState',
    defaults: {
        sites: [],
        history: [],
        siteId: null,
        categoryAssociations: [],
    },
})
@Injectable()
export class UpdatePricesState {
    @Selector()
    static sites({ sites }: UpdatePricesStateModel): SiteModel[] {
        return sites;
    }

    @Selector()
    static history({ history }: UpdatePricesStateModel): ParserInfoModel[] {
        return history;
    }

    @Selector()
    static siteId({ siteId }: UpdatePricesStateModel): number | null {
        return siteId;
    }

    @Selector()
    static categoryAssociations({
        categoryAssociations,
    }: UpdatePricesStateModel): AssociationModel[] {
        return categoryAssociations;
    }

    constructor(public apiService: ParserApiService) {}

    @Action(LoadHistory)
    onSendMessage(
        { patchState }: StateContext<UpdatePricesStateModel>,
        { siteId }: LoadHistory,
    ): Observable<any> {
        return this.apiService.getLastInfo(siteId).pipe(
            tap((history: ParserInfoModel[]) => {
                patchState({ history });
            }),
        );
    }

    @Action(LoadSites)
    onLoadSites({ patchState }: StateContext<UpdatePricesStateModel>): Observable<any> {
        return this.apiService.getSites().pipe(
            tap((sites: SiteModel[]) => {
                patchState({ sites });
            }),
        );
    }

    @Action(LoadСategoryAssociations)
    onLoadСategoryAssociations(
        { patchState }: StateContext<UpdatePricesStateModel>,
        { siteId }: LoadHistory,
    ): Observable<any> {
        return this.apiService.getAssociation(siteId).pipe(
            tap((categoryAssociation: CategoryAssotiationsModel) => {
                patchState({
                    categoryAssociations: categoryAssociation.associations,
                    siteId: categoryAssociation.siteId,
                });
            }),
        );
    }

    @Action(StartParsing)
    onStartParsing(
        { setState }: StateContext<UpdatePricesStateModel>,
        { model, id }: StartParsing,
    ): Observable<any> {
        return this.apiService.startParsing(model).pipe(
            tap(() => {
                setState(
                    patch({
                        sites: updateItem<SiteModel>(
                            (s) => s.id === id,
                            patch({ status: 'RUNNING' }),
                        ),
                    }),
                );
            }),
        );
    }

    @Action(CleareHistory)
    onCleareHistory({ patchState }: StateContext<UpdatePricesStateModel>): void {
        patchState({ history: [] });
    }

    @Action(StopParsing)
    onStopParsing(
        { setState }: StateContext<UpdatePricesStateModel>,
        { model, id }: StopParsing,
    ): Observable<any> {
        return this.apiService.stopParsing(model).pipe(
            tap(() => {
                setState(
                    patch({
                        sites: updateItem<SiteModel>(
                            (s) => s.id === id,
                            patch({ status: 'COMPLETED' }),
                        ),
                    }),
                );
            }),
        );
    }

    @Action(UpdateСategoryAssociations)
    onUpdateСategoryAssociations(
        { patchState, getState }: StateContext<UpdatePricesStateModel>,
        { models }: UpdateСategoryAssociations,
    ): Observable<any> {
        const { siteId } = getState();
        return this.apiService.updateAssociation({ siteId, associations: models }).pipe(
            tap((categoryAssociation: CategoryAssotiationsModel) => {
                patchState({
                    categoryAssociations: categoryAssociation.associations,
                });
            }),
        );
    }
}
