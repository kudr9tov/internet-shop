import { ParserStartModel } from '@common/models/parser-start.model';
import { UpdateAssociationModel } from '@common/models/update-association.model';

export class LoadHistory {
    static readonly type = '[UpdatePrice] Load History';

    constructor(public siteId: number) {}
}

export class CleareHistory {
    static readonly type = '[UpdatePrice] Cleare History';
}

export class LoadСategoryAssociations {
    static readonly type = '[UpdatePrice] Load Сategory Associations';

    constructor(public siteId: number) {}
}

export class UpdateСategoryAssociations {
    static readonly type = '[UpdatePrice] Update Сategory Associations';

    constructor(public models: UpdateAssociationModel[]) {}
}

export class StartParsing {
    static readonly type = '[UpdatePrice] Start Parsing';

    constructor(public model: ParserStartModel, public id: number) {}
}

export class StopParsing {
    static readonly type = '[UpdatePrice] Stop Parsing';

    constructor(public model: ParserStartModel, public id: number) {}
}

export class LoadSites {
    static readonly type = '[UpdatePrice] Load Sites';
}
