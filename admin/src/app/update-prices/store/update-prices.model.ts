import { AssociationModel } from '@common/models/association.model';
import { ParserInfoModel } from '@common/models/parser-info.model';
import { SiteModel } from '@common/models/site.model';

export interface UpdatePricesStateModel {
    sites: SiteModel[];
    history: ParserInfoModel[];
    categoryAssociations: AssociationModel[];
    siteId: number | null;
}
