import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

import { CatalogQuery } from '@common/catalogData/store/catalog.query';
import { Observable, Subscription } from 'rxjs';
import { ProductModel } from '@common/models/product.model';
import { UpdateProduct } from '@common/catalogData/store/catalog.actions';

import { ProductBaseComponent } from '../../product-base/product-base.component';
import { CategoryModel } from '@common/models/category.model';
import { CreateProductModel } from '@common/models/create-product.model';
import { TreeHelper } from '@common/helpers/tree.helper';
import { CreatePropertyModel } from '@common/models/create-property.model';

@Component({
    selector: 'product-edit',
    templateUrl: './../../product-base/product-base.component.html',
    styleUrls: ['./../../product-base/product-base.component.scss'],
})
export class ProductEditComponent extends ProductBaseComponent implements OnInit {
    @Select(CatalogQuery.product) product$!: Observable<ProductModel>;

    @Select(CatalogQuery.catalogIds) catalogIds$!: Observable<number[]>;

    @Select(CatalogQuery.selectedCategory)
    selectedCategory$!: Observable<CategoryModel>;

    simpleSelected!: CategoryModel;

    catalogIds: number[] = [];

    constructor(public override fb: FormBuilder, public override store: Store) {
        super(fb, store);
        this.isArrowBack = true;
    }

    override ngOnInit(): void {
        super.ngOnInit();
        this.subscriptions.push(
            this.productSubscription(),
            this.selectedCategorySubscription(),
            this.catalogIdsSubscription(),
        );
    }

    override onSave(): void {
        const newModel = new CreateProductModel({
            id: this.productGroup.value.id,
            image: this.productGroup.value.image,
            name: this.productGroup.value.name,
            price: this.productGroup.value.price,
            discount: this.productGroup.value.discount,
            inStock: true,
            description: this.productGroup.value?.description,
        });
        const techProperties = {} as any;
        this.techProperties.value.forEach(
            (x: { name: any; value: any }) => (techProperties[`${x.name}`] = x.value),
        );
        newModel.techProperties = techProperties;
        newModel.properties = new CreatePropertyModel({
            brand: this.productGroup.value.brandId,
            category: TreeHelper.findeFirstParent(this.simpleSelected, this.categories),
            country: this.productGroup.value.countryId,
            subCategory: this.simpleSelected.id,
            vendorCode: this.productGroup.value.vendorCode,
        });
        this.store.dispatch(new UpdateProduct(newModel));
    }

    override onBack(): void {
        this.store.dispatch(new Navigate(['tables']));
    }

    override onRoutingProduct(value: number): void {
        if (this.catalogIds.length < 2) {
            return;
        }

        const index = this.catalogIds.findIndex((x) => x === this.product.id);
        const nextIndex =
            index + value < 0
                ? this.catalogIds.length - 1
                : index + value < this.catalogIds.length
                ? index + value
                : 0;

        this.store.dispatch(new Navigate(['tables', this.catalogIds[nextIndex]]));
    }

    private productSubscription(): Subscription {
        return this.product$.subscribe((product: ProductModel) => {
            if (!product) {
                return;
            }
            this.product = product;
            this.productGroup = this.createFormGroups(product);
            this.techProperties = this.setupTechProperty(product.techProperties || {});
        });
    }

    private selectedCategorySubscription(): Subscription {
        return this.selectedCategory$.subscribe((product: CategoryModel) => {
            if (!product) {
                return;
            }
            this.simpleSelected = product;
        });
    }

    private catalogIdsSubscription(): Subscription {
        return this.catalogIds$.subscribe((catalogIds: number[]) => {
            this.catalogIds = catalogIds;
        });
    }

    createFormGroups(product: ProductModel): FormGroup {
        product = product || new ProductModel();
        return this.fb.group({
            id: [product.id],
            name: [product.name, [Validators.required]],
            price: [product.price],
            discount: [product.discount],
            vendorCode: [product.properties?.vendorCode],
            description: [product.description],
            brandId: [product.properties?.brand?.id],
            countryId: [product.properties?.country?.id],
            image: [product.image],
        });
    }

    private setupTechProperty(values: any): FormArray {
        const formArray = this.fb.array([]);

        Object.keys(values).forEach((key) => {
            formArray.push(
                this.fb.group({
                    name: [key],
                    value: [values[key]],
                }) as any,
            );
        });
        return formArray;
    }
}
