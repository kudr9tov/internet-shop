import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { CatalogDataModule } from '@common/catalogData/catalog-data.module';
import { MatSelectModule } from '@angular/material/select';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';
import { FilterDataModule } from '@common/filter/filter-data.module';
import { PipesModule } from '@common/pipes/pipes.module';

import { TablesRoutingModule } from './tables-routing.module';
import { TablesComponent } from './tables/tables.component';
import { ProductEditComponent } from './category-edit/product-edit.component';
import { ProductBaseModule } from '../product-base/product-base.module';
import { ExpandMode, NgxTreeSelectModule } from '@common/tree-select';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
    imports: [
        CommonModule,
        CatalogDataModule,
        FilterDataModule,
        ProductBaseModule,
        ImageUploadModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        PipesModule,
        MatDividerModule,
        MatInputModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        FormsModule,
        MatSortModule,
        MatTableModule,
        TablesRoutingModule,
        MatSelectModule,
        NgxTreeSelectModule.forRoot({
            idField: 'id',
            textField: 'name',
            expandMode: ExpandMode.Selection,
        }),
    ],
    declarations: [TablesComponent, ProductEditComponent],
})
export class TablesModule {}
