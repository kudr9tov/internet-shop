import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogResolver } from '@common/catalogData/resolvers/catalog.resolver';

import { TablesComponent } from './tables/tables.component';
import { ProductEditComponent } from './category-edit/product-edit.component';

const routes: Routes = [
    {
        path: '',
        component: TablesComponent,
    },
    {
        path: ':id',
        component: ProductEditComponent,
        resolve: {
            catalog: CatalogResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TablesRoutingModule {}
