import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Navigate } from '@ngxs/router-plugin';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { CatalogState } from '@common/catalogData/store/catalog.state';
import { ProductModel } from '@common/models/product.model';
import {
    LoadAdminProducts,
    DeleteProduct,
    LoadErrorProducts,
    ErrorChecked,
} from '@common/catalogData/store/catalog.actions';
import { BaseShopTableComponent } from '@common/abstract/base-shop-table.component';
import { CatalogQuery } from '@common/catalogData/store/catalog.query';

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss'],
})
export class TablesComponent extends BaseShopTableComponent<ProductModel> implements OnInit {
    @Select(CatalogQuery.catalog) catalog$!: Observable<ProductModel[]>;

    @Select(CatalogState.total) total$!: Observable<number>;

    @Select(CatalogState.isErrorChecked) isErrorChecked$!: Observable<boolean>;

    feature = 'Продукт';

    isErrorChecked = false;

    displayedColumns = ['select', 'id', 'name', 'price', 'count', 'delete'];

    constructor(public override store: Store, public override dialog: MatDialog) {
        super(store, dialog);
    }

    override ngOnInit() {
        this.subscriptions.push(this.catalogSubscription(), this.isErrorCheckedSubscription());
        super.ngOnInit();
    }

    onChangePaginator(event: any): void {
        this.pageSize = event.pageSize || 0;
        this.onLoad(this.pageSize * event.pageIndex);
    }

    onOpen(id: number): void {
        if (!id) {
            return;
        }

        this.store.dispatch(new Navigate(['tables', `${id}`]));
    }

    onSearch(event: any): void {
        this.store.dispatch(new ErrorChecked(event && event.checked));
    }

    onLoadItems(search: string): void {
        this.store.dispatch(new LoadAdminProducts(this.pageSize, 0, search));
    }

    onDeleteItem(id: number): void {
        this.store.dispatch(new DeleteProduct(id));
    }

    onLoad(skip: number): void {
        this.isErrorChecked
            ? this.store.dispatch(new LoadErrorProducts(this.pageSize, skip))
            : this.store.dispatch(new LoadAdminProducts(this.pageSize, skip, this.lastSearch));
    }

    private catalogSubscription(): Subscription {
        return this.catalog$.subscribe((catalog: ProductModel[]) => {
            if (!catalog) {
                return;
            }
            this.dataSource = new MatTableDataSource([...catalog]);
        });
    }

    private isErrorCheckedSubscription(): Subscription {
        return this.isErrorChecked$.subscribe((value: boolean) => {
            this.isErrorChecked = value;
            this.onLoad(0);
        });
    }
}
