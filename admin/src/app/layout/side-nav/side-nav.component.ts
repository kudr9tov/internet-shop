import { Component } from '@angular/core';
import { childRoutes } from '../../admin-routing.module';
import { Store } from '@ngxs/store';
import { Logout } from '@common/auth/store/current-user/current-user.actions';

@Component({
    selector: 'app-side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.scss'],
})
export class SideNavComponent {
    showMenu = false;
    routes = childRoutes;

    constructor(public store: Store) {}

    onButtonClick(): void {
        this.store.dispatch(new Logout());
    }
}
