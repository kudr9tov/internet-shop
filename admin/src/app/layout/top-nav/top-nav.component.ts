import { Component, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { CurrentUserQuery } from '@common/auth/store/current-user/current-user.query';
import { AuthorizedPersonModel } from '@common/models/authorized-person.model';
import { NavigateTo } from '@common/navigation/store/navigation.actions';

@Component({
    selector: 'app-top-nav',
    templateUrl: './top-nav.component.html',
    styleUrls: ['./top-nav.component.scss'],
})
export class TopNavComponent {
    @Select(CurrentUserQuery.user)
    currentUser$!: Observable<AuthorizedPersonModel>;

    @Output() sideNavToggled = new EventEmitter<void>();

    constructor(private store: Store) {}
    toggleSidebar(): void {
        this.sideNavToggled.emit();
    }

    onBack(): void {
        this.store.dispatch(new NavigateTo('/'));
    }
}
