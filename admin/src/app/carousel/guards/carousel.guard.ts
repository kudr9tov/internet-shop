import { Injectable } from '@angular/core';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { CarouselComponent } from '../components/carousel/carousel.component';

@Injectable()
export class CarouselGuard extends UnsavedChangesGuard<CarouselComponent> {}
