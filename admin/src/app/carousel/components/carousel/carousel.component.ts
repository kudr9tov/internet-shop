import { Component, OnInit } from '@angular/core';
import { ObserverComponent } from '@common/abstract/observer.component';
import { Slide } from '@common/models/slide.model';
import { MatDialog } from '@angular/material/dialog';
import { Store, Select } from '@ngxs/store';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { Observable, of } from 'rxjs';
import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';
import { DeleteSlide, UpdateSlides, CreateSlides } from '../../store/carousel.actions';
import { CarouselState } from '../../store/carousel.state';
import { moveItemInArray, CdkDragEnter } from '@angular/cdk/drag-drop';

@Component({
    selector: 'carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent extends ObserverComponent implements OnInit {
    @Select(CarouselState.slides)
    slides$!: Observable<Slide[]>;

    slidesFormArray!: FormArray;

    slides!: Slide[];

    isAfterDeleted!: boolean;

    constructor(private dialog: MatDialog, private store: Store, private fb: FormBuilder) {
        super();
    }

    ngOnInit(): void {
        this.subscriptions.push(this.slidesSubscription());
    }

    haveChanges(): boolean {
        return this.slidesFormArray && this.slidesFormArray.dirty;
    }

    onCancel(): void {
        this.openUnsavedChangesModal().subscribe((result) => {
            if (result) {
                this.slidesFormArray = this.createBrandsFormArray(this.slides);
            }
        });
    }

    onSave(): void {
        if (this.slidesFormArray.invalid) {
            return;
        }
        const models: Slide[] = [...this.slidesFormArray.value];
        models.forEach((item: Slide, index) => (item.step = index));
        const createModels = models.filter((x) => x.id === 0);
        const updateModels = models.filter((x) => x.id !== 0);
        this.store.dispatch([new UpdateSlides(updateModels), new CreateSlides(createModels)]);

        this.slidesFormArray.markAsPristine();
    }

    onCreateSlide(): void {
        this.slidesFormArray.push(
            this.fb.group({
                id: [0],
                description: [''],
                title: [''],
                image: ['', Validators.required],
                link: [''],
                step: [this.slidesFormArray.length],
            }),
        );
    }

    onRemoveSlide(index: number): void {
        if (index < 0 || index > this.slidesFormArray.length - 1) {
            return;
        }
        const listModel = this.slidesFormArray.at(index).value;

        if (listModel.id !== 0) {
            this.openConfirmDeleteDialog().subscribe((result) => {
                if (result) {
                    this.isAfterDeleted = true;
                    this.slidesFormArray.removeAt(index);
                    this.slidesFormArray.markAsPristine();
                    this.store.dispatch(new DeleteSlide(listModel.id));
                }
            });
        } else {
            this.slidesFormArray.removeAt(index);
        }
    }

    drop(event: CdkDragEnter) {
        moveItemInArray(this.slidesFormArray.controls, event.item.data, event.container.data);
        this.slidesFormArray.controls.forEach((item: any, index: number) =>
            item.controls['step'].setValue(index),
        );
        this.slidesFormArray.markAsDirty();
    }

    trackByFn(index: number, _: any): number {
        return index;
    }

    private slidesSubscription(): Subscription {
        return this.slides$.subscribe((value) => {
            this.slides = value;
            this.slidesFormArray = this.createBrandsFormArray(this.slides);
        });
    }

    private openUnsavedChangesModal(): Observable<any> {
        if (this.slidesFormArray.pristine) {
            return of(true);
        }
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Несохраненные изменения',
                message:
                    'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                confirmLabel: 'Отказаться',
                cancelLabel: 'Отменить',
            },
        });
        return dialogRef.afterClosed();
    }

    private openConfirmDeleteDialog(): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Подтверждение удаления',
                message: 'Выбранный элемент будет удален. Вы уверены, что хотите удалить его?',
            },
        });
        return dialogRef.afterClosed();
    }

    private createBrandsFormArray(slides: Slide[]): FormArray {
        const formArray = this.fb.array([]);

        (Array.isArray(slides) ? slides : []).forEach((slide: Slide) => {
            formArray.push(
                this.fb.group({
                    id: [slide.id],
                    description: [slide?.description],
                    image: [slide.image, Validators.required],
                    link: [slide.link],
                    title: [slide.title || ''],
                    step: [slide.step],
                }) as any,
            );
        });
        return formArray;
    }
}
