import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { LoadSlides } from '../store/carousel.actions';

@Injectable()
export class CarouselResolver implements Resolve<any> {
    constructor(public store: Store) {}

    resolve(): Observable<any> {
        return this.store.dispatch(new LoadSlides());
    }
}
