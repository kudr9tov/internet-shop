import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseApiService } from '@common/abstract/base-api.service';
import { Observable } from 'rxjs';
import { Slide } from '@common/models/slide.model';

@Injectable()
export class CarouselApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(): Observable<any> {
        return this.httpGet('carousel', (x) => new Slide(x));
    }

    public create(models: Slide[]): Observable<any> {
        return this.httpPost('carousel', (x) => new Slide(x), models);
    }

    public update(models: Slide[]): Observable<any> {
        return this.httpPut('carousel', (x) => new Slide(x), models);
    }

    public delete(id: number): Observable<any> {
        return this.httpDelete('carousel', id, (x) => x);
    }
}
