import { TestBed } from '@angular/core/testing';

import { CarouselApiService } from './carousel-api.service';

describe('CarouselApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: CarouselApiService = TestBed.get(CarouselApiService);
        expect(service).toBeTruthy();
    });
});
