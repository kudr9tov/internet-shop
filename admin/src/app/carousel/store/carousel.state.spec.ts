import { NgxsModule, Store } from '@ngxs/store';
import { TestBed } from '@angular/core/testing';

import { CarouselState } from './carousel.state';

describe('CarouselState', () => {
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([CarouselState])],
        });

        store = TestBed.get(Store);
        resetStore();
        resetMockProviders();
    });

    it('should create store', () => {
        expect(store).toBeTruthy();
    });

    function resetMockProviders(): void {}

    function resetStore(): void {
        store.reset({
            carouselState: {},
        });
    }
});
