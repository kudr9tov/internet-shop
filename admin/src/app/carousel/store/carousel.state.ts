import { State, Action, StateContext, Selector } from '@ngxs/store';

import { CarouselStateModel } from './carousel.model';
import { LoadSlides, CreateSlides, UpdateSlides, DeleteSlide } from './carousel.actions';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CarouselApiService } from '../services/carousel-api.service';
import { Slide } from '@common/models/slide.model';
import { patch, append, removeItem } from '@ngxs/store/operators';

@State<CarouselStateModel>({
    name: 'carouselState',
    defaults: {
        slides: [],
    },
})
@Injectable()
export class CarouselState {
    @Selector()
    static slides({ slides }: CarouselStateModel): Slide[] {
        return slides || [];
    }

    constructor(private apiService: CarouselApiService) {}

    @Action(LoadSlides)
    onLoadSlides({ patchState }: StateContext<CarouselStateModel>): Observable<any> {
        return this.apiService.get().pipe(
            tap((slides: Slide[]) => {
                patchState({ slides });
            }),
        );
    }

    @Action(CreateSlides)
    onCreateSlides(
        { setState }: StateContext<CarouselStateModel>,
        { models }: CreateSlides,
    ): Observable<any> {
        if (!models || !models.length) {
            return of();
        }
        return this.apiService.create(models).pipe(
            tap((slides: Slide[]) =>
                setState(
                    patch({
                        slides: append(slides),
                    }),
                ),
            ),
        );
    }

    @Action(UpdateSlides)
    onUpdateProduct(
        { setState }: StateContext<CarouselStateModel>,
        { models }: UpdateSlides,
    ): Observable<any> {
        if (!models || !models.length) {
            return of();
        }
        return this.apiService.update(models).pipe(
            tap(() =>
                setState(
                    patch({
                        slides: models,
                    }),
                ),
            ),
        );
    }

    @Action(DeleteSlide)
    onDeleteProduct(
        { setState }: StateContext<CarouselStateModel>,
        { id }: DeleteSlide,
    ): Observable<any> {
        return this.apiService.delete(id).pipe(
            tap(() =>
                setState(
                    patch({
                        slides: removeItem<Slide>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }
}
