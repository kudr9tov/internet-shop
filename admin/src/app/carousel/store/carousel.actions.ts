import { Slide } from '@common/models/slide.model';

export class LoadSlides {
    static readonly type = '[Carousel] Load slides';
}

export class CreateSlides {
    static readonly type = '[Carousel] Create slides';

    constructor(public models: Slide[]) {}
}

export class UpdateSlides {
    static readonly type = '[Carousel] Update slides';

    constructor(public models: Slide[]) {}
}

export class DeleteSlide {
    static readonly type = '[Carousel] delete slide';

    constructor(public id: number) {}
}
