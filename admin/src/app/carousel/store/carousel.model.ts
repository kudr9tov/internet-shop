import { Slide } from '@common/models/slide.model';

export interface CarouselStateModel {
    slides: Slide[];
}
