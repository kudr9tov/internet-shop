import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CarouselRoutingModule } from './carousel-routing.module';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CarouselApiService } from './services/carousel-api.service';
import { NgxsModule } from '@ngxs/store';
import { CarouselState } from './store/carousel.state';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';
import { MatButtonModule } from '@angular/material/button';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PipesModule } from '@common/pipes/pipes.module';
import { CardItemModule } from '@common/app/card-item/card-item.module';

@NgModule({
    declarations: [CarouselComponent],
    imports: [
        CommonModule,
        NgxsModule.forFeature([CarouselState]),
        CarouselRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        DragDropModule,
        CardItemModule,
        MatButtonModule,
        ImageUploadModule,
        MatFormFieldModule,
        PipesModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        ReactiveFormsModule,
    ],
    providers: [CarouselApiService],
})
export class CarouselModule {}
