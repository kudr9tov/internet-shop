import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CarouselResolver } from './resolvers/carousel.resolver';
import { CarouselGuard } from './guards/carousel.guard';
import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

const routes: Routes = [
    {
        path: '',
        component: CarouselComponent,
        resolve: {
            carouselResolvar: CarouselResolver,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [CarouselResolver, CarouselGuard, UnsavedChangesGuard],
})
export class CarouselRoutingModule {}
