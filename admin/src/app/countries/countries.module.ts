import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';

import { CountriesDataModule } from '@common/countries/countries-data.module';

import { CountriesRoutingModule } from './countries-routing.module';
import { CountriesPageComponent } from './countries-page/countries-page.component';

@NgModule({
    declarations: [CountriesPageComponent],
    imports: [
        CountriesRoutingModule,
        CountriesDataModule,
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSidenavModule,
        MatToolbarModule,
        ReactiveFormsModule,
    ],
})
export class CountriesModule {}
