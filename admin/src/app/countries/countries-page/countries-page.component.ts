import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { IHaveChanges } from '@common/guards/unsaved-changes.guard';

import { ObserverComponent } from '@common/abstract/observer.component';
import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';
import { ListModel } from '@common/models/list.model';
import { CountriesQuery } from '@common/countries/store/countries.query';
import { DirectoryListModel } from '@common/models/directory-list.model';
import { DirectoryType } from '@common/enums/directory-type.enum';
import {
    UpdateCountries,
    CreateCountries,
    DeleteCountry,
} from '@common/countries/store/countries.actions';

@Component({
    selector: 'countries-page',
    templateUrl: 'countries-page.component.html',
    styleUrls: ['countries-page.component.scss'],
})
export class CountriesPageComponent extends ObserverComponent implements IHaveChanges, OnInit {
    @Select(CountriesQuery.countries)
    countries$!: Observable<ListModel[]>;

    countriesFormArray!: FormArray;

    countries!: ListModel[];

    isAfterDeleted!: boolean;

    constructor(private dialog: MatDialog, private store: Store, private fb: FormBuilder) {
        super();
    }

    ngOnInit(): void {
        this.subscriptions.push(this.countriesSubscription());
    }

    haveChanges(): boolean {
        return this.countriesFormArray && this.countriesFormArray.dirty;
    }

    onCancel(): void {
        this.openUnsavedChangesModal().subscribe((result) => {
            if (result) {
                this.countriesFormArray = this.createCountriesFormArray(this.countries);
            }
        });
    }

    onSave(): void {
        if (this.countriesFormArray.invalid) {
            return;
        }
        const models: ListModel[] = [...this.countriesFormArray.value];
        const createModels = models.filter((x) => x.id === 0);
        const updateModels = models.filter((x) => x.id !== 0);
        this.store.dispatch([
            new UpdateCountries(
                new DirectoryListModel({
                    type: DirectoryType.Countries,
                    entries: updateModels,
                }),
            ),
            new CreateCountries(
                new DirectoryListModel({
                    type: DirectoryType.Countries,
                    entries: createModels,
                }),
            ),
        ]);
        this.countriesFormArray.markAsPristine();
    }

    onCreateCountry(): void {
        this.countriesFormArray.push(
            this.fb.group({
                id: [0],
                name: [null, Validators.required],
            }),
        );
    }

    onRemoveCountry(index: number): void {
        if (index < 0 || index > this.countriesFormArray.length - 1) {
            return;
        }
        const listModel = this.countriesFormArray.at(index).value;

        if (listModel.id !== 0) {
            this.openConfirmDeleteDialog().subscribe((result) => {
                if (result) {
                    this.isAfterDeleted = true;
                    this.countriesFormArray.removeAt(index);
                    this.countriesFormArray.markAsPristine();
                    this.store.dispatch(new DeleteCountry(listModel.id));
                }
            });
        } else {
            this.countriesFormArray.removeAt(index);
        }
    }

    private countriesSubscription(): Subscription {
        return this.countries$.subscribe((value) => {
            this.countries = value;
            this.countriesFormArray = this.createCountriesFormArray(this.countries);
        });
    }

    private openUnsavedChangesModal(): Observable<any> {
        if (this.countriesFormArray.pristine) {
            return of(true);
        }
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Несохраненные изменения',
                message:
                    'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                confirmLabel: 'Отказаться',
                cancelLabel: 'Отменить',
            },
        });
        return dialogRef.afterClosed();
    }

    private openConfirmDeleteDialog(): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Подтверждение удаления',
                message: 'Выбранная страна будет удалена. Вы уверены, что хотите удалить её?',
            },
        });
        return dialogRef.afterClosed();
    }

    private createCountriesFormArray(countries: ListModel[]): FormArray {
        const formArray = this.fb.array([]);

        (countries || []).forEach((country: ListModel) => {
            formArray.push(
                this.fb.group({
                    id: [country.id],
                    name: [country.name],
                }) as any,
            );
        });
        return formArray;
    }
}
