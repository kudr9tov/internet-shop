import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CountriesResolver } from '@common/countries/resolvers/countries.resolver';
import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { CountriesPageComponent } from './countries-page/countries-page.component';
import { CountriesUnsavedChangesGuard } from './guards/countries-unsaved-changes.guard';

const ROUTES: Routes = [
    {
        path: '',
        component: CountriesPageComponent,
        resolve: {
            countriesResolvar: CountriesResolver,
        },
        canDeactivate: [CountriesUnsavedChangesGuard],
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
    providers: [CountriesResolver, UnsavedChangesGuard, CountriesUnsavedChangesGuard],
})
export class CountriesRoutingModule {}
