import { Injectable } from '@angular/core';

import { UnsavedChangesGuard } from '@common/guards/unsaved-changes.guard';

import { CountriesPageComponent } from '../countries-page/countries-page.component';

@Injectable()
export class CountriesUnsavedChangesGuard extends UnsavedChangesGuard<CountriesPageComponent> {}
