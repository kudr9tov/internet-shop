import { RequestInfoModel } from '@common/models/request-info.model';

export interface PriceRequestStateModel {
    requests: RequestInfoModel[];
}
