export class LoadRequests {
    static readonly type = '[Requests] Load requests';
    constructor(public query: string = '') {}
}

export class DeleteRequest {
    static readonly type = '[Requests] delete requests';

    constructor(public id: number) {}
}
