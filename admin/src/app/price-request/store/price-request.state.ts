import { State, Action, Selector, StateContext } from '@ngxs/store';

import { PriceRequestStateModel } from './price-request.model';
import { PriceRequestApiService } from '@common/services/price-request-api.service';
import { LoadRequests, DeleteRequest } from './price-request.actions';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { RequestInfoModel } from '@common/models/request-info.model';
import { patch, removeItem } from '@ngxs/store/operators';

@State<PriceRequestStateModel>({
    name: 'priceRequestState',
    defaults: {
        requests: [],
    },
})
@Injectable()
export class PriceRequestState {
    @Selector()
    static requests({ requests }: PriceRequestStateModel): RequestInfoModel[] {
        return requests || [];
    }

    constructor(private apiService: PriceRequestApiService) {}

    @Action(LoadRequests)
    onLoadRequests({ patchState }: StateContext<PriceRequestStateModel>): Observable<any> {
        return this.apiService.get().pipe(
            tap((requests: RequestInfoModel[]) => {
                patchState({
                    requests,
                });
            }),
        );
    }

    @Action(DeleteRequest)
    onDeleteRequest(
        { setState }: StateContext<PriceRequestStateModel>,
        { id }: DeleteRequest,
    ): Observable<any> {
        return this.apiService.delete(id).pipe(
            tap(() =>
                setState(
                    patch({
                        requests: removeItem<RequestInfoModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }
}
