import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PriceRequestsComponent } from './components/price-requests/price-requests.component';

const routes: Routes = [
    {
        path: '',
        component: PriceRequestsComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PriceRequestRoutingModule {}
