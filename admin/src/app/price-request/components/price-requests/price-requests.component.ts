import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

import { RequestInfoModel } from '@common/models/request-info.model';
import { BaseShopTableComponent } from '@common/abstract/base-shop-table.component';

import { LoadRequests, DeleteRequest } from '../../store/price-request.actions';
import { PriceRequestState } from '../../store/price-request.state';

@Component({
    selector: 'price-requests',
    templateUrl: './price-requests.component.html',
    styleUrls: ['./price-requests.component.scss'],
})
export class PriceRequestsComponent
    extends BaseShopTableComponent<RequestInfoModel>
    implements OnInit, AfterViewInit
{
    @Select(PriceRequestState.requests) requests$!: Observable<RequestInfoModel[]>;

    displayedColumns = ['created', 'name', 'phone', 'email', 'productName', 'info', 'delete'];

    feature = 'Запрос';

    constructor(public override store: Store, public override dialog: MatDialog) {
        super(store, dialog);
    }

    override ngOnInit(): void {
        this.store.dispatch(new LoadRequests());
        this.subscriptions.push(this.ordersSubscription());
        super.ngOnInit();
    }

    onChangePaginator(event: any): void {
        this.pageSize = event.pageSize || 0;
        // this.store.dispatch(new LoadRequests(this.pageSize, this.pageSize * event.pageIndex));
    }

    onDeleteItem(id: number): void {
        this.store.dispatch(new DeleteRequest(id));
    }

    onLoadItems(search: string): void {
        this.store.dispatch(new LoadRequests(search));
    }

    private ordersSubscription(): Subscription {
        return this.requests$.subscribe((orders: RequestInfoModel[]) => {
            if (!orders) {
                return;
            }
            const newOrders = orders.map((x) => new RequestInfoModel({ ...x }));
            this.dataSource = new MatTableDataSource([...newOrders]);
        });
    }
}
