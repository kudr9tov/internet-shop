import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PriceRequestRoutingModule } from './price-request-routing.module';
import { PriceRequestsComponent } from './components/price-requests/price-requests.component';
import { PriceRequestResolver } from './resolvers/price-request.resolver';
import { PriceRequestApiService } from '../../../../common/src/services/price-request-api.service';
import { NgxsModule } from '@ngxs/store';
import { PriceRequestState } from './store/price-request.state';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
    declarations: [PriceRequestsComponent],
    imports: [
        CommonModule,
        PriceRequestRoutingModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatTooltipModule,
        MatInputModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        FormsModule,
        MatSortModule,
        MatTableModule,
        NgxsModule.forFeature([PriceRequestState]),
    ],
    providers: [PriceRequestResolver, PriceRequestApiService],
})
export class PriceRequestModule {}
