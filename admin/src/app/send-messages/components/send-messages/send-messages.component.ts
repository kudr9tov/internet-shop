import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Subscription, Observable } from 'rxjs';
import { MatChipInputEvent } from '@angular/material/chips';
import { Store, Select } from '@ngxs/store';

import { CustomValidators } from '@common/regex/custom.validators';
import { EmailModel } from '@common/models/email.model';
import { ObserverComponent } from '@common/abstract/observer.component';
import { AppConstants } from '@common/constants/app.const';

import { SendMessage, CheckSending, StopSending } from '../../store/send-messages.actions';
import { SendMessagesState } from '../../store/send-messages.state';

@Component({
    selector: 'send-messages',
    templateUrl: './send-messages.component.html',
    styleUrls: ['./send-messages.component.scss'],
})
export class SendMessagesComponent extends ObserverComponent implements OnInit {
    @Select(SendMessagesState.isSending) isSending$!: Observable<boolean>;

    visible = true;

    selectable = true;

    addOnBlur = true;

    readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    emailGroup!: FormGroup;

    emails!: FormArray;

    isSending!: boolean;

    subject = AppConstants.subjectTemplate;

    podpis = AppConstants.signature(AppConstants.location);

    constructor(private fb: FormBuilder, private store: Store) {
        super();
    }

    ngOnInit(): void {
        this.store.dispatch(new CheckSending());
        this.setupFormGroups();
        this.initSubscriptions();
    }

    onSend(): void {
        this.store.dispatch(new SendMessage(new EmailModel({ ...this.emailGroup.value })));
    }

    onStop(): void {
        this.store.dispatch(new StopSending());
    }

    setupFormGroups(): void {
        this.emails = this.fb.array([]);
        this.emailGroup = this.fb.group({
            subject: '',
            message: this.podpis,
            emails: this.emails,
            sendAll: false,
            sendTemplate: false,
        });
    }

    add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            this.emails.push(new FormControl(value.trim(), CustomValidators.emailValidator()));
        }

        if (input) {
            input.value = '';
        }
    }

    onSetupForm(event: any): void {
        if (event && event.checked) {
            this.emailGroup.controls['subject'].setValue(this.subject);
        }
    }

    remove(index: number): void {
        if (index < 0 || index > this.emails.length - 1) {
            return;
        }

        this.emails.removeAt(index);
    }

    private initSubscriptions(): void {
        this.subscriptions.push(
            this.sendAllValueChangesSubscription(),
            this.isSendingSubscription(),
        );
    }

    private sendAllValueChangesSubscription(): Subscription {
        return this.emailGroup.controls['sendAll'].valueChanges.subscribe((isSendAll) => {
            if (isSendAll) {
                this.emails.clear();
            }
        });
    }

    private isSendingSubscription(): Subscription {
        return this.isSending$.subscribe((isSending: boolean) => {
            this.isSending = isSending;
        });
    }
}
