import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { SendMessagesComponent } from './send-messages.component';

describe('SendMessagesComponent', () => {
    let component: SendMessagesComponent;
    let fixture: ComponentFixture<SendMessagesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SendMessagesComponent],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(SendMessagesComponent);
        component = fixture.componentInstance;
    }));

    afterEach(() => {
        fixture.destroy();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
