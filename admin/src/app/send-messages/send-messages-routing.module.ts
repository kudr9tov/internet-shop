import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SendMessagesComponent } from './components/send-messages/send-messages.component';

const routes: Routes = [
    {
        path: '',
        component: SendMessagesComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SendMessagesRoutingModule {}
