import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { NgxsModule } from '@ngxs/store';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SendMessagesComponent } from './components/send-messages/send-messages.component';
import { SendMessagesRoutingModule } from './send-messages-routing.module';
import { SendMessagesApiService } from './services/send-messages-api.service';
import { SendMessagesState } from './store/send-messages.state';

@NgModule({
    declarations: [SendMessagesComponent],
    imports: [
        CommonModule,
        SendMessagesRoutingModule,
        MatButtonModule,
        ReactiveFormsModule,
        FormsModule,
        NgxsModule.forFeature([SendMessagesState]),
        MatInputModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatChipsModule,
        MatIconModule,
    ],
    providers: [SendMessagesApiService],
})
export class SendMessagesModule {}
