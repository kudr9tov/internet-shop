import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { SendMessage, CheckSending, StopSending } from './send-messages.actions';
import { SendMessagesApiService } from '../services/send-messages-api.service';
import { SendMessagesStateModel } from './send-messages.model';
import { SetSuccess } from '@common/toast';

@State<SendMessagesStateModel>({
    name: 'sendMessagesState',
    defaults: {
        isSending: false,
    },
})
@Injectable()
export class SendMessagesState {
    @Selector()
    static isSending({ isSending }: SendMessagesStateModel): boolean {
        return isSending;
    }

    constructor(private apiService: SendMessagesApiService) {}

    @Action(SendMessage)
    onSendMessage(
        { dispatch }: StateContext<SendMessagesStateModel>,
        { model }: SendMessage,
    ): Observable<any> {
        return this.apiService
            .send(model)
            .pipe(tap(() => dispatch([new SetSuccess('Письмо отправлено'), new CheckSending()])));
    }

    @Action(CheckSending)
    onCheckSending({ patchState }: StateContext<SendMessagesStateModel>): Observable<any> {
        return this.apiService.checkIsHasTask().pipe(tap((isSending) => patchState({ isSending })));
    }

    @Action(StopSending)
    onStopSending({ dispatch }: StateContext<SendMessagesStateModel>): Observable<any> {
        return this.apiService.stopTask().pipe(tap(() => dispatch(new CheckSending())));
    }
}
