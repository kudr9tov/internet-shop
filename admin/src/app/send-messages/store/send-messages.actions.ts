import { EmailModel } from '@common/models/email.model';

export class SendMessage {
    static readonly type = '[Email] Send message';

    constructor(public model: EmailModel) {}
}

export class CheckSending {
    static readonly type = '[Email] Check sending';
}

export class StopSending {
    static readonly type = '[Email] Stop sending';
}
