import { NgxsModule, Store } from '@ngxs/store';
import { TestBed } from '@angular/core/testing';

import { SendMessagesState } from './send-messages.state';

describe('SendMessagesState', () => {
    let store: Store;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([SendMessagesState])],
        });

        store = TestBed.get(Store);
        resetStore();
        resetMockProviders();
    });

    it('should create store', () => {
        expect(store).toBeTruthy();
    });

    function resetMockProviders(): void {}

    function resetStore(): void {
        store.reset({
            sendMessagesState: {},
        });
    }
});
