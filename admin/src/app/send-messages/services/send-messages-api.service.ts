import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';

import { BaseApiService } from '@common/abstract/base-api.service';
import { EmailModel } from '@common/models/email.model';
import { Observable } from 'rxjs';

@Injectable()
export class SendMessagesApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(): Observable<any> {
        return this.httpGet('mail', (x) => x);
    }

    public checkIsHasTask(): Observable<any> {
        return this.httpPost('mail/hasTask', (x) => x);
    }

    public stopTask(): Observable<any> {
        return this.httpPost('mail/stopTask', (x) => x);
    }

    public send(model: EmailModel): Observable<any> {
        return this.httpPost('mail/send-template', (x) => new EmailModel(x), model);
    }
}
