import { TestBed } from '@angular/core/testing';

import { SendMessagesApiService } from './send-messages-api.service';

describe('SendMessagesApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: SendMessagesApiService = TestBed.get(SendMessagesApiService);
        expect(service).toBeTruthy();
    });
});
