import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngxs/store';

import { CreateProduct } from '@common/catalogData/store/catalog.actions';

import { ProductBaseComponent } from '../../product-base/product-base.component';
import { CategoryModel } from '@common/models/category.model';
import { CreateProductModel } from '@common/models/create-product.model';
import { TreeHelper } from '@common/helpers/tree.helper';
import { CreatePropertyModel } from '@common/models/create-property.model';

@Component({
    selector: 'product-create',
    styleUrls: ['./../../product-base/product-base.component.scss'],
    templateUrl: './../../product-base/product-base.component.html',
})
export class ProductCreateComponent extends ProductBaseComponent {
    constructor(public override fb: FormBuilder, public override store: Store) {
        super(fb, store);
    }

    simpleSelected!: CategoryModel;

    override onSave(): void {
        const newModel = new CreateProductModel({
            image: this.productGroup.value.image,
            name: this.productGroup.value.name,
            price: this.productGroup.value.price,
            discount: this.productGroup.value.discount,
            inStock: true,
            description: this.productGroup.value?.description,
        });
        const techProperties = {} as any;
        this.techProperties.value.forEach((x: any) => (techProperties[`${x.name}`] = x.value));
        newModel.techProperties = techProperties;
        newModel.properties = new CreatePropertyModel({
            brand: this.productGroup.value.brandId,
            category: TreeHelper.findeFirstParent(this.simpleSelected, this.categories),
            country: this.productGroup.value.countryId,
            subCategory: this.simpleSelected.id,
            vendorCode: this.productGroup.value.vendorCode,
        });
        this.store.dispatch(new CreateProduct(newModel));
    }
}
