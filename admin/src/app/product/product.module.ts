import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { CatalogDataModule } from '@common/catalogData/catalog-data.module';
import { ImageUploadModule } from '@common/image-upload/image-upload.module';

import { ProductCreateComponent } from './components/product-create.component';
import { ProductRoutingModule } from './product-routing.module';
import { ProductBaseModule } from '../product-base/product-base.module';
import { MatDividerModule } from '@angular/material/divider';
import { PipesModule } from '@common/pipes/pipes.module';
import { ExpandMode, NgxTreeSelectModule } from '@common/tree-select';

@NgModule({
    declarations: [ProductCreateComponent],
    imports: [
        CommonModule,
        CatalogDataModule,
        ProductBaseModule,
        ProductRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        PipesModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatSelectModule,
        ImageUploadModule,
        NgxTreeSelectModule.forRoot({
            idField: 'id',
            textField: 'name',
            expandMode: ExpandMode.Selection,
        }),
    ],
})
export class ProductModule {}
