import { Component, HostListener } from '@angular/core';
import { DeviceSizeComponent } from '@common/abstract/device-size.component';
import { Store } from '@ngxs/store';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent extends DeviceSizeComponent {
    sideNavOpened = true;
    sideNavMode: 'side' | 'over' = 'side';
    toolBarHeight = 64;
    constructor(public override store: Store) {
        super(store);
        this.onSidenavResizer();
        this.addMaterial();
        this.removePreloader();
    }

    @HostListener('window:orientationchange')
    @HostListener('window:resize')
    onSidenavResizer(): void {
        const windowWidth = window.innerWidth;
        if (windowWidth > 960) {
            this.sideNavOpened = true;
            this.sideNavMode = 'side';
            return;
        }

        if (windowWidth <= 960) {
            if (this.sideNavOpened) {
                this.sideNavOpened = false;
            }
            this.sideNavMode = 'over';
        }
        if (windowWidth <= 600) {
            this.toolBarHeight = 56;
        } else {
            this.toolBarHeight = 64;
        }
    }

    removePreloader(): void {
        const preloader = document.querySelector('.site-preloader');

        if (preloader === null) {
            return;
        }

        preloader.addEventListener('transitionend', (event: Event) => {
            if (event instanceof TransitionEvent && event.propertyName === 'opacity') {
                preloader.remove();
            }
        });
        preloader.classList.add('site-preloader__fade');

        // Sometimes, due to unexpected behavior, the browser (in particular Safari) may not play the transition, which leads
        // to blocking interaction with page elements due to the fact that the preloader is not deleted.
        // The following block covers this case.
        if (getComputedStyle(preloader).opacity === '0' && preloader.parentNode) {
            preloader.parentNode.removeChild(preloader);
        }
    }

    addMaterial(): void {
        const body = document.getElementsByTagName('head')[0];
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = 'https://fonts.googleapis.com/icon?family=Material+Icons';
        body.appendChild(link);
        link.onload = () => console.log('Angular fonts loaded');
    }
}
