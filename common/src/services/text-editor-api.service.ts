import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from '@common/abstract/base-api.service';
import { TextEditorModel } from '@common/models/text-editor.model';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Injectable()
export class TextEditorApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public delete(id: number): Observable<any> {
        return super.httpDelete('links', id, (x) => x);
    }

    public get(id: number): Observable<any> {
        return this.httpGet(`links/${id}`, (x) => new TextEditorModel(x));
    }

    public create(model: TextEditorModel[]): Observable<any> {
        return this.httpPost('links', (x) => new TextEditorModel(x), model);
    }

    public update(model: TextEditorModel[]): Observable<any> {
        return this.httpPut('links', (x) => new TextEditorModel(x), model);
    }

    public getList(): Observable<TextEditorModel[]> {
        return this.httpGet('links', (x) => x);
    }
}
