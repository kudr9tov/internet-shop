import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseApiService } from '@common/abstract/base-api.service';
import { Observable } from 'rxjs';
import { RequestPriceModel } from '@common/models/price-request.model';

@Injectable()
export class PriceRequestApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }
    public get(): Observable<any> {
        return this.httpGet('price', (x) => new RequestPriceModel(x));
    }

    public create(model: RequestPriceModel): Observable<any> {
        return this.httpPost('price', (x) => new RequestPriceModel(x), model);
    }

    public update(model: RequestPriceModel): Observable<any> {
        return this.httpPut('price', (x) => new RequestPriceModel(x), model);
    }

    public delete(id: number): Observable<any> {
        return this.httpDelete('price', id, (x) => x);
    }
}
