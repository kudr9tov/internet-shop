import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseApiService } from '@common/abstract/base-api.service';
import { Observable } from 'rxjs';

@Injectable()
export class CommonApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }
    public get(id: number): Observable<any> {
        return this.httpGet(`links/${id}`, (x) => x);
    }
}
