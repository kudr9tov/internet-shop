import { Component, Inject, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTooltip } from '@angular/material/tooltip';
import { TreeHelper } from '@common/helpers/tree.helper';
import { CategoryModel } from '@common/models/category.model';

@Component({
    selector: 'folder-select-dialog',
    templateUrl: 'folder-select-dialog.component.html',
    styleUrls: ['folder-select-dialog.component.scss'],
})
export class FolderSelectDialogComponent implements OnInit {
    @ViewChildren(MatTooltip) folderTooltips!: QueryList<MatTooltip>;

    allFolders!: CategoryModel[];

    isTouchDevice = false;

    folders!: CategoryModel[];

    forbiddenFolderId!: number;

    parentFolder!: CategoryModel;

    selectedFolder!: CategoryModel;

    childFolders!: CategoryModel[];

    folderId!: number;

    constructor(
        @Inject(MAT_DIALOG_DATA)
        public data: {
            allFolders: CategoryModel[];
            disableClose: boolean;
            folderId: number;
            forbiddenFolderId: number;
            root: string;
            title: string;
            tooltip: string;
        },
        public dialogRef: MatDialogRef<FolderSelectDialogComponent>,
    ) {
        dialogRef.disableClose = !!data.disableClose;
    }

    ngOnInit(): void {
        this.initAllFolders();
    }

    buildFoldersTree(): void {
        this.childFolders = this.parentFolder?.subCategory || this.allFolders;
    }

    onSelect(folder: CategoryModel): void {
        if (!folder || folder.id === this.forbiddenFolderId) {
            return;
        }

        if (this.isTouchDevice) {
            this.onOpen(folder);
        } else {
            this.selectedFolder = folder;
        }
    }

    onOpen(folder: CategoryModel): void {
        if (!folder || folder.id === this.forbiddenFolderId) {
            return;
        }

        this.parentFolder = folder;
        this.selectedFolder = this.parentFolder;
        this.childFolders = this.parentFolder?.subCategory || [];
    }

    onMove(): void {
        const parentId = this.selectedFolder ? this.selectedFolder.id : null;
        this.dialogRef.close({
            parentId,
            categoryIds: [this.forbiddenFolderId],
        });
    }

    goBack(): void {
        const folderId = this.parentFolder ? this.parentFolder.parentCategoryId : null;
        this.parentFolder = TreeHelper.findeById(folderId, this.folders);
        this.selectedFolder = this.parentFolder;
        this.buildFoldersTree();
    }

    private initAllFolders(): void {
        this.folderId = this.data.folderId;
        this.allFolders = this.data.allFolders;
        this.folders = this.allFolders || [];

        const currentFolder = TreeHelper.findeById(this.folderId, this.folders) || null;
        this.forbiddenFolderId = this.data.forbiddenFolderId;
        this.selectedFolder = currentFolder;
        this.parentFolder = currentFolder;

        this.buildFoldersTree();
    }
}
