import { Directive, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

@Directive()
export abstract class ObserverComponent implements OnDestroy {
    subscriptions: Subscription[] = [];
    destroyStream$ = new Subject<void>();

    ngOnDestroy(): void {
        this.clearSubscriptions();
    }

    protected clearSubscriptions(): void {
        this.subscriptions.forEach((s) => s && s.unsubscribe());
        this.destroyStream$.next();
        this.destroyStream$.complete();
    }
}
