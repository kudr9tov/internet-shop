import { Directive, HostListener } from '@angular/core';
import { Store } from '@ngxs/store';

import { ObserverComponent } from './observer.component';
import { DeviceSize } from '../enums/device-size.enum';
import { AppConstants } from '../constants/app.const';
import { SetDeviceSize } from '../auth/store/auth.actions';

@Directive()
export abstract class DeviceSizeComponent extends ObserverComponent {
    protected deviceSize!: DeviceSize;

    isMobile!: boolean;

    constructor(protected store: Store) {
        super();
        this.onResize();
    }

    @HostListener('window:orientationchange')
    @HostListener('window:resize')
    onResize(): void {
        const windowWidth = window.innerWidth;

        const current =
            windowWidth < AppConstants.mobileSize ? DeviceSize.Mobile : DeviceSize.Desktop;
        if (current !== this.deviceSize) {
            this.isMobile = current === DeviceSize.Mobile;
            this.deviceSize = current;
            this.store.dispatch(new SetDeviceSize(current));
        }
    }
}
