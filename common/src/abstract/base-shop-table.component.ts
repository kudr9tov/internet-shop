import { AfterViewInit, Directive, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Store } from '@ngxs/store';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';

import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';

import { ObserverComponent } from './observer.component';

@Directive()
export abstract class BaseShopTableComponent<T>
    extends ObserverComponent
    implements OnInit, AfterViewInit
{
    pageSize = 25;

    searchControl = new FormControl('');

    lastSearch = '';

    pageSizeOptions: number[] = [5, 10, 25, 100];

    dataSource!: MatTableDataSource<T>;

    selection!: SelectionModel<T>;

    @ViewChild(MatSort, { static: true }) sort!: MatSort;

    abstract displayedColumns: string[];

    abstract feature: string;

    abstract onDeleteItem(id: number): void;

    abstract onLoadItems(search: string): void;

    constructor(public store: Store, public dialog: MatDialog) {
        super();
    }

    ngOnInit() {
        this.selection = new SelectionModel<T>(true, []);
        this.searchControl.valueChanges
            .pipe(
                debounceTime(300),
                distinctUntilChanged(),
                filter((x) => x !== this.lastSearch),
            )
            .subscribe((search: any) => {
                this.lastSearch = search;
                this.onLoadItems(search);
            });
    }

    ngAfterViewInit() {
        if (this.dataSource) {
            this.dataSource.sort = this.sort;
        }
    }

    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected()
            ? this.selection.clear()
            : this.dataSource.data.forEach((row) => this.selection.select(row));
    }

    onDelete(id: number): void {
        const dialogRef = this.dialog
            .open(ConfirmDialogComponent, {
                data: {
                    title: `Удаление ${this.feature}a`,
                    message: `Вы уверены что хотите удалить ${this.feature}?`,
                },
            })
            .afterClosed();

        dialogRef.subscribe((result) => result && this.onDeleteItem(id));
    }
}
