import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';

import { LocationService } from '../services/location.service';
import { NavigateTo, OpenInWindow } from './navigation.actions';
import { NavigationState } from './navigation.state';

describe('Viewer NavigationState:', () => {
    let store: Store;
    let locationServiceMock: {
        goTo: jasmine.Spy;
        open: jasmine.Spy;
    };

    beforeEach(() => {
        initMockedProviders();
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([NavigationState])],
            providers: [{ provide: LocationService, useValue: locationServiceMock }],
        });

        store = TestBed.get(Store);
    });

    it('NavigateTo: should call goTo', () => {
        const link = 'test/';

        store.dispatch(new NavigateTo(link));

        expect(locationServiceMock.goTo).toHaveBeenCalledWith(link);
    });

    it('OpenInWindow: should call open', () => {
        const link = 'test/';

        store.dispatch(new OpenInWindow(link));

        expect(locationServiceMock.open).toHaveBeenCalledWith(link);
    });

    function initMockedProviders(): void {
        locationServiceMock = {
            goTo: jasmine.createSpy('goTo'),
            open: jasmine.createSpy('open'),
        };
    }
});
