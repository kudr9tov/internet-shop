export class NavigateTo {
    static readonly type = '[Navigation] Navigate to';
    constructor(public payload: string) {}
}

export class OpenInWindow {
    static readonly type = '[Navigation] Open in window';
    constructor(public payload: string) {}
}
