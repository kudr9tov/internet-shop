import { Action, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';

import { LocationService } from '../services/location.service';
import { NavigationStateModel } from './navigation.model';
import { NavigateTo, OpenInWindow } from './navigation.actions';

@State<NavigationStateModel>({
    name: 'navigation',
    defaults: {},
})
@Injectable()
export class NavigationState {
    constructor(private service: LocationService) {}

    @Action(NavigateTo)
    onNavigateTo({}: StateContext<NavigationStateModel>, { payload }: NavigateTo): void {
        this.service.goTo(payload);
    }

    @Action(OpenInWindow)
    onOpenInWindow({}: StateContext<NavigationStateModel>, { payload }: OpenInWindow): void {
        this.service.open(payload);
    }
}
