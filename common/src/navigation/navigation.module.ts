import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { NavigationState } from './store/navigation.state';
import { LocationService } from './services/location.service';

@NgModule({
    imports: [NgxsModule.forFeature([NavigationState])],
    providers: [LocationService],
})
export class NavigationModule {
    constructor(@Optional() @SkipSelf() parentModule: NavigationModule) {
        if (parentModule) {
            throw new Error('NavigationModule is already loaded. Import it in the AppModule only');
        }
    }
}
