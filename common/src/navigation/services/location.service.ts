import { Injectable } from '@angular/core';

/**
 *Use this service because of mock window.location.href in unit testing
 */
@Injectable()
export class LocationService {
    /**
     * An absolute URL.
     * Returns value of window.location.href
     */
    url = window.location.href;

    /**
     * Call window.location.href
     * @param url
     */
    goTo(url: string): void {
        window.location.href = url;
    }

    open(url: string): void {
        window.open(url);
    }
}
