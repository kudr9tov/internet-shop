import { CanDeactivate } from '@angular/router';
import { HostListener, Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';

import { ConfirmDialogComponent } from '@common/confirm-dialog/confirm-dialog.component';

export interface IHaveChanges {
    haveChanges: () => boolean;
}

@Injectable()
export class UnsavedChangesGuard<T extends IHaveChanges> implements CanDeactivate<T> {
    constructor(protected dialog: MatDialog) {}

    @HostListener('window:beforeunload')
    canDeactivate(component: T): Observable<boolean> {
        if (!component.haveChanges()) {
            return of(true);
        }

        return this.showConfirmDialog();
    }

    protected showConfirmDialog(): Observable<any> {
        return this.dialog
            .open(ConfirmDialogComponent, {
                data: {
                    booleanResponse: true,
                    title: 'Несохраненные изменения',
                    message:
                        'У вас есть несохраненные изменения. Вы уверены, что хотите отказаться от них?',
                    confirmLabel: 'Отказаться',
                    cancelLabel: 'Отмена',
                },
                panelClass: 'confirm-delete-panel',
            })
            .afterClosed();
    }
}
