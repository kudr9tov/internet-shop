import { Router } from '@angular/router';
import { Directive } from '@angular/core';
import { Store } from '@ngxs/store';

import { NavigateTo } from '@common/navigation/store/navigation.actions';
import { CurrentUserResolver } from '@common/auth/current-user-resolver/current-user-resolver';
import { Roles } from '@common/enums/roles.enum';

@Directive()
export abstract class AuthGuardBase {
    constructor(
        public currentUserResolver: CurrentUserResolver,
        public router: Router,
        public store: Store,
    ) {}

    checkUserAndRolesExisting(): Promise<boolean> {
        return this.currentUserResolver
            .resolve()
            .toPromise()
            .then((user) => {
                if (!user) {
                    this.store.dispatch(new NavigateTo('/login'));
                    return false;
                } else if (user.role !== Roles.Admin) {
                    this.store.dispatch(new NavigateTo('/login'));
                    return false;
                }
                return true;
            })
            .catch(() => {
                this.store.dispatch(new NavigateTo('/login'));
                return false;
            });
    }
}
