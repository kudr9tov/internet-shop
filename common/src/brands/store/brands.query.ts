import { Selector } from '@ngxs/store';

import { ListModel } from '@common/models/list.model';

import { BrandsState } from './brands.state';

export class BrandsQuery {
    @Selector([BrandsState.brands])
    static brands(brands: ListModel[]): ListModel[] {
        return brands;
    }

    @Selector([BrandsState.brands])
    static brandsByGroup(brands: ListModel[]): any[] {
        const grouped = brands.reduce((groups: any, brand: any) => {
            const letter = brand.name.charAt(0);

            groups[letter] = groups[letter] || [];
            groups[letter].push(brand);

            return groups;
        }, {});

        return Object.keys(grouped)
            .map((key) => ({ key, brands: grouped[key] }))
            .sort((a, b) => (a.key > b.key ? 1 : -1));
    }
}
