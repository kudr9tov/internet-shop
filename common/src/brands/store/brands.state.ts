import { Action, Selector, State, StateContext } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { DirectoryModel } from '@common/models/directory.model';
import { DirectoryType } from '@common/enums/directory-type.enum';
import { ListModel } from '@common/models/list.model';
import { SortHelper } from '@common/helpers/sort.helper';

import { BrandsApiService } from '../services/brands-api.service';
import { BrandsStateModel } from './brands.model';
import {
    CreateBrand,
    CreateBrands,
    DeleteBrand,
    LoadBrands,
    NavigateToBrand,
    NavigateToBrandsList,
    SelectBrandId,
    SelectDefaultBrand,
    SelectFirstBrand,
    UpdateBrands,
} from './brands.actions';

@State<BrandsStateModel>({
    name: 'brands',
    defaults: {
        brands: [],
        brand: new ListModel(),
        selectBrandId: null,
    },
})
@Injectable()
export class BrandsState {
    constructor(private apiService: BrandsApiService) {}

    @Selector()
    static selectedBrand({ brand }: BrandsStateModel): ListModel {
        return brand;
    }

    @Selector()
    static selectBrandId({ selectBrandId }: BrandsStateModel): number | null {
        return selectBrandId;
    }

    @Selector()
    static brands({ brands }: BrandsStateModel): ListModel[] {
        return brands || [];
    }

    @Action(SelectBrandId)
    onSelectBrandId({ patchState }: StateContext<BrandsStateModel>, { id }: SelectBrandId): void {
        patchState({ selectBrandId: id });
    }

    @Action(LoadBrands)
    onLoadBrands({ patchState }: StateContext<BrandsStateModel>): Observable<any> {
        return this.apiService.get().pipe(
            tap((brands: ListModel[]) => {
                patchState({ brands });
            }),
        );
    }

    @Action(CreateBrands)
    onCreateBrands(
        { setState }: StateContext<BrandsStateModel>,
        { model }: CreateBrands,
    ): Observable<any> {
        if (!model.entries.length) {
            return of();
        }
        return this.apiService.createList(model).pipe(
            tap((brands: ListModel[]) =>
                setState(
                    patch({
                        brands: append(brands),
                    }),
                ),
            ),
        );
    }

    @Action(UpdateBrands)
    onUpdateBrand(
        { setState }: StateContext<BrandsStateModel>,
        { model }: UpdateBrands,
    ): Observable<any> {
        if (!model.entries.length) {
            return of();
        }
        return this.apiService.update(model).pipe(
            tap(() => {
                model.entries.forEach((item) => {
                    setState(
                        patch({
                            brands: updateItem<ListModel>(
                                (s) => s.id === item.id,
                                patch({ name: item.name, image: item.image }),
                            ),
                        }),
                    );
                });
            }),
        );
    }

    @Action(DeleteBrand)
    onDeleteProduct(
        { setState }: StateContext<BrandsStateModel>,
        { id }: DeleteBrand,
    ): Observable<any> {
        const model = new DirectoryModel({ id, type: DirectoryType.Brands });
        return this.apiService.delete(model).pipe(
            tap(() =>
                setState(
                    patch({
                        brands: removeItem<ListModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }

    @Action(SelectDefaultBrand)
    onSelectDefaultMobileOperator({ dispatch, getState }: StateContext<BrandsStateModel>): void {
        const { brand } = getState();

        if (brand && brand.id) {
            dispatch(new NavigateToBrand(brand.id));
        } else {
            dispatch(new SelectFirstBrand());
        }
    }

    @Action(SelectFirstBrand)
    onSelectFirstMobileOperator({ dispatch, getState }: StateContext<BrandsStateModel>): void {
        const { brands } = getState();

        if (!brands || !brands.length) {
            dispatch(new NavigateToBrandsList());
            return;
        }

        const sortedResult = [...brands].sort(SortHelper.compareByNameFn);
        dispatch(new NavigateToBrand(sortedResult[0].id));
    }

    @Action(CreateBrand)
    onCreateBrand(
        { dispatch, setState }: StateContext<BrandsStateModel>,
        { name }: CreateBrand,
    ): Observable<any> {
        if (!name) {
            return of();
        }
        const model = new DirectoryModel({
            value: name,
            type: DirectoryType.Brands,
        });
        return this.apiService.create(model).pipe(
            tap((brand: ListModel) => {
                setState(patch({ brands: append([brand]) }));
                dispatch([new NavigateToBrand(brand.id)]);
            }),
        );
    }
}
