import { ListModel } from '@common/models/list.model';

export interface BrandsStateModel {
    brands: ListModel[];
    brand: ListModel;
    selectBrandId: number | null;
}
