import { DirectoryListModel } from '@common/models/directory-list.model';

export class LoadBrands {
    static readonly type = '[Brands] Load brands';
}

export class CreateBrands {
    static readonly type = '[Brands] Create brand';

    constructor(public model: DirectoryListModel) {}
}

export class UpdateBrands {
    static readonly type = '[Brands] Update brand';

    constructor(public model: DirectoryListModel) {}
}

export class DeleteBrand {
    static readonly type = '[Brands] delete brand';

    constructor(public id: number) {}
}

export class NavigateToBrand {
    static readonly type = '[Brands] Navigate to brand';
    constructor(public id: number) {}
}

export class SelectFirstBrand {
    static readonly type = '[Brands] Select first Brand';
}

export class SelectDefaultBrand {
    static readonly type = '[Brands] Select default Brand';
}

export class NavigateToBrandsList {
    static readonly type = '[Brands] Navigate to Brands list';
}

export class CreateBrand {
    static readonly type = '[Brands] Create new brand';
    constructor(public name: string) {}
}

export class SelectBrandId {
    static readonly type = '[Brands] Select brand';
    constructor(public id: number) {}
}
