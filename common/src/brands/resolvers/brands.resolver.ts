import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';

import { ListModel } from '@common/models/list.model';

import { LoadBrands } from '../store/brands.actions';
import { BrandsQuery } from '../store/brands.query';

@Injectable()
export class BrandsResolver implements Resolve<any> {
    constructor(public store: Store) {}

    resolve(): Observable<any> {
        const brands: ListModel[] = this.store.selectSnapshot(BrandsQuery.brands);
        return brands && brands.length ? of(brands) : this.store.dispatch(new LoadBrands());
    }
}
