import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { BrandsState } from './store/brands.state';
import { BrandsApiService } from './services/brands-api.service';
import { BrandsResolver } from './resolvers/brands.resolver';

@NgModule({
    imports: [NgxsModule.forFeature([BrandsState])],
    providers: [BrandsApiService, BrandsResolver],
})
export class BrandsDataModule {}
