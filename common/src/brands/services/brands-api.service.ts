import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { BaseApiService } from '@common/abstract/base-api.service';
import { ListModel } from '@common/models/list.model';
import { DirectoryListModel } from '@common/models/directory-list.model';
import { DirectoryModel } from '@common/models/directory.model';

@Injectable()
export class BrandsApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(): Observable<ListModel[]> {
        return this.httpGet('directory/brands', (x) => new ListModel(x));
    }

    public create(models: DirectoryModel): Observable<any> {
        return this.httpPost('directory', (x) => new ListModel(x), models);
    }

    public createList(models: DirectoryListModel): Observable<any> {
        return this.httpPost('directory/batch', (x) => new DirectoryListModel(x), models);
    }

    public update(models: DirectoryListModel): Observable<any> {
        return this.httpPut('directory/batch', (x) => new ListModel(x), models);
    }

    public delete(model: DirectoryModel): Observable<DirectoryModel> {
        return super.httpPost('directory/delete', (x) => x, model);
    }
}
