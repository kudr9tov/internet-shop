import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-image-lightbox',
    templateUrl: './image-lightbox.component.html',
    styleUrls: ['./image-lightbox.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ImageLightboxComponent {
    @Input() images: string[];

    slideIndex = 0;

    mainSlideIndex = 0;

    width = '365px';

    height = '450px';

    imageDerectives = {
        itemtype: 'http://schema.org/ImageObject',
        itemscope: '',
    };

    openModal(): void {
        document.getElementById('imgModal').style.display = 'block';
    }

    closeModal(): void {
        document.getElementById('imgModal').style.display = 'none';
    }

    plusSlides(n): void {
        this.showSlides((this.slideIndex += n));
    }

    currentSlide(n): void {
        this.mainSlideIndex = n - 1;
        this.showSlides((this.slideIndex = n));
    }

    showSlides(slideIndex);
    showSlides(n) {
        let i;
        const slides = document.getElementsByClassName(
            'img-slides',
        ) as HTMLCollectionOf<HTMLElement>;
        const dots = document.getElementsByClassName('images') as HTMLCollectionOf<HTMLElement>;
        if (n > slides.length) {
            this.slideIndex = 1;
        }
        if (n < 1) {
            this.slideIndex = slides.length;
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = 'none';
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(' active', '');
        }
        slides[this.slideIndex - 1].style.display = 'block';
        if (dots && dots.length > 0) {
            dots[this.slideIndex - 1].className += ' active';
        }
    }
}
