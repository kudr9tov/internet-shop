import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { AppIconModule } from '@common/app-icon/app-icon.module';
import { ImageWrapperModule } from '@common/image-wrapper/image-wrapper.module';
import { ImageLightboxComponent } from './image-lightbox.component';

@NgModule({
    declarations: [ImageLightboxComponent],
    imports: [CommonModule, MatCardModule, MatButtonModule, AppIconModule, ImageWrapperModule],
    exports: [ImageLightboxComponent],
})
export class ImageLightboxModule {}
