import { CategoryModel } from '@common/models/category.model';

export class LoadBreadcrumbs {
    static readonly type = '[Folders] Load entries for breadcrumbs';
    constructor(public payload: CategoryModel) {}
}

export class ResetBreadcrumbs {
    static readonly type = '[Folders] Reset breadcrumbs';
}
