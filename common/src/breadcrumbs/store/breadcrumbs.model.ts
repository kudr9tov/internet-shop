import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { CategoryModel } from '@common/models/category.model';

export interface BreadcrumbsStateModel {
    breadcrumbs: TodoItemFlatNode[];
    parent: CategoryModel;
}
