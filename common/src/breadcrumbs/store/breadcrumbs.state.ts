import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { Injectable } from '@angular/core';

import { BreadcrumbsStateModel } from './breadcrumbs.model';
import { LoadBreadcrumbs, ResetBreadcrumbs } from './breadcrumbs.actions';
import { CategoryModel } from '@common/models/category.model';
import { CategoriesQuery } from '@common/auth/store/categories/categories.query';
import { TreeHelper } from '@common/helpers/tree.helper';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';

@State<BreadcrumbsStateModel>({
    name: 'breadcrumbsState',
    defaults: {
        breadcrumbs: [],
        parent: new CategoryModel(),
    },
})
@Injectable()
export class BreadcrumbsState {
    @Selector()
    static breadcrumbs({ breadcrumbs }: BreadcrumbsStateModel): TodoItemFlatNode[] {
        return breadcrumbs;
    }

    @Selector()
    static parent({ parent }: BreadcrumbsStateModel): CategoryModel {
        return parent;
    }

    constructor(private store: Store) {}

    @Action(LoadBreadcrumbs)
    onLoadBreadcrumbs(
        { patchState }: StateContext<BreadcrumbsStateModel>,
        { payload }: LoadBreadcrumbs,
    ): void {
        const categories: CategoryModel[] = this.store.selectSnapshot(CategoriesQuery.categories);
        const breadcrumbs = TreeHelper.findeAllParent(payload, categories);
        patchState({
            breadcrumbs: breadcrumbs.subCategory || [],
            parent: breadcrumbs.categoryId,
        });
    }

    @Action(ResetBreadcrumbs)
    onResetBreadcrumbs({ patchState }: StateContext<BreadcrumbsStateModel>): void {
        patchState({ breadcrumbs: [] });
    }
}
