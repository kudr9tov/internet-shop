import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';

import { PipesModule } from '@common/pipes/pipes.module';

import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { BreadcrumbsState } from './store/breadcrumbs.state';
import { AppIconModule } from '@common/app-icon/app-icon.module';

@NgModule({
    declarations: [BreadcrumbsComponent],
    imports: [
        CommonModule,
        AppIconModule,
        MatButtonModule,
        MatTooltipModule,
        PipesModule,
        NgxsModule.forFeature([BreadcrumbsState]),
    ],
    exports: [BreadcrumbsComponent],
})
export class BreadcrumbsModule {}
