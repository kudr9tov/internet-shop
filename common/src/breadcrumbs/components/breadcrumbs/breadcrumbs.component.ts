import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { CategoryModel } from '@common/models/category.model';

@Component({
    selector: 'breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BreadcrumbsComponent {
    @Input() breadcrumbs!: TodoItemFlatNode[] | null;

    @Input() parent!: CategoryModel | null;

    @Output() navigateTo = new EventEmitter<any>();

    onNavigateToSubCategory(model: any): void {
        this.navigateTo.emit(model);
    }

    trackByFn(_: number, item: { id: number }): number {
        return item.id;
    }
}
