import { BreadcrumbsModule } from './breadcrumbs.module';

describe('BreadcrumbsModule', () => {
    let module: BreadcrumbsModule;

    beforeEach(() => {
        module = new BreadcrumbsModule();
    });

    it('should create an instance', () => {
        expect(module).toBeTruthy();
    });
});
