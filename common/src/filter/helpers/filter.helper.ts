export class FilterHelper {
    static addOrRemove(array: number[], values: number[]): number[] {
        const newArray = [...array];
        values.forEach((value) => {
            const index = newArray?.indexOf(value);

            if (index === -1) {
                newArray.push(value);
            } else {
                newArray.splice(index, 1);
            }
        });

        return newArray;
    }
}
