import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

import { SetAllFilter } from '@common/filter/store/filter.actions';

@Injectable()
export class PriceResolver implements Resolve<any> {
    constructor(public store: Store) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const actions: any = [];
        if (Object.keys(route.queryParams).length !== 0) {
            actions.push(new SetAllFilter(route.queryParams));
            if (route.queryParams.subCategory) {
                // console.log('prise');
                //   await this.store.dispatch(new LoadCategories()).toPromise();
            }
        }
        return this.store.dispatch(actions);
    }
}
