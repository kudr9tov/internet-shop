import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { FilterState } from './store/filter.state';
import { FilterApiService } from './services/filter-api.service';

@NgModule({
    imports: [NgxsModule.forFeature([FilterState])],
    providers: [FilterApiService],
})
export class FilterDataModule {}
