import { CategoryModel } from '@common/models/category.model';
import { PriceModel } from '@common/models/price.model';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';

export class SetCountry {
    static readonly type = '[Filter] set Country';

    constructor(public id: number) {}
}

export class SetBrand {
    static readonly type = '[Filter] set brand';

    constructor(public id: number) {}
}

export class SetAllFilter {
    static readonly type = '[Filter] set all filter';

    constructor(public payload: any) {}
}

export class SetCategory {
    static readonly type = '[Filter] set category';

    constructor(public id: number) {}
}

export class SetSubCategory {
    static readonly type = '[Filter] set sub category';

    constructor(public items: TodoItemFlatNode[]) {}
}

export class RemoveSubCategory {
    static readonly type = '[Filter] remove sub category';

    constructor(public id: number) {}
}

export class SetPageIndex {
    static readonly type = '[Filter] Set Page Index';

    constructor(public pageIndex: number) {}
}

export class SetItemsPerPage {
    static readonly type = '[Filter] Set Items Per Page';

    constructor(public itemsPerPage: number) {}
}

export class ClearCategory {
    static readonly type = '[Filter] clear category';
}

export class OpenCatalog {
    static readonly type = '[Filter] open catalog';

    constructor(public item: CategoryModel) {}
}

export class SearchInCatalog {
    static readonly type = '[Filter] open and search in catalog';
}

export class SetSearch {
    static readonly type = '[Filter] Set search term';

    constructor(public payload: string) {}
}

export class SetPrice {
    static readonly type = '[Filter] Set price';

    constructor(public payload: PriceModel) {}
}

export class SetOrigPrice {
    static readonly type = '[Filter] Set orig price';

    constructor(public payload: PriceModel) {}
}

export class GetSearchOptions {
    static readonly type = '[Filter] Get search options';

    constructor(public query: string) {}
}

export class ToggleSale {
    static readonly type = '[Filter] Toggle Sale';
}

export class ToggleNew {
    static readonly type = '[Filter] Toggle New';
}

export class ResetFilter {
    static readonly type = '[Filter] Reset Filter';
}
