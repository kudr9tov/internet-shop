import { Selector } from '@ngxs/store';

import { BrandsState } from '@common/brands/store/brands.state';
import { CategoryExtModel } from '@common/models/category-ext.model';
import { CategoryModel } from '@common/models/category.model';
import { CategoryState } from '@common/auth/store/categories/categories.state';
import { CountriesState } from '@common/countries/store/countries.state';
import { DirectoryType } from '@common/enums/directory-type.enum';
import { ListModel } from '@common/models/list.model';
import { PriceModel } from '@common/models/price.model';
import { SelectableListModel } from '@common/models/selectable-list.model';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { AppConstants, AppHelper } from '@common/constants/app.const';

import { FilterState } from './filter.state';

export class FilterQuery {
    @Selector([CountriesState.countries, FilterState.countryIds])
    static countries(counries: ListModel[], countryIds: number[]): SelectableListModel[] {
        if (!counries || !counries.length) {
            return [];
        }
        return counries.map((x) => {
            const index = countryIds?.indexOf(x.id);
            return new SelectableListModel({
                ...x,
                type: DirectoryType.Countries,
                selected: index !== -1,
            });
        });
    }

    @Selector([BrandsState.brands, FilterState.brandIds])
    static brands(brands: ListModel[], brandIds: number[]): SelectableListModel[] {
        if (!brands || !brands.length) {
            return [];
        }
        return brands.map((x) => {
            const index = brandIds?.indexOf(x.id);
            return new SelectableListModel({
                ...x,
                type: DirectoryType.Brands,
                selected: index !== -1,
            });
        });
    }

    @Selector([FilterQuery.selectedItems])
    static titleDescription(selectedItems: SelectableListModel[]): {
        title: string;
        description: string;
    } {
        let brandTitle,
            categoryTitle,
            subCategories,
            countriesTitle = '';
        if (selectedItems && selectedItems.length) {
            brandTitle = selectedItems
                .filter((x) => x.type === DirectoryType.Brands)
                ?.map((x) => x.name)
                ?.join(', ');
            categoryTitle = selectedItems.find(
                (x) => x.type === DirectoryType.Category && (x as any).parentCategoryId === null,
            )?.name;
            subCategories = selectedItems
                .filter(
                    (x) =>
                        x.type === DirectoryType.Category && (x as any).parentCategoryId !== null,
                )
                ?.map((x) => x.name)
                ?.join(', ');
            countriesTitle = selectedItems
                .filter((x) => x.type === DirectoryType.Countries)
                .map((x) => x.name)
                .join(', ');
        }
        return {
            title: AppHelper.getCatalogTitle(
                countriesTitle,
                brandTitle || '',
                categoryTitle || AppConstants.catalogTitle,
                subCategories || '',
            ),
            description: AppHelper.getCatalogdescription(
                categoryTitle || AppConstants.catalogDescription,
                subCategories,
            ),
        };
    }

    @Selector([CategoryState.categories, FilterState.categoryId])
    static categories(categories: CategoryModel[], categoryId: number): CategoryExtModel[] {
        if (!categories.length) {
            return [];
        }
        return categories.map(
            (x) =>
                new CategoryExtModel({
                    ...x,
                    selected: x.id === categoryId,
                }),
        );
    }

    @Selector([CategoryState.categories, FilterState.subCategoryIds, FilterState.categoryId])
    static subCategories(
        categories: CategoryModel[],
        subCategoryIds: number[],
        categoryId: number,
    ): CategoryExtModel[] {
        if (!categoryId) {
            return [];
        }
        const selected = categories.find((x) => x.id === categoryId);
        if (!selected || !selected.subCategory.length) {
            return [];
        }
        return selected.subCategory.map((x) => {
            const index = subCategoryIds && subCategoryIds.indexOf(x.id);
            return new CategoryExtModel({ ...x, selected: index !== -1 });
        });
    }

    @Selector([CategoryState.categories, FilterState.categoryId])
    static subCategories2(
        categories: CategoryModel[],
        categoryId: number,
    ): CategoryModel[] | undefined {
        if (!categoryId) {
            return [];
        }
        return categories.find((x) => x.id === categoryId)?.subCategory;
    }

    @Selector([FilterState.origPrice])
    static origPrice(origPrice: PriceModel): PriceModel {
        return origPrice;
    }

    @Selector([FilterState.pageIndex])
    static pageIndex(pageIndex: number): number {
        return pageIndex;
    }

    @Selector([FilterState.itemsPerPage])
    static itemsPerPage(itemsPerPage: number): number {
        return itemsPerPage;
    }

    @Selector([FilterState.pageSizeOptions])
    static pageSizeOptions(pageSizeOptions: number[]): number[] {
        return pageSizeOptions;
    }

    @Selector([FilterState.price])
    static price(price: PriceModel): PriceModel {
        return price;
    }

    @Selector([FilterState.query])
    static query(query: string): string {
        return query;
    }

    @Selector([FilterState.isNew])
    static isNew(isNew: boolean): boolean {
        return isNew;
    }

    @Selector([FilterState.isSale])
    static isSale(isSale: boolean): boolean {
        return isSale;
    }

    @Selector([
        FilterQuery.countries,
        FilterQuery.brands,
        CategoryState.categories,
        FilterState.subCategory,
        FilterState.categoryId,
    ])
    static selectedItems(
        countries: SelectableListModel[],
        brands: SelectableListModel[],
        categories: CategoryModel[],
        subCategory: TodoItemFlatNode[],
        categoryId: number,
    ): SelectableListModel[] {
        const result = [];
        if (countries) {
            result.push(...countries.filter((x) => x.selected));
        }
        if (brands) {
            result.push(...brands.filter((x) => x.selected));
        }
        if (categoryId) {
            const selected = categories.find((x) => x.id === categoryId);
            result.push(
                new SelectableListModel({
                    ...selected,
                    type: DirectoryType.Category,
                    selected: true,
                    isParent: true,
                }),
            );
            if (subCategory && subCategory.length) {
                const test = subCategory.slice().sort((a, b) => b.level - a.level);
                test.forEach((x) => {
                    if (!test.some((y) => y.id === x.parentCategoryId)) {
                        result.push(
                            new SelectableListModel({
                                ...x,
                                name: x.item,
                                type: DirectoryType.Category,
                                selected: true,
                            }),
                        );
                    }
                });
            }
        }
        return result;
    }
}
