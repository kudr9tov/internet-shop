import { PriceModel } from '@common/models/price.model';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { ProductModel } from '@common/models/product.model';

export interface FilterStateModel {
    countryIds: number[];
    brandIds: number[];
    categoryId: number;
    subCategory: TodoItemFlatNode[];
    query: string;
    isNew: boolean;
    isSale: boolean;
    price: PriceModel;
    pageIndex: number;
    itemsPerPage: number;
    origPrice: PriceModel;
    pageSizeOptions: number[];
    searchOptions: ProductModel[];
}
