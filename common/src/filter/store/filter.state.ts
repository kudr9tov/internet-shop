import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Navigate, RouterState } from '@ngxs/router-plugin';
import { Observable } from 'rxjs';
import { patch, removeItem } from '@ngxs/store/operators';
import { tap } from 'rxjs/operators';

import { CategoryState } from '@common/auth/store/categories/categories.state';
import { LoadCatalog, LoadPrice } from '@common/catalogData/store/catalog.actions';
import { PriceModel } from '@common/models/price.model';
import { ProductModel } from '@common/models/product.model';
import { ProductsSearchModel } from '@common/models/products-search.model';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { TreeHelper } from '@common/helpers/tree.helper';

import { FilterStateModel } from './filter.model';
import {
    ClearCategory,
    GetSearchOptions,
    OpenCatalog,
    RemoveSubCategory,
    ResetFilter,
    SearchInCatalog,
    SetAllFilter,
    SetBrand,
    SetCategory,
    SetCountry,
    SetItemsPerPage,
    SetPageIndex,
    SetPrice,
    SetSearch,
    SetSubCategory,
    ToggleNew,
    ToggleSale,
} from './filter.actions';
import { FilterHelper } from '../helpers/filter.helper';
import { FilterApiService } from '../services/filter-api.service';
import { AppConstants } from '@common/constants/app.const';
import { CategoryModel } from '@common/models/category.model';

@State<FilterStateModel>({
    name: 'filter',
    defaults: {
        countryIds: [],
        brandIds: [],
        subCategory: [],
        categoryId: 0,
        query: '',
        price: new PriceModel({ max: 0, min: 0 }),
        isSale: false,
        isNew: false,
        origPrice: new PriceModel(),
        pageIndex: AppConstants.DefaultPageIndex,
        itemsPerPage: AppConstants.DefaultItemsPerPage,
        pageSizeOptions: [AppConstants.DefaultItemsPerPage, 24, 36],
        searchOptions: [],
    },
})
@Injectable()
export class FilterState {
    constructor(
        private apiService: FilterApiService,
        public store: Store, //    public route: Route
    ) {}

    @Selector()
    static searchOptions({ searchOptions }: FilterStateModel): ProductModel[] {
        return searchOptions;
    }

    @Selector()
    static countryIds({ countryIds }: FilterStateModel): number[] {
        return countryIds;
    }

    @Selector()
    static pageSizeOptions({ pageSizeOptions }: FilterStateModel): number[] {
        return pageSizeOptions;
    }

    @Selector()
    static brandIds({ brandIds }: FilterStateModel): number[] {
        return brandIds;
    }

    @Selector()
    static subCategoryIds({ subCategory }: FilterStateModel): number[] {
        return subCategory && subCategory.length ? subCategory.map((x) => x.id) : [];
    }

    @Selector()
    static subCategory({ subCategory }: FilterStateModel): TodoItemFlatNode[] {
        return subCategory;
    }

    @Selector()
    static categoryId({ categoryId }: FilterStateModel): number {
        return categoryId;
    }

    @Selector()
    static disabledSubCategory({ categoryId }: FilterStateModel): boolean {
        return categoryId === null;
    }

    @Selector()
    static origPrice({ origPrice }: FilterStateModel): PriceModel {
        return origPrice;
    }

    @Selector()
    static price({ price }: FilterStateModel): PriceModel {
        return price;
    }

    @Selector()
    static isSale({ isSale }: FilterStateModel): boolean {
        return isSale;
    }

    @Selector()
    static isNew({ isNew }: FilterStateModel): boolean {
        return isNew;
    }

    @Selector()
    static pageIndex({ pageIndex }: FilterStateModel): number {
        return pageIndex;
    }

    @Selector()
    static itemsPerPage({ itemsPerPage }: FilterStateModel): number {
        return itemsPerPage;
    }

    @Selector()
    static query({ query }: FilterStateModel): string {
        return query;
    }

    @Action(SetPageIndex)
    onSetPageIndex(
        { patchState, getState, dispatch }: StateContext<FilterStateModel>,
        { pageIndex }: SetPageIndex,
    ): void {
        const { itemsPerPage } = getState();
        patchState({ pageIndex });
        dispatch(
            new Navigate(
                [],
                { skip: (pageIndex - 1) * itemsPerPage },
                { queryParamsHandling: 'merge' },
            ),
        );
    }

    @Action(SetItemsPerPage)
    onSetItemsPerPage(
        { patchState, dispatch }: StateContext<FilterStateModel>,
        { itemsPerPage }: SetItemsPerPage,
    ): void {
        patchState({ itemsPerPage });
        dispatch(
            new Navigate([], { limit: itemsPerPage, skip: null }, { queryParamsHandling: 'merge' }),
        );
    }

    @Action(ToggleSale)
    onToggleSale({ patchState, getState, dispatch }: StateContext<FilterStateModel>): void {
        const { isSale } = getState();
        patchState({ isSale: !isSale });
        dispatch(
            new Navigate([], { newItems: !isSale, skip: null }, { queryParamsHandling: 'merge' }),
        );
    }

    @Action(ToggleNew)
    onToggleNew({ patchState, getState, dispatch }: StateContext<FilterStateModel>): void {
        const { isNew } = getState();
        patchState({ isNew: !isNew });
        dispatch(
            new Navigate([], { newItems: !isNew, skip: null }, { queryParamsHandling: 'merge' }),
        );
    }

    @Action(SetCountry)
    onSetCountry(
        { patchState, getState, dispatch }: StateContext<FilterStateModel>,
        { id }: SetCountry,
    ): void {
        const { countryIds } = getState();
        const newCountryIds = FilterHelper.addOrRemove(countryIds, [id]);
        patchState({ countryIds: newCountryIds });
        dispatch(
            new Navigate(
                [],
                {
                    country: newCountryIds.length ? newCountryIds : null,
                    skip: null,
                },
                { queryParamsHandling: 'merge' },
            ),
        );
    }

    @Action(SetBrand)
    onSetBrand(
        { patchState, getState, dispatch }: StateContext<FilterStateModel>,
        { id }: SetBrand,
    ): void {
        const { brandIds } = getState();
        const newBrandIds = FilterHelper.addOrRemove(brandIds, [id]);
        patchState({ brandIds: newBrandIds });
        dispatch(
            new Navigate(
                [],
                { brand: newBrandIds.length ? newBrandIds : null, skip: null },
                { queryParamsHandling: 'merge' },
            ),
        );
    }

    @Action(SetSubCategory)
    onSetSubCategory(
        { patchState, dispatch }: StateContext<FilterStateModel>,
        { items }: SetSubCategory,
    ): void {
        patchState({ subCategory: items });
        dispatch(
            new Navigate(
                [],
                { subCategory: items.map((x) => x.id), skip: null },
                { queryParamsHandling: 'merge' },
            ),
        );
    }

    @Action(RemoveSubCategory)
    onRemoveSubCategory(
        { setState, dispatch, getState }: StateContext<FilterStateModel>,
        { id }: RemoveSubCategory,
    ): void {
        setState(
            patch({
                subCategory: removeItem<TodoItemFlatNode>((x) => x.id === id),
            }),
        );
        const { subCategory } = getState();
        dispatch(
            new Navigate(
                [],
                { subCategory: subCategory.map((x) => x.id), skip: null },
                { queryParamsHandling: 'merge' },
            ),
        );
    }

    @Action(LoadPrice)
    onLoadPrice({ patchState }: StateContext<FilterStateModel>): Observable<any> {
        return this.apiService
            .getPrice()
            .pipe(tap((price: PriceModel) => patchState({ origPrice: price, price })));
    }

    @Action(SetCategory)
    onSetCategory(
        { patchState, getState, dispatch }: StateContext<FilterStateModel>,
        { id }: SetCountry,
    ): void {
        const { categoryId } = getState();

        if (categoryId === id) {
            dispatch(new ClearCategory());
        } else {
            patchState({ subCategory: [], categoryId: id });
            dispatch(
                new Navigate(
                    [],
                    { category: id, subCategory: null, skip: null },
                    { queryParamsHandling: 'merge' },
                ),
            );
        }
    }

    @Action(OpenCatalog)
    onOpenCatalog(
        { dispatch, patchState }: StateContext<FilterStateModel>,
        { item }: OpenCatalog,
    ): void {
        let categoryId = item.id;
        const subCategory: TodoItemFlatNode[] = [];
        const categories = this.store.selectSnapshot(CategoryState.categories);
        if (item.parentCategoryId) {
            categoryId = TreeHelper.findeFirstParent(item, [...categories]);
            subCategory.push(
                new TodoItemFlatNode({
                    id: item.id,
                    item: item.name,
                    level: item.level,
                }),
            );
        }
        patchState({
            categoryId,
            subCategory,
        });
        const { state } = this.store.selectSnapshot(RouterState);
        if (state.url?.indexOf('catalog') > -1 && !(state.url?.indexOf('catalog/product') > -1)) {
            dispatch(new LoadCatalog(state.root.queryParams));
            return;
        }
        dispatch(new Navigate(['catalog']));
    }

    @Action(SearchInCatalog)
    onSearchInCatalog({ dispatch, patchState, getState }: StateContext<FilterStateModel>): void {
        const { origPrice } = getState();
        patchState({
            countryIds: [],
            brandIds: [],
            subCategory: [],
            categoryId: undefined,
            searchOptions: [],
            price: origPrice,
        });

        const { state } = this.store.selectSnapshot(RouterState);
        if (state.url?.indexOf('catalog') > -1 && !(state.url?.indexOf('catalog/product') > -1)) {
            dispatch(new LoadCatalog(state.root.queryParams));
            return;
        }
        dispatch(new Navigate(['catalog'], state.root?.queryParams));
    }

    @Action(ClearCategory)
    onClearCategory({ patchState, dispatch }: StateContext<FilterStateModel>): void {
        patchState({
            categoryId: undefined,
            subCategory: undefined,
        });
        dispatch(
            new Navigate(
                [],
                { category: null, subCategory: null },
                { queryParamsHandling: 'merge' },
            ),
        );
    }

    @Action(SetAllFilter)
    onSetAllFilter(
        { patchState, dispatch }: StateContext<FilterStateModel>,
        { payload }: SetAllFilter,
    ): void {
        const subCategoryIds = payload.subCategory
            ? Array.isArray(payload.subCategory)
                ? [...payload.subCategory.map((x: string | number) => +x)]
                : [+payload.subCategory]
            : [];
        const brandIds = payload.brand
            ? Array.isArray(payload.brand)
                ? [...payload.brand.map((x: string | number) => +x)]
                : [+payload.brand]
            : [];
        const countryIds = payload.country
            ? Array.isArray(payload.country)
                ? [...payload.country.map((x: string | number) => +x)]
                : [+payload.country]
            : [];
        const itemsPerPage = +payload.limit || AppConstants.DefaultItemsPerPage;
        const pageIndex = payload.skip
            ? +payload.skip / itemsPerPage
            : AppConstants.DefaultPageIndex;
        let subCategory: TodoItemFlatNode[] = [];
        let categoryId = +payload?.category;
        if (subCategoryIds.length) {
            const categories = this.store.selectSnapshot(CategoryState.categories);
            subCategory = TreeHelper.findeByIds(subCategoryIds, categories);
            if (!categoryId) {
                categoryId = TreeHelper.findeFirstParent(
                    new CategoryModel({ id: subCategoryIds[0] }),
                    categories,
                );
            }
        }
        if (payload.maxPrice || payload.minPrice) {
            patchState({
                price: new PriceModel({
                    max: +payload.maxPrice || 0,
                    min: +payload.minPrice || 0,
                }),
            });
        } else {
            dispatch(new LoadPrice());
        }

        patchState({
            countryIds,
            brandIds,
            subCategory,
            itemsPerPage,
            pageIndex,
            categoryId,
            query: payload?.query || '',
        });
        dispatch(new LoadCatalog(payload));
    }

    @Action(ResetFilter)
    onResetFilter({ patchState, getState, dispatch }: StateContext<FilterStateModel>): void {
        const { origPrice } = getState();
        patchState({
            countryIds: [],
            brandIds: [],
            subCategory: [],
            categoryId: undefined,
            query: '',
            searchOptions: [],
            price: origPrice,
            isNew: false,
            isSale: false,
        });
        dispatch(new Navigate([], {}, { queryParamsHandling: '' }));
    }

    @Action(SetSearch)
    onSetSearch(
        { patchState, dispatch }: StateContext<FilterStateModel>,
        { payload }: SetSearch,
    ): void {
        patchState({ query: payload });
        if (!payload) {
            patchState({
                searchOptions: [],
                pageIndex: AppConstants.DefaultPageIndex,
            });
            dispatch(new Navigate([], { skip: null }, { queryParamsHandling: 'merge' }));
            return;
        }
        dispatch([
            new GetSearchOptions(payload),
            new Navigate([], { query: payload }, { queryParamsHandling: 'merge' }),
        ]);
    }

    @Action(SetPrice)
    onSetPrice(
        { patchState, getState, dispatch }: StateContext<FilterStateModel>,
        { payload }: SetPrice,
    ): void {
        patchState({ price: payload });
        const { origPrice } = getState();
        const isCorrect =
            payload &&
            origPrice &&
            (payload.max !== origPrice.max || payload.min !== origPrice.min);
        dispatch(
            new Navigate(
                [],
                {
                    minPrice: isCorrect ? payload.min : null,
                    maxPrice: isCorrect ? payload.max : null,
                    skip: null,
                },
                { queryParamsHandling: 'merge' },
            ),
        );
    }

    @Action(GetSearchOptions)
    onGetSearchOptions(
        { patchState }: StateContext<FilterStateModel>,
        { query }: GetSearchOptions,
    ): Observable<any> {
        const search = {} as any;
        if (query) {
            search['query'] = query;
        }
        return this.apiService.getOptions(search, 5).pipe(
            tap((filterProducts: ProductsSearchModel) => {
                patchState({ searchOptions: filterProducts.products || [] });
            }),
        );
    }
}

// Router.prototype.updateQueryParams = function (activatedRoute: ActivatedRoute, params: Params): Promise<boolean> {
//     const context: Router = this;

//     if (isNullOrUndefined(activatedRoute)) {
//         throw new Error('Cannot update the query parameters - Activated Route not provided to use relative route');
//     }

//     // setTimeout required because there is an unintended behaviour when rapidly firing router updates in the same repaint cycle:
//     //
//     // NavigationCancel - Navigation ID 2 is not equal to the current navigation id 3
//     // https://stackoverflow.com/a/42802182/1335789
//     return new Promise<boolean>((resolve) => {
//         setTimeout(() => {
//             resolve(context.navigate([], {
//                 relativeTo: activatedRoute,
//                 queryParams: params,
//                 queryParamsHandling: 'merge'
//             }));
//         });
//     });
// };
