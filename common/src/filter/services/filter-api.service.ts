import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { BaseApiService } from '@common/abstract/base-api.service';
import { ListModel } from '@common/models/list.model';
import { ProductsSearchModel } from '@common/models/products-search.model';
import { PriceModel } from '@common/models/price.model';

@Injectable()
export class FilterApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(): Observable<ListModel[]> {
        return this.httpGet('directory/brands', (x) => new ListModel(x));
    }

    public getPrice(): Observable<PriceModel> {
        return this.httpGet('price/maxmin', (x) => new PriceModel(x));
    }

    public getOptions(filter: any = null, count: number): Observable<any> {
        const url =
            Object.keys(filter).length !== 0
                ? `products?limit=${count}&skip=${0}&filter=${encodeURIComponent(
                      JSON.stringify(filter),
                  )}`
                : `products?limit=${count}&skip=${0}`;
        return this.httpGet(url, (x) => new ProductsSearchModel(x));
    }
}
