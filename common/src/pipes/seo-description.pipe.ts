import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'seoDescription',
})
export class SeoDescriptionPipes implements PipeTransform {
    transform(content: string, length: number): string {
        if (!content) {
            return '';
        }

        return content.replace(/(<([^>]+)>)/gi, '').substr(0, length) + '...';
    }
}
