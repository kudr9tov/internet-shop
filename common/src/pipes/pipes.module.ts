import { NgModule } from '@angular/core';

import { precentageDeductionPipe } from './percentage-deduction.pipe';
import { OrderBy } from './order-by.pipe';
import { ArraySortPipe } from './formarray-sort.pipe';
import { NoSanitizePipe } from './no-sanitize.pipe';
import { SeoDescriptionPipes } from './seo-description.pipe';
import { GetQueryParams } from './get-query-params.pipe';
import { AbsoluteUrlPipe } from './absolute-url.pipe';
import { LettersPipe } from './get-letters.pipe';

const pipes = [
    precentageDeductionPipe,
    ArraySortPipe,
    NoSanitizePipe,
    OrderBy,
    GetQueryParams,
    AbsoluteUrlPipe,
    SeoDescriptionPipes,
    LettersPipe,
];

@NgModule({
    declarations: pipes,
    exports: pipes,
    providers: pipes,
})
export class PipesModule {}
