import { Pipe, PipeTransform } from '@angular/core';
import { TreeHelper } from '@common/helpers/tree.helper';
import { CategoryModel } from '@common/models/category.model';

@Pipe({
    name: 'queryParams',
})
export class GetQueryParams implements PipeTransform {
    transform(categories: CategoryModel[], category: CategoryModel): any {
        if (!categories.length) {
            return;
        }

        return category.parentCategoryId
            ? {
                  subCategory: `${category.id}`,
                  category: `${TreeHelper.findeFirstParent(category, [...categories])}`,
              }
            : { category: `${category.id}` };
    }
}
