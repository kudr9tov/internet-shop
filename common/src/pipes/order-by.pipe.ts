import { Pipe, PipeTransform } from '@angular/core';

export class OrderByModel {
    OrderType: string;
    Reverse: string;
    constructor(type: string, reverse: '' | '-' = '-') {
        this.OrderType = type;
        this.Reverse = reverse;
    }
    OrderBy(): string {
        return this.Reverse + this.OrderType;
    }
}

@Pipe({
    name: 'orderBy',
})
export class OrderBy implements PipeTransform {
    private static _orderBy(a: any, b: any): number {
        if (a === undefined && b === undefined) {
            return 0;
        } else if (a === undefined) {
            return 1;
        } else if (b === undefined) {
            return -1;
        }
        if (a instanceof Date && b instanceof Date) {
            return a < b ? -1 : a > b ? 1 : 0;
        }
        // if (a instanceof moment && b instanceof moment) {
        //     return moment.utc(a).isBefore(moment.utc(b)) ? 1 : moment.utc(a).isAfter(moment.utc(b)) ? -1 : 0;
        // }

        if (typeof a === 'boolean' && typeof b === 'boolean') {
            return b > a ? 1 : a > b ? -1 : 0;
        }

        if (typeof a === 'string' && typeof b === 'string') {
            const lowerA = a.toLowerCase();
            const lowerB = b.toLowerCase();
            return lowerA < lowerB ? -1 : lowerA > lowerB ? 1 : 0;
        }

        const floatA = parseFloat(a);
        const floatB = parseFloat(b);

        if (isNaN(floatA) || !isFinite(a) || isNaN(floatB) || !isFinite(b)) {
            // if (a instanceof moment) { a = (a as moment.Moment).format('MM/DD/YYYY'); }
            // if (b instanceof moment) { b = (b as moment.Moment).format('MM/DD/YYYY'); }

            if (a === null || a === undefined) {
                a = '';
            }
            if (b === null || b === undefined) {
                b = '';
            }

            const lowerA = a.toLowerCase();
            const lowerB = b.toLowerCase();
            return lowerA < lowerB ? -1 : lowerA > lowerB ? 1 : 0;
        } else {
            return floatA < floatB ? -1 : floatA > floatB ? 1 : 0;
        }
    }

    transform(input: any, config: any = '+'): any {
        if (!Array.isArray(input)) {
            return input;
        }

        const configIsArray = Array.isArray(config);

        if (!configIsArray || (configIsArray && config.length === 1)) {
            const propertyToCheck: string = configIsArray ? config[0] : config;
            const first = propertyToCheck.substr(0, 1);
            const desc = first === '-';

            if (!propertyToCheck || propertyToCheck === '-' || propertyToCheck === '+') {
                return desc ? [...input].sort().reverse() : [...input].sort();
            } else {
                const property =
                    first === '+' || desc ? propertyToCheck.substr(1) : propertyToCheck;

                return [...input].sort((a: any, b: any) => {
                    const propList = property.split('.');
                    let valueA = a[propList[0]];
                    let valueB = b[propList[0]];
                    for (let i = 1; i < propList.length; i++) {
                        valueA = valueA[propList[i]];
                        valueB = valueB[propList[i]];
                    }
                    const comparator = OrderBy._orderBy(valueA, valueB);
                    return desc ? -comparator : comparator;
                });
            }
        } else {
            return [...input].sort((a: any, b: any) => {
                for (let i = 0; i < config.length; ++i) {
                    const first = config[i].substr(0, 1);
                    const desc = first === '-';
                    const property = first === '+' || desc ? config[i].substr(1) : config[i];
                    const propList = property.split('.');

                    let valueA = a[propList[0]];
                    let valueB = b[propList[0]];
                    for (let j = 1; j < propList.length; j++) {
                        valueA = valueA[propList[j]];
                        valueB = valueB[propList[j]];
                    }
                    const comparator = OrderBy._orderBy(valueA, valueB);
                    const comparison = desc ? -comparator : comparator;

                    if (comparison !== 0) {
                        return comparison;
                    }
                }

                return 0;
            });
        }
    }
}
