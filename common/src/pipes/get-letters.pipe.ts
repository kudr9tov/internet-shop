import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'getLetters' })
export class LettersPipe implements PipeTransform {
    transform(input: string): string {
        if (!input) {
            return '';
        }

        const txtArr = input.trim().replace(/  +/g, ' ').split(' ');

        return !txtArr.length
            ? ''
            : txtArr.length > 1
            ? `${Array.from(txtArr[0])[0]}${Array.from(txtArr[1])[0]}`
            : txtArr[0].length > 1
            ? `${Array.from(txtArr[0])[0]}${Array.from(txtArr[0])[1]}`
            : Array.from(txtArr[0])[0];
    }
}
