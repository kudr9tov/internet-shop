import { Pipe, PipeTransform } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Pipe({
    name: 'formSort',
    pure: false,
})
export class ArraySortPipe implements PipeTransform {
    transform(array: Array<AbstractControl>, args: string): Array<AbstractControl> {
        if (array !== undefined) {
            return array.sort((a: any, b: any) => {
                const aValue = a.controls[args].value;
                const bValue = b.controls[args].value;

                if (aValue < bValue) {
                    return -1;
                } else if (aValue > bValue) {
                    return 1;
                } else {
                    return 0;
                }
            });
        }
        return array;
    }
}
