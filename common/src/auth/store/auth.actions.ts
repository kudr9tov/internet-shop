import { DeviceSize } from '../../enums/device-size.enum';

export class LoadBasicRoles {
    static readonly type = '[Auth] Load basic roles';
}

export class SetDeviceSize {
    static readonly type = '[Auth] Set device size';
    constructor(public payload: DeviceSize) {}
}

export class TogglePrimaryMenu {
    static readonly type = '[Auth] Call toggle method';
}
