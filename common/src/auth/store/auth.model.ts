import { ListModel } from '../../models/list.model';
import { DeviceSize } from '../../enums/device-size.enum';

export interface AuthStateModel {
    basicRoles: ListModel[];
    deviceSize: DeviceSize;
    mobilePhones: string[];
    email: string;
}
