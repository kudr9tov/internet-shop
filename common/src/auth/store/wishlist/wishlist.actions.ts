import { ProductModel } from '@common/models/product.model';

export class AddToWishlist {
    static readonly type = '[Wishlist] add to Wishlist';

    constructor(public payload: ProductModel) {}
}

export class RemoveFromWishlist {
    static readonly type = '[Wishlist] remove from Wishlist';

    constructor(public payload: number) {}
}
