import { Injectable } from '@angular/core';
import { ProductModel } from '@common/models/product.model';
import { SetSuccess } from '@common/toast';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { append, patch, removeItem } from '@ngxs/store/operators';
import { AddToWishlist, RemoveFromWishlist } from './wishlist.actions';

import { WishlistStateModel } from './wishlist.model';

@State<WishlistStateModel>({
    name: 'wishlistState',
    defaults: {
        wishlist: [],
    },
})
@Injectable()
export class WishlistState {
    @Selector()
    static wishlist({ wishlist }: WishlistStateModel): ProductModel[] {
        return wishlist;
    }

    @Selector()
    static wishlistCount({ wishlist }: WishlistStateModel): number {
        return wishlist?.length;
    }

    @Action(AddToWishlist)
    onAddToWishlist(
        { setState, getState, dispatch }: StateContext<WishlistStateModel>,
        { payload }: AddToWishlist,
    ): void {
        const { wishlist } = getState();
        const index = wishlist.findIndex((x) => x.id === payload.id);

        if (index === -1) {
            setState(
                patch({
                    wishlist: append([payload]),
                }),
            );
        }
        dispatch(new SetSuccess(`Товар "${payload.name}" добавлен в список ожидания!`));
    }

    @Action(RemoveFromWishlist)
    onRemoveFromBasket(
        { setState }: StateContext<WishlistStateModel>,
        { payload }: RemoveFromWishlist,
    ): void {
        setState(
            patch({
                wishlist: removeItem<ProductModel>((x) => x.id === payload),
            }),
        );
    }
}
