import { ProductModel } from '@common/models/product.model';

export interface WishlistStateModel {
    wishlist: ProductModel[];
}
