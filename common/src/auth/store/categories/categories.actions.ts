import { CategoryModel } from '@common/models/category.model';
import { DirectoryListModel } from '@common/models/directory-list.model';
import { DirectoryModel } from '@common/models/directory.model';

export class LoadCategories {
    static readonly type = '[Categories] Load categories';
}

export class SelectCategory {
    static readonly type = '[Categories] Select category';
    constructor(public category: CategoryModel) {}
}

export class DeleteCategory {
    static readonly type = '[Categories] Delete category';
    constructor(public id: number) {}
}

export class MoveCategory {
    static readonly type = '[Categories] Move category';
    constructor(public parentId: number, public categoryIds: number[]) {}
}

export class NavigateToCategory {
    static readonly type = '[Categories] Navigate to category';
    constructor(public id: number) {}
}

export class NavigateToCategoriesList {
    static readonly type = '[Categories] Navigate to categories listv';
}

export class UpdateCategory {
    static readonly type = '[Categories] Update category';
    constructor(public model: DirectoryModel) {}
}

export class UpdateSubCategory {
    static readonly type = '[Categories] Update sub category';
    constructor(public model: DirectoryListModel) {}
}

export class CreateCategory {
    static readonly type = '[Categories] Create new category';
    constructor(public name: string) {}
}

export class CreateSubCategory {
    static readonly type = '[Categories] Create new sub category';
    constructor(public models: DirectoryListModel) {}
}

export class SelectDefaultCategory {
    static readonly type = '[Categories] Select default category';
}

export class SelectFirstCategory {
    static readonly type = '[Categories] Select first category';
}

export class SelectCategoryId {
    static readonly type = '[Categories] Select category id';
    constructor(public id: number) {}
}
