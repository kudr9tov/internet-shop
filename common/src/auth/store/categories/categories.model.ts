import { CategoryModel } from '../../../models/category.model';

export interface CategoriesStateModel {
    categories: CategoryModel[];
    category: CategoryModel;
    selectCategoryId: number | null;
}
