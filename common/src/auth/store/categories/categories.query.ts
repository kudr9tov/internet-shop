import { Selector } from '@ngxs/store';

import { CategoryModel } from '@common/models/category.model';
import { ListModel } from '@common/models/list.model';
import { SortHelper } from '@common/helpers/sort.helper';
import { TreeHelper } from '@common/helpers/tree.helper';

import { CategoryState } from './categories.state';
import { CategoryLinkModel } from '@common/models/category-link.model';
import { LinkModel } from '@common/models/link.model';

export class CategoriesQuery {
    @Selector([CategoryState.categories])
    static allCategories(categories: CategoryModel[]): CategoryModel[] {
        return categories;
    }

    @Selector([CategoryState.categories])
    static categories(categories: CategoryModel[]): CategoryModel[] {
        return categories
            .map((x) => new CategoryModel({ ...x }))
            .sort(SortHelper.compareByNamePriorFn);
    }

    @Selector([CategoryState.categories, CategoryState.selectCategoryId])
    static subCategories(categories: CategoryModel[], id: number): CategoryLinkModel[] {
        return this.getsubCategory(categories, id).map((category) => {
            return new CategoryLinkModel({
                ...category,
                link:
                    category.subCategory && category.subCategory.length
                        ? ['../../subcategories/' + category.id]
                        : ['../../catalog'],
                queryParams:
                    category.subCategory && category.subCategory.length
                        ? null
                        : {
                              subCategory: category.id,
                              category: TreeHelper.findeFirstParent(category, [...categories]),
                          },
            });
        });
    }

    @Selector([CategoryState.selectedCategory])
    static selectedCategory(category: CategoryModel): CategoryModel {
        return category;
    }

    @Selector([CategoryState.selectCategoryId, CategoryState.categories])
    static subCategoryBreadcrumb(id: number, categories: CategoryModel[]): LinkModel[] {
        return TreeHelper.findeSubCategoryParent(TreeHelper.findeById(id, categories), categories);
    }

    @Selector([CategoryState.categories])
    static categoriesList(categories: CategoryModel[]): ListModel[] {
        return categories && categories.length
            ? categories.map((x) => new ListModel({ ...x })).sort(SortHelper.compareByNamePriorFn)
            : [];
    }

    @Selector([CategoryState.categories])
    static mainCategories(categories: CategoryModel[]): CategoryLinkModel[] {
        return categories && categories.length
            ? categories
                  .map(
                      (x) =>
                          new CategoryLinkModel({
                              id: x.id,
                              name: x.name,
                              image: x.image,
                              link: 'subcategories/' + x.id,
                              subCategoryLink: x.subCategory.map(
                                  (y) =>
                                      new CategoryLinkModel({
                                          id: y.id,
                                          name: y.name,
                                          link:
                                              y.subCategory && y.subCategory.length
                                                  ? 'subcategories/' + y.id
                                                  : 'catalog',
                                          queryParams:
                                              y.subCategory && y.subCategory.length
                                                  ? null
                                                  : {
                                                        subCategory: y.id,
                                                        category: x.id,
                                                    },
                                      }),
                              ),
                          }),
                  )
                  .sort(SortHelper.compareByNamePriorFn)
            : [];
    }

    static getsubCategory(categories: CategoryModel[], id: number): CategoryModel[] {
        let result: CategoryModel[] = [];
        (categories || []).forEach((category) => {
            if (category.id === id) {
                result = category.subCategory;
                return;
            }
            if (!result.length && category.subCategory && category.subCategory.length) {
                result = this.getsubCategory(category.subCategory, id);
            }
        });
        return result;
    }
}
