import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';

import { SortHelper } from '@common/helpers/sort.helper';

import {
    CreateCategory,
    CreateSubCategory,
    DeleteCategory,
    LoadCategories,
    MoveCategory,
    NavigateToCategoriesList,
    NavigateToCategory,
    SelectCategory,
    SelectCategoryId,
    SelectDefaultCategory,
    SelectFirstCategory,
    UpdateCategory,
    UpdateSubCategory,
} from './categories.actions';
import { CategoriesStateModel } from './categories.model';
import { CategoryModel } from '../../../models/category.model';
import { CategoryApiService } from '../../services/category-api.service';
import { DirectoryType } from '@common/enums/directory-type.enum';
import { DirectoryModel } from '@common/models/directory.model';
import { TreeHelper } from '@common/helpers/tree.helper';

@State<CategoriesStateModel>({
    name: 'categoryState',
    defaults: {
        categories: [],
        category: new CategoryModel(),
        selectCategoryId: null,
    },
})
@Injectable()
export class CategoryState {
    @Selector()
    static categories({ categories }: CategoriesStateModel): CategoryModel[] {
        return categories;
    }

    @Selector()
    static selectedCategory({ category }: CategoriesStateModel): CategoryModel | null {
        return category;
    }

    @Selector()
    static selectCategoryId({ selectCategoryId }: CategoriesStateModel): number | null {
        return selectCategoryId;
    }

    constructor(private apiService: CategoryApiService) {}

    @Action(LoadCategories)
    onLoadCategories({ patchState }: StateContext<CategoriesStateModel>): Observable<any> {
        return this.apiService.get().pipe(tap((categories) => patchState({ categories })));
    }

    @Action(SelectCategory)
    onSelectCategory(
        { patchState }: StateContext<CategoriesStateModel>,
        { category }: SelectCategory,
    ): void {
        patchState({ category });
    }

    @Action(SelectCategoryId)
    onSelectCategoryId(
        { patchState }: StateContext<CategoriesStateModel>,
        { id }: SelectCategoryId,
    ): void {
        patchState({ selectCategoryId: id });
    }

    @Action(DeleteCategory)
    onDeleteCategory(
        { setState }: StateContext<CategoriesStateModel>,
        { id }: DeleteCategory,
    ): Observable<any> {
        const model = new DirectoryModel({ id, type: DirectoryType.Category });
        return this.apiService.delete(model).pipe(
            tap(() => {
                setState(
                    patch({
                        categories: removeItem<CategoryModel>((x) => x.id === id),
                        category: patch<CategoryModel>({
                            subCategory: removeItem<CategoryModel>((x) => x.id === id),
                        }),
                    }),
                );
            }),
        );
    }

    @Action(MoveCategory)
    onMoveCategory(
        { dispatch }: StateContext<CategoriesStateModel>,
        { parentId, categoryIds }: MoveCategory,
    ): Observable<any> {
        const model = { parentId, categoryIds };
        return this.apiService.move(model).pipe(
            tap(() => {
                () => dispatch(new LoadCategories());
            }),
        );
    }

    @Action(SelectDefaultCategory)
    onSelectDefaultMobileOperator({
        dispatch,
        getState,
    }: StateContext<CategoriesStateModel>): void {
        const { category } = getState();

        if (category && category.id) {
            dispatch(new NavigateToCategory(category.id));
        } else {
            dispatch(new SelectFirstCategory());
        }
    }

    @Action(CreateCategory)
    onCreateCategory(
        { dispatch, setState }: StateContext<CategoriesStateModel>,
        { name }: CreateCategory,
    ): Observable<any> {
        if (!name) {
            return of();
        }
        const model = new DirectoryModel({
            value: name,
            type: DirectoryType.Category,
        });
        return this.apiService.create(model).pipe(
            tap((categories: DirectoryModel) => {
                const category = new CategoryModel({
                    ...categories,
                    name: categories.value,
                });
                setState(patch({ categories: append([category]) }));
                dispatch([new NavigateToCategory(category.id)]);
            }),
        );
    }

    @Action(UpdateCategory)
    onUpdateCategory(
        { setState }: StateContext<CategoriesStateModel>,
        { model }: UpdateCategory,
    ): Observable<any> {
        return this.apiService.update(model).pipe(
            tap((category: DirectoryModel) => {
                if (!category) {
                    return;
                }
                setState(
                    patch({
                        categories: updateItem<CategoryModel>(
                            (x) => x.id === category.id,
                            patch({
                                name: category.value,
                                image: category.image,
                            }),
                        ),
                    }),
                );
            }),
        );
    }

    @Action(CreateSubCategory)
    onCreateSubCategory(
        { setState, getState }: StateContext<CategoriesStateModel>,
        { models }: CreateSubCategory,
    ): Observable<any> {
        if (!models.entries.length) {
            return of();
        }
        const { category } = getState();
        return this.apiService.createList(models).pipe(
            tap((categories: CategoryModel[]) => {
                const createdModel = categories.filter(
                    (x) => category?.subCategory.findIndex((y) => y.id === x.id) === -1,
                );
                setState(
                    patch({
                        categories: updateItem<CategoryModel>(
                            (x) => x.id === models.parentId,
                            patch<CategoryModel>({
                                subCategory: append(createdModel),
                            }),
                        ),
                        category: patch<CategoryModel>({
                            subCategory: append(createdModel),
                        }),
                    }),
                );
            }),
        );
    }

    @Action(UpdateSubCategory)
    onUpdateSubCategory(
        { setState, getState }: StateContext<CategoriesStateModel>,
        { model }: UpdateSubCategory,
    ): Observable<any> {
        if (!model.entries.length) {
            return of();
        }
        return this.apiService.updateList(model).pipe(
            tap(() => {
                const { categories } = getState();

                const update = TreeHelper.findeById(model.parentId, categories);
                const updateSub = update.subCategory.map((x) => {
                    const index = model.entries.findIndex((y) => y.id === x.id);
                    if (index !== -1) {
                        return new CategoryModel({ ...model.entries[index] });
                    }
                    return x;
                });
                setState(
                    patch({
                        categories: updateItem<CategoryModel>(
                            (x) => x.id === model.parentId,
                            patch<CategoryModel>({
                                subCategory: updateSub,
                            }),
                        ),
                    }),
                );
            }),
        );
    }

    @Action(SelectFirstCategory)
    onSelectFirstMobileOperator({ dispatch, getState }: StateContext<CategoriesStateModel>): void {
        const { categories } = getState();

        if (!categories || !categories.length) {
            dispatch(new NavigateToCategoriesList());
            return;
        }

        const sortedResult = [...categories].sort(SortHelper.compareByNameFn);
        dispatch(new NavigateToCategory(sortedResult[0].id));
    }
}
