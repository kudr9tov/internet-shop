import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { mergeMap, tap } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';
import { Observable } from 'rxjs';

import { CurrentUserApiService } from '../../services/current-user-api.service';
import { CurrentUserStateModel } from './current-user.model';
import {
    ForgotPassword,
    LoadCurrentUser,
    Login,
    LoginForResetPassword,
    Logout,
    Unauthorized,
    CheckCurrentUser,
} from './current-user.actions';
import { AuthorizedPersonModel } from '../../../models/authorized-person.model';
import { Roles } from '../../../enums/roles.enum';
import { SetError } from '../../../toast';
import { ForgotPasswordModel } from '../../../models/forgot-password.model';
import { NavigateTo } from '@common/navigation/store/navigation.actions';

@State<CurrentUserStateModel>({
    name: 'currentUserState',
    defaults: {
        current: null,
    },
})
@Injectable()
export class CurrentUserState {
    @Selector()
    static user({ current }: CurrentUserStateModel): AuthorizedPersonModel | null {
        return current;
    }

    @Selector()
    static userEmail({ current }: CurrentUserStateModel): string {
        return current?.email || '';
    }

    @Selector()
    static userId({ current }: CurrentUserStateModel): number {
        return current?.id || 0;
    }

    @Selector()
    static userRole({ current }: CurrentUserStateModel): string {
        return current?.role || '';
    }

    @Selector()
    static userFullName({ current }: CurrentUserStateModel): string {
        return current?.name || '';
    }

    @Selector()
    static isUseradmin(state: CurrentUserStateModel): boolean {
        return !!state.current && state.current.role === Roles.Admin;
    }

    constructor(private apiService: CurrentUserApiService) {}

    @Action(LoadCurrentUser)
    onLoadCurrentUser({ patchState }: StateContext<CurrentUserStateModel>): Observable<any> {
        return this.apiService.checkCurrent().pipe(
            tap((current) => {
                patchState({ current });
            }),
        );
    }

    @Action(CheckCurrentUser)
    onCheckCurrentUser({
        dispatch,
        patchState,
    }: StateContext<CurrentUserStateModel>): Observable<any> {
        return this.apiService.checkCurrent().pipe(
            tap((current) => {
                patchState({ current });
                if (current.role === Roles.Admin) {
                    dispatch(new NavigateTo('/admin/#/dashboard'));
                }
            }),
        );
    }

    @Action(Unauthorized)
    onUnauthorized({ dispatch, patchState }: StateContext<CurrentUserStateModel>): void {
        patchState({ current: undefined });
        dispatch(new Navigate(['login']));
    }

    @Action(Login)
    onLogin(
        { patchState, dispatch }: StateContext<CurrentUserStateModel>,
        { payload }: Login,
    ): Observable<any> {
        return this.apiService.login(payload).pipe(
            tap(
                (current) => patchState({ current }),
                (err) => {
                    dispatch(new SetError(err.error.message));
                },
            ),
            mergeMap(() => dispatch(new NavigateTo('/admin/#/dashboard'))),
        );
    }

    @Action(Logout)
    onLogout({ dispatch, patchState }: StateContext<CurrentUserStateModel>): Observable<any> {
        return this.apiService.logout().pipe(
            tap(() => {
                return dispatch(new NavigateTo('/')).subscribe(() => {
                    patchState({ current: undefined });
                });
            }),
        );
    }

    @Action(LoginForResetPassword)
    onLoginForResetPassword(
        { patchState }: StateContext<CurrentUserStateModel>,
        { payload }: LoginForResetPassword,
    ): Observable<any> {
        return this.apiService.loginForResetPassword(payload).pipe(
            tap((current) => {
                patchState({ current });
            }),
        );
    }

    @Action(ForgotPassword)
    onForgotPassword(
        {}: StateContext<CurrentUserStateModel>,
        { payload }: ForgotPassword,
    ): Observable<ForgotPasswordModel> {
        return this.apiService.forgotPassword(payload);
    }
}
