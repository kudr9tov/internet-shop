import { AuthorizedPersonModel } from '../../../models/authorized-person.model';

export interface CurrentUserStateModel {
    current: AuthorizedPersonModel | null;
}
