import { Selector } from '@ngxs/store';

import { CurrentUserState } from './current-user.state';
import { AuthorizedPersonModel } from '../../../models/authorized-person.model';

export class CurrentUserQuery {
    @Selector([CurrentUserState.user])
    static user(user: AuthorizedPersonModel): AuthorizedPersonModel {
        return user;
    }
}
