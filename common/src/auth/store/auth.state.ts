import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthStateModel } from './auth.model';
import { CurrentUserState } from './current-user/current-user.state';
import { LoadBasicRoles, SetDeviceSize } from './auth.actions';
import { RolesApiService } from '../services/roles-api.service';
import { CategoryState } from './categories/categories.state';
import { DeviceSize } from '../../enums/device-size.enum';
import { ListModel } from '../../models/list.model';
import { environment } from '@environments/environment';

@State<AuthStateModel>({
    name: 'authState',
    defaults: {
        basicRoles: [],
        deviceSize: DeviceSize.Desktop,
        mobilePhones: [environment.phone],
        email: `info@${environment.domen}`,
    },
    children: [CurrentUserState, CategoryState],
})
@Injectable()
export class AuthState {
    @Selector()
    static basicRoles(state: AuthStateModel): ListModel[] {
        return state.basicRoles;
    }

    @Selector()
    static mobilePhones({ mobilePhones }: AuthStateModel): string[] {
        return mobilePhones;
    }

    @Selector()
    static email({ email }: AuthStateModel): string {
        return email;
    }

    @Selector()
    static isMobile(state: AuthStateModel): boolean {
        return state.deviceSize === DeviceSize.Mobile;
    }

    constructor(private rolesApiService: RolesApiService) {}

    @Action(LoadBasicRoles)
    onLoadBasicRoles({ patchState }: StateContext<AuthStateModel>): Observable<any> {
        return this.rolesApiService.get().pipe(
            tap((roles: ListModel[]) =>
                patchState({
                    basicRoles: roles,
                }),
            ),
        );
    }

    @Action(SetDeviceSize)
    onSetDeviceSize(
        { patchState }: StateContext<AuthStateModel>,
        { payload }: SetDeviceSize,
    ): void {
        patchState({ deviceSize: payload });
    }
}
