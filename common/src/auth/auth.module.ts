import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { HttpClientModule } from '@angular/common/http';

import { NavigationModule } from '@common/navigation/navigation.module';

import { AuthState } from './store/auth.state';
import { CategoryState } from './store/categories/categories.state';
import { CurrentUserApiService } from './services/current-user-api.service';
import { CurrentUserState } from './store/current-user/current-user.state';
import { RolesApiService } from './services/roles-api.service';
import { CategoryApiService } from './services/category-api.service';
import { CurrentUserResolver } from './current-user-resolver/current-user-resolver';
import { WishlistState } from './store/wishlist/wishlist.state';

@NgModule({
    imports: [
        NgxsModule.forFeature([AuthState, CurrentUserState, CategoryState, WishlistState]),
        NavigationModule,
        HttpClientModule,
    ],
    providers: [CurrentUserApiService, RolesApiService, CategoryApiService, CurrentUserResolver],
})
export class AuthModule {
    constructor(@Optional() @SkipSelf() parentModule: AuthModule) {
        if (parentModule) {
            throw new Error('AuthModule is already loaded. Import it in the AppModule only');
        }
    }
}
