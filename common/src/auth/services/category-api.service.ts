import { HttpClient } from '@angular/common/http';
import { Store } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { BaseAuthorizedApiService } from './base-authorized-api.service';
import { CategoryModel } from '../../models/category.model';
import { DirectoryListModel } from '@common/models/directory-list.model';
import { DirectoryModel } from '@common/models/directory.model';

@Injectable()
export class CategoryApiService extends BaseAuthorizedApiService {
    constructor(http: HttpClient, protected override store: Store) {
        super(http, store);
    }

    public get(): Observable<CategoryModel[]> {
        return this.httpGet('directory/category', (x) => new CategoryModel(x));
    }

    public create(models: DirectoryModel): Observable<DirectoryModel> {
        return this.httpPost('directory', (x) => new DirectoryListModel(x), models);
    }

    public update(models: DirectoryModel): Observable<DirectoryModel> {
        return super.httpPut('directory', (x) => new DirectoryListModel(x), models);
    }

    public createList(models: DirectoryListModel): Observable<CategoryModel[]> {
        return this.httpPost('directory/batch', (x) => new CategoryModel(x), models);
    }

    public updateList(models: DirectoryListModel): Observable<DirectoryListModel> {
        return super.httpPut('directory/batch', (x) => new DirectoryListModel(x), models);
    }

    public delete(model: DirectoryModel): Observable<DirectoryModel> {
        return super.httpPost('directory/delete', (x) => x, model);
    }

    public move(model: any): Observable<any> {
        return super.httpPost('directory/category/move', (x) => x, model);
    }
}
