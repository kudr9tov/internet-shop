import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { HttpClient } from '@angular/common/http';

import { BaseAuthorizedApiService } from './base-authorized-api.service';
import { AuthorizedPersonModel } from '../../models/authorized-person.model';
import { ChangePasswordModel } from '../../models/change-password.model';
import { LoginModel } from '../../models/login.model';
import { ForgotPasswordModel } from '../../models/forgot-password.model';

@Injectable()
export class CurrentUserApiService extends BaseAuthorizedApiService {
    constructor(http: HttpClient, protected override store: Store) {
        super(http, store);
    }

    public get(): Observable<AuthorizedPersonModel> {
        return this.httpGet('directory/category', (x) => new AuthorizedPersonModel(x));
    }

    public changePassword(model: ChangePasswordModel): Observable<any> {
        return this.httpPost('Authorization/ChangePassword', (x) => x, model);
    }

    public login(model: LoginModel): Observable<AuthorizedPersonModel> {
        return this.httpPost('users/login', (x) => new AuthorizedPersonModel(x), model);
    }

    public logout(): Observable<any> {
        return this.httpGet('auth/logout', (x) => x);
    }

    public checkCurrent(): Observable<AuthorizedPersonModel> {
        return this.httpGet('users/current-user', (x) => new AuthorizedPersonModel(x));
    }

    public restore(): Observable<AuthorizedPersonModel> {
        return this.httpGet('auth/restore', (x) => new AuthorizedPersonModel(x));
    }

    public loginForResetPassword(model: any): Observable<AuthorizedPersonModel> {
        return this.httpPost(
            'Authorization/LoginForResetPassword',
            (x) => new AuthorizedPersonModel(x),
            model,
        );
    }

    public forgotPassword(model: any): Observable<ForgotPasswordModel> {
        return this.httpPost(
            'Authorization/ForgotPassword',
            (x) => new ForgotPasswordModel(x),
            model,
        );
    }
}
