import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { HttpClient } from '@angular/common/http';

import { BaseAuthorizedApiService } from './base-authorized-api.service';
import { ListModel } from '../../models/list.model';

@Injectable()
export class RolesApiService extends BaseAuthorizedApiService {
    constructor(http: HttpClient, protected override store: Store) {
        super(http, store);
    }

    public get(): Observable<ListModel[]> {
        return this.httpGet('BasicRoleList', (x) => new ListModel(x));
    }
}
