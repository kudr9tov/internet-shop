import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngxs/store';

import { environment } from '@environments/environment';

import { Unauthorized } from '../store/current-user/current-user.actions';
import { BaseApiService } from '../../abstract/base-api.service';

@Injectable()
export class BaseAuthorizedApiService extends BaseApiService {
    constructor(http: HttpClient, protected override store: Store) {
        super(http, store);
    }

    public override handleError(error: any): void {
        if (!environment.production) {
            let message = error._body;
            message = error.status ? `${error.status} - ${error.statusText}` : message;
            message = message ? message : 'Server error';
            console.error(message);
        }
        if (error && error.status) {
            switch (error.status) {
                case 401:
                    this.store.dispatch(new Unauthorized());
                    break;
            }
        }
    }
}
