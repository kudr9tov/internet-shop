import { ChangeDetectorRef, Component, HostBinding, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Store } from '@ngxs/store';

import { UploadModel } from '@common/models/upload.model';
import { SetError } from '@common/toast';
import { FileType } from '@common/enums/file-type.enum';

import { UploaderService } from '../services/uploader.service';
import { FileMimeType } from '@common/enums/file-mime-type.enum';

@Component({
    selector: 'upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss'],
})
export class UploadComponent {
    isUploading = false;

    @Input() control!: AbstractControl | null;

    @Input() title = 'фотографию';

    @Input() apiPath = 'files/upload';

    @HostBinding('style.width')
    @Input()
    width = '300px';

    @HostBinding('style.height')
    @Input()
    height = '215px';

    file!: File;

    fileName = 'Файл не выбран';

    icon = 'photo';

    fileType = FileMimeType;

    format = FileMimeType.Image;

    selectMimeTypes: FileMimeType[] = [FileMimeType.Image];

    @Input() formats = [FileType.gif, FileType.jpeg, FileType.jpg, FileType.png, FileType.svg];

    constructor(
        private uploader: UploaderService,
        private store: Store,
        private ref: ChangeDetectorRef,
    ) {}

    ngOnInit(): void {
        const type =
            (this.control?.value && this.control.value.split('.').pop()) || this.formats[0];
        this.selectMimeTypes = this.onGetSelectMimeTypes(type);
        this.format = this.onGetType(type);
    }

    onChange(file: any): void {
        if (file) {
            this.fileName = file[0].name;
            this.file = file[0];

            const reader = new FileReader();
            reader.readAsDataURL(file[0]);
            reader.onload = () => {
                const type = (reader.result as any).split(';')[0].split('/')[1];
                this.format = this.onGetType(type);
                this.onUpload();
            };
        }
    }

    onDelete(): void {
        this.control?.setValue(null);
    }

    onUpload(): void {
        if (!this.file) {
            return;
        }

        this.isUploading = true;
        this.ref.detectChanges();
        this.uploader.upload(this.file, this.apiPath).subscribe(
            (model: UploadModel) => {
                this.control?.setValue(model.link);
                this.control?.markAsDirty();
                this.isUploading = false;
                this.ref.detectChanges();
            },
            (error) => {
                this.store.dispatch(new SetError(error.message));
                this.isUploading = false;
                this.ref.detectChanges();
            },
        );
    }

    onGetType(type: string): FileMimeType {
        switch (type) {
            case FileType.Pdf:
                this.icon = 'picture_as_pdf';
                return FileMimeType.Pdf;
            case FileType.Xls:
                this.icon = 'library_books';
                return FileMimeType.Xls;
            case FileType.Xlsx:
                this.icon = 'library_books';
                return FileMimeType.Xlsx;
            default:
                return FileMimeType.Image;
        }
    }

    onGetSelectMimeTypes(type: string): FileMimeType[] {
        switch (type) {
            case FileType.Pdf:
                return [FileMimeType.Pdf];
            case FileType.Xls:
            case FileType.Xlsx:
                return [FileMimeType.Xlsx, FileMimeType.Xls];
            default:
                return [FileMimeType.Image];
        }
    }
}
