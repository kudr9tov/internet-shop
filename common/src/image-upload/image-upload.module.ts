import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgModule } from '@angular/core';

import { UploadComponent } from './upload/upload.component';
import { UploaderService } from './services/uploader.service';

@NgModule({
    declarations: [UploadComponent],
    imports: [CommonModule, MatButtonModule, MatIconModule, MatProgressSpinnerModule],
    exports: [UploadComponent],
    providers: [UploaderService],
})
export class ImageUploadModule {}
