import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { BaseAuthorizedApiService } from '@common/auth/services/base-authorized-api.service';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { UploadModel } from '@common/models/upload.model';

export interface UploadResponse {
    imageUrl: string;
}

@Injectable()
export class UploaderService extends BaseAuthorizedApiService {
    constructor(http: HttpClient, protected override store: Store) {
        super(http, store);
    }

    public upload(file: File, apiPath: string): Observable<any> {
        // Instantiate a FormData to store form fields and encode the file
        const body = new FormData();
        // Add file content to prepare the request
        body.append('file', file);
        // Launch post request
        return this.httpPost(apiPath, (x) => new UploadModel(x), body);
    }

    public uploadForEditor(file: File, apiPath: string): Observable<HttpEvent<UploadResponse>> {
        // Instantiate a FormData to store form fields and encode the file
        const body = new FormData();
        // Add file content to prepare the request
        body.append('file', file);
        // Launch post request
        return this.httpPost(apiPath, (x) => ({ imageUrl: x.link } as UploadResponse), body);
    }
}
