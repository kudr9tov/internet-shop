export class ImageHelper {
    static isBase64(input: string): boolean {
        if (!input) {
            return false;
        }
        const base64regex = /^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$/;

        return base64regex.test(input);
    }
}
