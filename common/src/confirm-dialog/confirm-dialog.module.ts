import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { NgModule } from '@angular/core';

import { ConfirmDialogComponent } from './confirm-dialog.component';
import { ConfirmDialogLiteComponent } from './confirm-dialog-lite.component';

@NgModule({
    declarations: [ConfirmDialogComponent, ConfirmDialogLiteComponent],
    imports: [CommonModule, FormsModule, MatCheckboxModule, MatDialogModule, MatButtonModule],
    exports: [ConfirmDialogComponent, ConfirmDialogLiteComponent],
})
export class ConfirmDialogModule {}
