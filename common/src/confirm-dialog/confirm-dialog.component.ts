import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'confirm-dialog',
    templateUrl: 'confirm-dialog.component.html',
    styleUrls: ['confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
    isShowConfirm!: boolean;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    ) {}

    onClose(status: boolean): void {
        let result = 0;
        if (status) {
            result = this.isShowConfirm ? 2 : 1;
        }
        this.dialogRef.close(this.data.booleanResponse ? !!result : result);
    }
}
