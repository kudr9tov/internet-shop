import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'confirm-dialog-lite',
    templateUrl: 'confirm-dialog.component.html',
    styleUrls: ['confirm-dialog.component.scss'],
})
export class ConfirmDialogLiteComponent {
    isShowConfirm!: boolean;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<ConfirmDialogLiteComponent>,
    ) {}

    onClose(status: boolean): void {
        this.dialogRef.close(status);
    }
}
