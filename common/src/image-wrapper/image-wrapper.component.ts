import { Component, ElementRef, HostBinding, Input, ViewChild } from '@angular/core';

@Component({
    selector: 'image-wrapper',
    templateUrl: 'image-wrapper.component.html',
    styleUrls: ['image-wrapper.component.scss'],
})
export class ImageWrapperComponent {
    @ViewChild('wrapper', { static: true }) wrapper!: ElementRef;

    @Input() image!: string;

    @Input() name = 'image';

    @Input() link!: string;

    @Input() target: '_self' | '_blank' = '_blank';

    @Input() queryParams: any;

    @Input()
    set wrapperAttribute(value: any) {
        Object.keys(value).forEach((key) =>
            this.wrapper.nativeElement.setAttribute(key, value[key]),
        );
    }

    @HostBinding('style.width') @Input() width = '300px';

    @HostBinding('style.height') @Input() height = '200px';
}
