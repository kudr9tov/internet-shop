import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ImageWrapperComponent } from './image-wrapper.component';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS } from 'ng-lazyload-image';
import { LazyLoadImageHooks } from '@common/hooks/lazyload-image.hook';
import { RouterModule } from '@angular/router';

@NgModule({
    exports: [ImageWrapperComponent],
    imports: [CommonModule, LazyLoadImageModule, RouterModule],
    declarations: [ImageWrapperComponent],
    providers: [{ provide: LAZYLOAD_IMAGE_HOOKS, useClass: LazyLoadImageHooks }],
})
export class ImageWrapperModule {}
