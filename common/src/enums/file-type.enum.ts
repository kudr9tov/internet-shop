export enum FileType {
    Other,
    png = 'png',
    jpeg = 'jpeg',
    gif = 'gif',
    jpg = 'jpg',
    svg = 'svg',
    Xls = 'xls',
    Xlsx = 'xlsx',
    Pdf = 'pdf',
}
