export enum DirectoryType {
    Category = 'CATEGORY',
    Countries = 'COUNTRY',
    Brands = 'BRAND',
    Certificate = 'CERT',
}
