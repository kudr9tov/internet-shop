export enum FileMimeType {
    Other,
    Image = 'image/*',
    Xls = 'application/vnd.ms-excel',
    Xlsx = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    Pdf = 'application/pdf',
}
