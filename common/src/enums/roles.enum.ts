export enum Roles {
    Guest = 'GUEST',
    User = 'USER',
    Admin = 'ADMIN',
    Manager = 'MANAGER',
}
