import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { ImageUploadModule } from '@common/image-upload/image-upload.module';

import { CardItemComponent } from './components/card-item/card-item.component';
import { CardItemsComponent } from './components/card-items/card-items.component';

@NgModule({
    declarations: [CardItemComponent, CardItemsComponent],
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        ImageUploadModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        ReactiveFormsModule,
    ],
    exports: [CardItemComponent, CardItemsComponent],
})
export class CardItemModule {}
