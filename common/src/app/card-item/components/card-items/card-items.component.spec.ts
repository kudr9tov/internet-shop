import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CardItemsComponent } from './card-items.component';

describe('CardItemsComponent', () => {
    let component: CardItemsComponent;
    let fixture: ComponentFixture<CardItemsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CardItemsComponent],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(CardItemsComponent);
        component = fixture.componentInstance;
    }));

    afterEach(() => {
        fixture.destroy();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
