import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormArray } from '@angular/forms';

import { FileType } from '@common/enums/file-type.enum';

@Component({
    selector: 'card-items',
    templateUrl: './card-items.component.html',
    styleUrls: ['./card-items.component.scss'],
})
export class CardItemsComponent {
    @Input() formArray!: FormArray;

    @Input() label!: string;

    @Input() isHasItems!: boolean;

    @Input() placeholder!: string;

    @Input() title = 'фотографию';

    @Input() apiPath = 'files/upload';

    @Input() formats = [FileType.gif, FileType.jpeg, FileType.jpg, FileType.png, FileType.svg];

    @Output() removeItem = new EventEmitter<number>();

    onRemoveItem(index: number) {
        this.removeItem.emit(index);
    }

    trackByFn(index: number, _: any): number {
        return index;
    }
}
