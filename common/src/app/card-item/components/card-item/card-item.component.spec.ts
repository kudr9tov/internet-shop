import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CardItemComponent } from './card-item.component';

describe('CardItemComponent', () => {
    let component: CardItemComponent;
    let fixture: ComponentFixture<CardItemComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CardItemComponent],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(CardItemComponent);
        component = fixture.componentInstance;
    }));

    afterEach(() => {
        fixture.destroy();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
