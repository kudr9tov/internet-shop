import { Component, Input } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

import { FileType } from '@common/enums/file-type.enum';

@Component({
    selector: 'card-item',
    templateUrl: './card-item.component.html',
    styleUrls: ['./card-item.component.scss'],
})
export class CardItemComponent {
    @Input() label!: string;

    @Input() placeholder!: string;

    @Input()
    set nameControl(val: AbstractControl | null) {
        this.name = val as FormControl;
    }

    @Input() imageControl!: AbstractControl | null;

    @Input() title = 'фотографию';

    @Input() apiPath = 'files/upload';

    @Input() formats = [FileType.gif, FileType.jpeg, FileType.jpg, FileType.png, FileType.svg];

    name!: FormControl;
}
