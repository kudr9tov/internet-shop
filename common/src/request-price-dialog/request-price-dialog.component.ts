import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '@common/regex/custom.validators';

@Component({
    selector: 'request-price-dialog',
    templateUrl: 'request-price-dialog.component.html',
    styleUrls: ['request-price-dialog.component.scss'],
})
export class RequestPriceDialogComponent {
    requestGroup!: FormGroup;

    constructor(
        private dialogRef: MatDialogRef<RequestPriceDialogComponent>,
        public ref: ChangeDetectorRef,
        public fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {}

    ngOnInit(): void {
        this.setupFormGroups();
    }

    onSave(): void {
        this.dialogRef.close(this.requestGroup.value);
    }

    onClose(): void {
        this.dialogRef.close();
    }

    setupFormGroups(): void {
        this.requestGroup = this.fb.group({
            id: [null],
            phone: ['', Validators.required],
            email: ['', [Validators.required, CustomValidators.emailValidator()]],
            city: [''],
            name: [''],
            comment: [''],
            productId: [this.data.productId],
        });
    }
}
