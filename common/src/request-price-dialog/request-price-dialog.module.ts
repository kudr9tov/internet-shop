import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { RequestPriceDialogComponent } from './request-price-dialog.component';

@NgModule({
    declarations: [RequestPriceDialogComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatFormFieldModule,
        MatDialogModule,
        MatDividerModule,
        MatInputModule,
        ReactiveFormsModule,
    ],
})
export class RequestPriceDialogModule {}
