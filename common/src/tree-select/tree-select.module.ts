import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemPipe } from './pipes/item.pipe';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { OffClickDirective } from './directives/off-click.directive';
import { TreeSelectComponent } from './components/tree-select.component';
import { TreeSelectDefaultOptions } from './models/tree-select-default-options';
import { TreeSelectItemComponent } from './components/tree-select-item.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    imports: [
        CommonModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatMenuModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        FormsModule,
    ],
    declarations: [TreeSelectComponent, TreeSelectItemComponent, OffClickDirective, ItemPipe],
    exports: [TreeSelectComponent],
})
export class NgxTreeSelectModule {
    public static forRoot(
        options: TreeSelectDefaultOptions,
    ): ModuleWithProviders<NgxTreeSelectModule> {
        return {
            ngModule: NgxTreeSelectModule,
            providers: [{ provide: TreeSelectDefaultOptions, useValue: options }],
        };
    }
}
