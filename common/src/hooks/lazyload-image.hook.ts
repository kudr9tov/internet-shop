import { isPlatformServer } from '@angular/common';
import { Injectable } from '@angular/core';
import { IntersectionObserverHooks, Attributes } from 'ng-lazyload-image';

@Injectable()
export class LazyLoadImageHooks extends IntersectionObserverHooks {
    override skipLazyLoading(_: Attributes): boolean {
        // Skipping lazyloading the image for SSR
        return isPlatformServer(this.platformId);
    }

    override setup(attributes: Attributes): void {
        // Overwride the path to the default image for all lazyloaded images
        attributes.defaultImagePath = !!attributes.imagePath
            ? 'assets/svg/spinner.svg'
            : 'assets/svg/no-image.svg';
        attributes.errorImagePath = 'assets/svg/no-image.svg';
        // You can always call the base `setup` if you want to keep the default behavior
        super.setup(attributes);
    }

    override isBot(_?: Attributes): boolean {
        // Check if the user is a bot or not.
        // Is the same as `window.navigator` if window is defined otherwise undefined.
        return !this.navigator && isPlatformServer(this.platformId); // True if the code is running on the server
    }
}
