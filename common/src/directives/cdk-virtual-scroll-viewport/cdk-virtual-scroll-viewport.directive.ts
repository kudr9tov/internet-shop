import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { debounceTime } from 'rxjs/operators';
import { Directive, Inject, NgZone, OnDestroy, OnInit, Self } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';

@Directive({
    selector: 'cdk-virtual-scroll-viewport',
})
export class CdkVirtualScrollViewportPatchDirective implements OnInit, OnDestroy {
    protected subscription!: Subscription;

    constructor(
        @Self()
        @Inject(CdkVirtualScrollViewport)
        private readonly viewportComponent: CdkVirtualScrollViewport,
        private zone: NgZone,
    ) {}

    ngOnInit(): void {
        this.zone.runOutsideAngular(() => {
            this.subscription = fromEvent(window, 'resize')
                .pipe(debounceTime(10))
                .subscribe(() => this.viewportComponent.checkViewportSize());
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
