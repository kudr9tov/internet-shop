import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CdkVirtualScrollViewportPatchDirective } from './cdk-virtual-scroll-viewport.directive';

@NgModule({
    declarations: [CdkVirtualScrollViewportPatchDirective],
    imports: [CommonModule],
    exports: [CdkVirtualScrollViewportPatchDirective],
})
export class CdkVirtualScrollViewportPatchModule {}
