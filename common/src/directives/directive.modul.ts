import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ClickDirective } from './click.directive';

import {
    CollapseContentDirective,
    CollapseDirective,
    CollapseItemDirective,
} from './collapse.directive';
import { DropdownDirective } from './dropdown.directive';

const items = [
    CollapseContentDirective,
    CollapseItemDirective,
    CollapseDirective,
    DropdownDirective,
    ClickDirective,
];

@NgModule({
    declarations: [...items],
    imports: [CommonModule],
    exports: [...items],
})
export class DirectiveModule {}
