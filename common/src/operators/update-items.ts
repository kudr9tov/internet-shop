import { Predicate } from '@ngxs/store/operators/internals';
import { RepairType } from '@ngxs/store/operators/utils';
import { StateOperator } from '@ngxs/store';

import {
    findIndices,
    invalidIndexs,
    isArrayNumber,
    isObject,
    isPredicate,
    isStateOperator,
} from './utils';

/**
 * @param selector - Array of indices or a predicate function
 * that can be provided in `Array.prototype.findIndex`
 * @param operatorOrValue - New value under the `selector` index or a
 * function that can be applied to an existing value
 */
export function updateItems<T>(
    selector: number[] | Predicate<T>,
    operatorOrValue: Partial<T> | StateOperator<T>,
): StateOperator<T[]> {
    return function updateItemsOperator(existing: Readonly<T[]>): T[] {
        const clone = [...existing];
        let indices = [];
        if (!selector || !operatorOrValue) {
            return clone;
        }
        if (!clone) {
            return clone;
        }
        if (isPredicate(selector)) {
            indices = findIndices(selector, clone as RepairType<T>[]);
        } else if (isArrayNumber(selector)) {
            indices = selector;
        }

        if (invalidIndexs(indices, clone)) {
            return clone;
        }
        let values: Record<number, T> = {};

        if (isStateOperator(operatorOrValue)) {
            values = indices.reduce(
                (acc, it) => ({ ...acc, [it]: operatorOrValue(clone[it]) }),
                {},
            );
        } else {
            values = indices.reduce(
                (acc, it) =>
                    isObject(clone[it])
                        ? { ...acc, [it]: { ...clone[it], ...operatorOrValue } }
                        : { ...acc, [it]: operatorOrValue },
                {},
            );
        }

        const keys = Object.keys(values);
        keys.forEach((key) => {
            clone[key] = values[key];
        });
        return clone;
    };
}
