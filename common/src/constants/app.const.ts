import { ProductModel } from '@common/models/product.model';
import { environment } from '@environments/environment';

export const AppConstants = {
    DefaultItemsPerPage: 12,
    DefaultPageIndex: 1,
    mobileSize: 992,
    location: environment.location,
    signature: (location: any) =>
        `\n\n -------- \nС уважением "${environment.name}" \nАдрес: ${location} \nТелефон:  ${environment.phone} \nФакс:  ${environment.phone} \nE-mail: info@${environment.domen} \nАдрес сайта: ${environment.domen}`,
    subjectTemplate: 'Комплектация предприятий торговли, общепита, производства и строительства',
    titlePrefix: 'Купить ',
    titleSufix: ` купить в ${environment.domen}`,
    descriptionSufix: ` в каталоге интернет-магазина ${environment.domen}. Доставка по Москве. Звоните ${environment.phone}`,
    tile: `${environment.name} - оснащение предприятий торговли и общепита`,
    description: `Компания ${environment.name} является дилером и официальным представителем крупнейших отечественных, иностранных производителей оборудования торговли и общепита. Доставка по Москве. Звоните ${environment.phone}`,
    keywords:
        'металлическая мебель, оборудование, предприятий питания, торговое, холодильное, мебель, инструмент, лестницы, шкафы, промышленная, хлебопекарное, кондитерское, мясоперерабатывающее, нейтральное, кейтеринга, пивоварения, фастфуда, мармиты, стелажи, торг, лари, бонеты, камеры, холодильный, морозильный, моноблок, стол, льдогенераторы, сплит-система, горки, продажа, цены',
    catalogTitle: 'Каталог оборудования',
    catalogDescription:
        'Металлическая мебель, оборудование для предприятий питания, торговое оборудование, холодильное оборудование',
    aboutTitle: `Основной задачей Компании «${environment.name}» является максимальное удовлетворение потребностей и запросов наших клиентов`,
    aboutDescription: `Компания «${environment.name}» является торговой структурой и специализируется на комплексном оснащении предприятий торговли и общественного питания – магазины, супермаркеты, рестораны, кафе, столовые, школы, детские сады, промышленные предприятия, складские помещения, архивы, логистические центры и т.д.`,
    magazinesTitle: `${environment.name} каталоги оборудования`,
    magazinesDescription: `${environment.name}: каталог оборудования, приборов и инвентаря, мебели — цены, купить • ${environment.name}.`,
    serviceTitle: `${environment.name} Сервис`,
    serviceDescription: `Сервисное обслуживание, производимое сотрудниками Компании «${environment.name}», включает в себя: Консультации, Проектирование, Подбор оборудования, Продажа розничная, Продажа оптовая, Доставка оборудования, Подключение и ремонт, Сервисное обслуживание, Обучение персонала`,
    loginTitle: `Вход в систему Компании «${environment.name}»`,
    loginDescription: `Получение бонусов и персональных скидок при регистрации в интернет магазине ${environment.domen}`,
    contactsTitle: `Контакты интернет магазина ${environment.name}`,
    contactsDescription:
        'Удобные средства связи для покупателей, рекламодателей и соискателей, а также для потенциальных поставщиков и партнеров',
    brandsTitle: `Производители - ${environment.name}`,
    brandsDescription: `Крупнейшие отечественные производители оборудования и мебели в Москве, а также иностранных заводов – изготовителей оборудования для торговли и общепита.`,
    privacyTitle: `Политика в отношении обработки персональных данных - ${environment.name}`,
    privacyDescription:
        'Общие положения. Порядок сбора, основные понятия, хранения, цели, передачи и других видов обработки персональных данных. Заключительные положения',
    newsTitle: `Новости оборудования компании ${environment.name}.`,
    newsDescription: 'Новости 📃 об оборудовании для общепита, торговом, промышленном холодильном.',
};

export class AppHelper {
    public static description(product: ProductModel): string {
        return product && product.name
            ? product.name +
                  (!!product.vendorCode ? ` (${product.vendorCode}): ` : '') +
                  (!!product.price ? ` по цене ${product.price} руб.` : '') +
                  AppConstants.descriptionSufix
            : AppConstants.tile;
    }

    public static categoryDescription(title: string): string {
        return `${title} купить в интернет-магазине ${environment.name} по доступным ценам с доставкой по Москве`;
    }

    public static getCatalogdescription(
        productName: string,
        subCategories: string | undefined,
    ): string {
        return `Купить ${productName} - ${subCategories} со скидкой и 🚚 доставкой по всей Москве. ✅Самый большой выбор расцветок форм и размеров! 🎁 Скидки до 60% на акционные товары. ✅Всегда в наличии! ✅Можно в аренду! Звоните ☎${environment.phone}`;
    }

    public static getCatalogTitle(
        country: string,
        brand: string,
        category: string,
        subCategories: string,
    ): string {
        return (
            `${category} - ${subCategories || ''} Купить в Москве` +
            (!!brand ? ` [${brand}]` : '') +
            (!!country ? ' производитель: ' + country : '')
        );
    }

    public static getCategoryTitle(title: string): string {
        return `${title} 🎁 Скидки до 60% Купить c 🚚 доставкой по Москве`;
    }

    public static getProductTitle(product: ProductModel): string {
        return product && product.name
            ? product.name +
                  (!!product.vendorCode ? ` (${product.vendorCode}): ` : '') +
                  AppConstants.titleSufix +
                  (!!product.price ? ` по цене ${product.price} руб` : '')
            : AppConstants.tile;
    }
}
