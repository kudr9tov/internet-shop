import {
    AfterViewChecked,
    Component,
    EventEmitter,
    HostBinding,
    Input,
    OnDestroy,
    OnInit,
    Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSidenav } from '@angular/material/sidenav';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import {
    CloseSecondaryMenu,
    ExitCreatingMode,
    OpenSecondaryMenu,
} from './store/secondary-menu.actions';
import { SecondaryMenuState } from './store/secondary-menu.state';
import { SelectableListModel } from '@common/models/selectable-list.model';
import { ListModel } from '@common/models/list.model';

/**
 * Component Secondary menu
 *
 * @example
 *  <secondary-menu
 *      [items]="yourItems"
 *      [filterItems]="yourfilterItems"
 *      [menuElement]="yourSideNav"
 *      [menuStatus]="yourMenuStatus"
 *      (create)="onCreate($event)"
 *      (collapseChanged)="onCollapseChange($event)"
 *      (filter)="filterMenu($event)">
 *  </secondary-menu>
 */
@Component({
    selector: 'secondary-menu',
    templateUrl: 'secondary-menu.component.html',
    styleUrls: ['secondary-menu.component.scss'],
})
export class SecondaryMenuComponent implements AfterViewChecked, OnInit, OnDestroy {
    @Select(SecondaryMenuState.isCreatingMode)
    isCreatingMode$!: Observable<boolean>;

    /**
     * Filter items list. If it not empty, filter will be displayed, hidden otherwise
     * @type {ListModel[]}
     */
    private _filterItems!: SelectableListModel[];
    @Input()
    set filterItems(value: SelectableListModel[]) {
        this._filterItems = value || [];
        this.selectedItems = this.filterItems.filter((x) => x.selected);
    }
    get filterItems(): SelectableListModel[] {
        return this._filterItems;
    }

    /**
     * Menu items
     */
    @Input() items!: ListModel[] | null;

    @Input() showCreating = true;

    /**
     * Send an event to create new item
     */
    @Output() create = new EventEmitter();

    /**
     * Send an event to create new item
     */
    @Output() creatingMode = new EventEmitter();

    /**
     * Send an event with filter value
     */
    @Output() filter = new EventEmitter();

    /**
     * SideNav menu element
     */
    @Input() menuElement!: MatSidenav;

    @HostBinding('class.hidden-secondary-menu')
    private _isCollapsed!: boolean | null;
    @Input()
    get isCollapsed(): boolean | null {
        return this._isCollapsed;
    }
    set isCollapsed(val: boolean | null) {
        this.updateMenu(val);
        this._isCollapsed = val;
    }

    /**
     * FormGroup for validation
     */
    formGroup!: FormGroup;

    /**
     * Checking should show editable field with name or not
     */
    isCreating = false;

    /**
     * Selected filter items
     */
    selectedItems!: SelectableListModel[];

    /**
     * Checking when should scroll to new element
     */
    protected needToScroll = false;

    constructor(protected store: Store, protected formBuilder: FormBuilder) {}

    ngOnInit(): void {
        this.formGroup = this.formBuilder.group({
            name: [null, Validators.required],
        });
        this.isCreatingModeSubscription();
    }

    ngOnDestroy(): void {
        if (this.isCreating) {
            this.store.dispatch(new ExitCreatingMode());
        }
    }

    ngAfterViewChecked(): void {
        const isThereActiveItem = document.getElementById('activeItem') !== null;
        if (this.needToScroll && isThereActiveItem) {
            document.getElementById('activeItem')?.scrollIntoView();
            this.needToScroll = false;
        }
    }

    /**
     * Callback to change selected in filter items
     * @param item
     */
    onFilterChange(item: SelectableListModel): void {
        this.onSelectChange(item);

        this.selectedItems = this.filterItems.filter((x) => x.selected);
        this.filter.emit(this.selectedItems);
    }

    /**
     * Entering creating mode with editable field
     */
    onCreatingMode(): void {
        this.formGroup.reset();
        this.isCreating = true;
        this.creatingMode.emit();
    }

    /**
     * Creating new item on enter clicking
     */
    onEnterClick(): void {
        if (this.formGroup.valid) {
            this.onCreate();
        }
    }

    /**
     * Closing creating mode
     */
    onCancel(): void {
        this.isCreating = false;
        this.store.dispatch(ExitCreatingMode);
    }

    /**
     * Transmit 'create' event to parent, marking that need to scroll
     */
    onCreate(): void {
        if (this.formGroup.invalid) {
            return;
        }

        this.isCreating = false;
        this.create.emit(this.formGroup.controls['name'].value);
        this.needToScroll = true;
    }

    trackByFn(_: number, item: { id: number }): number {
        return item.id;
    }

    /**
     * Toggle selected item
     * @param item
     */
    private onSelectChange(item: SelectableListModel): void {
        if (!item) {
            return;
        }

        item.selected = !item.selected;
    }

    /**
     * Open/Close menu
     * @param {boolean}
     */
    toggleMenu(): void {
        if (this.isCollapsed) {
            this.store.dispatch(new CloseSecondaryMenu());
        } else {
            this.store.dispatch(new OpenSecondaryMenu());
        }
        this.updateView();
    }

    /**
     * Changing Primary Menu Status
     */
    private updateMenu(value: boolean | null): void {
        if (value) {
            this.menuElement.close();
        } else {
            this.menuElement.open();
        }

        this.updateView();
    }

    /**
     * Way to recalculate datatable size, that locates at inner components (datatable does not scale by itself)
     */
    private updateView(): void {
        setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
        }, 350); // elements has an animation for its appearance in full size
    }

    private isCreatingModeSubscription(): Subscription {
        return this.isCreatingMode$.subscribe((status) => {
            if (status) {
                this.onCreatingMode();
            }
        });
    }
}
