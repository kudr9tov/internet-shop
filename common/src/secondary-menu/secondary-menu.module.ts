import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PipesModule } from '@common/pipes/pipes.module';

import { SecondaryMenuComponent } from './secondary-menu.component';
import { SecondaryMenuState } from './store/secondary-menu.state';

@NgModule({
    declarations: [SecondaryMenuComponent],
    imports: [
        CommonModule,
        NgxsModule.forFeature([SecondaryMenuState]),
        MatInputModule,
        MatButtonModule,
        MatChipsModule,
        MatIconModule,
        MatListModule,
        MatMenuModule,
        MatTooltipModule,
        RouterModule,
        PipesModule,
        ReactiveFormsModule,
    ],
    exports: [SecondaryMenuComponent],
})
export class SecondaryMenuModule {}
