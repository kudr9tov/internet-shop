export interface SecondaryMenuStateModel {
    isCreatingMode: boolean;
    isSecondaryCollapsed: boolean;
}
