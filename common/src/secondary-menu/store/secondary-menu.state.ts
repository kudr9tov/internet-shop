import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';

import {
    CloseSecondaryMenu,
    EnterCreatingMode,
    ExitCreatingMode,
    OpenSecondaryMenu,
} from './secondary-menu.actions';
import { SecondaryMenuStateModel } from './secondary-menu.model';

@State<SecondaryMenuStateModel>({
    name: 'secondaryMenu',
    defaults: {
        isCreatingMode: false,
        isSecondaryCollapsed: false,
    },
})
@Injectable()
export class SecondaryMenuState {
    @Selector()
    static isCreatingMode(state: SecondaryMenuStateModel): boolean {
        return state.isCreatingMode;
    }

    @Selector()
    static isSecondaryCollapsed(state: SecondaryMenuStateModel): boolean {
        return state.isSecondaryCollapsed;
    }

    @Action(EnterCreatingMode)
    onEnterCreatingMode({ patchState }: StateContext<SecondaryMenuStateModel>): void {
        patchState({ isCreatingMode: true });
    }

    @Action(ExitCreatingMode)
    onExitCreatingMode({ patchState }: StateContext<SecondaryMenuStateModel>): void {
        patchState({ isCreatingMode: false });
    }

    @Action(OpenSecondaryMenu)
    onOpenSecondaryMenu({ patchState }: StateContext<SecondaryMenuStateModel>): void {
        patchState({
            isSecondaryCollapsed: true,
        });
    }

    @Action(CloseSecondaryMenu)
    onCloseSecondaryMenu({ patchState }: StateContext<SecondaryMenuStateModel>): void {
        patchState({
            isSecondaryCollapsed: false,
        });
    }
}
