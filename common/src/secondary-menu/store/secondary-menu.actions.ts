export class EnterCreatingMode {
    static readonly type = '[SecondaryMenu] Enter creating mode';
}

export class ExitCreatingMode {
    static readonly type = '[SecondaryMenu] Exit creating mode';
}

export class OpenSecondaryMenu {
    static readonly type = '[Layout] Open secondary menu';
}

export class CloseSecondaryMenu {
    static readonly type = '[Layout] Close secondary menu';
}
