import { SecondaryMenuComponent } from './secondary-menu.component';

describe('Core Application: SecondaryMenuComponent', () => {
    let component: SecondaryMenuComponent;

    beforeEach(() => {
        component = new SecondaryMenuComponent(null, null);
    });

    it('should create an instance', () => {
        expect(component).toBeTruthy();
    });
});
