import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { CatalogState } from './store/catalog.state';
import { ProductApiService } from './services/product-api.service';
import { CatalogResolver } from './resolvers/catalog.resolver';

@NgModule({
    imports: [NgxsModule.forFeature([CatalogState])],
    providers: [ProductApiService, CatalogResolver],
})
export class CatalogDataModule {}
