import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { BaseApiService } from '@common/abstract/base-api.service';
import { ProductModel } from '@common/models/product.model';
import { ProductsSearchModel } from '@common/models/products-search.model';
import { CreateProductModel } from '@common/models/create-product.model';
import { AppConstants } from '@common/constants/app.const';

@Injectable()
export class ProductApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(id: number): Observable<any> {
        return this.httpGet(`products/${id}`, (x) => new ProductModel(x));
    }

    public getProduct(filter: any = null): Observable<any> {
        // eslint-disable-next-line prefer-const
        let { limit, skip, ...filterRest } = filter;
        limit = limit || AppConstants.DefaultItemsPerPage;
        skip = +skip || AppConstants.DefaultPageIndex - 1;

        const url =
            Object.keys(filterRest).length !== 0
                ? `products?limit=${limit}&skip=${skip}&filter=${encodeURIComponent(
                      JSON.stringify(filterRest),
                  )}`
                : `products?limit=${limit}&skip=${skip}`;
        return this.httpGet(url, (x) => new ProductsSearchModel(x));
    }

    public getCotalog(limit: number, skip: number, filter: any = null): Observable<any> {
        const url =
            Object.keys(filter).length !== 0
                ? `products?limit=${limit}&skip=${skip}&filter=${encodeURIComponent(
                      JSON.stringify(filter),
                  )}`
                : `products?limit=${limit}&skip=${skip}`;
        return this.httpGet(url, (x) => new ProductsSearchModel(x));
    }

    public getError(limit: number, skip: number): Observable<any> {
        return this.httpGet(
            `products/errors?limit=${limit}&skip=${skip}`,
            (x) => new ProductsSearchModel(x),
        );
    }

    public create(model: CreateProductModel): Observable<any> {
        return this.httpPost('products', (x) => new CreateProductModel(x), model);
    }

    public update(model: CreateProductModel): Observable<any> {
        return this.httpPost('products', (x) => new CreateProductModel(x), model);
    }

    public delete(id: number): Observable<any> {
        return this.httpDelete('products', id, (x) => x);
    }
}
