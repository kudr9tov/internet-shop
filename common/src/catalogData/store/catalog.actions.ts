import { CreateProductModel } from '@common/models/create-product.model';
import { ProductModel } from '@common/models/product.model';

export class LoadCatalog {
    static readonly type = '[Catalog] Load';
    constructor(public payload: any) {}
}

export class LoadAdminProducts {
    static readonly type = '[Catalog] Load Admin Products';
    constructor(public limit: any = null, public skip: any = null, public filter: any = null) {}
}

export class LoadProduct {
    static readonly type = '[Catalog] Load product';

    constructor(public id: number) {}
}

export class LoadErrorProducts {
    static readonly type = '[Catalog] Load error products';

    constructor(public limit: number, public skip: number = 0) {}
}

export class LoadPrice {
    static readonly type = '[Catalog] Load price';
}

export class CreateProduct {
    static readonly type = '[Catalog] Create product';

    constructor(public model: CreateProductModel) {}
}

export class UpdateProduct {
    static readonly type = '[Catalog] Update product';

    constructor(public model: CreateProductModel) {}
}

export class DeleteProduct {
    static readonly type = '[Catalog] delete product';

    constructor(public id: number) {}
}

export class ErrorChecked {
    static readonly type = '[Catalog] Check error products';

    constructor(public isErrorChecked: boolean) {}
}

export class LoadRecomendedProducts {
    static readonly type = '[Catalog] Load recomended products';

    constructor(public category: number) {}
}

export class LoadingCatalog {
    static readonly type = '[Catalog] Is loading Catalog';

    constructor(public isLoadingCatalog: boolean) {}
}

export class AddLastViewProducts {
    static readonly type = '[Catalog] Is loading Catalog';

    constructor(public model: ProductModel) {}
}
