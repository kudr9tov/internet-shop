import { Selector } from '@ngxs/store';
import { CatalogState } from './catalog.state';
import { ProductModel } from '@common/models/product.model';
import { CategoryState } from '@common/auth/store/categories/categories.state';
import { CategoryModel } from '@common/models/category.model';
import { TreeHelper } from '@common/helpers/tree.helper';
import { FilterState } from '@common/filter/store/filter.state';
import { LinkModel } from '@common/models/link.model';

export class CatalogQuery {
    @Selector([CatalogState.catalog])
    static catalog(items: ProductModel[]): ProductModel[] {
        return items;
    }

    @Selector([CatalogState.catalog])
    static catalogIds(items: ProductModel[]): number[] {
        return items && items.length ? items.map((x) => x.id) : [];
    }

    @Selector([CatalogState.product])
    static product(product: ProductModel): ProductModel {
        return product;
    }

    @Selector([FilterState.itemsPerPage, CatalogState.total])
    static pageCount(itemsPerPage: number, total: number): number {
        return Math.ceil(total / itemsPerPage);
    }

    @Selector([FilterState.itemsPerPage, FilterState.pageIndex, CatalogState.total])
    static legendOptions(itemsPerPage: number, pageIndex: number, total: number): string {
        const selectedItems = pageIndex * itemsPerPage;
        return `Товары с ${
            selectedItems > itemsPerPage ? selectedItems - itemsPerPage : 1
        } по ${selectedItems} из ${total}`;
    }

    @Selector([CatalogState.product, CategoryState.categories])
    static breadcrumb(product: ProductModel, categories: CategoryModel[]): LinkModel[] {
        return TreeHelper.buildTree(
            TreeHelper.findeById(product.properties.subCategory.id, categories),
            categories,
            product.name,
            product.id,
        );
    }

    @Selector([CatalogState.product, CategoryState.categories])
    static selectedCategory(product: ProductModel, categories: CategoryModel[]): CategoryModel {
        return (
            product &&
            product.properties &&
            product.properties.subCategory &&
            TreeHelper.findeById(product.properties.subCategory.id, categories)
        );
    }
}
