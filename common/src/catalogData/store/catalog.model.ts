import { ProductModel } from '@common/models/product.model';

export interface CatalogStateModel {
    products: ProductModel[];
    product: ProductModel | null;
    filter: string;
    total: number;
    hasNextPage: boolean;
    isErrorChecked: boolean;
    isLoadingCatalog: boolean;
    isLoadingRecommendedProducts: boolean;
    recomendedProducts: ProductModel[];
    lastViewProducts: ProductModel[];
}
