import { Action, Selector, State, StateContext } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';
import { catchError, finalize, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Observable, of } from 'rxjs';

import { ProductModel } from '@common/models/product.model';
import { ProductsSearchModel } from '@common/models/products-search.model';
import { SetError, SetSuccess } from '@common/toast';

import { CatalogStateModel } from './catalog.model';
import {
    AddLastViewProducts,
    CreateProduct,
    DeleteProduct,
    ErrorChecked,
    LoadAdminProducts,
    LoadCatalog,
    LoadErrorProducts,
    LoadingCatalog,
    LoadProduct,
    LoadRecomendedProducts,
    UpdateProduct,
} from './catalog.actions';
import { ProductApiService } from '../services/product-api.service';

@State<CatalogStateModel>({
    name: 'catalog',
    defaults: {
        products: [],
        filter: '',
        product: null,
        total: 0,
        hasNextPage: false,
        isErrorChecked: false,
        isLoadingRecommendedProducts: false,
        recomendedProducts: [],
        isLoadingCatalog: false,
        lastViewProducts: [],
    },
})
@Injectable()
export class CatalogState {
    constructor(private apiService: ProductApiService) {}

    @Selector()
    static catalog({ products }: CatalogStateModel): ProductModel[] {
        return products || [];
    }

    @Selector()
    static recomendedProducts({ recomendedProducts }: CatalogStateModel): ProductModel[] {
        return recomendedProducts;
    }

    @Selector()
    static lastViewProducts({ lastViewProducts }: CatalogStateModel): ProductModel[] {
        return lastViewProducts;
    }

    @Selector()
    static filter({ filter }: CatalogStateModel): string {
        return filter;
    }

    @Selector()
    static product({ product }: CatalogStateModel): ProductModel | null {
        return product;
    }

    @Selector()
    static total({ total }: CatalogStateModel): number {
        return total;
    }

    @Selector()
    static hasNextPage({ hasNextPage }: CatalogStateModel): boolean {
        return hasNextPage;
    }

    @Selector()
    static isLoadingCatalog({ isLoadingCatalog }: CatalogStateModel): boolean {
        return isLoadingCatalog;
    }

    @Selector()
    static isLoadingRecommendedProducts({
        isLoadingRecommendedProducts,
    }: CatalogStateModel): boolean {
        return isLoadingRecommendedProducts;
    }

    @Selector()
    static isErrorChecked({ isErrorChecked }: CatalogStateModel): boolean {
        return isErrorChecked;
    }

    @Action(LoadCatalog)
    onLoadCatalog(
        { patchState }: StateContext<CatalogStateModel>,
        { payload }: LoadCatalog,
    ): Observable<any> {
        patchState({ isLoadingCatalog: true });
        return this.apiService.getProduct(payload).pipe(
            tap((filterProducts: ProductsSearchModel) => {
                patchState({
                    products: filterProducts.products || [],
                    filter: filterProducts.filter || '',
                    total: filterProducts.total || 0,
                    hasNextPage: filterProducts.hasNextPage,
                });
            }),
            finalize(() => patchState({ isLoadingCatalog: false })),
        );
    }

    @Action(LoadAdminProducts)
    onLoadAdminProducts(
        { patchState }: StateContext<CatalogStateModel>,
        { limit, skip, filter }: LoadAdminProducts,
    ): Observable<any> {
        const search = {} as any;
        const count = limit;
        if (filter) {
            search['query'] = filter;
        }
        return this.apiService
            .getCotalog(count, Number.isInteger(skip) ? skip : limit, search)
            .pipe(
                tap((filterProducts: ProductsSearchModel) => {
                    patchState({
                        products: filterProducts.products || [],
                        filter: filterProducts.filter || '',
                        total: filterProducts.total || 0,
                        hasNextPage: filterProducts.hasNextPage,
                    });
                }),
            );
    }

    @Action(LoadRecomendedProducts)
    onLoadRecomendedProducts(
        { patchState }: StateContext<CatalogStateModel>,
        { category }: LoadRecomendedProducts,
    ): Observable<any> {
        patchState({ isLoadingRecommendedProducts: true });
        const search = {} as any;
        if (category) {
            search['category'] = category;
        }

        return this.apiService.getCotalog(10, 0, search).pipe(
            tap((products: ProductsSearchModel) => {
                patchState({
                    recomendedProducts: products.products || [],
                });
            }),
            finalize(() => patchState({ isLoadingRecommendedProducts: false })),
        );
    }

    @Action(LoadErrorProducts)
    onLoadErrorProducts(
        { patchState }: StateContext<CatalogStateModel>,
        { limit, skip }: LoadErrorProducts,
    ): Observable<any> {
        return this.apiService.getError(limit, skip).pipe(
            tap((filterProducts: ProductsSearchModel) => {
                patchState({
                    products: filterProducts.products || [],
                    total: filterProducts.total || 0,
                });
            }),
        );
    }

    @Action(ErrorChecked)
    onErrorChecked(
        { patchState }: StateContext<CatalogStateModel>,
        { isErrorChecked }: ErrorChecked,
    ): void {
        patchState({ isErrorChecked });
    }

    @Action(LoadProduct)
    onLoadProduct(
        { patchState, dispatch }: StateContext<CatalogStateModel>,
        { id }: LoadProduct,
    ): Observable<any> {
        return this.apiService.get(id).pipe(
            tap((product: ProductModel) => {
                patchState({ product });
                dispatch(new LoadRecomendedProducts(product.properties.subCategory.id));
            }),
        );
    }

    @Action(CreateProduct)
    onCreateProduct(
        { setState, dispatch }: StateContext<CatalogStateModel>,
        { model }: CreateProduct,
    ): Observable<any> {
        return this.apiService.create(model).pipe(
            tap((product: ProductModel) => {
                setState(
                    patch({
                        products: append([product]),
                    }),
                ),
                    dispatch([
                        new SetSuccess('Продукт создался'),
                        new Navigate(['tables', `${product.id}`]),
                    ]);
            }),
            catchError((error: HttpErrorResponse) => {
                dispatch(new SetError(error.message));
                return of(error);
            }),
        );
    }

    @Action(UpdateProduct)
    onUpdateProduct(
        { setState }: StateContext<CatalogStateModel>,
        { model }: UpdateProduct,
    ): Observable<any> {
        return this.apiService.update(model).pipe(
            tap((product: ProductModel) =>
                setState(
                    patch({
                        products: updateItem<ProductModel>((x) => x.id === product.id, product),
                    }),
                ),
            ),
        );
    }

    @Action(DeleteProduct)
    onDeleteProduct(
        { setState }: StateContext<CatalogStateModel>,
        { id }: DeleteProduct,
    ): Observable<any> {
        return this.apiService.delete(id).pipe(
            tap(() =>
                setState(
                    patch({
                        products: removeItem<ProductModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }

    @Action(LoadingCatalog)
    onLoadingCatalog(
        { patchState }: StateContext<CatalogStateModel>,
        { isLoadingCatalog }: LoadingCatalog,
    ): void {
        patchState({ isLoadingCatalog });
    }

    @Action(AddLastViewProducts)
    onAddLastViewProducts(
        { setState, getState }: StateContext<CatalogStateModel>,
        { model }: AddLastViewProducts,
    ): void {
        const { lastViewProducts } = getState();
        if (lastViewProducts?.findIndex((x) => x.id === model.id) > -1) {
            return;
        }

        if (lastViewProducts?.length >= 5) {
            setState(
                patch({
                    lastViewProducts: removeItem(0),
                }),
            );
        }
        setState(
            patch({
                lastViewProducts: append([model]),
            }),
        );
    }
}
