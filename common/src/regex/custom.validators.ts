import {
    ValidatorFn,
    ValidationErrors,
    AbstractControl,
    FormControl,
    FormGroup,
    FormArray,
} from '@angular/forms';
import { EMAIL_PATTERN } from './custom-patterns.const';

export class CustomValidators {
    static emailValidator(): ValidatorFn {
        return this.pattern(EMAIL_PATTERN, { email: true });
    }

    /**
     * If matches pattern
     * @param pattern
     */
    static pattern(
        sample: RegExp | string,
        error: ValidationErrors = { pattern: true },
    ): ValidatorFn {
        const pattern = typeof sample === 'string' ? new RegExp(sample) : sample;
        return (control: AbstractControl): ValidationErrors | null => {
            return control && control.value && !pattern.test(control.value) ? error : null;
        };
    }

    static validateAllFormFields(form: FormGroup | FormArray): void {
        if (!form || !form.controls) {
            return;
        }

        Object.keys(form.controls).forEach((field) => {
            const control = form.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched();
                control.updateValueAndValidity();
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
}
