import { Action, State, StateContext } from '@ngxs/store';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';

import { SetError, SetSuccess } from './toast.actions';
import { ToastStateModel } from './toast.model';

@State<ToastStateModel>({
    name: 'toastState',
    defaults: {
        payload: '',
    },
})
@Injectable()
export class ToastState {
    constructor(private toastService: ToastrService) {}

    @Action(SetSuccess)
    onSetSuccess({}: StateContext<ToastStateModel>, { payload }: ToastStateModel): void {
        this.toastService.success(payload);
    }

    @Action(SetError)
    onSetError({}: StateContext<ToastStateModel>, { payload }: ToastStateModel): void {
        this.toastService.error(payload);
    }
}
