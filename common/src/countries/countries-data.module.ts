import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { CountriesState } from './store/countries.state';
import { CountriesApiService } from './services/countries-api.service';
import { CountriesResolver } from './resolvers/countries.resolver';

@NgModule({
    imports: [NgxsModule.forFeature([CountriesState])],
    providers: [CountriesApiService, CountriesResolver],
})
export class CountriesDataModule {}
