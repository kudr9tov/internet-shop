import { DirectoryListModel } from '@common/models/directory-list.model';

export class LoadCountries {
    static readonly type = '[Countries] Load Countries';
}

export class CreateCountries {
    static readonly type = '[Countries] Create Country';

    constructor(public model: DirectoryListModel) {}
}

export class UpdateCountries {
    static readonly type = '[Countries] Create Country';

    constructor(public model: DirectoryListModel) {}
}

export class DeleteCountry {
    static readonly type = '[Countries] delete Country';

    constructor(public id: number) {}
}
