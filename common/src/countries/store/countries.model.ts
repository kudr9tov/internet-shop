import { ListModel } from '@common/models/list.model';

export interface CountriesStateModel {
    countries: ListModel[];
}
