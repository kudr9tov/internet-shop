import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';

import { ListModel } from '@common/models/list.model';
import { DirectoryModel } from '@common/models/directory.model';
import { DirectoryType } from '@common/enums/directory-type.enum';

import { CountriesStateModel } from './countries.model';
import {
    CreateCountries,
    DeleteCountry,
    LoadCountries,
    UpdateCountries,
} from './countries.actions';
import { CountriesApiService } from '../services/countries-api.service';

@State<CountriesStateModel>({
    name: 'countries',
    defaults: {
        countries: [],
    },
})
@Injectable()
export class CountriesState {
    constructor(private apiService: CountriesApiService) {}

    @Selector()
    static countries({ countries }: CountriesStateModel): ListModel[] {
        return countries || [];
    }

    @Action(LoadCountries)
    onLoadCountries({ patchState }: StateContext<CountriesStateModel>): Observable<any> {
        return this.apiService.get().pipe(
            tap((countries: ListModel[]) => {
                patchState({ countries });
            }),
        );
    }

    @Action(CreateCountries)
    onCreateCountries(
        { setState }: StateContext<CountriesStateModel>,
        { model }: CreateCountries,
    ): Observable<any> {
        if (!model.entries.length) {
            return of();
        }
        return this.apiService.create(model).pipe(
            tap((brand: ListModel) =>
                setState(
                    patch({
                        countries: append([brand]),
                    }),
                ),
            ),
        );
    }

    @Action(UpdateCountries)
    onUpdateCountries(
        { setState }: StateContext<CountriesStateModel>,
        { model }: UpdateCountries,
    ): Observable<any> {
        if (!model.entries.length) {
            return of();
        }
        return this.apiService.update(model).pipe(
            tap((countries: ListModel[]) => {
                countries.forEach((item) => {
                    setState(
                        patch({
                            countries: updateItem<ListModel>(
                                (s) => s.id === item.id,
                                patch({ name: item.name }),
                            ),
                        }),
                    );
                });
            }),
        );
    }

    @Action(DeleteCountry)
    onDeleteCountry(
        { setState }: StateContext<CountriesStateModel>,
        { id }: DeleteCountry,
    ): Observable<any> {
        const model = new DirectoryModel({ id, type: DirectoryType.Countries });
        return this.apiService.delete(model).pipe(
            tap(() =>
                setState(
                    patch({
                        countries: removeItem<ListModel>((x) => x.id === id),
                    }),
                ),
            ),
        );
    }
}
