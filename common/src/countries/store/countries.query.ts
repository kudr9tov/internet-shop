import { Selector } from '@ngxs/store';

import { ListModel } from '@common/models/list.model';
import { CountriesState } from './countries.state';

export class CountriesQuery {
    @Selector([CountriesState.countries])
    static countries(countries: ListModel[]): ListModel[] {
        return countries;
    }
}
