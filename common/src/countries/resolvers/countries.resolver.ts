import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

import { LoadCountries } from '../store/countries.actions';

@Injectable()
export class CountriesResolver implements Resolve<any> {
    constructor(public store: Store) {}

    resolve(): Observable<any> {
        return this.store.dispatch(new LoadCountries());
    }
}
