import { CategoryModel } from '@common/models/category.model';
import { TodoItemFlatNode } from '@common/models/item-flat-node.model';
import { LinkModel } from '@common/models/link.model';

export class TreeHelper {
    static findeAllParent(
        item: CategoryModel,
        categories: CategoryModel[],
    ): { categoryId: CategoryModel | undefined; subCategory: TodoItemFlatNode[] } {
        const selectedItem = item;
        const parents: CategoryModel[] = [];
        function findParents(acc: any, el: CategoryModel): any {
            if (el.subCategory && el.subCategory.some((child) => child.id === item.id)) {
                parents.push(el);
                item = el;
                return categories.reduce(findParents, null);
            }
            if (el.subCategory) {
                return el.subCategory.reduce(findParents, acc);
            }
            return acc;
        }
        categories.reduce(findParents, null);
        const category = parents.pop();
        const selectedParents =
            parents && parents.length
                ? parents.map(
                      (x) =>
                          new TodoItemFlatNode({
                              id: x.id,
                              item: x.name,
                              level: x.level,
                              parentCategoryId: x.parentCategoryId,
                          }),
                  )
                : [];
        if (!selectedItem.parentCategoryId) {
            return { categoryId: selectedItem, subCategory: [] };
        }

        return {
            categoryId: category,
            subCategory: [
                ...selectedParents,
                new TodoItemFlatNode({
                    id: selectedItem.id,
                    item: selectedItem.name,
                    level: selectedItem.level,
                    parentCategoryId: selectedItem.parentCategoryId,
                }),
            ],
        };
    }

    static findeSubCategoryParent(item: CategoryModel, categories: CategoryModel[]): LinkModel[] {
        if (!item.parentCategoryId) {
            return [
                new LinkModel({
                    label: item.name,
                    level: item.level,
                    url: `../../subcategories/${item.id}`,
                    image: item.image,
                }),
            ];
        }
        const selectedItem = item;
        const parents: CategoryModel[] = [];
        function findParents(acc: any, el: CategoryModel): any {
            if (el.subCategory && el.subCategory.some((child) => child.id === item.id)) {
                parents.push(el);
                item = el;
                return categories.reduce(findParents, null);
            }
            if (el.subCategory) {
                return el.subCategory.reduce(findParents, acc);
            }
            return acc;
        }
        categories.reduce(findParents, null);
        const selectedParents = [
            new LinkModel({
                label: selectedItem.name,
                level: selectedItem.level,
                url: `../../subcategories/${selectedItem.id}`,
                image: selectedItem.image,
            }),
        ];
        if (parents && parents.length) {
            selectedParents.push(
                ...parents.map(
                    (x) =>
                        new LinkModel({
                            label: x.name,
                            level: x.level,
                            url: `../../subcategories/${x.id}`,
                            image: x.image,
                        }),
                ),
            );
        }
        return selectedParents.sort((a: any, b: any) => a.level - b.level);
    }

    static buildTree(
        item: CategoryModel,
        categories: CategoryModel[],
        productName: string,
        productId: number,
    ): LinkModel[] {
        const selectedItem = item;
        const parents: CategoryModel[] = [];
        function findParents(acc: any, el: CategoryModel): any {
            if (el.subCategory && el.subCategory.some((child) => child.id === item.id)) {
                parents.push(el);
                item = el;
                return categories.reduce(findParents, null);
            }
            if (el.subCategory) {
                return el.subCategory.reduce(findParents, acc);
            }
            return acc;
        }
        categories.reduce(findParents, null);
        const category = parents.pop();
        const selectedParents =
            parents && parents.length
                ? parents.map(
                      (x) =>
                          new LinkModel({
                              label: x.name,
                              level: x.level,
                              url: '/catalog',
                              queryParams: {
                                  category: category?.id,
                                  subCategory: x.id,
                              },
                          }),
                  )
                : [];

        const result = [
            new LinkModel({ label: 'Главная', level: 0, url: '/' }),
            new LinkModel({
                label: selectedItem.name,
                level: selectedItem.level,
                url: '/catalog',
                queryParams: {
                    category: category?.id,
                    subCategory: selectedItem.id,
                },
            }),
        ];
        if (!selectedItem.parentCategoryId) {
            return result;
        }
        result.push(
            ...selectedParents,
            new LinkModel({
                label: category?.name,
                level: category?.level,
                url: '/catalog',
                queryParams: { category: category?.id },
            }),
            new LinkModel({
                label: productName,
                level: 10,
                url: `/catalog/product/${productId}`,
            }),
        );
        return result.sort((a: any, b: any) => a.level - b.level);
    }

    static findeFirstParent(item: CategoryModel, categories: CategoryModel[]): number {
        function findParent(acc: any, el: CategoryModel): any {
            if (el.subCategory && el.subCategory.some((child) => child.id === item.id)) {
                item = el;
                return categories.reduce(findParent, null);
            }
            if (el.subCategory) {
                return el.subCategory.reduce(findParent, acc);
            }
            return acc;
        }
        categories.reduce(findParent, null);
        return item.id;
    }

    static findeById(id: number | null, categories: CategoryModel[]): CategoryModel {
        function findById(acc: any, el: { id: number; subCategory: any[] }): any {
            if (el.id === id) {
                return el;
            }
            if (el.subCategory) {
                return el.subCategory.reduce(findById, acc);
            }
            return acc;
        }
        return categories.reduce(findById, null);
    }

    static findeByIds(ids: number[], categories: CategoryModel[]): any {
        const array: TodoItemFlatNode[] = [];
        function findByIds(
            acc: any,
            el: { id: number; name: any; level: number; parentCategoryId: any; subCategory: any[] },
        ): any {
            if (ids.includes(el.id)) {
                array.push(
                    new TodoItemFlatNode({
                        id: el.id,
                        item: el.name,
                        level: el.level - 1,
                        parentCategoryId: el.parentCategoryId,
                    }),
                );
            }
            if (ids.length === array.length) {
                return array;
            }
            if (el.subCategory) {
                return el.subCategory.reduce(findByIds, acc);
            }
            return acc;
        }
        return categories.reduce(findByIds, null);
    }
}
