interface INamed {
    name: string;
}

interface IFullNamed {
    fullName: string;
}

interface ITitled {
    title: string;
}

interface ICreated {
    created: string;
}

export class SortHelper {
    static compareByNameFn<T extends INamed>(a: T, b: T): number {
        const nameA = a && a.name ? a.name.trim().toLowerCase() : '';
        const nameB = b && b.name ? b.name.trim().toLowerCase() : '';
        return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
    }

    static compareByCreatedFn<T extends ICreated>(a: T, b: T): number {
        const nameA = a && a.created ? a.created.trim().toLowerCase() : '';
        const nameB = b && b.created ? b.created.trim().toLowerCase() : '';
        return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
    }

    static compareByFullNameFn<T extends IFullNamed>(a: T, b: T): number {
        const nameA = a && a.fullName ? a.fullName.trim().toLowerCase() : '';
        const nameB = b && b.fullName ? b.fullName.trim().toLowerCase() : '';
        return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
    }

    static compareByTitleFn<T extends ITitled>(a: T, b: T): number {
        const titleA = a && a.title ? a.title.trim().toLowerCase() : '';
        const titleB = b && b.title ? b.title.trim().toLowerCase() : '';
        return titleA < titleB ? -1 : titleA > titleB ? 1 : 0;
    }

    static compareByNamePriorFn(a: any, b: any): number {
        const nameA = a && a.name ? a.name.toLowerCase() : '';
        const nameB = b && b.name ? b.name.toLowerCase() : '';
        if (nameA === 'торговое оборудование') {
            return -1;
        } else if (nameB === 'торговое оборудование') {
            return 1;
        }
        if (nameA === 'холодильное оборудование') {
            return -1;
        } else if (nameB === 'холодильное оборудование') {
            return 1;
        }
        if (nameA === 'оборудование для предприятий питания') {
            return -1;
        } else if (nameB === 'оборудование для предприятий питания') {
            return 1;
        }
        return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
    }
}
