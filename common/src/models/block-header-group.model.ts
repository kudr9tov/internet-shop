import { AbstractIdDto } from './abstract-id-dto';

export class BlockHeaderGroupModel extends AbstractIdDto<number> {
    name!: string;
    current!: boolean;

    public constructor(fields?: Partial<BlockHeaderGroupModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
