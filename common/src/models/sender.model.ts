import { AbstractIdDto } from './abstract-id-dto';

export class SenderModel extends AbstractIdDto<number> {
    address!: string;
    director!: string;
    email!: string;
    isSend!: boolean;
    isSendTemplate!: boolean;
    lastSendMessage!: string;
    organization!: string;
    phone!: string;
    requisites!: string;
    site!: string;
    updated!: Date;

    public constructor(fields?: Partial<SenderModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
