export class FilterModel {
    filterColumn!: string;
    queryString!: string;
    type!: 'ASC' | 'DESC' | 'QUERY';

    public constructor(fields?: Partial<FilterModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
