import { AbstractIdDto } from './abstract-id-dto';

export class CatalogModel extends AbstractIdDto<number> {
    public name!: string;
    public brand!: string;
    public brandId!: number;
    public catalogFileUrl!: string;

    public constructor(fields?: Partial<CatalogModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
