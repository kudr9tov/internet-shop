import { AbstractIdDto } from './abstract-id-dto';
import { ParserResultModel } from './parser-result.model';

export class ParserInfoModel extends AbstractIdDto<number> {
    public status!: string;
    public created!: number;
    public updated!: number;
    public duration!: string;
    public result!: ParserResultModel;

    public constructor(fields?: Partial<ParserInfoModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
