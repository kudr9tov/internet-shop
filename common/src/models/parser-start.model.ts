export class ParserStartModel {
    public site!: string;
    public updateProducts!: boolean;
    public updateProductsPrice!: boolean;
    public updateCategories!: boolean;

    public constructor(fields?: Partial<ParserStartModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
