import { ListModel } from './list.model';
import { DirectoryType } from '@common/enums/directory-type.enum';

export class SelectableListModel extends ListModel {
    selected!: boolean;
    isParent!: boolean;
    type!: DirectoryType;

    public constructor(fields?: Partial<SelectableListModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
