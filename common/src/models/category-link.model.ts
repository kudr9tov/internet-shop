import { CategoryModel } from './category.model';

export class CategoryLinkModel extends CategoryModel {
    link!: string;
    subCategoryLink!: CategoryLinkModel[];
    queryParams: any;

    public constructor(fields?: Partial<any>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
