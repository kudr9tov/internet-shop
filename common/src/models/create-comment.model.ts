import { CommentType } from '@common/enums/comment-type.enum';

export class CreateCommentModel {
    id!: number;
    commentType!: CommentType;
    entityLinkedId!: number;
    commentParentId!: number;
    author!: string;
    comment!: string;

    public constructor(fields?: Partial<CreateCommentModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
