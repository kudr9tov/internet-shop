import { AbstractIdDto } from './abstract-id-dto';
import { ListModel } from './list.model';
import { RequestPriceModel } from './price-request.model';

export class RequestInfoModel extends AbstractIdDto<number> {
    public comment!: string;
    public created!: string;
    public product!: ListModel;
    public userInfo!: RequestPriceModel;

    public constructor(fields?: Partial<RequestInfoModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
