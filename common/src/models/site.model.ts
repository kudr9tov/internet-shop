export class SiteModel {
    id!: number;
    isAvailableUpdateCategory!: boolean;
    isAvailableUpdateProduct!: boolean;
    isAvailableUpdateProductPrice!: boolean;
    status!: string;
    siteUrl!: string;
    siteName!: string;

    public constructor(fields?: Partial<SiteModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
