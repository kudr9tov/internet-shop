export class Slide {
    public id!: number;
    public image!: string;
    public description!: string;
    public link!: string;
    public title!: string;
    public guid!: string;
    public step!: number;

    public constructor(fields?: Partial<Slide>) {
        if (fields) {
            Object.assign(this, fields);
            this.guid = this.id ? `${this.id}` : '';
        }
    }
}
