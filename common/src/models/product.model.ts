import { AbstractIdDto } from './abstract-id-dto';
import { PropertyModel } from './property.model';

export class ProductModel extends AbstractIdDto<number> {
    public name!: string;
    public description!: string;
    public image!: string;
    public vendorCode!: string;
    public price!: number;
    public discount!: number;
    public properties!: PropertyModel;
    public techProperties: any;
    public updated!: number;
    public inStock!: boolean;

    public count!: number;

    public constructor(fields?: Partial<ProductModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
        this.count = this.count || 1;
    }
}
