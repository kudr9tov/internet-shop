/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
    id!: number;
    item!: string;
    level!: number;
    parentCategoryId!: number;
    expandable!: boolean;

    public constructor(fields?: Partial<TodoItemFlatNode>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
