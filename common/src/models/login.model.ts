export class LoginModel {
    email!: string;
    password!: string;

    public constructor(fields?: Partial<LoginModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
