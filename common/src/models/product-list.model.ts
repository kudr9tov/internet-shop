export class ProductListModel {
    public productId!: number;
    public quantity!: number;
    public name!: string;

    public constructor(fields?: Partial<ProductListModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
