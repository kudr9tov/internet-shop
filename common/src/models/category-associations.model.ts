import { AssociationModel } from './association.model';

export class CategoryAssotiationsModel {
    public siteId!: number;
    public siteBrand!: string;
    public siteUrl!: string;
    public associations!: AssociationModel[];

    public constructor(fields?: Partial<CategoryAssotiationsModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
