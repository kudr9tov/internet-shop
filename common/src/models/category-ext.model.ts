import { CategoryModel } from './category.model';

export class CategoryExtModel extends CategoryModel {
    selected!: boolean;

    public constructor(fields?: Partial<CategoryExtModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
