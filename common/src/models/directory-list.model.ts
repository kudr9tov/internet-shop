import { DirectoryType } from '@common/enums/directory-type.enum';

import { AbstractIdDto } from './abstract-id-dto';
import { ListModel } from './list.model';

export class DirectoryListModel extends AbstractIdDto<number> {
    parentId!: number;
    type!: DirectoryType;
    level!: number;
    entries!: ListModel[];

    public constructor(fields?: Partial<DirectoryListModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
