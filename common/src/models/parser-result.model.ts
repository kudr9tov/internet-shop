export class ParserResultModel {
    public lastError!: string;
    public categories!: number;
    public newProducts!: number;
    public foundedProducts!: number;
    public priceProductsUpdated!: number;

    public constructor(fields?: Partial<ParserResultModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
