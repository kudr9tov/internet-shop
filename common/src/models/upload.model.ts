export class UploadModel {
    fileName!: string;
    link!: string;

    public constructor(fields?: Partial<UploadModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
