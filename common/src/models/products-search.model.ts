import { ProductModel } from './product.model';
import { PriceModel } from './price.model';

export class ProductsSearchModel {
    products!: ProductModel[];
    limit!: number;
    skip!: number;
    filter!: string;
    total!: number;
    price!: PriceModel;
    hasNextPage!: boolean;

    public constructor(fields?: Partial<ProductsSearchModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
