import { FilterModel } from './filter.model';

export class EmailSearchModel {
    limit!: number;
    page!: number;
    rightPage!: number;

    filters!: FilterModel[];

    public constructor(fields?: Partial<EmailSearchModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
