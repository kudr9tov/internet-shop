import { AbstractIdDto } from './abstract-id-dto';

export class CategoryModel extends AbstractIdDto<number> {
    public name!: string;
    public image!: string;
    public level!: number;
    public parentCategoryId!: number;
    public subCategory!: CategoryModel[];

    public constructor(fields?: Partial<CategoryModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
