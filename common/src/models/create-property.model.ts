export class CreatePropertyModel {
    public brand!: number;
    public country!: number;
    public category!: number;
    public subCategory!: number;
    public vendorCode!: string;

    public constructor(fields?: Partial<CreatePropertyModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
