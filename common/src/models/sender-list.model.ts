import { SenderModel } from './sender.model';

export class SenderListModel {
    count!: number;
    limit!: number;
    page!: number;

    mails!: SenderModel[];

    public constructor(fields?: Partial<SenderListModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
