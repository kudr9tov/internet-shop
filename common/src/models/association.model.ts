export class AssociationModel {
    public currentCategoryId!: number;
    public currentCategoryName!: string;
    public isSiteCategoryExcluded!: boolean;
    public siteCategoryId!: number;
    public siteCategoryName!: string;
    public siteCategoryUrl!: string;

    public constructor(fields?: Partial<AssociationModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
