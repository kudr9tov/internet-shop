import { DirectoryType } from '@common/enums/directory-type.enum';

import { AbstractIdDto } from './abstract-id-dto';

export class DirectoryModel extends AbstractIdDto<number> {
    parentId!: number;
    value!: string;
    image!: string;
    type!: DirectoryType;

    public constructor(fields?: Partial<DirectoryModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
