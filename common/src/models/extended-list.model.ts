import { ListModel } from './list.model';

export class ExtendedListModel extends ListModel {
    public isFaded!: boolean;

    public constructor(fields?: Partial<ExtendedListModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
