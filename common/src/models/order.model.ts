import { AbstractIdDto } from './abstract-id-dto';
import { CustomerInfoModel } from './customer-info.model';
import { ProductListModel } from './product-list.model';

export class OrderModel extends AbstractIdDto<number> {
    public customerInfo!: CustomerInfoModel;
    public products!: ProductListModel[];
    public comment!: string;
    public created!: string;
    public updated!: string;
    public totalCost!: number;
    public status!: string;

    public constructor(fields?: Partial<OrderModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
