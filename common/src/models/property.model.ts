import { AbstractIdDto } from './abstract-id-dto';
import { ListValueModel } from './list-value.model';
import { BrandListModel } from './brand-list.model';

export class PropertyModel extends AbstractIdDto<number> {
    public brand!: BrandListModel;
    public country!: ListValueModel;
    public category!: ListValueModel;
    public subCategory!: ListValueModel;
    public vendorCode!: string;

    public constructor(fields?: Partial<PropertyModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
