export class PriceModel {
    max!: number;
    min!: number;

    public constructor(fields?: Partial<PriceModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
