import { AbstractIdDto } from './abstract-id-dto';

export class TextEditorModel extends AbstractIdDto<number> {
    public name!: string;
    public content!: string;
    public seoUrl!: string;
    public created!: number;
    public imageUrl!: string;
    public system!: boolean;

    public constructor(fields?: Partial<TextEditorModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
