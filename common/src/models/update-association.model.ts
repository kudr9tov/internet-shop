export class UpdateAssociationModel {
    public siteCategoryId!: number;
    public currentCategoryId!: number;
    public action!: string;
    public exclude!: boolean;

    public constructor(fields?: Partial<UpdateAssociationModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
