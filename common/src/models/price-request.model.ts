import { AbstractIdDto } from './abstract-id-dto';

export class RequestPriceModel extends AbstractIdDto<number> {
    public phone!: string;
    public email!: string;
    public city!: string;
    public name!: string;
    public comment!: string;
    public productId!: number;

    public constructor(fields?: Partial<RequestPriceModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
