import { CategoryModel } from './category.model';

export class FilterCategoryModel {
    public categories!: CategoryModel[];
    public selectedSubCategory!: number[];

    public constructor(fields?: Partial<FilterCategoryModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
