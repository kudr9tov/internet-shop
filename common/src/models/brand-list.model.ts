import { CatalogModel } from './catalog.model';
import { ListValueModel } from './list-value.model';

export class BrandListModel extends ListValueModel {
    public image!: string;
    public catalogs!: CatalogModel[];

    public constructor(fields?: Partial<BrandListModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
