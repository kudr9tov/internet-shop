export class CommentModel {
    id!: number;
    author!: string;
    comment!: string;
    created!: number;
    children!: CommentModel[];

    public constructor(fields?: Partial<CommentModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
