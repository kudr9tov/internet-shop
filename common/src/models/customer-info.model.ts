export class CustomerInfoModel {
    public organization!: string;
    public requisites!: string;
    public city!: string;
    public address!: string;
    public name!: string;
    public phone!: string;
    public email!: string;

    public constructor(fields?: Partial<CustomerInfoModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
