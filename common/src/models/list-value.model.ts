import { AbstractIdDto } from './abstract-id-dto';

export class ListValueModel extends AbstractIdDto<number> {
    public value!: string;

    public constructor(fields?: Partial<ListValueModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
