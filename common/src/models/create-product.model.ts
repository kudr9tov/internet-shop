import { AbstractIdDto } from './abstract-id-dto';
import { CreatePropertyModel } from './create-property.model';

export class CreateProductModel extends AbstractIdDto<number> {
    public name!: string;
    public description!: string;
    public image!: string;
    public price!: number;
    public discount!: number;
    public properties!: CreatePropertyModel;
    public techProperties: any;
    public inStock!: boolean;

    public constructor(fields?: Partial<CreateProductModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
