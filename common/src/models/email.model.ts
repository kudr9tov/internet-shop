export class EmailModel {
    public sendAll!: boolean;
    public sendTemplate!: boolean;
    public message!: string;
    public subject!: string;
    public emailIds!: number[];
    public emails!: string[];

    public constructor(fields?: Partial<EmailModel>) {
        Object.assign(this, fields);
    }
}
