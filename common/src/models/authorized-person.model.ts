import { AbstractIdDto } from './abstract-id-dto';

export class AuthorizedPersonModel extends AbstractIdDto<number> {
    public email!: string;
    public status!: string;
    public photo!: string;
    public role!: string;
    public name!: string;

    public constructor(fields?: Partial<AuthorizedPersonModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
