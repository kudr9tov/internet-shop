import { AbstractIdDto } from './abstract-id-dto';
import { CatalogModel } from './catalog.model';

export class BrandModel extends AbstractIdDto<number> {
    public brand!: string;
    public brandId!: number;
    public catalogs!: CatalogModel[];

    public constructor(fields?: Partial<BrandModel>) {
        super(fields);
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
