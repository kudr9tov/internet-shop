export class LinkModel {
    public label!: string;
    public url!: string;
    public image?: string;
    public level?: number;
    public queryParams?: any;
    public external?: boolean;
    public target?: '_self' | '_blank';

    public constructor(fields?: Partial<LinkModel>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
