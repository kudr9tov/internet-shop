import { ListModel } from '@common/models/list.model';

export interface CertificateListStateModel {
    certificates: ListModel[];
}
