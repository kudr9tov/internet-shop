import { Injectable } from '@angular/core';
import { ListModel } from '@common/models/list.model';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { CertificateStateModel } from 'admin/src/app/certificate/store/certificate.model';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CertificateListApiService } from '../services/certificate-list-api.service';
import { LoadCertificateList } from './certificate-list.actions';

import { CertificateListStateModel } from './certificate-list.model';

@State<CertificateListStateModel>({
    name: 'certificateListState',
    defaults: {
        certificates: [],
    },
})
@Injectable()
export class CertificateListState {
    @Selector()
    static certificates({ certificates }: CertificateStateModel): ListModel[] {
        return certificates || [];
    }

    constructor(private apiService: CertificateListApiService) {}

    @Action(LoadCertificateList)
    onLoadCertificateList({ patchState }: StateContext<CertificateStateModel>): Observable<any> {
        return this.apiService.get().pipe(
            tap((certificates: ListModel[]) => {
                patchState({ certificates });
            }),
        );
    }
}
