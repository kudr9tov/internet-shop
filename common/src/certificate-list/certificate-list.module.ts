import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { CertificateListApiService } from './services/certificate-list-api.service';
import { CertificateListState } from './store/certificate-list.state';

@NgModule({
    declarations: [],
    imports: [CommonModule, NgxsModule.forFeature([CertificateListState])],
    providers: [CertificateListApiService],
})
export class CertificateListModule {}
