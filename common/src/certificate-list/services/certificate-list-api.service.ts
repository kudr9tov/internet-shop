import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseApiService } from '@common/abstract/base-api.service';
import { ListModel } from '@common/models/list.model';
import { Store } from '@ngxs/store';

import { Observable } from 'rxjs';

@Injectable()
export class CertificateListApiService extends BaseApiService {
    constructor(http: HttpClient, store: Store) {
        super(http, store);
    }

    public get(): Observable<ListModel[]> {
        return this.httpGet('directory/cert', (x) => new ListModel(x));
    }
}
