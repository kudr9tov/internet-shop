import { TestBed } from '@angular/core/testing';

import { CertificateListApiService } from './certificate-list-api.service';

describe('CertificateListApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: CertificateListApiService = TestBed.get(CertificateListApiService);
        expect(service).toBeTruthy();
    });
});
